package com.alphabit.sportnotes.notification;

import com.alphabit.sportnotes.utils.SchedulersInjector;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by fly12 on 13.11.2017.
 */

public class BasePresenter {

    private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private final SchedulersInjector mSchedulersInjector;

    public BasePresenter() {
        mSchedulersInjector = new SchedulersInjector();
    }

    protected <T> Single<T> scheduling(final Single<T> single) {
        return single.observeOn(mSchedulersInjector.getMainThread());
    }

    protected <T> Flowable<T> scheduling(final Flowable<T> single) {
        return single.observeOn(mSchedulersInjector.getMainThread());
    }


    protected void addDisposable(Disposable disposable) {
        mCompositeDisposable.add(disposable);
    }
}
