package com.alphabit.sportnotes.notification;

import android.content.Intent;
import android.net.Uri;
import android.util.Pair;

import com.alphabit.sportnotes.App;

import com.alphabit.sportnotes.timer.AlarmReceiver;

import java.io.Serializable;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Flowable;
import sportnotes.alphabit.com.business.usecase.SettingsUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IExerciseDetailUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISettingsUseCase;

/**
 * Created by fly12 on 13.11.2017.
 */

public class NotificationPresenter extends BasePresenter implements INotificationPresenter {

    private final NotificationHandler notificationHandler;

    @Inject
    IExerciseDetailUseCase useCase;
    @Inject
    ISettingsUseCase settingsUseCase;
    private UUID uuid;
    private boolean fromPush;



    public NotificationPresenter(NotificationHandler notificationHandler) {
        this.notificationHandler = notificationHandler;
        App.getAppComponent().inject(this);
    }

    @Override
    public void handle(Intent intent) {
        if (intent == null)
            return;
        if (Notifications.EXERCISE_TIMER_END_ACTION.equals(intent.getAction())) {
            uuid = (UUID) intent.getSerializableExtra(AlarmReceiver.EXERCISE_UUID);
            fromPush = intent.getBooleanExtra(AlarmReceiver.FROM_PUSH, false);

            addDisposable(scheduling(Flowable.combineLatest(useCase.getExercise(uuid).take(1), settingsUseCase.getRingtone(), Pair::new))
                    .take(1)
                    .subscribe(pair -> {
                        Uri uri = Uri.parse(pair.second);
                        notificationHandler.showExerciseTimerEnd(pair.first.getName(), fromPush, uri);
                    }));

            intent.putExtra(AlarmReceiver.EXERCISE_UUID, (Serializable) null);
        }
    }


    @Override
    public void onClickCancel() {

    }

    @Override
    public void onClickOpen() {
        if (uuid != null)
            notificationHandler.openExercise(uuid);
    }
}
