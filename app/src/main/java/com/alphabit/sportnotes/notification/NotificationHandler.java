package com.alphabit.sportnotes.notification;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.databinding.DialogExerciseTimerEndBinding;
import com.alphabit.sportnotes.databinding.DialogNewComplexBinding;
import com.alphabit.sportnotes.ui.Router;
import com.alphabit.sportnotes.view.customdialog.CustomDialogBuilder;

import java.util.UUID;
import sportnotes.alphabit.com.business.model.Exercise;

/**
 * Created by fly12 on 13.11.2017.
 */

public class NotificationHandler {

    private final INotificationPresenter notificationPresenter = new NotificationPresenter(this);

    private final Activity activity;
    private Ringtone ringtone;

    public NotificationHandler(Activity activity) {
        this.activity = activity;
    }

    public void handle(Intent intent) {
        notificationPresenter.handle(intent);
    }

    private Context getContext() {
        return activity;
    }

    protected void showExerciseTimerEnd(String exerciseName, boolean fromPush, Uri uri) {

        boolean hasPermission;
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            hasPermission = false;
        } else
            hasPermission = true;

        if (hasPermission && (!fromPush && (ringtone == null || !ringtone.isPlaying()))) {
            ringtone = RingtoneManager.getRingtone(activity.getApplicationContext(), uri);
            if (ringtone != null)
                ringtone.play();
        }

        final DialogExerciseTimerEndBinding dialogBinding = DataBindingUtil.bind(LayoutInflater.from(getContext())
                .inflate(R.layout.dialog_exercise_timer_end, null, false));
        dialogBinding.title.setText(getContext().getString(R.string.timer_for_exercise_x_end, exerciseName));
        CustomDialogBuilder.newBuilder(getContext())
                .setTitle(getContext().getString(R.string.timer))
                .setCancelable(false)
                .setView(dialogBinding.getRoot())
                .setOnPositiveClickListener(getContext().getString(R.string.open), customDialog -> {
                    notificationPresenter.onClickOpen();
                    if (ringtone != null && ringtone.isPlaying())
                        ringtone.stop();
                })
                .setOnNegativeClickListener(getContext().getString(R.string.cancel), customDialog -> {
                    notificationPresenter.onClickCancel();
                    if (ringtone != null && ringtone.isPlaying())
                        ringtone.stop();
                })
                .build().show();
    }

    public void openExercise(UUID uuid) {
        Router.openExerciseDetail(activity, uuid);
    }

}
