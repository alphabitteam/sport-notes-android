package com.alphabit.sportnotes.notification.push;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.v4.app.NotificationCompat;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.notification.Notifications;
import com.alphabit.sportnotes.timer.AlarmReceiver;
import com.alphabit.sportnotes.ui.screens.SplashActivity;
import java.util.UUID;

import sportnotes.alphabit.com.business.usecase.contract.ISettingsUseCase;

/**
 * Created by fly12 on 13.11.2017.
 */

public class CoreNotificationCreator {

    public static final String TIMER_CHANNEL_ID = "timer";
    int importance = NotificationManager.IMPORTANCE_LOW;

    private NotificationManager notificationManager;

    private final Context context;

    public CoreNotificationCreator(Context context) {
        this.context = context;
        notificationManager = ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE));
    }

    private static final @DrawableRes int SMALL_ICON_RES_ID = R.mipmap.ic_launcher;

    public void showTimerPushNotification(String exerciseName, UUID uuid, Uri ringtone) {
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), SMALL_ICON_RES_ID);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(SMALL_ICON_RES_ID)
                .setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_SOUND)
                .setChannelId(TIMER_CHANNEL_ID)
                .setOngoing(true)
                .setSound(ringtone)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentTitle(context.getString(R.string.timer_is_over))
                .setContentText(context.getString(R.string.timer_for_exercise_x_end, exerciseName))
                .setLargeIcon(largeIcon);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(TIMER_CHANNEL_ID, TIMER_CHANNEL_ID, importance);
            mChannel.setDescription("");
            mChannel.enableLights(true);
            mChannel.setShowBadge(true);
            mChannel.setSound(ringtone,
                    new AudioAttributes.Builder()
                            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                            .setLegacyStreamType(AudioManager.STREAM_NOTIFICATION)
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION_EVENT)
                            .build());
            // Sets the notification light color for notifications posted to this
            // channel, if the device supports this feature.
            mChannel.setLightColor(Color.TRANSPARENT);
            //mChannel.enableVibration(true);
            //mChannel.setVibrationPattern(new long[]{300, 300});
            notificationManager.createNotificationChannel(mChannel);
            builder.setChannelId(mChannel.getId());
        }

        Intent intent = new Intent(context, SplashActivity.class)
                .setPackage(context.getApplicationContext().getPackageName())
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setAction(Notifications.EXERCISE_TIMER_END_ACTION);
        intent.putExtra(AlarmReceiver.EXERCISE_UUID, uuid);
        intent.putExtra(AlarmReceiver.FROM_PUSH, true);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, AlarmReceiver.NOTIFICATION_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        Notification notification = builder.build();
        notification.sound = ringtone;

        notificationManager.cancel(AlarmReceiver.NOTIFICATION_CODE);
        notificationManager.notify(AlarmReceiver.NOTIFICATION_CODE, notification);
    }
}
