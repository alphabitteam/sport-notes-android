package com.alphabit.sportnotes.notification;

import android.content.Intent;

/**
 * Created by fly12 on 13.11.2017.
 */

public interface INotificationPresenter {
    void handle(Intent intent);

    void onClickCancel();

    void onClickOpen();
}
