package com.alphabit.sportnotes.timer;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Yuri Zigunov on 02.10.2017.
 */
public class TimerPrinter {

    private static final DateTimeFormatter formatterHMillisJ = DateTimeFormat.forPattern("HH:mm:ss.SS");
    private static final DateTimeFormatter formatterMillisJ = DateTimeFormat.forPattern("mm:ss.SS");
    private static final DateTimeFormatter formatterHJ = DateTimeFormat.forPattern("HH:mm:ss");
    private static final DateTimeFormatter formatterJ = DateTimeFormat.forPattern("mm:ss");

    public static String printWithMillis(long millis) {
        if (millis < TimeUnit.HOURS.toMillis(1))
            return format(formatterMillisJ, millis);
        else
            return format(formatterHMillisJ, millis);
    }

    public static String printWithoutMillis(long millis) {
        if (millis < TimeUnit.HOURS.toMillis(1))
            return format(formatterJ, millis);
        else
            return format(formatterHJ, millis);
    }

    private static String format(DateTimeFormatter formatter, long millis) {
        return formatter.print(new DateTime(millis).withZone(DateTimeZone.UTC));
    }
}
