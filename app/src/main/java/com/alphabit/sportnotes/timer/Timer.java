package com.alphabit.sportnotes.timer;

import android.os.CountDownTimer;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import sportnotes.alphabit.com.business.model.TimerTask;

/**
 * Created by Yuri Zigunov on 02.10.2017.
 */

public class Timer {

    private final long mMillis;
    private final CountDownTimer mCountDownTimer;
    private OnTimerListener mOnTimerListener;

    public Timer(final long millis, final long period) {
        mMillis = millis;
        mCountDownTimer = new CountDownTimer(millis, period) {
            @Override
            public void onTick(final long l) {
                if (mOnTimerListener != null)
                    mOnTimerListener.onTick(l);
            }

            @Override
            public void onFinish() {
                if (mOnTimerListener != null)
                    mOnTimerListener.onFinish();
            }
        };
    }

    public void setOnTimerListener(final OnTimerListener onTimerListener) {
        mOnTimerListener = onTimerListener;
    }

    public void start() {
        mCountDownTimer.start();
    }

    public void cancel() {
        mOnTimerListener = null;
        mCountDownTimer.cancel();
    }

    public interface OnTimerListener {
        void onTick(long millis);
        void onFinish();
    }
}
