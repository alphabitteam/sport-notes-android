package com.alphabit.sportnotes.timer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import java.util.UUID;

import sportnotes.alphabit.com.business.model.TimerTask;

/**
 * Created by Yuri Zigunov on 02.10.2017.
 */

public class ExerciseTimer extends RenewableTimer {

    private final UUID mExerciseUuid;
    private final Context mContext;

    public ExerciseTimer(Context context, UUID exerciseUuid, @NonNull final TimerTask timerTask,
                         final OnTimerListener onTimerListener) {
        super(timerTask, onTimerListener);
        mContext = context;
        mExerciseUuid = exerciseUuid;
    }

    public ExerciseTimer(Context context, UUID exerciseUuid, final long millis,
                         final OnTimerListener onTimerListener) {
        super(millis, onTimerListener);
        mContext = context;
        mExerciseUuid = exerciseUuid;
    }

    @Override
    public void start() {
        super.start();
        createAlarm();
    }

    @Override
    public void pause() {
        super.pause();
        removeAlarm();
    }

    @Override
    public void cancel() {
        super.cancel();
        removeAlarm();
    }

    private void createAlarm() {
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(mContext, AlarmReceiver.class);
        intent.putExtra(AlarmReceiver.EXERCISE_UUID, mExerciseUuid);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 232, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + getMillis(), pendingIntent);
    }

    private void removeAlarm() {
        AlarmManager alarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(mContext, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 232, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);
    }
}
