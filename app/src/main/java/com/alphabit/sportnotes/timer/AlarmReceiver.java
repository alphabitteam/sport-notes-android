package com.alphabit.sportnotes.timer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.Pair;

import com.alphabit.sportnotes.App;
import com.alphabit.sportnotes.notification.push.CoreNotificationCreator;
import java.util.UUID;
import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.usecase.contract.IExerciseDetailUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISettingsUseCase;

import com.alphabit.sportnotes.notification.Notifications;
import com.google.gson.Gson;

/**
 * Created by Yuri Zigunov on 02.10.2017.
 */

public class AlarmReceiver extends BroadcastReceiver {

    public static final int NOTIFICATION_CODE = 137;

    @Inject
    IExerciseDetailUseCase useCase;
    @Inject
    ISettingsUseCase settingsUseCase;

    public static final String EXERCISE_UUID = "exercise_uuid";
    public static final String FROM_PUSH = "from_push";
    Disposable disposable;


    public AlarmReceiver() {
        App.getAppComponent().inject(this);
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Activity activity = App.getCurrentActivity();
        UUID uuid = (UUID) intent.getSerializableExtra(EXERCISE_UUID);
        Log.d(getClass().getSimpleName(), "onReceive: uuid = " + uuid);
        if (activity != null && !activity.isFinishing()) {
            disposable = useCase.getExercise(uuid).observeOn(Schedulers.io())
                    .subscribe(exercise -> {
                        Intent notificationIntent = new Intent(Notifications.EXERCISE_TIMER_END_ACTION);
                        notificationIntent.putExtra(EXERCISE_UUID, exercise.getUuid());
                        LocalBroadcastManager.getInstance(activity).sendBroadcast(notificationIntent);
                        if (disposable != null && !disposable.isDisposed())
                            disposable.dispose();
                    }, throwable -> {});

        } else {
            disposable = Flowable.combineLatest(useCase.getExercise(uuid), settingsUseCase.getRingtone(), Pair::new)
                    .observeOn(Schedulers.io())
                    .subscribe(pair -> {
                        new CoreNotificationCreator(context)
                                .showTimerPushNotification(pair.first.getName(), pair.first.getUuid(), Uri.parse(pair.second));
                        if (disposable != null && !disposable.isDisposed())
                            disposable.dispose();
                    }, throwable -> {
                        Log.d(getClass().getSimpleName(), "onReceive: " + throwable);
                    });
        }
    }


}
