package com.alphabit.sportnotes.timer;

import android.support.annotation.NonNull;

import sportnotes.alphabit.com.business.model.TimerTask;

/**
 * Created by Yuri Zigunov on 02.10.2017.
 */

public class RenewableTimer implements Timer.OnTimerListener {

    private static final long TICK_PERIOD = 25;

    private final long mInitMillis;
    private long mMillis = 0;
    private TimerTask mTimerTask;
    private final OnTimerListener mOnTimerListener;

    private boolean isPause;
    private boolean mIsActive;

    private Timer mTimer;

    public RenewableTimer(@NonNull TimerTask timerTask, final OnTimerListener onTimerListener) {
        mTimerTask = timerTask;
        mInitMillis = mTimerTask.getMillis();
        mOnTimerListener = onTimerListener;
        calculateMillis();
    }

    public RenewableTimer(final long millis, final OnTimerListener onTimerListener) {
        mInitMillis = millis;
        mMillis = mInitMillis;
        mOnTimerListener = onTimerListener;
    }

    private void calculateMillis() {

        long current = System.currentTimeMillis();
        long diff = mInitMillis - current;

        if (diff > 0) {
            mMillis = diff;
        } else {
            mMillis = 0;
        }
    }

    public void start() {
        isPause = false;
        mIsActive = true;
        mTimer = new Timer(mMillis, TICK_PERIOD);
        mTimer.setOnTimerListener(this);
        mTimer.start();
    }

    public void pause() {
        isPause = true;
        if (mTimer != null)
            mTimer.cancel();
        mTimer = null;
    }

    public void cancel() {
        isPause = false;
        mIsActive = false;
        mMillis = mInitMillis;
        if (mTimer != null)
            mTimer.cancel();
        mTimer = null;
        callListener(mOnTimerListener::onFinish);
    }

    @Override
    public void onTick(final long millis) {
        mMillis = millis;
        callListener(() -> mOnTimerListener.onTick(millis));
    }

    @Override
    public void onFinish() {
        mMillis = 0;
        callListener(() -> mOnTimerListener.onTick(mMillis));
        callListener(mOnTimerListener::onFinish);
    }

    private void callListener(Action action) {
        if (mOnTimerListener != null)
            action.call();
    }

    public long getMillis() {
        return mMillis;
    }

    public long getInitMillis() {
        return mInitMillis;
    }

    public boolean isPause() {
        return isPause;
    }

    public boolean isActive() {
        return mIsActive;
    }

    public interface OnTimerListener {

        void onTick(long millis);

        void onFinish();
    }

    private interface Action {
        void call();
    }
}
