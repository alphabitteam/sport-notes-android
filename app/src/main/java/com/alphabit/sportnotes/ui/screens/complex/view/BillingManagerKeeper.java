package com.alphabit.sportnotes.ui.screens.complex.view;

import com.alphabit.sportnotes.utils.billing.BillingManager;

public interface BillingManagerKeeper {

    BillingManager getBillingManager();

}
