package com.alphabit.sportnotes.ui.screens.exercises.view;

import com.alphabit.sportnotes.ui.screens.base.view.BaseMvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;
import java.util.UUID;

import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.WeekDay;

/**
 * Created by Fly on 04.09.2017.
 */

public interface IExerciseMvpView extends BaseMvpView {
    @StateStrategyType(SkipStrategy.class)
    void addExercises(List<Exercise> exercises);
    @StateStrategyType(SkipStrategy.class)
    void addLastExercises(List<Exercise> exercises);
    @StateStrategyType(SkipStrategy.class)
    void showDragModeList();
    @StateStrategyType(SkipStrategy.class)
    void showNormalModeList();
    @StateStrategyType(SkipStrategy.class)
    void onDeletedComplexItem(Exercise exercise);
    @StateStrategyType(SkipStrategy.class)
    void showCreateExercise(final WeekDay weekDay, UUID complexUuid);
    @StateStrategyType(SkipStrategy.class)
    void openExerciseDetail(UUID uuid);
    @StateStrategyType(SkipStrategy.class)
    void clearExercise();
    @StateStrategyType(SkipStrategy.class)
    void showDeleteExerciseConfirm();
    @StateStrategyType(SkipStrategy.class)
    void applyWeightUnit(String second);
}
