package com.alphabit.sportnotes.ui.screens.exercisedetail.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.databinding.ActivityExercisesDetailBinding;
import com.alphabit.sportnotes.ui.screens.base.BaseActivity;

import java.util.UUID;

/**
 * Created by Yuri Zigunov on 02.10.2017.
 */
@Layout(id = R.layout.activity_exercises_detail)
public class ExerciseDetailActivity extends BaseActivity<ActivityExercisesDetailBinding> {

    public static final String EXERCISE_UUID = "exercise_uuid";

    public static void start(Context context, final UUID exerciseUuid, boolean clearTop) {
        Intent intent = new Intent(context, ExerciseDetailActivity.class);
        intent.putExtra(EXERCISE_UUID, exerciseUuid);
        if (clearTop) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID exerciseUuid = (UUID) getIntent().getSerializableExtra(EXERCISE_UUID);
        if (exerciseUuid != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, ExerciseDetailFragment.newInstance(exerciseUuid))
                    .commitAllowingStateLoss();
        } else finish();
    }

    @Override
    protected void initView() {

    }
}
