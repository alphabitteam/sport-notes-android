package com.alphabit.sportnotes.ui.screens.exercises.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.databinding.ItemExerciseBinding;
import com.alphabit.sportnotes.ui.adapter.BaseRenderer;
import com.alphabit.sportnotes.ui.adapter.CompositeAdapter;
import com.alphabit.sportnotes.ui.adapter.BaseHolder;
import com.alphabit.sportnotes.view.DatePrinter;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import java.util.List;
import sportnotes.alphabit.com.business.Serializer;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.ExerciseSum;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.SetSum;
import sportnotes.alphabit.com.business.model.SetTime;

public class ExerciseItemRenderer extends BaseRenderer<ExerciseItemRenderer.ViewHolder, Exercise> {

    private OnExerciseListenerListener mOnExerciseListenerListener;
    private ViewBinderHelper viewBinderHelper;
    private final DatePrinter datePrinter;
    Serializer serializer = new Serializer();

    private String weightUnit;

    private LastSetRenderer lastSetRenderer;

    private CompositeAdapter.Mode mMode = CompositeAdapter.Mode.NORMAL;

    public ExerciseItemRenderer(final OnExerciseListenerListener onExerciseListenerListener,
                                ViewBinderHelper viewBinderHelper, DatePrinter datePrinter) {
        mOnExerciseListenerListener = onExerciseListenerListener;
        this.viewBinderHelper = viewBinderHelper;
        this.datePrinter = datePrinter;
    }

    @NonNull
    @Override
    public ExerciseItemRenderer.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_exercise, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @NonNull List<Exercise> items, int position) {
        holder.bind(items.get(position));
        System.out.println("ExerciseItemRenderer.onBindViewHolder = " + serializer.serialize(items.get(position)));
    }

    @Override
    public void onRecycled(ViewHolder holder) {
    }

    public CompositeAdapter.Mode getMode() {
        return mMode;
    }

    public void setMode(CompositeAdapter.Mode mode) {
        this.mMode = mode;
    }

    @Override
    public boolean isForViewType(@NonNull List<?> items, int position) {
        return items.get(position) instanceof Exercise;
    }

    public void setWeightUnit(String weightUnit) {
        this.weightUnit = weightUnit;
    }

    public class ViewHolder extends BaseHolder<Exercise, ItemExerciseBinding> {

        private final CompositeAdapter adapter;

        public ViewHolder(View itemView) {
            super(itemView);
            getBinding().setRecycler.setLayoutManager(new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, true));
            lastSetRenderer = new LastSetRenderer(datePrinter);
            adapter = new CompositeAdapter.Builder<>().add(lastSetRenderer).build();
            prepareRecyclerView(getBinding().setRecycler,
                    (LinearLayoutManager) getBinding().setRecycler.getLayoutManager());
            getBinding().setRecycler.setAdapter(adapter);
        }

        @Override
        public void bind(Exercise item) {
            getBinding().title.setText(item.getName());
            getBinding().text.setText(item.getDescription());
            Log.d(getClass().getSimpleName(), "bind: " + getMode());
            switch (getMode()) {
                case NORMAL:
                    getBinding().setRecycler.setVisibility(View.VISIBLE);
                    getBinding().arrow.setVisibility(View.VISIBLE);
                    getBinding().itemLayout.setOnTouchListener(null);
                    getBinding().sortIcon.setOnTouchListener(null);
                    getBinding().sortIcon.setVisibility(View.INVISIBLE);
                    break;
                case DRAG:
                    getBinding().setRecycler.setVisibility(View.GONE);
                    getBinding().arrow.setVisibility(View.GONE);
                    getBinding().sortIcon.setOnTouchListener((view, motionEvent) -> onTouch(item, motionEvent));
                    getBinding().sortIcon.setVisibility(View.VISIBLE);
                    break;
            }

            Log.d(getClass().getSimpleName(), "bind: " + getBinding().sortIcon.getVisibility());

            getBinding().deleteLayout.delete.setOnClickListener(view ->
                    mOnExerciseListenerListener.onClickDelete(item));

            getBinding().itemLayout.setOnClickListener(view ->
                    mOnExerciseListenerListener.onItemClick(item));

            viewBinderHelper.bind(getBinding().swipe, String.valueOf(item.getUuid()));

            System.out.println("ViewHolder.bind2 = " + item.getName() + " count = " + item.getSets().size() + " " + item.getUuid());

            boolean withWeight = false;

            if (item instanceof ExerciseSum) {
                if (((ExerciseSum) item).getSetsValue() != 0) {
                    withWeight = true;
                }
            }

            lastSetRenderer.setWeight(withWeight, weightUnit);
            adapter.clearData();
            adapter.setData(item.getSets());
        }

        private boolean onTouch(Exercise item, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                mOnExerciseListenerListener.onTouchDrag(item, this);
            }
            return false;
        }
    }

    public interface OnExerciseListenerListener {
        void onTouchDrag(Exercise exercise, BaseHolder holder);
        void onClickDelete(Exercise exercise);
        void onItemClick(Exercise exercise);
    }
}
