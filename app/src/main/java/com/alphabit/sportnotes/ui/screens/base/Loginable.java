package com.alphabit.sportnotes.ui.screens.base;

public interface Loginable {
    void requestLogin();
}
