package com.alphabit.sportnotes.ui.screens.complex.view;

import com.alphabit.sportnotes.ui.screens.base.view.BaseMvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;
import java.util.UUID;

import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.model.WeekDay;

/**
 * Created by Fly on 27.08.2017.
 */

public interface ComplexMvpView extends BaseMvpView {
    @StateStrategyType(SkipStrategy.class)
    void showExerciseForWeekday(UUID uuid, WeekDay weekDay);
    @StateStrategyType(SkipStrategy.class)
    void addComplexes(List<Complex> complexes);
    @StateStrategyType(SkipStrategy.class)
    void addLastComplexes(List<Complex> complexes);
    @StateStrategyType(SkipStrategy.class)
    void showCurrentComplex(Complex complex);
    @StateStrategyType(SkipStrategy.class)
    void showSelectCurrentComplex();
    @StateStrategyType(SkipStrategy.class)
    void onDeletedComplexItem(Complex complex);
    @StateStrategyType(SkipStrategy.class)
    void showDeleteComplexConfirm(Complex complex);
    @StateStrategyType(SkipStrategy.class)
    void showCreateNewComplex();
    @StateStrategyType(SkipStrategy.class)
    void clearComplexes();
    @StateStrategyType(SkipStrategy.class)
    void hideComplexSheet();
    @StateStrategyType(SkipStrategy.class)
    void showEditComplex(Complex complex);
    @StateStrategyType(SkipStrategy.class)
    void sync();
}
