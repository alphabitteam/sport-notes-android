package com.alphabit.sportnotes.ui.adapter;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Fly on 05.09.2017.
 */

public abstract class MoveAdapter<T extends BaseHolder> extends RecyclerView.Adapter<T> {

    public void swap(int oldPosition, int newPosition) {
        notifyItemMoved(oldPosition, newPosition);
    }

    public void remove(int position) {
        notifyItemRemoved(position);
    }

}
