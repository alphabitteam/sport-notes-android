package com.alphabit.sportnotes.ui;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.alphabit.sportnotes.App;
import com.alphabit.sportnotes.ui.screens.base.BaseActivity;
import com.alphabit.sportnotes.ui.screens.editexercise.view.EditExerciseActivity;
import com.alphabit.sportnotes.ui.screens.exercisedetail.view.ExerciseDetailActivity;
import com.alphabit.sportnotes.ui.screens.exercisedetail.view.ExerciseDetailFragment;
import com.alphabit.sportnotes.ui.screens.exercises.view.ExercisesActivity;
import com.alphabit.sportnotes.ui.screens.sets.view.SetsActivity;

import java.util.UUID;

import sportnotes.alphabit.com.business.model.WeekDay;

/**
 * Created by Fly on 04.09.2017.
 */

public class Router {

    public static void openExerciseListScreen(final Activity activity, final UUID complexUuid,
                                              final WeekDay weekDay) {
        ExercisesActivity.start(activity, complexUuid, weekDay);
    }

    public static void openCreateExerciseScreen(final Activity activity, final WeekDay weekDay, final UUID complexUuid) {
        EditExerciseActivity.startCreate(activity, weekDay, complexUuid);
    }

    public static void openExerciseDetail(final Activity activity, final UUID exerciseUuid) {
        BaseActivity currentActivity = App.getCurrentActivity();
        if (currentActivity != null && !currentActivity.isFinishing()) {
            for (Fragment fragment : currentActivity.getSupportFragmentManager().getFragments()) {
                if (fragment instanceof ExerciseDetailFragment) {
                    ExerciseDetailFragment f = (ExerciseDetailFragment) fragment;
                    if (f.getPresenter().equalsExerciseUuid(exerciseUuid))
                        return;
                }
            }
            if (currentActivity.getClass().getCanonicalName().equals(ExerciseDetailActivity.class.getCanonicalName())) {
                ExerciseDetailActivity.start(activity, exerciseUuid, false);
                return;
            }
        }
        ExerciseDetailActivity.start(activity, exerciseUuid, true);
    }

    public static void openSetsScreen(final Activity activity, final UUID exerciseUuid, boolean withWeight) {
        SetsActivity.start(activity, exerciseUuid, withWeight);
    }

    public static void openEditExerciseScreen(Activity activity, UUID exerciseUuid) {
        EditExerciseActivity.startEdit(activity, exerciseUuid);
    }
}
