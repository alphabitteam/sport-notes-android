package com.alphabit.sportnotes.ui.screens.base.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.databinding.DialogNewComplexBinding;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.alphabit.sportnotes.ui.screens.base.view.BaseMvpView;
import com.alphabit.sportnotes.view.customdialog.CustomDialogBuilder;
import com.arellomobile.mvp.MvpAppCompatFragment;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.lang.annotation.Annotation;
import java.util.UUID;

/**
 * Created by Fly on 27.08.2017.
 */

public abstract class BaseFragment<Binding extends ViewDataBinding> extends MvpAppCompatFragment {

    private Binding mBinding;

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Class cls = getClass();
        if (!cls.isAnnotationPresent(Layout.class)) return null;
        Annotation annotation = cls.getAnnotation(Layout.class);
        Layout layout = (Layout) annotation;
        View view = inflater.inflate(layout.id(), null);
        mBinding = DataBindingUtil.bind(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initAd();
    }

    protected void initAd() {
        if (getView() == null)
            return;
        AdView adView = getView().findViewById(R.id.adView);
        if (adView == null)
            return;
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    @Override
    public void onPause() {
        super.onPause();
        getMvpDelegate().onDetach();
    }

    public void onNewIntent(Intent intent) {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("BaseFragment", "onCreate: ");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("BaseFragment", "onViewCreated: ");
        initView();
    }

    public final Binding getBinding() {
        return mBinding;
    }

    protected abstract void initView();

}
