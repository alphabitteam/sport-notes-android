package com.alphabit.sportnotes.ui.screens.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.databinding.DialogNewComplexBinding;
import com.alphabit.sportnotes.databinding.DialogSimpleBinding;
import com.alphabit.sportnotes.ui.screens.aboutpremium.view.AboutPremiumDialog;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.alphabit.sportnotes.ui.screens.base.view.BaseMvpView;
import com.alphabit.sportnotes.view.customdialog.CustomDialogBuilder;

import java.util.UUID;

/**
 * Created by Fly on 03.09.2017.
 */

public abstract class BaseMvpActivity<Binding extends ViewDataBinding> extends BaseActivity<Binding>
        implements BaseMvpView {


    @Override
    public void showNotifyMessage(final int stringRes) {
        Toast.makeText(this, getString(stringRes), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNotifyMessage(String string) {
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoadingIndicator() {
        if (getIndicatorMenuItem() != null)
            getToolbar().post(() -> {
                getIndicatorMenuItem().setVisible(true);
            });
    }

    @Override
    public void refreshAdState() {
        invalidateAd();
    }

    @Override
    public void hideLoadingIndicator() {
        if (getIndicatorMenuItem() != null)
            getToolbar().post(() -> {
                getIndicatorMenuItem().setVisible(false);
            });
    }

    @Override
    public void showUserLevelError() {
        //showNotifyMessage(getString(R.string.user_level_error));
        DialogSimpleBinding view = DataBindingUtil.bind(LayoutInflater.from(this)
                .inflate(R.layout.dialog_simple, null, false));
        view.text.setText(getString(R.string.for_create_complex_need_premium));
        CustomDialogBuilder.newBuilder(this)
                .setTitle(getString(R.string.error_access))
                .setOnNegativeClickListener(getString(R.string.cancel), customDialog -> {

                })
                .setOnPositiveClickListener(getString(R.string.more), customDialog -> {
                    AboutPremiumDialog dialog = AboutPremiumDialog.newInstance();
                    dialog.show(getSupportFragmentManager(), null);
                })
                .setView(view.getRoot())
                .build().show();
    }

}
