package com.alphabit.sportnotes.ui.screens.exercises.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.databinding.ActivityExercisesBinding;
import com.alphabit.sportnotes.databinding.DialogSimpleBinding;
import com.alphabit.sportnotes.ui.Router;
import com.alphabit.sportnotes.ui.adapter.CompositeAdapter;
import com.alphabit.sportnotes.ui.adapter.BaseHolder;
import com.alphabit.sportnotes.ui.screens.base.BaseMvpActivity;
import com.alphabit.sportnotes.ui.screens.exercises.presenter.ExercisesPresenter;
import com.alphabit.sportnotes.view.DatePrinter;
import com.alphabit.sportnotes.view.MarginItemDecorator;
import com.alphabit.sportnotes.view.MovieTouchHelper;
import com.alphabit.sportnotes.view.SwipeLinearLayoutManager;
import com.alphabit.sportnotes.view.customdialog.CustomDialogBuilder;
import com.alphabit.sportnotes.view.date.DatePrintableImpl;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.google.gson.Gson;

import org.joda.time.format.DateTimeFormat;

import java.util.List;
import java.util.UUID;

import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.WeekDay;

/**
 * Created by Fly on 04.09.2017.
 */
@Layout(id = R.layout.activity_exercises)
public class ExercisesActivity extends BaseMvpActivity<ActivityExercisesBinding> implements
        IExerciseMvpView, ExerciseItemRenderer.OnExerciseListenerListener, MovieTouchHelper.OnChangePositionListener {

    public static final String UUID = "uuid";
    public static final String WEEKDAY = "weekday";

    @InjectPresenter
    ExercisesPresenter mPresenter;

    private CompositeAdapter adapter;
    ExerciseItemRenderer renderer;

    private ItemTouchHelper mTouchHelper;

    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    private DatePrinter datePrinter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        final UUID complexUuid = (UUID) getIntent().getSerializableExtra(UUID);
        final WeekDay weekDay = (WeekDay) getIntent().getSerializableExtra(WEEKDAY);
        datePrinter = new DatePrinter(new DatePrintableImpl(getResources()),
                DateTimeFormat.forPattern("dd.MM"));
        super.onCreate(savedInstanceState);
        mPresenter.setParams(complexUuid, weekDay);

    }

    @Override
    protected void initView() {
        getBinding().recycler.setLayoutManager(
                new SwipeLinearLayoutManager(this, LinearLayoutManager.VERTICAL));
        getBinding().recycler.addItemDecoration(new MarginItemDecorator(getResources().getDimension(R.dimen.item_margin)));

        viewBinderHelper.setOpenOnlyOne(true);

        renderer = new ExerciseItemRenderer(this, viewBinderHelper, datePrinter);
        adapter = new CompositeAdapter.Builder<>()
                .add(renderer)
                .build();
        adapter.setOnLazyListener(mPresenter::onGetLastItem);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getBinding().recycler.setAdapter(adapter);

        ItemTouchHelper.Callback callback = new MovieTouchHelper(adapter, this);
        mTouchHelper = new ItemTouchHelper(callback);
        mTouchHelper.attachToRecyclerView(getBinding().recycler);
        checkForEmptyAdapter();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (renderer != null && renderer.getMode() == CompositeAdapter.Mode.DRAG) {
            getMenuInflater().inflate(R.menu.fragment_exercises_drag, menu);
        } else {
            getMenuInflater().inflate(R.menu.fragment_exercises, menu);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (renderer != null && renderer.getMode() == CompositeAdapter.Mode.DRAG) {
            renderer.setMode(CompositeAdapter.Mode.NORMAL);
            adapter.notifyDataSetChanged();
            invalidateOptionsMenu();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                mPresenter.onAddClickMenu();
                return true;
            case R.id.sort:
                mPresenter.onSortClickMenu(adapter.getData());
                return true;
            case R.id.complete:
                mPresenter.onSortCompleteClickMenu(adapter.getData());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTouchDrag(Exercise exercise, final BaseHolder holder) {
        mTouchHelper.startDrag(holder);
        mPresenter.onStartDrag(exercise);
    }

    @Override
    public void onClickDelete(final Exercise exercise) {
        mPresenter.onClickDelete(exercise);
    }

    @Override
    public void onItemClick(final Exercise exercise) {
        mPresenter.onItemClick(exercise);
    }

    @Override
    public void onChangePosition(final int oldPosition, final int newPosition) {
        mPresenter.onChangeListPosition(oldPosition, newPosition);
        Log.d("changePosItem", "onChangePosition: " + oldPosition + " new = " + newPosition);
    }

    @Override
    public void onOrderChanged() {
        mPresenter.onOrderChanged();
    }

    public static void start(final Context context, final UUID complexUuid, final WeekDay weekDay) {
        Intent intent = new Intent(context, ExercisesActivity.class);
        intent.putExtra(UUID, complexUuid);
        intent.putExtra(WEEKDAY, weekDay);
        context.startActivity(intent);
    }

    @Override
    public void applyWeightUnit(String weightUnit) {
        renderer.setWeightUnit(weightUnit);
    }

    @Override
    public void addExercises(final List<Exercise> exercises) {

        adapter.addData(exercises);
        Log.d(getClass().getSimpleName(), "addExercises: " + exercises.size());
        checkForEmptyAdapter();
    }

    @Override
    public void addLastExercises(final List<Exercise> exercises) {
        adapter.addLastData(exercises);
        Log.d(getClass().getSimpleName(), "addLastExercises: " + exercises.size());
        checkForEmptyAdapter();
    }

    @Override
    public void showDragModeList() {
        if (renderer != null) {
            renderer.setMode(CompositeAdapter.Mode.DRAG);
            adapter.notifyDataSetChanged();
        }
        invalidateOptionsMenu();
        checkForEmptyAdapter();
    }

    @Override
    public void showNormalModeList() {
        if (renderer != null) {
            renderer.setMode(CompositeAdapter.Mode.NORMAL);
            adapter.notifyDataSetChanged();
        }
        invalidateOptionsMenu();
        checkForEmptyAdapter();
    }

    @Override
    public void onDeletedComplexItem(final Exercise exercise) {
        adapter.deleteItem(exercise);
        checkForEmptyAdapter();
    }

    @Override
    public void showDeleteExerciseConfirm() {
        final DialogSimpleBinding dialogBinding = DataBindingUtil.bind(LayoutInflater.from(this)
                .inflate(R.layout.dialog_simple, null, false));
        dialogBinding.text.setText(R.string.confirm_exercise_delete_text);

        CustomDialogBuilder.newBuilder(this)
                .setTitle(getString(R.string.deleting_exercise_title))
                .setView(dialogBinding.getRoot())
                .setOnPositiveClickListener(getString(R.string.ok), customDialog ->
                        mPresenter.onDeleteExerciseConfirmed())
                .setOnNegativeClickListener(getString(R.string.cancel), customDialog -> {
                })
                .build().show();
    }

    @Override
    public void showCreateExercise(final WeekDay weekDay, UUID complexUuid) {
        Router.openCreateExerciseScreen(this, weekDay, complexUuid);
    }

    @Override
    public void openExerciseDetail(final UUID uuid) {
        Router.openExerciseDetail(this, uuid);
    }

    @Override
    public void clearExercise() {
        adapter.clearData();
    }

    private void checkForEmptyAdapter() {
        if (adapter.getItemCount() == 0)
            getBinding().empty.setVisibility(View.VISIBLE);
        else getBinding().empty.setVisibility(View.GONE);
    }
}
