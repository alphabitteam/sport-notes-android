package com.alphabit.sportnotes.ui.screens.complex.view;

import android.support.v7.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.databinding.DialogComplesesBinding;
import com.alphabit.sportnotes.ui.adapter.CompositeAdapter;
import com.alphabit.sportnotes.view.SwipeLinearLayoutManager;
import com.alphabit.sportnotes.view.dialogsheet.DialogSheetView;
import com.alphabit.sportnotes.view.dialogsheet.Params;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.List;

import sportnotes.alphabit.com.business.model.Complex;

/**
 * Created by Fly on 29.08.2017.
 */

public class ComplexesDialogSheet extends DialogSheetView<DialogComplesesBinding>
        implements ComplexRenderer.OnItemComplexListener, AddComplexRenderer.OnNewComplexListener {
    private final int mMarginTop;
    private final OnDialogListener mOnDialogListener;
    private CompositeAdapter mAdapter;

    private final OnComplexListener mOnComplexListener;

    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

    public ComplexesDialogSheet(OnDialogListener onDialogListener, OnComplexListener onComplexListener, int marginTop) {
        mMarginTop = marginTop;
        mOnDialogListener = onDialogListener;
        mOnComplexListener = onComplexListener;
    }

    @Override
    public View onViewCreate() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_compleses, null, false);
        return view;
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        getBinding().recycler.setLayoutManager(
                new SwipeLinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL));

        createAdapter();

        getBinding().recycler.setAdapter(mAdapter);
    }

    private void createAdapter() {
        viewBinderHelper.setOpenOnlyOne(true);

        mAdapter = new CompositeAdapter.Builder<>()
                .add(new ComplexRenderer(this, viewBinderHelper))
                .setFooter(new AddComplexRenderer(this))
                .build();

        mAdapter.setOnLazyListener(mOnComplexListener::onGetLastItem);
    }

    @Override
    public Params withParams(final Params params) {
        params.setGravity(Gravity.TOP);
        params.setHeight((int) getContext().getResources().getDimension(R.dimen.complexes_sheet_height));
        params.setMargin(0, mMarginTop, 0, 0);
        return params;
    }

    @Override
    public void onShow() {
        super.onShow();
        mOnDialogListener.onSheetShow();
    }

    @Override
    public void onHide() {
        super.onHide();
        mOnDialogListener.onSheetHide();
    }

    public void addData(final List<Complex> complexes) {
        mAdapter.addData(complexes);
    }

    @Override
    public void onSelectComplex(final Complex complex) {
        mOnComplexListener.onSelectComplex(complex);
    }

    @Override
    public void onClickDeleteComplex(final int position, final Complex complex) {
        mOnComplexListener.onClickDeleteComplex(complex);
    }

    @Override
    public void onClickEditComplex(final Complex complex) {
        mOnComplexListener.onClickEditComplex(complex);
    }

    @Override
    public void onClickNewComplex() {
        mOnComplexListener.onClickNewComplex();
    }

    public void addLastData(final List<Complex> complexes) {
        mAdapter.addLastData(complexes);
    }

    public void showCurrentComplex(final Complex complex) {
        mAdapter.setSelectedItem(complex);
    }

    public void deleteItem(Complex complex) {
        mAdapter.deleteItem(complex);
    }

    public void clearData() {
        createAdapter();
        getBinding().recycler.setAdapter(mAdapter);
    }

    public interface OnDialogListener {
        void onSheetShow();
        void onSheetHide();
    }

    public interface OnComplexListener {
        void onSelectComplex(Complex complex);
        void onClickDeleteComplex(Complex complex);
        void onClickEditComplex(Complex complex);
        void onClickNewComplex();
        void onGetLastItem(int position);
    }
}
