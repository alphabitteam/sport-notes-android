package com.alphabit.sportnotes.ui.screens.aboutpremium.view;

import com.alphabit.sportnotes.ui.screens.base.view.BaseMvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by fly12 on 19.11.2017.
 */

public interface AboutPremiumView extends BaseMvpView {
    @StateStrategyType(SkipStrategy.class)
    void enableBuy();
    @StateStrategyType(SkipStrategy.class)
    void disableBuy();
    @StateStrategyType(SkipStrategy.class)
    void showBillingError();
    @StateStrategyType(SkipStrategy.class)
    void showBillingConnecting();
    @StateStrategyType(SkipStrategy.class)
    void showBillingConnected();
    @StateStrategyType(SkipStrategy.class)
    void requestLogin();
    @StateStrategyType(SkipStrategy.class)
    void showPurchased();
    @StateStrategyType(SkipStrategy.class)
    void sync();
}
