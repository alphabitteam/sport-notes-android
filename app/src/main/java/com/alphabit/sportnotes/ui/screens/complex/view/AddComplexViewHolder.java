package com.alphabit.sportnotes.ui.screens.complex.view;

import android.view.View;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.databinding.ItemAddComplexBinding;
import com.alphabit.sportnotes.ui.adapter.BaseHolder;

/**
 * Created by Fly on 02.09.2017.
 */
@Layout(id = R.layout.item_add_complex)
public class AddComplexViewHolder extends BaseHolder<Object, ItemAddComplexBinding> {

    private AddComplexRenderer.OnNewComplexListener mOnNewComplexListener;

    public AddComplexViewHolder(final View itemView) {
        super(itemView);
    }

    @Override
    public void bind(final Object item) {
        getBinding().text.setOnClickListener(view -> mOnNewComplexListener.onClickNewComplex());
    }

    public void setOnNewComplexListener(final AddComplexRenderer.OnNewComplexListener onNewComplexListener) {
        mOnNewComplexListener = onNewComplexListener;
    }
}
