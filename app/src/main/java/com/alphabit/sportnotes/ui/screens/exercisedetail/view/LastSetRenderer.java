package com.alphabit.sportnotes.ui.screens.exercisedetail.view;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.databinding.ItemLastSetsBinding;
import com.alphabit.sportnotes.timer.TimerPrinter;
import com.alphabit.sportnotes.ui.adapter.BaseRenderer;
import com.alphabit.sportnotes.ui.adapter.BaseHolder;
import com.alphabit.sportnotes.view.DatePrinter;

import java.util.List;

import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.SetSum;
import sportnotes.alphabit.com.business.model.SetTime;

public class LastSetRenderer extends BaseRenderer<LastSetRenderer.ViewHolder, Set> {

    private OnClickSetListener mOnClickSetListener;
    private DatePrinter datePrinter;
    private String unit;

    public LastSetRenderer(OnClickSetListener onClickSetListener, DatePrinter datePrinter) {
        mOnClickSetListener = onClickSetListener;
        this.datePrinter = datePrinter;
    }

    @NonNull
    @Override
    public BaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_last_sets, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @NonNull List<Set> items, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public void onRecycled(ViewHolder holder) {

    }


    @Override
    public boolean isForViewType(@NonNull List items, int position) {
        return items.get(position) instanceof Set;
    }

    public void setOnClickSetListener(final OnClickSetListener onClickSetListener) {
        mOnClickSetListener = onClickSetListener;
    }

    public void setDatePrinter(DatePrinter datePrinter) {
        this.datePrinter = datePrinter;
    }

    public void setWeightUnit(String unit) {
        this.unit = unit;
    }

    public class ViewHolder extends BaseHolder<Set, ItemLastSetsBinding> {

        public ViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bind(Set item) {
            getBinding().getRoot().setOnClickListener(view -> mOnClickSetListener.onClickSet(item));

            if (item instanceof SetSum)
                bindSum((SetSum)item);
            else if (item instanceof SetTime)
                bindTime((SetTime)item);
        }

        private void bindTime(final SetTime item) {
            getBinding().value.setText(TimerPrinter.printWithoutMillis(item.getValue()));
            getBinding().sets.setText(getBinding().getRoot().getContext().getString(R.string.x_set, item.getNumber()));
            getBinding().date.setText(datePrinter.printLastSets(item.getCreatedAt()));
        }

        private void bindSum(final SetSum item) {
            getBinding().value.setText(String.valueOf(item.getValue()) + ( unit == null ? "" : " " + unit));
            getBinding().sets.setText(getBinding().getRoot().getContext().getString(R.string.x_set, item.getNumber()));
            getBinding().date.setText(datePrinter.printLastSets(item.getCreatedAt()));
        }

    }

    public interface OnClickSetListener {
        void onClickSet(Set set);
    }
}
