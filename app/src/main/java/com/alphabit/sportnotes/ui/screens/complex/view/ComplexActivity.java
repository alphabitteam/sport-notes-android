package com.alphabit.sportnotes.ui.screens.complex.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.databinding.ActivityComplexBinding;
import com.alphabit.sportnotes.databinding.DialogNewComplexBinding;
import com.alphabit.sportnotes.databinding.DialogSimpleBinding;
import com.alphabit.sportnotes.ui.Router;
import com.alphabit.sportnotes.ui.screens.base.BaseMvpActivity;
import com.alphabit.sportnotes.ui.screens.base.Loginable;
import com.alphabit.sportnotes.ui.screens.complex.presenter.ComplexScreenPresenter;
import com.alphabit.sportnotes.ui.screens.navigation.view.NavigationFragment;
import com.alphabit.sportnotes.utils.billing.BillingManager;
import com.alphabit.sportnotes.view.customdialog.CustomDialog;
import com.alphabit.sportnotes.view.customdialog.CustomDialogBuilder;
import com.alphabit.sportnotes.view.customdialog.SimpleCustomDialog;
import com.alphabit.sportnotes.view.dialogsheet.DialogSheetBuilder;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.CompositePermissionListener;
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Transformation;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.model.WeekDay;

/**
 * Created by Fly on 27.08.2017.
 */
@Layout(id = R.layout.activity_complex)
public class ComplexActivity extends BaseMvpActivity<ActivityComplexBinding> implements
        ComplexMvpView, ComplexesDialogSheet.OnDialogListener, ComplexesDialogSheet.OnComplexListener, DrawerLayout.DrawerListener,
        BillingManagerKeeper, Loginable {

    @InjectPresenter
    ComplexScreenPresenter mComplexPresenter;

    ComplexesDialogSheet mDialogSheet;

    private BillingManager billingManager;

    @BindView(R.id.tileImage1)
    ImageView tileImage1;
    @BindView(R.id.tileImage2)
    ImageView tileImage2;
    @BindView(R.id.tileImage3)
    ImageView tileImage3;
    @BindView(R.id.tileImage4)
    ImageView tileImage4;
    @BindView(R.id.tileImage5)
    ImageView tileImage5;
    @BindView(R.id.tileImage6)
    ImageView tileImage6;
    @BindView(R.id.tileImage7)
    ImageView tileImage7;
    @BindView(R.id.tileImage8)
    ImageView tileImage8;

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        billingManager = new BillingManager(this);
        mComplexPresenter.setBillingManager(billingManager);
        checkPermission();
    }



    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted

            DialogSimpleBinding view = DataBindingUtil.bind(LayoutInflater.from(this).inflate(R.layout.dialog_simple, null, false));
            view.text.setText(getString(R.string.need_permission_for_rington));
            CustomDialog dialog = CustomDialogBuilder.newBuilder(this)
                    .setTitle(getString(R.string.warning))
                    .setView(view.getRoot())
                    .setOnPositiveClickListener(getString(R.string.ok), customDialog -> {
                        Dexter.withActivity(this)
                                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                                .withListener(new PermissionListener() {
                                    @Override
                                    public void onPermissionGranted(PermissionGrantedResponse response) {

                                    }

                                    @Override
                                    public void onPermissionDenied(PermissionDeniedResponse response) {
                                        SimpleCustomDialog.create(ComplexActivity.this, getString(R.string.warning), getString(R.string.no_permission_to_ringtone)).show();
                                    }

                                    @Override
                                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                        token.continuePermissionRequest();
                                    }
                                })
                                .check();
                    })
                    .build();
            dialog.show();
        }
    }


    @Override
    protected void initView() {

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, getBinding().drawerLayout, getToolbar(), R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        getBinding().drawerLayout.addDrawerListener(toggle);
        getBinding().drawerLayout.addDrawerListener(this);
        toggle.syncState();

        getBinding().contentLayout.openSheet.post(() -> {
            initComplexesSheet(getBinding().contentLayout.openSheet);

            getBinding().contentLayout.tile1.setOnClickListener(view -> mComplexPresenter.onClickTile(WeekDay.MONDAY));
            getBinding().contentLayout.tile2.setOnClickListener(view -> mComplexPresenter.onClickTile(WeekDay.THURSDAY));
            getBinding().contentLayout.tile3.setOnClickListener(view -> mComplexPresenter.onClickTile(WeekDay.WEDNESDAY));
            getBinding().contentLayout.tile4.setOnClickListener(view -> mComplexPresenter.onClickTile(WeekDay.THURSDAY));
            getBinding().contentLayout.tile5.setOnClickListener(view -> mComplexPresenter.onClickTile(WeekDay.FRIDAY));
            getBinding().contentLayout.tile6.setOnClickListener(view -> mComplexPresenter.onClickTile(WeekDay.SATURDAY));
            getBinding().contentLayout.tile7.setOnClickListener(view -> mComplexPresenter.onClickTile(WeekDay.SUNDAY));
            getBinding().contentLayout.openSheet.setOnClickListener(this::onOpenPanelControl);
            initImages();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_complex, menu);
        return true;
    }

    private void initImages() {
        setImage(R.mipmap.gum1, tileImage1);
        setImage(R.mipmap.gum2, tileImage2);
        setImage(R.mipmap.gum3, tileImage3);
        setImage(R.mipmap.gum4, tileImage4);
        setImage(R.mipmap.gum5, tileImage5);
        setImage(R.mipmap.gum6, tileImage6);
        setImage(R.mipmap.gum7, tileImage7);
        setImage(R.mipmap.gum8, tileImage8);
    }

    @Override
    public void sync() {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof NavigationFragment) {
                ((NavigationFragment) fragment).sync();
                return;
            }
        }
    }

    private void setImage(@DrawableRes int resId, ImageView imageView) {
        int w = imageView.getWidth() + (imageView.getWidth() / 2);
        int h = imageView.getHeight() + (imageView.getHeight() / 2);
        RequestCreator builder = Picasso.with(this).load(resId);
        if (w < 1280)
            builder.resize(w, h).centerCrop();
        builder.into(imageView);
    }


    private void initComplexesSheet(final View target) {
        mDialogSheet = DialogSheetBuilder.newDialogSheet(this)
                .dialog(new ComplexesDialogSheet(this, this, target.getMeasuredHeight()))
                .build();
    }

    @Override
    public void showExerciseForWeekday(final UUID uuid, final WeekDay weekDay) {
        Router.openExerciseListScreen(this, uuid, weekDay);
    }
    private void setMyTheme(int res) {
        getTheme().applyStyle(res, true);
    }

    @Override
    public void addComplexes(final List<Complex> complexes) {
        getBinding().contentLayout.openSheet.post(() ->
            mDialogSheet.addData(complexes));
    }

    @Override
    public void addLastComplexes(final List<Complex> complexes) {
        getBinding().contentLayout.openSheet.post(() ->
            mDialogSheet.addLastData(complexes));
    }

    @Override
    public void showCurrentComplex(final Complex complex) {
        getBinding().contentLayout.openSheet.post(() ->
                mDialogSheet.showCurrentComplex(complex));
    }

    @Override
    public void showSelectCurrentComplex() {
        //TODO: показать диалог
        Toast.makeText(this, getString(R.string.select_current_complex), Toast.LENGTH_LONG).show();
        getBinding().contentLayout.openSheet.post(() ->
                mDialogSheet.show());
    }

    @Override
    public void onDeletedComplexItem(final Complex complex) {
        mDialogSheet.deleteItem(complex);
    }

    @Override
    public void showDeleteComplexConfirm(final Complex complex) {
        final DialogSimpleBinding dialogBinding = DataBindingUtil.bind(LayoutInflater.from(this)
                .inflate(R.layout.dialog_simple, null, false));
        dialogBinding.text.setText(R.string.confirm_complex_delete_text);

        CustomDialogBuilder.newBuilder(this)
                .setTitle(getString(R.string.deleting_complex_title))
                .setView(dialogBinding.getRoot())
                .setOnPositiveClickListener(getString(R.string.ok), customDialog ->
                        mComplexPresenter.onDeleteComplexConfirmed())
                .setOnNegativeClickListener(getString(R.string.cancel), customDialog -> {
                })
                .build().show();
    }

    @Override
    public void showCreateNewComplex() {
        final DialogNewComplexBinding dialogBinding = DataBindingUtil.bind(LayoutInflater.from(this)
                .inflate(R.layout.dialog_new_complex, null, false));

        CustomDialogBuilder.newBuilder(this)
                .setTitle(getString(R.string.create_new_complex_title))
                .setView(dialogBinding.getRoot())
                .setOnPositiveClickListener(getString(R.string.create_complex), customDialog -> {
                    mComplexPresenter.onCreateNewComplexClick(dialogBinding.edit.getText().toString());
                }).build().show();
    }

    @Override
    public void showEditComplex(Complex complex) {
        final DialogNewComplexBinding dialogBinding = DataBindingUtil.bind(LayoutInflater.from(this)
                .inflate(R.layout.dialog_new_complex, null, false));

        dialogBinding.edit.setText(complex.getName());

        CustomDialogBuilder.newBuilder(this)
                .setTitle(getString(R.string.edit_complex_title))
                .setView(dialogBinding.getRoot())
                .setOnPositiveClickListener(getString(R.string.edit_complex), customDialog -> {
                    mComplexPresenter.onEditedComplexClick(complex, dialogBinding.edit.getText().toString());
                }).build().show();
    }

    @Override
    public void clearComplexes() {
        mDialogSheet.clearData();
    }

    @Override
    public void hideComplexSheet() {
        if (mDialogSheet != null)
            mDialogSheet.dismiss();
        getBinding().contentLayout.openSheet.setTag(null);
    }

    private void onOpenPanelControl(final View view) {
        if (view.getTag() == null) {
            if (mDialogSheet != null)
                mDialogSheet.show();
            view.setTag(mDialogSheet);
        } else {
            if (mDialogSheet != null)
                mDialogSheet.dismiss();
            view.setTag(null);
        }
    }

    @Override
    public void onSheetShow() {
        getBinding().contentLayout.openSheetArrow.setImageResource(R.mipmap.arrow_top);

    }

    @Override
    public void onSheetHide() {
        getBinding().contentLayout.openSheet.setTag(null);
        getBinding().contentLayout.openSheetArrow.setImageResource(R.mipmap.arrow_bottom);
    }

    @Override
    public void onSelectComplex(final Complex complex) {
        mComplexPresenter.onSelectComplex(complex);
    }

    @Override
    public void onClickDeleteComplex(final Complex complex) {
        mComplexPresenter.onClickDeleteComplex(complex);
    }

    @Override
    public void onClickEditComplex(final Complex complex) {
        mComplexPresenter.onClickEditComplex(complex);
    }

    @Override
    public void onClickNewComplex() {
        mComplexPresenter.onClickNewComplex();
    }

    @Override
    public void onGetLastItem(final int position) {
        Log.d(getClass().getSimpleName(), "onGetLastItem: " + position);
        mComplexPresenter.onGetLastItem(position);
    }

    @Override
    public void onDrawerSlide(final View drawerView, final float slideOffset) {
    }

    @Override
    public void onDrawerOpened(final View drawerView) {
    }

    @Override
    public void onDrawerClosed(final View drawerView) {
    }

    @Override
    public void onDrawerStateChanged(final int newState) {
        if (newState == 2)
            mComplexPresenter.onChangeNavigationMenuState();
    }

    @Override
    public BillingManager getBillingManager() {
        return billingManager;
    }

    @Override
    public void requestLogin() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.nav_view);
        if (f != null && f instanceof Loginable)
            ((Loginable)f).requestLogin();

    }
}
