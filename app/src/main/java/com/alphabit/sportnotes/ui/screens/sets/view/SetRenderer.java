package com.alphabit.sportnotes.ui.screens.sets.view;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.databinding.ItemSetBinding;
import com.alphabit.sportnotes.timer.TimerPrinter;
import com.alphabit.sportnotes.ui.adapter.BaseRenderer;
import com.alphabit.sportnotes.ui.adapter.BaseHolder;
import com.alphabit.sportnotes.view.DatePrinter;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.List;

import sportnotes.alphabit.com.business.Serializer;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.SetSum;
import sportnotes.alphabit.com.business.model.SetTime;
import sportnotes.alphabit.com.business.model.ValueType;

public class SetRenderer extends BaseRenderer<SetRenderer.ViewHolder, Set> {

    private OnSetsListener onSetsListener;
    private ViewBinderHelper viewBinderHelper;
    private String weightUnit;

    private final DatePrinter datePrinter;

    public SetRenderer(OnSetsListener onSetsListener, ViewBinderHelper viewBinderHelper,
                       WeightUnitListener weightUnitSetListener, DatePrinter datePrinter) {
        this.onSetsListener = onSetsListener;
        this.viewBinderHelper = viewBinderHelper;
        weightUnitSetListener.weightUnitListener(new WeightUnitSetListener() {
            @Override
            public void setWeightUnit(String unit) {
                weightUnit = unit;
            }
        });
        this.datePrinter = datePrinter;
    }

    @NonNull
    @Override
    public BaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_set, parent, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @NonNull List<Set> items, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public void onRecycled(ViewHolder holder) {

    }


    @Override
    public boolean isForViewType(@NonNull List items, int position) {
        return items.get(position) instanceof Set;
    }

    public void setOnSetsListener(OnSetsListener onSetsListener) {
        this.onSetsListener = onSetsListener;
    }

    public void setViewBinderHelper(ViewBinderHelper viewBinderHelper) {
        this.viewBinderHelper = viewBinderHelper;
    }

    public void setWeightUnit(String weightUnit) {
        this.weightUnit = weightUnit;
        System.out.println("SetRenderer.setWeightUnit " + weightUnit);
    }

    public class ViewHolder extends BaseHolder<Set, ItemSetBinding> implements WeightUnitSetListener {

        public ViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bind(Set item) {
            System.out.println("ViewHolder.bindsets" + new Serializer().serialize(item));
            switch (item.getRepeatType()) {
                case SUM:
                    final SetSum setSum = (SetSum) item;
                    StringBuilder value = new StringBuilder();
                    value.append(setSum.getValue());
                    if (!TextUtils.isEmpty(weightUnit))
                        value.append(" ").append(weightUnit);
                    getBinding().title.setText(value);
                    break;
                case TIME:
                    final SetTime setTime = (SetTime) item;
                    getBinding().title.setText(TimerPrinter.printWithoutMillis(setTime.getValue()));
                    break;
            }
            getBinding().date.setText(datePrinter.printLastSets(item.getCreatedAt()));
            getBinding().text.setText(getBinding().getRoot().getContext().getString(R.string.x_set, item.getNumber()));
            getBinding().deleteLayout.delete.setOnClickListener(view ->
                    onSetsListener.onDeleteSet(item));

            viewBinderHelper.bind(getBinding().swipe, String.valueOf(item.getUuid()));
        }

        @Override
        public void setWeightUnit(String unit) {
            weightUnit = unit;
            System.out.println("ViewHolder.setWeightUnit " + unit);
        }
    }

    public static interface OnSetsListener {
        void onDeleteSet(Set set);
    }

    public static interface WeightUnitListener {
        void weightUnitListener(WeightUnitSetListener listener);
    }

    public static interface WeightUnitSetListener {
        void setWeightUnit(String unit);
    }

}
