package com.alphabit.sportnotes.ui.screens.exercisedetail.view;

import com.alphabit.sportnotes.ui.screens.base.view.BaseMvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;
import java.util.UUID;

import sportnotes.alphabit.com.business.model.ExerciseSum;
import sportnotes.alphabit.com.business.model.ExerciseTime;
import sportnotes.alphabit.com.business.model.Set;

/**
 * Created by Yuri Zigunov on 02.10.2017.
 */

public interface IExerciseDetailView extends BaseMvpView {
    @StateStrategyType(SkipStrategy.class)
    void buildViewSumWithWeightsExercise(ExerciseSum exercise);
    @StateStrategyType(SkipStrategy.class)
    void buildViewSumWithoutWeightsExercise(ExerciseSum exercise);
    @StateStrategyType(SkipStrategy.class)
    void buildViewTimeExercise(ExerciseTime exercise);
    @StateStrategyType(SkipStrategy.class)
    void applyWeightUnit(String unit);
    @StateStrategyType(SkipStrategy.class)
    void showTimer();
    @StateStrategyType(SkipStrategy.class)
    void showPickerSetter();
    @StateStrategyType(SkipStrategy.class)
    void showTimerNotify();
    @StateStrategyType(SkipStrategy.class)
    void hideTimerNotify();
    @StateStrategyType(SkipStrategy.class)
    void printTimerTime(String time);
    @StateStrategyType(SkipStrategy.class)
    void onStartTimer();
    @StateStrategyType(SkipStrategy.class)
    void onPauseTimer();
    @StateStrategyType(SkipStrategy.class)
    void showLastSets(List<Set> sets);
    @StateStrategyType(SkipStrategy.class)
    void openSetsScreen(final UUID exerciseUuid, boolean withWeight);
    @StateStrategyType(SkipStrategy.class)
    void openEditExercise(UUID mExerciseUuid);
    @StateStrategyType(SkipStrategy.class)
    void setCurrentSetNumber(int setNumber);
    @StateStrategyType(SkipStrategy.class)
    void openExerciseDetail(UUID uuid);
    @StateStrategyType(SkipStrategy.class)
    void initPickerSetter(int set, int pickerValue1, int pickerValue2);
    @StateStrategyType(SkipStrategy.class)
    void clearLastSets();
    @StateStrategyType(SkipStrategy.class)
    void setCountSetsToday(int integer);
    @StateStrategyType(SkipStrategy.class)
    void enableTrackActivity();
    @StateStrategyType(SkipStrategy.class)
    void disableTrackActivity();
    @StateStrategyType(SkipStrategy.class)
    void showTrackActivityConfirm(String setValueString);
}
