package com.alphabit.sportnotes.ui.screens.sets.view;

import com.alphabit.sportnotes.ui.screens.base.view.BaseMvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import sportnotes.alphabit.com.business.model.Set;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public interface ISetsMvpView extends BaseMvpView {
    @StateStrategyType(SkipStrategy.class)
    void showSets(List<Set> sets);
    @StateStrategyType(SkipStrategy.class)
    void onDeletedSetItem(Set set);
    @StateStrategyType(SkipStrategy.class)
    void setWeightUnit(String first);
    @StateStrategyType(SkipStrategy.class)
    void addLastSets(List<Set> sets);
    @StateStrategyType(SkipStrategy.class)
    void addSet(List<Set> sets);
}
