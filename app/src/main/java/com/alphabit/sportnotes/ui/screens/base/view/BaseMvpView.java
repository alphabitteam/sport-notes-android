package com.alphabit.sportnotes.ui.screens.base.view;

import android.support.annotation.StringRes;

import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.UUID;

/**
 * Created by Fly on 27.08.2017.
 */
@StateStrategyType(SkipStrategy.class)
public interface BaseMvpView extends MvpView {
    @StateStrategyType(SkipStrategy.class)
    void showNotifyMessage(@StringRes int stringRes);
    @StateStrategyType(SkipStrategy.class)
    void showNotifyMessage(String string);
    @StateStrategyType(SkipStrategy.class)
    void setTitle(@StringRes int titleRes);
    @StateStrategyType(SkipStrategy.class)
    void showLoadingIndicator();

    void hideLoadingIndicator();
    @StateStrategyType(SkipStrategy.class)
    void showUserLevelError();
    @StateStrategyType(SkipStrategy.class)
    void refreshAdState();
}
