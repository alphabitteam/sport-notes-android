package com.alphabit.sportnotes.ui.screens.sets.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.databinding.ActivitySetsBinding;
import com.alphabit.sportnotes.ui.adapter.CompositeAdapter;
import com.alphabit.sportnotes.ui.screens.base.BaseMvpActivity;
import com.alphabit.sportnotes.ui.screens.sets.presenter.SetsPresenter;
import com.alphabit.sportnotes.view.DatePrinter;
import com.alphabit.sportnotes.view.MarginItemDecorator;
import com.alphabit.sportnotes.view.date.DatePrintableImpl;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import org.joda.time.format.DateTimeFormat;

import java.util.List;
import java.util.UUID;

import sportnotes.alphabit.com.business.model.Set;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */
@Layout(id = R.layout.activity_sets)
public class SetsActivity extends BaseMvpActivity<ActivitySetsBinding>
        implements ISetsMvpView, SetRenderer.OnSetsListener, SetRenderer.WeightUnitListener {

    public static final String UUID = "uuid";
    public static final String WEIGHT_UNIT = "weight_unit";

    private CompositeAdapter adapter;

    @InjectPresenter
    SetsPresenter mPresenter;
    private SetRenderer.WeightUnitSetListener weightUnitSetListener;
    private SetRenderer setRenderer;

    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    private DatePrinter datePrinter;

    @Override
    protected void initView() {
        datePrinter = new DatePrinter(new DatePrintableImpl(getResources()),
                DateTimeFormat.forPattern("dd.MM.yyyy"));

        mPresenter.setArguments((UUID) getIntent().getSerializableExtra(UUID),
                getIntent().getBooleanExtra(WEIGHT_UNIT, false));
        getBinding().recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        getBinding().recycler.addItemDecoration(new MarginItemDecorator(getResources().getDimension(R.dimen.item_margin)));

        viewBinderHelper.setOpenOnlyOne(true);

        setRenderer = new SetRenderer(this, viewBinderHelper, this, datePrinter);
        adapter = new CompositeAdapter.Builder<>()
                .add(setRenderer)
                .build();
        adapter.setOnLazyListener(mPresenter::onGetLastItem);
        getBinding().recycler.setAdapter(adapter);
    }

    public static void start(final Context context, final UUID exerciseUuid, boolean withWeight) {
        Intent intent = new Intent(context, SetsActivity.class);
        intent.putExtra(UUID, exerciseUuid);
        intent.putExtra(WEIGHT_UNIT, withWeight);
        context.startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fragment_sets, menu);
        return true;
    }

    @Override
    public void showSets(final List<Set> sets) {
        adapter.setData(sets);
    }

    @Override
    public void onDeletedSetItem(Set set) {
        adapter.deleteItem(set);
    }

    @Override
    public void setWeightUnit(String unit) {
        if (setRenderer != null)
            setRenderer.setWeightUnit(unit);
        if (weightUnitSetListener != null)
            weightUnitSetListener.setWeightUnit(unit);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void addLastSets(List<Set> sets) {
        adapter.addLastData(sets);
    }

    @Override
    public void addSet(List<Set> sets) {
        adapter.addData(sets);
    }

    @Override
    public void onDeleteSet(Set set) {
        mPresenter.onDeleteSet(set);
    }

    @Override
    public void weightUnitListener(SetRenderer.WeightUnitSetListener listener) {
        weightUnitSetListener = listener;
    }
}
