package com.alphabit.sportnotes.ui.screens.editexercise.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.StringRes;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.databinding.ActivityEditExerciseBinding;
import com.alphabit.sportnotes.ui.screens.base.BaseActivity;
import com.alphabit.sportnotes.ui.screens.base.BaseMvpActivity;

import java.util.UUID;

import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.WeekDay;

/**
 * Created by Fly on 11.09.2017.
 */
@Layout(id = R.layout.activity_edit_exercise)
public class EditExerciseActivity extends BaseActivity<ActivityEditExerciseBinding> {

    public static final String WEEKDAY = "weekday";
    public static final String COMPLEX_UUID = "complex_uuid";
    public static final String EXERCISE_UUID = "exercise_uuid";

    @Override
    protected void initView() {
        final WeekDay weekDay = (WeekDay) getIntent().getSerializableExtra(WEEKDAY);
        final UUID complexUuid = (UUID) getIntent().getSerializableExtra(COMPLEX_UUID);
        final UUID exerciseUuid = (UUID) getIntent().getSerializableExtra(EXERCISE_UUID);
        if (exerciseUuid != null)
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, EditExerciseFragment.newInstanceEdit(exerciseUuid))
                    .commitAllowingStateLoss();
        else if (weekDay != null && complexUuid != null)
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, EditExerciseFragment.newInstanceCreate(weekDay, complexUuid))
                    .commitAllowingStateLoss();
        else finish();
    }

    public static final void startCreate(Context context, WeekDay weekDay, UUID complexUuid) {
        Intent intent = new Intent(context, EditExerciseActivity.class);
        intent.putExtra(WEEKDAY, weekDay);
        intent.putExtra(COMPLEX_UUID, complexUuid);
        context.startActivity(intent);
    }

    public static void startEdit(Context context, UUID exerciseUuid) {
        Intent intent = new Intent(context, EditExerciseActivity.class);
        intent.putExtra(EXERCISE_UUID, exerciseUuid);
        context.startActivity(intent);
    }
}
