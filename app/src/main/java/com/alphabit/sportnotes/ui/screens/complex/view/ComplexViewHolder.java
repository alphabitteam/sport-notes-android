package com.alphabit.sportnotes.ui.screens.complex.view;

import android.graphics.Color;
import android.view.View;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.databinding.ItemComplexBinding;
import com.alphabit.sportnotes.ui.adapter.BaseHolder;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import sportnotes.alphabit.com.business.model.Complex;

/**
 * Created by Fly on 02.09.2017.
 */
@Layout(id = R.layout.item_complex)
public class ComplexViewHolder extends BaseHolder<Complex, ItemComplexBinding> {

    private ComplexRenderer.OnItemComplexListener mOnItemComplexListener;
    private ViewBinderHelper viewBinderHelper;

    public ComplexViewHolder(final View itemView) {
        super(itemView);
    }

    @Override
    public void bind(final Complex complex) {
        viewBinderHelper.bind(getBinding().swipe, String.valueOf(complex.getUuid()));
        getBinding().text.setText(complex.getName());

        if (getSelectedItem() instanceof Complex && complex.getUuid().equals(((Complex) getSelectedItem()).getUuid())) {
            getBinding().deleteLayout.delete.setVisibility(View.GONE);
            getBinding().itemLayout.setBackgroundColor(Color.YELLOW);
        } else {
            getBinding().deleteLayout.delete.setVisibility(View.VISIBLE);
            getBinding().itemLayout.setBackgroundColor(Color.WHITE);
        }

        getBinding().deleteLayout.delete.setOnClickListener(view ->
                mOnItemComplexListener.onClickDeleteComplex(getAdapterPosition(), complex));
        getBinding().editLayout.edit.setOnClickListener(view ->
                mOnItemComplexListener.onClickEditComplex(complex));
        getBinding().itemLayout.setOnClickListener(view ->
                mOnItemComplexListener.onSelectComplex(complex));
    }

    public void setOnItemComplexListener(final ComplexRenderer.OnItemComplexListener onItemComplexListener) {
        mOnItemComplexListener = onItemComplexListener;
    }

    public void setViewBinderHelper(ViewBinderHelper viewBinderHelper) {
        this.viewBinderHelper = viewBinderHelper;
    }

}
