package com.alphabit.sportnotes.ui.screens.aboutpremium.view;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.databinding.DialogPremiumBinding;
import com.alphabit.sportnotes.ui.screens.aboutpremium.presenter.AboutPremiumPresenter;
import com.alphabit.sportnotes.ui.screens.base.BaseMvpDialogFragment;
import com.alphabit.sportnotes.ui.screens.base.Loginable;
import com.alphabit.sportnotes.ui.screens.complex.view.BillingManagerKeeper;
import com.alphabit.sportnotes.ui.screens.complex.view.ComplexActivity;
import com.alphabit.sportnotes.view.customdialog.CustomDialog;
import com.arellomobile.mvp.MvpAppCompatDialogFragment;
import com.arellomobile.mvp.MvpDialogFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

/**
 * Created by fly12 on 19.11.2017.
 */

public class AboutPremiumDialog extends BaseMvpDialogFragment implements AboutPremiumView {

    @InjectPresenter
    AboutPremiumPresenter presenter;

    private DialogPremiumBinding binding;

    @ProvidePresenter
    protected AboutPremiumPresenter provideAboutPremiumPresenter() {
        return new AboutPremiumPresenter(((BillingManagerKeeper)getActivity()).getBillingManager());
    }

    public static AboutPremiumDialog newInstance() {

        Bundle args = new Bundle();

        AboutPremiumDialog fragment = new AboutPremiumDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        binding = DataBindingUtil
                .bind(LayoutInflater.from(getContext()).inflate(R.layout.dialog_premium, null));
        AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
        binding.buy.setOnClickListener(view -> presenter.onClickBuy());
        adb.setView(binding.getRoot());
        return adb.create();
    }

    @Override
    public void showNotifyMessage(int stringRes) {
        Toast.makeText(getContext(), stringRes, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNotifyMessage(String string) {
        Toast.makeText(getContext(), string, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setTitle(int titleRes) {

    }

    @Override
    public void enableBuy() {
        binding.buy.setEnabled(true);
    }

    @Override
    public void disableBuy() {
        binding.buy.setEnabled(false);
    }

    @Override
    public void showBillingError() {

    }

    @Override
    public void showBillingConnecting() {
        binding.buy.setText(getString(R.string.connecting_for_billing));
    }

    @Override
    public void showBillingConnected() {
        binding.buy.setText(getString(R.string.take));
    }

    @Override
    public void requestLogin() {
        ((Loginable)getActivity()).requestLogin();
    }

    @Override
    public void showPurchased() {
        dismiss();
    }

    @Override
    public void sync() {
        if (getActivity() instanceof ComplexActivity)
            ((ComplexActivity) getActivity()).sync();
    }
}
