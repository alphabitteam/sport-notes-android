package com.alphabit.sportnotes.ui.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import sportnotes.alphabit.com.business.model.UuidModel;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 31.01.18.
 */
public abstract class BaseRenderer<VH extends BaseHolder, T extends UuidModel> implements Renderer<VH,T> {

    private final int POOL_SIZE = 10;

    private final RecyclerView.RecycledViewPool recycledViewPool;

    public BaseRenderer() {
        this.recycledViewPool = new RecyclerView.RecycledViewPool();
    }

    /**
     * Предназначен для вложенных списков, применяет к вложенному списку операции оптимизации
     * @param recyclerView - список к которому будут применены операции
     */
    protected final void prepareRecyclerView(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager) {
        recyclerView.setHasFixedSize(true);
        linearLayoutManager.setInitialPrefetchItemCount(POOL_SIZE);
        recycledViewPool.setMaxRecycledViews(0, POOL_SIZE);
        recyclerView.setRecycledViewPool(recycledViewPool);
    }
}
