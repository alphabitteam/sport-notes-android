package com.alphabit.sportnotes.ui.screens.sets.presenter;

import android.support.v4.util.Pair;
import android.util.Log;

import com.alphabit.sportnotes.App;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.alphabit.sportnotes.ui.screens.sets.view.ISetsMvpView;
import com.arellomobile.mvp.InjectViewState;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.functions.BiFunction;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.usecase.contract.ISetUseCase;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */
@InjectViewState
public class SetsPresenter extends BasePresenter<ISetsMvpView> {

    private UUID mExerciseUuid;
    private boolean withWeight;

    @Inject
    ISetUseCase mUseCase;

    public SetsPresenter() {
        App.getAppComponent().inject(this);
    }

    public void setArguments(final UUID arguments, boolean withWeight) {
        mExerciseUuid = arguments;
        this.withWeight = withWeight;
        Log.d(getClass().getSimpleName(), "setArguments: " + arguments);
    }

    @Override
    public void attachView(ISetsMvpView view) {
        super.attachView(view);
        updateSets();
    }

    private void updateSets() {
        addDisposable(scheduling(Flowable.zip(mUseCase.getWeightUnit(),
                onGetLastItemObs(0), Pair::new)).subscribe(pair -> {
            if (withWeight)
                getViewState().setWeightUnit(pair.first);
            getViewState().showSets(pair.second);
        }));
    }

    @Override
    protected void showThrowable(final Throwable throwable) {
        super.showThrowable(throwable);
        Log.d(getClass().getSimpleName(), "showThrowable: " + throwable);
    }

    public void onDeleteSet(Set set) {
       addDisposable(scheduling(mUseCase.deleteSet(set))
               .subscribe(() -> getViewState().onDeletedSetItem(set)));
    }

    public void onGetLastItem(int position) {
        addDisposable(onGetLastItemObs(position)
                .subscribe(this::addSets,
                        throwable -> showNotifyMessage(throwable, R.string.get_exercise_failed)));
    }

    public Flowable<List<Set>> onGetLastItemObs(final int lastPosition) {
        return scheduling(mUseCase.getSets(mExerciseUuid, lastPosition))
                .take(1);
    }

    private void addSets(List<Set> sets) {
        if (sets.size() == 0)
            getViewState().addLastSets(sets);
        else getViewState().addSet(sets);
    }
}
