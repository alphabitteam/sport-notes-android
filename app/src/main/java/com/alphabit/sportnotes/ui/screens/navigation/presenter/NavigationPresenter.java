package com.alphabit.sportnotes.ui.screens.navigation.presenter;

import android.util.Log;

import com.alphabit.sportnotes.App;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.alphabit.sportnotes.ui.screens.navigation.view.INavigationView;
import com.alphabit.sportnotes.utils.billing.BillingManager;
import com.arellomobile.mvp.InjectViewState;
import com.google.gson.JsonSyntaxException;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import sportnotes.alphabit.com.business.exceptions.UserLevelException;
import sportnotes.alphabit.com.business.model.Provider;
import sportnotes.alphabit.com.business.usecase.contract.IAuthorizationUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISynchronizationUseCase;
import sportnotes.alphabit.com.data.utils.UserLevelManager;

/**
 * Created by Fly on 08.09.2017.
 */
@InjectViewState
public class NavigationPresenter extends BasePresenter<INavigationView> {

    @Inject
    ISynchronizationUseCase useCase;
    @Inject
    IAuthorizationUseCase authorizationUseCase;
    @Inject
    UserLevelManager userLevelManager;

    private BillingManager billingManager;

    public NavigationPresenter() {
        App.getAppComponent().inject(this);
    }

    public void setBillingManager(BillingManager billingManager) {
        this.billingManager = billingManager;
        Log.d(getClass().getSimpleName(), "setBillingManager: " + billingManager);
    }
    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    @Override
    public void attachView(INavigationView view) {
        super.attachView(view);
        getUser();
        Log.d(getClass().getSimpleName(), "attachView: ");
        addDisposable(billingManager.getUpdateEvents().subscribe(aBoolean -> updatePayEnable()));
        updatePayEnable();
        addDisposable(scheduling(useCase.getSyncState()).subscribe(syncState -> {
            switch (syncState) {
                case SYNCHRONIZED:
                    getViewState().showStateSync();
                    break;
                case NOT_SYNCHRONIZED:
                    getViewState().showStateNotSync();
                    break;
            }
        }, throwable -> {}));
    }

    @Override
    public void detachView(INavigationView view) {
        super.detachView(view);
        Log.d(getClass().getSimpleName(), "detachView: ");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        syncIsProcess = false;
        Log.d(getClass().getSimpleName(), "onDestroy: ");
    }

    @Override
    protected void clearObserve() {
        super.clearObserve();
        syncIsProcess = false;
    }

    private void updatePayEnable() {
        //сделать пропадание/появление кнопки "премиум"
        //сделать возможность подписки только авторизованным юзерам,
        //если юзер не авторизован, при попытки купить подписку появляется предложение авторизоваться
        if (userLevelManager.isFullVersion()) {
            getViewState().hidePay();
            getViewState().showPremiumStatus();
        } else {
            getViewState().showPay();
            getViewState().showFreemiumStatus();
        }
    }

    private void getUser() {
        addDisposable(scheduling(authorizationUseCase.getUser())
                .subscribe(userOptional -> {
                    if (userOptional.isPresent()) {
                        getViewState().showUser();
                        getViewState().bindUser(userOptional.get());
                    } else {
                        getViewState().showLogin();
                    }
                }, throwable -> {}));
    }

    public void onClickLoginG() {
        getViewState().showLoginProcess();
        Log.d(getClass().getSimpleName(), "onClickLoginG: ");
        getViewState().authorizationGooglePlus();
    }

    public void onGoogleTokenReceived(final String token) {
        Log.d(getClass().getSimpleName(), "onGoogleTokenReceived: " + token);
        addDisposable(scheduling(authorizationUseCase.login(Provider.G, token)
                .doOnComplete(this::getUser)
                .andThen(useCase.synchronize())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof UserLevelException)
                        return Completable.complete();
                    else return Completable.error(throwable);
                })
                .doOnSubscribe(disposable1 -> getViewState().showLoadingIndicator())
                .doOnTerminate(() -> getViewState().hideLoadingIndicator()))
                .doOnComplete(this::updateBilling)
                .subscribe(() -> {
                            updatePayEnable();
                            getViewState().hideLoginProcess();
                        }
                        , throwable -> {
                            if (!userLevelManager.hasAuth())
                                return;
                            updatePayEnable();
                            getViewState().hideLoginProcess();
                            getViewState().showLogin();
                            getViewState().showNotifyMessage(throwable.getMessage());
                        }
                ));
    }

    private void updateBilling() {
        billingManager.restart();
    }

    public void onGoogleTokenReceivedError(final Exception error) {
        getViewState().hideLoginProcess();
        Log.d(getClass().getSimpleName(), "onGoogleTokenReceivedError: " + error);
        getViewState().showNotifyMessage(error.getMessage());
    }

    private Disposable disposable;
    private boolean syncIsProcess = false;
    public void onClickSync() {
        if (syncIsProcess)
            return;
        syncIsProcess = true;
        getViewState().showSyncProcess();
        disposable = scheduling(useCase.synchronize()
                .doOnSubscribe(disposable1 -> getViewState().showLoadingIndicator())
                .doOnTerminate(() -> getViewState().hideLoadingIndicator()))
                .subscribe(() -> {
                    syncIsProcess = false;
                    getViewState().hideSyncProcess();
                    if (disposable != null)
                        disposable.dispose();
                }, throwable -> {
                    syncIsProcess = false;
                    if (!userLevelManager.hasAuth())
                        return;
                    getViewState().hideSyncProcess();
                    if (throwable instanceof JsonSyntaxException) {
                        getViewState().authorizationGooglePlus();
                        return;
                    }

                    if (throwable instanceof UserLevelException) {
                        return;
                    }

                    getViewState().showNotifyMessage(throwable.getMessage());
                });
    }

    public void onClickBuy() {
        System.out.println("NavigationFragment.onClickBuy presenter");
        getViewState().showAboutPremiumDialog();
    }

    public void onClickLogout() {
        addDisposable(scheduling(authorizationUseCase.logout())
                .doOnComplete(this::updateBilling)
                .subscribe(() -> {
                    if (disposable != null)
                        disposable.dispose();
                    clearObserve();
                    getViewState().showLogin();
                    updatePayEnable();
                }, throwable -> {
                    updatePayEnable();
                    getViewState().showUser();
                    getViewState().showNotifyMessage(throwable.getMessage());
                }));
    }

    public void onClickSendLog() {
        getViewState().showSendLogDialog();
    }

    public void onClickSettings() {
        getViewState().showSettingScreen();
    }

    public void onClickAboutUs() {
        getViewState().showAboutUsScreen();
    }
}
