package com.alphabit.sportnotes.ui.screens.complex.view;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.ui.adapter.BaseRenderer;
import com.alphabit.sportnotes.ui.adapter.BaseHolder;

import java.util.List;

import sportnotes.alphabit.com.business.model.UuidModel;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 21.04.18.
 */
public class AddComplexRenderer extends BaseRenderer<AddComplexViewHolder, UuidModel> {

    private final OnNewComplexListener onNewComplexListener;

    public AddComplexRenderer(OnNewComplexListener onNewComplexListener) {
        this.onNewComplexListener = onNewComplexListener;
    }

    @NonNull
    @Override
    public BaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AddComplexViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_add_complex, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AddComplexViewHolder holder, @NonNull List<UuidModel> items,
                                 int position) {
        holder.setOnNewComplexListener(onNewComplexListener);
        holder.bind(null);
    }

    @Override
    public void onRecycled(AddComplexViewHolder holder) {

    }

    @Override
    public boolean isForViewType(@NonNull List<?> items, int position) {
        return false;
    }

    public interface OnNewComplexListener {
        void onClickNewComplex();
    }
}
