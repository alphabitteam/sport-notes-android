package com.alphabit.sportnotes.ui.screens.navigation.view;

import com.alphabit.sportnotes.ui.screens.base.view.BaseMvpView;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import sportnotes.alphabit.com.business.model.User;

/**
 * Created by Fly on 08.09.2017.
 */
public interface INavigationView extends BaseMvpView {
    @StateStrategyType(SkipStrategy.class)
    void authorizationGooglePlus();
    @StateStrategyType(SkipStrategy.class)
    void showAboutPremiumDialog();

    void bindUser(User user);

    void showUser();

    void showLogin();
    @StateStrategyType(SkipStrategy.class)
    void showLoginProcess();
    @StateStrategyType(SkipStrategy.class)
    void hideLoginProcess();
    @StateStrategyType(SkipStrategy.class)
    void showSyncProcess();
    @StateStrategyType(SkipStrategy.class)
    void hideSyncProcess();
    @StateStrategyType(SkipStrategy.class)
    void hidePay();
    @StateStrategyType(SkipStrategy.class)
    void showPay();
    @StateStrategyType(SkipStrategy.class)
    void showPremiumStatus();
    @StateStrategyType(SkipStrategy.class)
    void showFreemiumStatus();
    @StateStrategyType(SkipStrategy.class)
    void showSendLogDialog();
    @StateStrategyType(SkipStrategy.class)
    void showSettingScreen();
    @StateStrategyType(SkipStrategy.class)
    void showAboutUsScreen();

    void showStateSync();

    void showStateNotSync();
}
