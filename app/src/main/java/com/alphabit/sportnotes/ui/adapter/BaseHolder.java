package com.alphabit.sportnotes.ui.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.alphabit.sportnotes.annotations.Layout;

import java.lang.annotation.Annotation;

import butterknife.ButterKnife;

/**
 * Created by Fly on 02.09.2017.
 */

public abstract class BaseHolder<T, Binding extends ViewDataBinding> extends RecyclerView.ViewHolder {

    private Binding mBinding;
    private Object mSelectedItem;


    public BaseHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mBinding = DataBindingUtil.bind(itemView);
    }

    public Binding getBinding() {
        return mBinding;
    }

    protected void setBinding(final Binding binding) {
        mBinding = binding;
    }

    public abstract void bind(T item);

    public void setSelectedItem(final Object selectedItem) {
        mSelectedItem = selectedItem;
    }

    public Object getSelectedItem() {
        return mSelectedItem;
    }
}
