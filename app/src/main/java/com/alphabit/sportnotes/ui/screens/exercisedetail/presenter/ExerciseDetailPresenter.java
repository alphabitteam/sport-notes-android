package com.alphabit.sportnotes.ui.screens.exercisedetail.presenter;

import android.content.Context;
import android.os.Handler;
import android.support.v4.util.Pair;
import android.util.Log;

import com.alphabit.sportnotes.App;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.timer.ExerciseTimer;
import com.alphabit.sportnotes.timer.RenewableTimer;
import com.alphabit.sportnotes.timer.TimerPrinter;
import com.alphabit.sportnotes.ui.Router;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.alphabit.sportnotes.ui.screens.exercisedetail.view.IExerciseDetailView;
import com.alphabit.sportnotes.utils.OptionalUtils;
import com.alphabit.sportnotes.view.DatePrinter;
import com.annimon.stream.Optional;
import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import sportnotes.alphabit.com.business.Serializer;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.ExerciseSum;
import sportnotes.alphabit.com.business.model.ExerciseTime;
import sportnotes.alphabit.com.business.model.RepeatType;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.SetSum;
import sportnotes.alphabit.com.business.model.SetTime;
import sportnotes.alphabit.com.business.model.TimerTask;
import sportnotes.alphabit.com.business.model.ValueType;
import sportnotes.alphabit.com.business.usecase.contract.IExerciseDetailUseCase;

/**
 * Created by Yuri Zigunov on 02.10.2017.
 */
@InjectViewState
public class ExerciseDetailPresenter extends BasePresenter<IExerciseDetailView> implements RenewableTimer.OnTimerListener {
    private UUID mExerciseUuid;
    private Exercise mExercise;
    private String weightUnit;

    private final Handler handler = new Handler();

    @Inject
    IExerciseDetailUseCase mUseCase;
    @Inject
    Context mContext;


    private ExerciseTimer mTimer;
    private int mPickerValue1;
    private int mPickerValue2;
    private long mMillis;
    private int mSetsPicker;
    private TimerTask timerTask;
    private long timerStartValue;
    private boolean withWeight;

    public ExerciseDetailPresenter() {
        App.getAppComponent().inject(this);
    }

    public void setArguments(final UUID exerciseUuid) {
        mExerciseUuid = exerciseUuid;
    }

    @Override
    public void attachView(final IExerciseDetailView view) {
        super.attachView(view);

        //addDisposable(scheduling(mUseCase.getWeightUnit()).subscribe(this::onGetWeightUnit, throwable -> {}));

        getViewState().showPickerSetter();
        getViewState().hideTimerNotify();

        getViewState().setTitle(R.string.menu_exercise_detail);

        addDisposable(scheduling(mUseCase.getTimerTask()).subscribe(this::onGetTimerTask, throwable -> {}));
        //addDisposable(scheduling(mUseCase.getExercise(mExerciseUuid))
        //        .subscribe(this::onGetExercise,
        //                throwable -> showNotifyMessage(throwable, R.string.load_exercise_failed)));

        addDisposable(scheduling(Flowable.combineLatest(mUseCase.getExercise(mExerciseUuid), mUseCase.getWeightUnit(), Pair::new))
                .subscribe(exerciseStringPair -> {
                    weightUnit = exerciseStringPair.second;
                    computeWithWeight(exerciseStringPair.first);
                    if (withWeight)
                        getViewState().applyWeightUnit(weightUnit);
                    else getViewState().applyWeightUnit("");
                    onGetExercise(exerciseStringPair.first);
                }, throwable -> showNotifyMessage(throwable, R.string.load_exercise_failed)));

        getLastSets();
    }

    private void computeWithWeight(Exercise exercise) {
        withWeight = false;
        if (exercise instanceof ExerciseSum) {
            if (((ExerciseSum) exercise).getSetsValue() != 0) {
                withWeight = true;
            }
        }
    }

    @Override
    public void detachView(final IExerciseDetailView view) {
        Log.d(getClass().getSimpleName(), "detachView: ");
        if (mTimer != null) {
            if (mTimer.isActive()) {
                saveTimerTask();
            } else {
                removeTimerTask();
            }
        }
        super.detachView(view);
    }

    protected void saveTimerTask() {
        TimerTask timerTask = new TimerTask(timerStartValue, calculateTimerTaskMillis(), !mTimer.isPause(), mExerciseUuid);
        saveTimerTask(timerTask);
    }

    private long calculateTimerTaskMillis() {
        if (mTimer.isPause())
            return mTimer.getMillis();
        else
            return System.currentTimeMillis() + mTimer.getMillis();
    }

    private void onGetTimerTask(final Optional<TimerTask> tt) {
        this.timerTask = OptionalUtils.getObject(tt);
        Log.d(getClass().getSimpleName(), "onGetTimerTask: " + new Gson().toJson(timerTask));
        if (tt.isPresent()) {

            timerStartValue = timerTask.getStartValue();
            initTimerStartValue();
            if (!isActualTimerTask(timerTask)) {
                removeTimerTask();
                return;
            }

            mMillis = timerTask.getMillis();
            if (mExerciseUuid.equals(timerTask.getUuid())) {
                if (timerTask.isRun())
                    startTimer(timerTask);
                else {
                    getViewState().printTimerTime(TimerPrinter.printWithMillis(timerTask.getMillis()));
                    getViewState().showTimer();
                    getViewState().onPauseTimer();
                }
            } else {
                getViewState().showTimerNotify();
            }
        } else {
            getViewState().showPickerSetter();
            getViewState().hideTimerNotify();
            getViewState().onPauseTimer();
        }
    }

    private void initTimerStartValue() {
        DateTime dateTime = new DateTime(timerStartValue).withZone(DateTimeZone.UTC);
        getViewState().initPickerSetter(1, dateTime.getMinuteOfDay(), dateTime.getSecondOfMinute());
    }

    private boolean isActualTimerTask(final TimerTask timerTask) {
        if (!timerTask.isRun())
            return timerTask.getMillis() > 0;
        else {
            final long millis = timerTask.getMillis();
            final long currentMillis = System.currentTimeMillis();
            return millis > currentMillis;
        }
    }

    private void onGetExercise(final Exercise exercise) {
        mExercise = exercise;

        if (exercise instanceof ExerciseSum) {
            if (withWeight) {
                buildViewSumWithWeightsExercise(((ExerciseSum) exercise));
            } else {
                buildViewSumWithoutWeightsExercise((ExerciseSum) exercise);
            }
        } else if (exercise instanceof ExerciseTime) {
            buildViewTimeExercise((ExerciseTime) exercise);
        }
    }

    private void buildViewSumWithWeightsExercise(ExerciseSum exercise) {
        getViewState().buildViewSumWithWeightsExercise(exercise);
    }

    private void buildViewSumWithoutWeightsExercise(ExerciseSum exercise) {
        getViewState().buildViewSumWithoutWeightsExercise(exercise);
    }

    private void buildViewTimeExercise(ExerciseTime exercise) {
        getViewState().buildViewTimeExercise(exercise);
    }

    @Override
    public void onTick(final long millis) {
        getViewState().printTimerTime(TimerPrinter.printWithMillis(millis));
    }

    @Override
    public void onFinish() {
        getViewState().showPickerSetter();
        getViewState().onPauseTimer();
        removeTimerTask();
    }

    public void onClickStartTimer() {
        if (mTimer != null && mTimer.isPause()) {
            mTimer.start();
        } else {
            long millis;

            if (mMillis > 0)
                millis = mMillis;
            else millis = calculateMillis(mPickerValue1, mPickerValue2);

            if (millis < 999) {
                onFinish();
                return;
            }

            if (timerTask == null)
                timerStartValue = millis;

            mTimer = new ExerciseTimer(mContext, mExerciseUuid, millis ,this);
            mTimer.start();
        }
        getViewState().showTimer();
    }

    private long calculateMillis(int minutes, int seconds) {
        return ((minutes * 60) + seconds) * 1000;
    }

    private void startTimer(TimerTask timerTask) {
        mTimer = new ExerciseTimer(mContext, mExerciseUuid, timerTask ,this);
        mTimer.start();
        getViewState().showTimer();
        getViewState().onStartTimer();
        saveTimerTask();
    }

    public void onClickCancelTimer() {
        mMillis = 0;
        if (mTimer != null)
            mTimer.cancel();
        getViewState().showPickerSetter();
        getViewState().onPauseTimer();
        removeTimerTask();
    }

    public void onClickPauseTimer() {
        if (mTimer != null)
            mTimer.pause();
    }

    public void onNumberPicker1Changed(int newVal) {
        mPickerValue1 = newVal;
    }

    public void onNumberPicker2Changed(int newVal) {
        mPickerValue2 = newVal;
    }

    private String getSetValueString() {
        String result = "";
        switch (mExercise.getRepeatType()) {
            case SUM:
                ValueType valueType = ((ExerciseSum) mExercise).getSetsValue() != 0 ? ValueType.WEIGHT : ValueType.COUNT;
                if (valueType == ValueType.WEIGHT) {
                    result = mPickerValue2 + " " + weightUnit;
                } else {
                    result = String.valueOf(mPickerValue1);
                }
                break;
            case TIME:
                result = TimerPrinter.printWithoutMillis(getTimeLeft());
                break;
        }
        return result;
    }

    private long getTimeLeft() {
        long timerTime;
        long resultTime;
        if (mTimer != null && mTimer.isActive()) {
            timerTime = mTimer.getMillis();
        } else {
            timerTime = calculateMillis(mPickerValue1, mPickerValue2);
        }

        if (mTimer != null && mTimer.isActive()) {
            resultTime = mTimer.getInitMillis() - timerTime;
        } else {
            resultTime = timerTime;
        }
        return resultTime;
    }

    public void onClickTrackActivity() {
        getViewState().showTrackActivityConfirm(getSetValueString());
    }

    public void onTrackActivityConfirmed() {
        getViewState().disableTrackActivity();
        Set set = null;

        switch (mExercise.getRepeatType()) {
            case SUM:
                ValueType valueType = ((ExerciseSum) mExercise).getSetsValue() != 0 ? ValueType.WEIGHT : ValueType.COUNT;
                final SetSum setSum = new SetSum(valueType);
                if (valueType == ValueType.WEIGHT) {
                    setSum.setValue(mPickerValue2);
                } else {
                    setSum.setValue(mPickerValue1);
                }
                set = setSum;
                break;
            case TIME:
                final SetTime setTime = new SetTime();
                setTime.setValue(getTimeLeft());
                set = setTime;
                break;
        }

        set.setExerciseUuid(mExerciseUuid);
        set.setRepeat(mPickerValue1);
        set.setNumber(mSetsPicker);
        System.out.println("ExerciseDetailPresenter.onClickTrackActivity = " + mSetsPicker);

        mSetsPicker++;
        getViewState().setCurrentSetNumber(set.getNumber() + 1);
        addDisposable(scheduling(mUseCase.saveSet(set)).subscribe(this::getLastSets, this::showThrowable));

        onClickCancelTimer();
        handler.postDelayed(() -> getViewState().enableTrackActivity(), 500);
    }

    @Override
    protected void showThrowable(final Throwable throwable) {
        super.showThrowable(throwable);
    }

    private void getLastSets() {
        addDisposable(scheduling(mUseCase.getLastSets(mExerciseUuid)).subscribe(this::showLastSets, this::showThrowable));
    }

    private void showLastSets(List<Set> sets) {
        getViewState().clearLastSets();
        if (sets.size() > 0) {
            Set set = sets.get(0);
            if (set instanceof SetTime) {
                timerStartValue = ((SetTime) set).getValue();
                initTimerStartValue();
            } else if (set instanceof SetSum) {
                int value = ((SetSum) set).getValue();
                int repeat = set.getRepeat();
                getViewState().initPickerSetter(1, repeat, value);
            }
        }

        getViewState().showLastSets(sets);
        addDisposable(scheduling(mUseCase.getCountSetsToday(mExerciseUuid)).subscribe(integer -> {
            if (mSetsPicker == 0 || mSetsPicker == 1)
                mSetsPicker = integer;
            //если пользователь пропустил один подход и не затрекал его,
            // то можем попасть в петлю устанавливая № подхода по количеству затреканых за сегодня
            getViewState().setCurrentSetNumber(mSetsPicker);
        }, throwable -> {
            if (mSetsPicker != 0 && mSetsPicker != 1)
                getViewState().setCurrentSetNumber(--mSetsPicker);
            showThrowable(throwable);
        }));
    }

    public void saveTimerTask(final TimerTask timerTask) {
        addDisposable(scheduling(mUseCase.saveTimerTask(timerTask)).subscribe(() -> {
        }, throwable -> {}));
    }

    public void removeTimerTask() {
        addDisposable(scheduling(mUseCase.saveTimerTask(null)).subscribe(() -> {
        }, throwable -> {}));
    }

    public void onSetsPickerChanged(final int newVal) {
        mSetsPicker = newVal;
    }

    public void onClickSets() {
        getViewState().openSetsScreen(mExerciseUuid, withWeight);
    }

    public void onClickEdit() {
        if (mTimer != null && (mTimer.isActive() || mTimer.isPause())) {
            getViewState().showNotifyMessage(R.string.finish_set);
            return;
        }
        getViewState().openEditExercise(mExerciseUuid);
    }

    public void onClickNotify() {
        if (timerTask != null) {
            getViewState().openExerciseDetail(timerTask.getUuid());
        }
    }

    public boolean equalsExerciseUuid(UUID exerciseUuid) {
        return mExerciseUuid.equals(exerciseUuid);
    }
}
