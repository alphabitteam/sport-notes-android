package com.alphabit.sportnotes.ui.screens.navigation.view;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.alphabit.logsender.LogSenderPublisher;
import com.alphabit.sportnotes.BuildConfig;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.databinding.FragmentNavigationBinding;
import com.alphabit.sportnotes.ui.screens.aboutpremium.view.AboutPremiumDialog;
import com.alphabit.sportnotes.ui.screens.aboutus.AboutUsActivity;
import com.alphabit.sportnotes.ui.screens.base.Loginable;
import com.alphabit.sportnotes.ui.screens.base.fragment.BaseFragment;
import com.alphabit.sportnotes.ui.screens.base.fragment.BaseMvpFragment;
import com.alphabit.sportnotes.ui.screens.complex.view.BillingManagerKeeper;
import com.alphabit.sportnotes.ui.screens.complex.view.ComplexMvpView;
import com.alphabit.sportnotes.ui.screens.navigation.presenter.NavigationPresenter;
import com.alphabit.sportnotes.ui.screens.settings.SettingsActivity;
import com.alphabit.sportnotes.view.customdialog.CustomDialogBuilder;
import com.alphabit.sportnotes.view.customdialog.SimpleCustomDialog;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.Objects;

import javax.inject.Inject;

import sportnotes.alphabit.com.business.model.User;
import sportnotes.alphabit.com.data.utils.UserLevelManager;

/**
 * Created by Fly on 08.09.2017.
 */
@Layout(id = R.layout.fragment_navigation)
public class NavigationFragment extends BaseMvpFragment<FragmentNavigationBinding> implements INavigationView, Loginable {

    private final static String G_PLUS_SCOPE =
            "oauth2:https://www.googleapis.com/auth/plus.login";
    private final static String USERINFO_SCOPE =
            "https://www.googleapis.com/auth/userinfo.email";
    private final static String EMAIL_SCOPE =
            "https://www.googleapis.com/auth/userinfo.profile";
    private final static String SCOPES_GOOGLE = G_PLUS_SCOPE + " " + USERINFO_SCOPE + " " + EMAIL_SCOPE;

    private int G_AUTH_RESULT = 982;

    @InjectPresenter
    NavigationPresenter mPresenter;

    @Override
    protected void initView() {
        getBinding().login.setOnClickListener(view -> onClickGooglePlusLogIn());
        getBinding().getRoot().setOnClickListener(view -> {});

        getBinding().sync.setOnClickListener(v -> sync());

        getBinding().about.setOnClickListener(v -> mPresenter.onClickAboutUs());

        getBinding().logcat.setOnClickListener(v -> mPresenter.onClickSendLog());

        if (BuildConfig.DEBUG) {
            getBinding().logcat.setVisibility(View.VISIBLE);
        } else {
            getBinding().logcat.setVisibility(View.GONE);
        }


        getBinding().buy.setOnClickListener(view -> mPresenter.onClickBuy());

        getBinding().logout.setOnClickListener(view -> mPresenter.onClickLogout());

        getBinding().settings.setOnClickListener(v -> mPresenter.onClickSettings());
    }

    public void sync() {
        mPresenter.onClickSync();
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.setBillingManager(((BillingManagerKeeper)getActivity()).getBillingManager());
    }

    private void onClickGooglePlusLogIn() {
        mPresenter.onClickLoginG();
    }

    @Override
    public void authorizationGooglePlus() {
        System.out.println("NavigationFragment.authorizationGooglePlus");
        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.GET_ACCOUNTS)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        rebuildGoogleApiClient();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        SimpleCustomDialog.create(getContext(), getString(R.string.warning), getString(R.string.no_permission_to_auth)).show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    protected synchronized void rebuildGoogleApiClient() {
        // When we build the GoogleApiClient we specify where connected and connection failed
        // callbacks should be returned, which Google APIs our app uses and which OAuth 2.0
        // scopes our app requests.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 10111);
    }

    @Override
    public void showLoadingIndicator() {
        super.showLoadingIndicator();
        ((ComplexMvpView)Objects.requireNonNull(getActivity())).showLoadingIndicator();
    }

    ProgressDialog dialog;

    @Override
    public void hideLoadingIndicator() {
        super.hideLoadingIndicator();
        ((ComplexMvpView)Objects.requireNonNull(getActivity())).hideLoadingIndicator();
    }

    @Override
    public void showAboutPremiumDialog() {
        System.out.println("NavigationFragment.showAboutPremiumDialog");
        AboutPremiumDialog dialog = AboutPremiumDialog.newInstance();
        dialog.show(getFragmentManager(), null);
    }

    @Override
    public void bindUser(User user) {
        Picasso.with(getContext()).load(user.getAvatarUrl()).placeholder(R.mipmap.avatar).into(getBinding().avatar);
        getBinding().headTitle.setText(user.getFirstname() + " " + user.getLastname());
        System.out.println("NavigationFragment.bindUser");
    }

    @Override
    public void showUser() {
        System.out.println("NavigationFragment.showUser");
        getBinding().headAccount.setVisibility(View.VISIBLE);
        getBinding().login.setVisibility(View.GONE);
        hideLoginProcess();
    }

    @Override
    public void showLogin() {
        System.out.println("NavigationFragment.showLogin");
        getBinding().headAccount.setVisibility(View.GONE);
        getBinding().login.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoginProcess() {
        System.out.println("NavigationFragment.showLoginProcess");
        getBinding().login.setVisibility(View.GONE);
        getBinding().loadingIndicator.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginProcess() {
        System.out.println("NavigationFragment.hideLoginProcess");
        if (getBinding().headAccount.getVisibility() == View.GONE)
            getBinding().login.setVisibility(View.VISIBLE);
        getBinding().loadingIndicator.setVisibility(View.GONE);
    }

    @Override
    public void showSyncProcess() {
        System.out.println("NavigationFragment.showSyncProcess");
        getBinding().accountStatus.setEnabled(false);
    }

    @Override
    public void hideSyncProcess() {
        System.out.println("NavigationFragment.hideSyncProcess");
        getBinding().accountStatus.setEnabled(true);
    }

    @Override
    public void hidePay() {
        System.out.println("NavigationFragment.hidePay");
        getBinding().buy.setVisibility(View.GONE);
    }

    @Override
    public void showPay() {
        System.out.println("NavigationFragment.showPay");
        getBinding().buy.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPremiumStatus() {
        System.out.println("NavigationFragment.showPremiumStatus");
        getBinding().accountStatus.setText(R.string.premium_account);
        getBinding().accountStatus.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.premium_account_status));
        getBinding().sync.setVisibility(View.VISIBLE);
        getBinding().buy.setVisibility(View.GONE);
    }

    @Override
    public void showFreemiumStatus() {
        System.out.println("NavigationFragment.showFreemiumStatus");
        getBinding().accountStatus.setText(R.string.free_account);
        getBinding().accountStatus.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.free_account_status));
        getBinding().sync.setVisibility(View.GONE);
        getBinding().buy.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSendLogDialog() {
        System.out.println("NavigationFragment.showSendLogDialog");
        LogSenderPublisher.showLogSenderDialog(getActivity());
    }

    @Override
    public void showSettingScreen() {
        System.out.println("NavigationFragment.showSettingScreen");
        startActivity(new Intent(getContext(), SettingsActivity.class));
    }

    @Override
    public void showAboutUsScreen() {
        System.out.println("NavigationFragment.showAboutUsScreen");
        startActivity(new Intent(getContext(), AboutUsActivity.class));
    }

    @Override
    public void showStateSync() {
        getBinding().syncStatus.setText(getString(R.string.synchronizzed));
    }

    @Override
    public void showStateNotSync() {
        getBinding().syncStatus.setText(getString(R.string.not_synchronizzed));
    }

    public void requestGoogleToken() {
        final AccountManager am = AccountManager.get(getContext());
        final Account[] accounts = am.getAccountsByType("com.google");
        Log.d(getClass().getSimpleName(), "requestGoogleToken: " + accounts.length);
        if (accounts.length > 1) {
            takeGoogleToken(am, accounts[0], true);
        } else if (accounts.length > 0) {
            takeGoogleToken(am, accounts[0], true);
        } else {
            SimpleCustomDialog.create(getActivity(), getString(R.string.error), getString(R.string.no_google_account_for_auth)).show();
        }
    }

    private void takeGoogleToken(AccountManager am, Account account, boolean needUpdate) {
        am.getAuthToken(account, SCOPES_GOOGLE, new Bundle(), getActivity(), accountManagerFuture -> {
            Exception error = null;
            Bundle bundle = new Bundle();
            try {
                bundle = accountManagerFuture.getResult();
            } catch (OperationCanceledException e) {
                error = e;
                e.printStackTrace();
            } catch (IOException e) {
                error = e;
                e.printStackTrace();
            } catch (AuthenticatorException e) {
                error = e;
                //No static method printStackTrace
                //e.printStackTrace();
            }
            String token = bundle.getString(AccountManager.KEY_AUTHTOKEN);
            if (needUpdate) {
                am.invalidateAuthToken("com.google", token);
                takeGoogleToken(am, account, false);
            } else {
                Intent launch = (Intent) bundle.get(AccountManager.KEY_INTENT);
                if (launch != null) {
                    startActivityForResult(launch, G_AUTH_RESULT);
                    return;
                }
                if (token != null) {
                    mPresenter.onGoogleTokenReceived(token);
                } else {
                    mPresenter.onGoogleTokenReceivedError(error);
                }
            }
        }, new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                return false;
            }
        }));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("NavigationFragment", "onActivityResult: requestCode " + requestCode + " resultCode = " + resultCode);
        if (requestCode == G_AUTH_RESULT) {
            onClickGooglePlusLogIn();
        } else if (requestCode == 10111) {
            if (resultCode == Activity.RESULT_CANCELED) {
                hideLoginProcess();
                //return;
            }

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            final AccountManager am = AccountManager.get(getContext());
            takeGoogleToken(am, account.getAccount(), true);
            // Signed in successfully, show authenticated UI.
            Log.d("NavigationFragment", "handleSignInResult: " + account.getIdToken());
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.d("NavigationFragment", "handleSignInResult: " + e);
            requestGoogleToken();
        }
    }

    @Override
    public void requestLogin() {
        onClickGooglePlusLogIn();
    }
}
