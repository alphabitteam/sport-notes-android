package com.alphabit.sportnotes.ui.screens.exercisedetail.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.databinding.DialogSimpleBinding;
import com.alphabit.sportnotes.databinding.FragmentExerciseDetailBinding;
import com.alphabit.sportnotes.ui.Router;
import com.alphabit.sportnotes.ui.adapter.CompositeAdapter;
import com.alphabit.sportnotes.ui.screens.base.fragment.BaseMvpFragment;
import com.alphabit.sportnotes.ui.screens.exercisedetail.presenter.ExerciseDetailPresenter;
import com.alphabit.sportnotes.utils.DateUtils;
import com.alphabit.sportnotes.utils.NumberUtils;
import com.alphabit.sportnotes.view.DatePrinter;
import com.alphabit.sportnotes.view.customdialog.CustomDialogBuilder;
import com.alphabit.sportnotes.view.date.DatePrintableImpl;
import com.arellomobile.mvp.presenter.InjectPresenter;

import org.joda.time.format.DateTimeFormat;

import java.util.List;
import java.util.UUID;

import sportnotes.alphabit.com.business.Serializer;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.ExerciseSum;
import sportnotes.alphabit.com.business.model.ExerciseTime;
import sportnotes.alphabit.com.business.model.Set;

/**
 * Created by Yuri Zigunov on 02.10.2017.
 */
@Layout(id = R.layout.fragment_exercise_detail)
public class ExerciseDetailFragment extends BaseMvpFragment<FragmentExerciseDetailBinding> implements
        IExerciseDetailView, LastSetRenderer.OnClickSetListener {

    public static final int REPEAT_COUNT_BUFFER = 100;
    public static final int WEIGHTS_BUFFER = 100;

    @InjectPresenter
    ExerciseDetailPresenter mPresenter;

    private String mMinutesSuf;
    private String mSecondsSuf;

    public static final String EXERCISE_UUID = "exercise_uuid";
    private String mWeightUnit;
    private CompositeAdapter lastSetsAdapter;

    private DatePrinter datePrinter;

    private LastSetRenderer lastSetRenderer;

    public static ExerciseDetailFragment newInstance(final UUID exerciseUuid) {

        Bundle args = new Bundle();
        args.putSerializable(EXERCISE_UUID, exerciseUuid);

        ExerciseDetailFragment fragment = new ExerciseDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ExerciseDetailPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mPresenter.setArguments((UUID) getArguments().get(EXERCISE_UUID));
        datePrinter = new DatePrinter(new DatePrintableImpl(getResources()),
                DateTimeFormat.forPattern("dd.MM"));
        lastSetRenderer = new LastSetRenderer(this, datePrinter);

        mMinutesSuf = getString(R.string.m);
        mSecondsSuf = getString(R.string.s);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_detail_exercise, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                mPresenter.onClickEdit();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initView() {
        getBinding().start.setOnClickListener(view -> mPresenter.onClickStartTimer());
        getBinding().cancel.setOnClickListener(view -> mPresenter.onClickCancelTimer());
        getBinding().start.setOnClickListener(view -> {
            if (getBinding().start.isChecked()) {
                mPresenter.onClickStartTimer();
            } else mPresenter.onClickPauseTimer();
        });

        getBinding().setsPicker.setOnValueChangedListener((picker, oldVal, newVal) ->
                mPresenter.onSetsPickerChanged(newVal));
        getBinding().numberPicker1.setOnValueChangedListener((picker, oldVal, newVal) ->
                mPresenter.onNumberPicker1Changed(newVal));
        getBinding().numberPicker2.setOnValueChangedListener((picker, oldVal, newVal) ->
                mPresenter.onNumberPicker2Changed(newVal));

        getBinding().trackActivity.setOnClickListener(view -> mPresenter.onClickTrackActivity());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
        };
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        getBinding().setsRecycler.setLayoutManager(layoutManager);

        lastSetsAdapter = new CompositeAdapter.Builder<>().add(lastSetRenderer).build();
        getBinding().setsRecycler.setAdapter(lastSetsAdapter);

        getBinding().setsRecycler.addOnLayoutChangeListener((view, i, i1, i2, i3, i4, i5, i6, i7) ->
                getBinding().setsRecycler.scrollToPosition(0));

        getBinding().notifyTimer.setOnClickListener(view -> {
            mPresenter.onClickNotify();
        });
        getBinding().lockTrackerLayout.setOnClickListener(view -> {});
    }

    @Override
    public void buildViewTimeExercise(ExerciseTime exercise) {
        getBinding().setsInfoLayout.setVisibility(View.VISIBLE);
        getBinding().repeatInfoLayout.setVisibility(View.GONE);
        getBinding().valueInfoLayout.setVisibility(View.VISIBLE);

        DateUtils.Formatter formatter = new DateUtils.Formatter(exercise.getSetsValue());
        int minutes = formatter.getMinutes();
        int seconds = formatter.getSeconds();

        getBinding().setsValue.setText(String.format(getString(R.string.x_m_x_s), formatter.getMinutes(),
                mMinutesSuf, formatter.getSeconds(), mSecondsSuf));

        if (minutes > 600)
            minutes = 600;
        else if (minutes < 0)
            minutes = 0;

        if (seconds > 59)
            seconds = 59;
        else if (seconds < 0)
            seconds = 0;

        getBinding().numberPicker1.setMinValue(0);
        getBinding().numberPicker1.setMaxValue(600);
        getBinding().numberPicker1.setValue(minutes);

        getBinding().numberPicker2.setMinValue(0);
        getBinding().numberPicker2.setMaxValue(59);
        getBinding().numberPicker2.setValue(seconds);

        getBinding().numberPicker1.setLabel(getString(R.string.minutes));
        getBinding().numberPicker2.setLabel(getString(R.string.seconds));

        getBinding().numberPicker1.setFormatter(value -> NumberUtils.zeroPrefix(value) + " " + mMinutesSuf);
        getBinding().numberPicker2.setFormatter(value -> NumberUtils.zeroPrefix(value) + " " + mSecondsSuf);

        getBinding().timerControlLayout.setVisibility(View.VISIBLE);

        bindCommonFields(exercise);
    }

    @Override
    public void applyWeightUnit(final String unit) {
        mWeightUnit = unit;
        lastSetRenderer.setWeightUnit(unit);
    }

    @Override
    public void buildViewSumWithoutWeightsExercise(ExerciseSum exercise) {
        getBinding().numberPicker2.setVisibility(View.GONE);
        getBinding().setsInfoLayout.setVisibility(View.VISIBLE);
        getBinding().repeatInfoLayout.setVisibility(View.VISIBLE);
        getBinding().valueInfoLayout.setVisibility(View.GONE);

        bindCommonFields(exercise);
        bindSumCommonFields(exercise);
    }

    @Override
    public void buildViewSumWithWeightsExercise(ExerciseSum exercise) {
        getBinding().numberPicker2.setVisibility(View.VISIBLE);
        getBinding().setsInfoLayout.setVisibility(View.VISIBLE);
        getBinding().repeatInfoLayout.setVisibility(View.VISIBLE);
        getBinding().valueInfoLayout.setVisibility(View.VISIBLE);

        getBinding().numberPicker2.setMinValue((int) (exercise.getSetsValue() - WEIGHTS_BUFFER < 0 ?
                0 : (exercise.getSetsValue() - WEIGHTS_BUFFER)));

        getBinding().numberPicker2.setMaxValue((int) (exercise.getSetsValue() + WEIGHTS_BUFFER));
        getBinding().numberPicker2.setValue((int) exercise.getSetsValue());
        getBinding().numberPicker2.setFormatter(value -> NumberUtils.zeroPrefix(value) + "" + mWeightUnit);

        bindCommonFields(exercise);
        bindSumCommonFields(exercise);
    }

    private void bindSumCommonFields(ExerciseSum exercise) {
        getBinding().numberPicker1.setLabel(getString(R.string.repeats));
        getBinding().numberPicker2.setLabel(getString(R.string.value));
        getBinding().repeatCount.setText(exercise.getRepeatCount().toString());

        getBinding().setsValue.setText(exercise.getSetsValue() + " " + mWeightUnit);

        int repeatCount = exercise.getRepeatCount().getStart();
        getBinding().numberPicker1.setMinValue((repeatCount - REPEAT_COUNT_BUFFER < 0 ? 0 : (repeatCount - REPEAT_COUNT_BUFFER)));
        getBinding().numberPicker1.setMaxValue(repeatCount + REPEAT_COUNT_BUFFER);
        getBinding().numberPicker1.setValue(repeatCount);

        getBinding().timerControlLayout.setVisibility(View.GONE);
    }

    public void bindCommonFields(Exercise exercise) {

        getBinding().title.setText(exercise.getName());
        getBinding().text.setText(exercise.getDescription());
        getBinding().setsCount.setText(String.valueOf(exercise.getSetsCount()));

        getBinding().setsPicker.setMinValue(1);
        getBinding().setsPicker.setMaxValue(exercise.getSetsCount());
        getBinding().setsPicker.setLabel(getString(R.string.set));

        mPresenter.onSetsPickerChanged(getBinding().setsPicker.getValue());
        mPresenter.onNumberPicker1Changed(getBinding().numberPicker1.getValue());
        mPresenter.onNumberPicker2Changed(getBinding().numberPicker2.getValue());
    }

    @Override
    public void showTimer() {
        getBinding().stopwatch.setVisibility(View.VISIBLE);
        getBinding().timeValueContainer.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showPickerSetter() {
        getBinding().stopwatch.setVisibility(View.GONE);
        getBinding().timeValueContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTimerNotify() {
        getBinding().lockTrackerLayout.setVisibility(View.VISIBLE);
        getBinding().notifyTimer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideTimerNotify() {
        getBinding().lockTrackerLayout.setVisibility(View.GONE);
        getBinding().notifyTimer.setVisibility(View.GONE);
    }

    @Override
    public void printTimerTime(final String time) {
        getBinding().stopwatch.setText(time);
    }

    @Override
    public void onStartTimer() {
        getBinding().start.setChecked(true);
    }

    @Override
    public void onPauseTimer() {
        getBinding().start.setChecked(false);
    }

    @Override
    public void showLastSets(final List<Set> sets) {
        lastSetsAdapter.setData(sets);
    }

    @Override
    public void openSetsScreen(final UUID exerciseUuid, boolean withWeight) {
        Router.openSetsScreen(getActivity(), exerciseUuid, withWeight);
    }

    @Override
    public void openEditExercise(UUID exerciseUuid) {
        Router.openEditExerciseScreen(getActivity(), exerciseUuid);
    }

    @Override
    public void setCurrentSetNumber(int setNumber) {
        System.out.println("ExerciseDetailFragment.setCurrentSetNumber = " + setNumber);
        if (setNumber >= getBinding().setsPicker.getMaxValue())
            getBinding().setsPicker.setMaxValue(setNumber);
        getBinding().setsPicker.setValue(setNumber);
    }

    @Override
    public void openExerciseDetail(UUID uuid) {
        Router.openExerciseDetail(getActivity(), uuid);
    }

    @Override
    public void initPickerSetter(int set, int pickerValue1, int pickerValue2) {
        getBinding().numberPicker1.setValue(pickerValue1);
        getBinding().numberPicker2.setValue(pickerValue2);
    }

    @Override
    public void clearLastSets() {
        lastSetsAdapter.clearData();
    }

    @Override
    public void setCountSetsToday(int integer) {
        System.out.println("ExerciseDetailFragment.setCountSetsToday = " + integer);
        if (getBinding().setsPicker.getMaxValue() <= integer)
            getBinding().setsPicker.setMaxValue(++integer);
        else ++integer;
        getBinding().setsPicker.setValue(integer);
    }

    @Override
    public void enableTrackActivity() {
        getBinding().trackActivity.setEnabled(true);
    }

    @Override
    public void disableTrackActivity() {
        getBinding().trackActivity.setEnabled(false);
    }

    @Override
    public void showTrackActivityConfirm(String setValueString) {
        final DialogSimpleBinding dialogBinding = DataBindingUtil.bind(LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_simple, null, false));
        dialogBinding.text.setText(String.format(getString(R.string.confirm_track_activity_text), setValueString));

        CustomDialogBuilder.newBuilder(getActivity())
                .setTitle(getString(R.string.deleting_exercise_title))
                .setView(dialogBinding.getRoot())
                .setOnPositiveClickListener(getString(R.string.ok), customDialog ->
                        mPresenter.onTrackActivityConfirmed())
                .setOnNegativeClickListener(getString(R.string.cancel), customDialog -> {
                })
                .build().show();
    }

    @Override
    public void onClickSet(final Set set) {
        mPresenter.onClickSets();
    }
}
