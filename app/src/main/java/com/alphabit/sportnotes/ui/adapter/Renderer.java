package com.alphabit.sportnotes.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 31.01.18.
 */
public interface Renderer<VH extends RecyclerView.ViewHolder, T> {

    @NonNull
    BaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType);

    void onBindViewHolder(@NonNull VH holder,
                          @NonNull List<T> items,
                          int position);

    void onRecycled(VH holder);

    boolean isForViewType(@NonNull List<?> items, int position);

}
