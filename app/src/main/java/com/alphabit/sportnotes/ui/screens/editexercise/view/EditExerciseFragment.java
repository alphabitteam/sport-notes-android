package com.alphabit.sportnotes.ui.screens.editexercise.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.databinding.FragmentEditExerciseBinding;
import com.alphabit.sportnotes.ui.screens.base.fragment.BaseMvpFragment;
import com.alphabit.sportnotes.ui.screens.editexercise.presenter.EditExercisePresenter;
import com.alphabit.sportnotes.utils.DateUtils;
import com.alphabit.sportnotes.utils.SimpleTextWatcher;
import com.alphabit.sportnotes.utils.UiUtils;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;
import com.jakewharton.rxbinding2.widget.RxTextView;

import org.joda.time.DateTime;

import java.util.UUID;

import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.ExerciseSum;
import sportnotes.alphabit.com.business.model.ExerciseTime;
import sportnotes.alphabit.com.business.model.RangeInt;
import sportnotes.alphabit.com.business.model.WeekDay;

/**
 * Created by Fly on 11.09.2017.
 */
@Layout(id = R.layout.fragment_edit_exercise)
public class EditExerciseFragment extends BaseMvpFragment<FragmentEditExerciseBinding> implements IEditExerciseView {

    public static final String WEEKDAY = "weekday";
    public static final String EXERCISE_UUID = "exercise_uuid";
    public static final String COMPLEX_UUID = "complex_uuid";

    private static final int MINUTES_LIMIT = 600;

    @InjectPresenter
    EditExercisePresenter mPresenter;

    public static EditExerciseFragment newInstanceCreate(final WeekDay weekDay, UUID complexUuid) {

        Bundle args = new Bundle();

        args.putSerializable(WEEKDAY, weekDay);
        args.putSerializable(COMPLEX_UUID, complexUuid);

        EditExerciseFragment fragment = new EditExerciseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void hideKeyboard() {
        UiUtils.hideKeyboard(getActivity());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_edit_exercise, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();
        getBinding().repeatBtn.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) mPresenter.onClickRepeatCheckButton();});
        getBinding().timeBtn.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) mPresenter.onClickTimeCheckButton();});
        getBinding().valueChb.setOnCheckedChangeListener((compoundButton, isChecked) ->
                mPresenter.onClickWeightCheckButton(isChecked));
        getBinding().setsValue.addTextChangedListener(setValueListener);
        getBinding().repeatCount.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onChanged(final CharSequence charSequence) {
                mPresenter.onRepeatCountTextChanged(charSequence);
            }
        });
        getBinding().repeatCount2.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onChanged(final CharSequence charSequence) {
                mPresenter.onRepeatCount2TextChanged(getBinding().repeatCount2.hasFocus(), charSequence);
            }
        });

        getBinding().setsCount.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onChanged(final CharSequence charSequence) {
                mPresenter.onSetsCountTextChanged(charSequence);
            }
        });

        getBinding().description.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onChanged(final CharSequence charSequence) {
                mPresenter.onDescriptionTextChanged(charSequence);
            }
        });

        getBinding().name.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onChanged(final CharSequence charSequence) {
                mPresenter.onNameTextChanged(charSequence);
            }
        });

        getBinding().repeatCount2.setOnFocusChangeListener((v, hasFocus) ->
                mPresenter.onRepeatCount2FocusChanged(hasFocus, getBinding().repeatCount2.getText()));

        getBinding().name.setOnFocusChangeListener(this::setHideHintOnFocus);
        getBinding().description.setOnFocusChangeListener(this::setHideHintOnFocus);
        getBinding().setsValue.setOnFocusChangeListener(this::setHideHintOnFocus);

        getBinding().setsValue.setOnSelectionChangedListener((selStart, selEnd) -> {
            mPresenter.onSetsValueSelectionChanged(getBinding().setsValue.getText(), selEnd);
        });
    }

    SimpleTextWatcher setValueListener = new SimpleTextWatcher() {
        @Override
        public void onChanged(CharSequence charSequence) {
            mPresenter.onSetsValueTextChanged(charSequence);
        }
    };

    private void setHideHintOnFocus(View view, boolean hasFocus) {
        if (view instanceof EditText) {
            EditText editText = (EditText) view;
            if (editText.getTag() == null)
                editText.setTag(editText.getHint());
            if (hasFocus)
                editText.setHint("");
            else
                editText.setHint(String.valueOf(editText.getTag()));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().repeatBtn.setOnCheckedChangeListener(null);
        getBinding().timeBtn.setOnCheckedChangeListener(null);
        getBinding().valueChb.setOnCheckedChangeListener(null);
        getBinding().setsValue.removeTextChangedListener(null);
        getBinding().repeatCount.removeTextChangedListener(null);
        getBinding().repeatCount2.removeTextChangedListener(null);
        getBinding().setsCount.removeTextChangedListener(null);
        getBinding().description.removeTextChangedListener(null);
        getBinding().name.removeTextChangedListener(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                mPresenter.onClickSave();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static EditExerciseFragment newInstanceEdit(final UUID exerciseUuid) {

        Bundle args = new Bundle();

        args.putSerializable(EXERCISE_UUID, exerciseUuid);

        EditExerciseFragment fragment = new EditExerciseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mPresenter.setArguments((WeekDay) getArguments().getSerializable(WEEKDAY),
                (UUID)getArguments().getSerializable(COMPLEX_UUID),
                (UUID) getArguments().getSerializable(EXERCISE_UUID));
    }

    @Override
    protected void initView() {
        //mPresenter.onNameChanged(RxTextView.textChanges(getBinding().name).skipInitialValue());
        //mPresenter.onDescriptionChanged(RxTextView.textChanges(getBinding().description).skipInitialValue());
        //mPresenter.onSetsCountChanged(RxTextView.textChanges(getBinding().setsCount).skipInitialValue());
        //mPresenter.onRepeatCountChanged(RxTextView.textChanges(getBinding().repeatCount).skipInitialValue());
        //mPresenter.onRepeatCount2Changed(RxTextView.textChanges(getBinding().repeatCount2).skipInitialValue());
        //mPresenter.onSetsValueChanged(RxTextView.textChanges(getBinding().setsValue).skipInitialValue());


        getBinding().minutesPicker.setMinValue(0);
        getBinding().minutesPicker.setMaxValue(MINUTES_LIMIT);
        getBinding().secondsPicker.setMinValue(0);
        getBinding().secondsPicker.setMaxValue(59);

        getBinding().minutesPicker.setFormatter(value -> mPresenter.formatMinutesPicker(value));
        getBinding().secondsPicker.setFormatter(value -> mPresenter.formatSecondsPicker(value));

        getBinding().minutesPicker.setOnValueChangedListener((picker, oldVal, newVal) -> mPresenter.onMinutesPickerChanged(newVal));
        getBinding().secondsPicker.setOnValueChangedListener((picker, oldVal, newVal) -> mPresenter.onSecondsPickerChanged(newVal));

        getBinding().minutesPicker.setValue(10);
        getBinding().secondsPicker.setValue(0);

        mPresenter.onMinutesPickerChanged(getBinding().minutesPicker.getValue());
        mPresenter.onSecondsPickerChanged(getBinding().secondsPicker.getValue());
    }

    @Override
    public void setRepeatCount2Secondary() {
        getBinding().repeatCount2.setAlpha(0.3f);
    }

    @Override
    public void setRepeatCount2Primary() {
        getBinding().repeatCount2.setAlpha(1f);
    }

    @Override
    public void showRepeatCount2() {
        getBinding().repeatCount2.setVisibility(View.VISIBLE);
        getBinding().hyphen.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRepeatCount2() {
        getBinding().repeatCount2.setText("");
        getBinding().repeatCount2.setVisibility(View.GONE);
        getBinding().hyphen.setVisibility(View.GONE);
    }

    @Override
    public void setSetsValueText(final String s) {
        getBinding().setsValue.setText(s);
    }

    @Override
    public void setSetsValueCursor(final int index) {
        getBinding().setsValue.setSelection(index);
    }

    @Override
    public void updateSetsValueText() {
        mPresenter.onSetsValueTextChanged(getBinding().setsValue.getText());
    }

    @Override
    public void showCountState() {
        getBinding().repeatBtn.setChecked(true);
        getBinding().timeBtn.setChecked(false);
        getBinding().repeatValueContainer.setVisibility(View.VISIBLE);
        getBinding().timeValueContainer.setVisibility(View.GONE);
        getBinding().weightLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTimeState() {
        getBinding().repeatBtn.setChecked(false);
        getBinding().timeBtn.setChecked(true);
        getBinding().repeatValueContainer.setVisibility(View.GONE);
        getBinding().timeValueContainer.setVisibility(View.VISIBLE);
        getBinding().weightLayout.setVisibility(View.GONE);
    }

    @Override
    public void showWeightField() {
        getBinding().valueChb.setChecked(true);
        getBinding().valueLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideWeightField() {
        getBinding().valueChb.setChecked(false);
        getBinding().valueLayout.setVisibility(View.GONE);
    }

    @Override
    public void showNameEmptyError() {
        getBinding().name.setError(getString(R.string.name_not_be_empty));
    }

    @Override
    public void showSetsCountEmptyError() {
        getBinding().setsCount.setError(getString(R.string.sets_count_not_be_empty));
    }

    @Override
    public void showRepeatCountStartEmptyError() {
        getBinding().repeatCount.setError(getString(R.string.repeat_count_not_be_empty));
    }

    @Override
    public void showRepeatCountEndEmptyError() {
        getBinding().repeatCount2.setError(getString(R.string.repeat_count_not_be_empty));
    }

    @Override
    public void bindExerciseSum(ExerciseSum exercise) {
        bindCommonField(exercise);

        getBinding().repeatBtn.setChecked(true);
        getBinding().timeBtn.setChecked(false);

        RangeInt rangeInt = exercise.getRepeatCount();
        getBinding().repeatCount.setText(rangeInt.getStartString());
        getBinding().repeatCount2.setText(rangeInt.getEndString());

        getBinding().valueChb.setChecked(exercise.hasWeight());
        getBinding().setsValue.setText(String.valueOf(exercise.getSetsValue()));
    }

    private void bindCommonField(Exercise exercise) {
        getBinding().name.setText(exercise.getName());
        getBinding().description.setText(exercise.getDescription());
        getBinding().setsCount.setText(String.valueOf(exercise.getSetsCount()));
    }

    @Override
    public void bindExerciseTime(ExerciseTime exercise) {
        bindCommonField(exercise);

        getBinding().repeatBtn.setChecked(false);
        getBinding().timeBtn.setChecked(true);

        DateUtils.Formatter formatter = new DateUtils.Formatter(exercise.getSetsValue());
        getBinding().minutesPicker.setValue(formatter.getMinutes());
        getBinding().secondsPicker.setValue(formatter.getSeconds());
    }

    @Override
    public void disableWeightChange() {
        getBinding().valueChb.setClickable(false);
    }

    @Override
    public void disableTypeChange() {
        getBinding().repeatBtn.setClickable(false);
        getBinding().timeBtn.setClickable(false);
    }

    @Override
    public void showSetsValueEmptyError() {
        getBinding().setsValue.setError(getString(R.string.sets_value_not_be_empty));
    }

    @Override
    public void showPopupError() {

    }

    @Override
    public void terminate() {
        getActivity().finish();
    }

    @Override
    public void showLoadingIndicator() {

    }

    @Override
    public void hideLoadingIndicator() {

    }

}
