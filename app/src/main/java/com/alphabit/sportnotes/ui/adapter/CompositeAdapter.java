package com.alphabit.sportnotes.ui.adapter;

import android.support.annotation.NonNull;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import io.reactivex.Observable;
import sportnotes.alphabit.com.business.model.UuidModel;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 31.01.18.
 */
public class CompositeAdapter<T extends UuidModel> extends MoveAdapter<BaseHolder> {

    private static final int HEADER = Integer.MAX_VALUE - 2;
    private static final int FOOTER = Integer.MAX_VALUE - 1;

    private static final int FIRST_VIEW_TYPE = 0;

    protected final SparseArray<Renderer<BaseHolder, T>> typeToAdapterMap;
    protected final @NonNull ArrayList<T> data = new ArrayList<>();

    private OnLazyListener mOnLazyListener;
    private boolean mEnableLazy;
    private Renderer footer;
    private Object selectedItem;

    protected CompositeAdapter(
            @NonNull SparseArray<Renderer<BaseHolder, T>> typeToAdapterMap) {
        this.typeToAdapterMap = typeToAdapterMap;
    }

    @Override
    public void onViewAttachedToWindow(final BaseHolder holder) {
        if (mOnLazyListener != null && mEnableLazy && holder.getAdapterPosition() == getItemCount() - 1) {
            mOnLazyListener.onGetLastItem(data.size());
        }
    }

    @Override
    public final int getItemViewType(int position) {
        if (footer != null && position == getItemCount() - 1)
            return FOOTER;
        for (int i = FIRST_VIEW_TYPE; i < typeToAdapterMap.size(); i++) {
            final Renderer delegate = typeToAdapterMap.valueAt(i);
            //noinspection unchecked
            if (delegate.isForViewType(data, position)) {
                return typeToAdapterMap.keyAt(i);
            }
        }
        throw new NullPointerException(
                "Can not get viewType for position " + position + " " + data.get(position));
    }

    @Override
    public final BaseHolder onCreateViewHolder(
            ViewGroup parent, int viewType) {
        if (viewType == FOOTER)
            return footer.onCreateViewHolder(parent, viewType);
        return typeToAdapterMap.get(viewType)
                .onCreateViewHolder(parent, viewType);
    }

    @Override
    public void swap(int oldPosition, int newPosition) {
        super.swap(oldPosition, newPosition);
        T oldI = data.get(oldPosition);
        data.set(oldPosition, data.set(newPosition, oldI));
    }

    @Override
    public final void onBindViewHolder(
            BaseHolder holder, int position) {
        if (getItemViewType(position) == FOOTER) {
            footer.onBindViewHolder(holder, data, position);
            return;
        }
        final Renderer delegateAdapter =
                typeToAdapterMap.get(getItemViewType(position));
        if (delegateAdapter != null) {
            //noinspection unchecked
            holder.setSelectedItem(selectedItem);
            delegateAdapter.onBindViewHolder(holder, data, position);
        } else {
            throw new NullPointerException(
                    "can not find adapter for position " + position);
        }
    }

    @Override
    public void onViewRecycled(BaseHolder holder) {
        //noinspection unchecked
        if (holder.getItemViewType() == FOOTER) {
            footer.onRecycled(holder);
            return;
        }
        typeToAdapterMap.get(holder.getItemViewType())
                .onRecycled(holder);
    }

    public void addData(List<T> data) {
        mEnableLazy = true;
        applyData(data);
    }

    public void addLastData(final List<T> data) {
        mEnableLazy = false;
        applyData(data);
    }

    public void setData(List<T> data) {
        applyData(data);
    }

    private void applyData(List<T> newData) {
        if (newData.size() == 0)
            return;

        for (T item : newData) {
            if (!data.contains(item)) {
                data.add(item);
            }
        }

        notifyDataSetChanged();
        if (true)
            return;

        Observable.fromIterable(newData)
                .filter(uuidModel -> !data.contains(uuidModel))
                .doOnNext(data::add)
                .toList()
                .subscribe(ignored -> {
                    Log.d(getClass().getSimpleName(), "applyData: subscr adapter = " + data.size() + " data = " + newData.size());
                    notifyDataSetChanged();
                });
    }

    public void clearData() {
        data.clear();
        notifyDataSetChanged();
    }

    public void deleteItem(T o) {
        boolean isSuccess = data.remove(o);
        notifyDataSetChanged();
    }

    public void setFooter(final Renderer footer) {
        this.footer = footer;
        notifyDataSetChanged();
    }

    @NonNull
    public List<T> getData() {
        return data;
    }

    @Override
    public final int getItemCount() {
        int overCount = 0;
        if (footer != null)
            overCount++;
        return data.size() + overCount;
    }

    //==============================================================================================
    public void setOnLazyListener(final OnLazyListener onLazyListener) {
        mOnLazyListener = onLazyListener;
        mEnableLazy = true;
    }

    public void setSelectedItem(final Object selectedItem) {
        this.selectedItem = selectedItem;
        notifyDataSetChanged();
    }
    //==============================================================================================

    public static class Builder<T extends UuidModel> {

        private int count;
        private final SparseArray<Renderer<? extends BaseHolder, ? extends T>> typeToAdapterMap;
        private Renderer footer;

        public Builder() {
            typeToAdapterMap = new SparseArray<>();
        }

        public Builder<T> add(
                @NonNull Renderer<? extends BaseHolder, ? extends T> delegateAdapter) {
            typeToAdapterMap.put(count++, delegateAdapter);
            return this;
        }

        public CompositeAdapter<T> build() {
            if (count == 0) {
                throw new IllegalArgumentException("Register at least one adapter");
            }
            CompositeAdapter<T> adapter = new CompositeAdapter(typeToAdapterMap);
            adapter.setFooter(footer);
            return adapter;
        }

        public Builder<T> setFooter(Renderer footer) {
            this.footer = footer;
            return this;
        }
    }

    public enum Mode {
        NORMAL, DRAG
    }

    public interface OnLazyListener {
        void onGetLastItem(int position);
    }
}