package com.alphabit.sportnotes.ui.screens.editexercise.view;

import com.alphabit.sportnotes.ui.screens.base.view.BaseMvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import sportnotes.alphabit.com.business.model.ExerciseSum;
import sportnotes.alphabit.com.business.model.ExerciseTime;

/**
 * Created by Fly on 11.09.2017.
 */

public interface IEditExerciseView extends BaseMvpView {
    @StateStrategyType(SkipStrategy.class)
    void setRepeatCount2Secondary();
    @StateStrategyType(SkipStrategy.class)
    void setRepeatCount2Primary();
    @StateStrategyType(SkipStrategy.class)
    void showRepeatCount2();
    @StateStrategyType(SkipStrategy.class)
    void hideRepeatCount2();
    @StateStrategyType(SkipStrategy.class)
    void setSetsValueText(String s);
    @StateStrategyType(SkipStrategy.class)
    void setSetsValueCursor(int length);
    @StateStrategyType(SkipStrategy.class)
    void updateSetsValueText();
    @StateStrategyType(SkipStrategy.class)
    void showCountState();
    @StateStrategyType(SkipStrategy.class)
    void showTimeState();
    @StateStrategyType(SkipStrategy.class)
    void showWeightField();
    @StateStrategyType(SkipStrategy.class)
    void hideWeightField();
    @StateStrategyType(SkipStrategy.class)
    void showNameEmptyError();
    @StateStrategyType(SkipStrategy.class)
    void showSetsCountEmptyError();
    @StateStrategyType(SkipStrategy.class)
    void showRepeatCountStartEmptyError();
    @StateStrategyType(SkipStrategy.class)
    void showSetsValueEmptyError();
    @StateStrategyType(SkipStrategy.class)
    void showPopupError();
    @StateStrategyType(SkipStrategy.class)
    void terminate();
    @StateStrategyType(SkipStrategy.class)
    void showLoadingIndicator();
    @StateStrategyType(SkipStrategy.class)
    void hideLoadingIndicator();
    @StateStrategyType(SkipStrategy.class)
    void showRepeatCountEndEmptyError();
    @StateStrategyType(SkipStrategy.class)
    void bindExerciseSum(ExerciseSum exercise);
    @StateStrategyType(SkipStrategy.class)
    void bindExerciseTime(ExerciseTime exercise);
    @StateStrategyType(SkipStrategy.class)
    void disableWeightChange();
    @StateStrategyType(SkipStrategy.class)
    void disableTypeChange();
    @StateStrategyType(SkipStrategy.class)
    void hideKeyboard();
}
