package com.alphabit.sportnotes.ui.screens.complex.view;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.alphabit.sportnotes.ui.adapter.BaseHolder;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.ui.adapter.BaseRenderer;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.List;

import sportnotes.alphabit.com.business.model.Complex;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 21.04.18.
 */
public class ComplexRenderer extends BaseRenderer<ComplexViewHolder, Complex> {

    private final OnItemComplexListener mOnItemComplexListener;
    private final ViewBinderHelper viewBinderHelper;

    public ComplexRenderer(OnItemComplexListener onItemComplexListener,
                           ViewBinderHelper viewBinderHelper) {
        mOnItemComplexListener = onItemComplexListener;
        this.viewBinderHelper = viewBinderHelper;
    }

    @NonNull
    @Override
    public BaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ComplexViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_complex, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ComplexViewHolder holder, @NonNull List<Complex> items,
                                 int position) {
        holder.setOnItemComplexListener(mOnItemComplexListener);
        holder.setViewBinderHelper(viewBinderHelper);
        holder.bind(items.get(position));
    }

    @Override
    public void onRecycled(ComplexViewHolder holder) {

    }

    @Override
    public boolean isForViewType(@NonNull List<?> items, int position) {
        return items.get(position) instanceof Complex;
    }

    public interface OnItemComplexListener {
        void onSelectComplex(Complex complex);
        void onClickDeleteComplex(int position, Complex complex);
        void onClickEditComplex(Complex complex);
    }
}
