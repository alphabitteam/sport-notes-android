package com.alphabit.sportnotes.ui.screens.complex.presenter;

import android.os.Handler;
import android.support.annotation.StringRes;
import android.util.Log;

import com.alphabit.sportnotes.App;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.alphabit.sportnotes.ui.screens.complex.view.ComplexMvpView;
import com.alphabit.sportnotes.utils.billing.BillingManager;
import com.annimon.stream.Optional;
import com.arellomobile.mvp.InjectViewState;

import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import sportnotes.alphabit.com.business.exceptions.SerjException;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.ExerciseSum;
import sportnotes.alphabit.com.business.model.Purchase;
import sportnotes.alphabit.com.business.model.RangeInt;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.SetSum;
import sportnotes.alphabit.com.business.model.ValueType;
import sportnotes.alphabit.com.business.model.WeekDay;
import sportnotes.alphabit.com.business.usecase.UseCase;
import sportnotes.alphabit.com.business.usecase.contract.IComplexUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IEditExerciseUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IExerciseDetailUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IExerciseListUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISetUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISynchronizationUseCase;
import sportnotes.alphabit.com.business.utils.Pager;
import sportnotes.alphabit.com.data.utils.UserLevelManager;

/**
 * Created by Fly on 27.08.2017.
 */
@InjectViewState
public class ComplexScreenPresenter extends BasePresenter<ComplexMvpView> implements BillingManager.BillingUpdatesListener {

    @Inject
    ISynchronizationUseCase synchronizationUseCase;
    @Inject
    IComplexUseCase mComplexUseCase;
    @Inject
    IEditExerciseUseCase editExerciseUseCase;
    @Inject
    IExerciseDetailUseCase setUseCase;

    @Inject
    UserLevelManager userLevelManager;

    private BillingManager billingManager;

    private Complex mCurrentComplex;

    private Complex mCandidateDeleteComplex;
    private boolean isCreateNewComplex = false;

    private final Handler handler = new Handler();
    private final AtomicInteger complexesCount = new AtomicInteger();

    public void setBillingManager(BillingManager billingManager) {
        this.billingManager = billingManager;
    }

    Throwable t;
    String name;
    Complex complex;
    ExerciseSum exercise;
    Set set;
    UUID uuid;
    Random random = new Random();

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        complexesCount.set(0);
        onGetLastItem(0);
        getCurrentComplex();
        handler.postDelayed(() ->  billingManager.startClient(this), 200);
        //createMock();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        billingManager.destroy();
    }

    AtomicInteger aC = new AtomicInteger();
    AtomicInteger aE = new AtomicInteger();
    PublishSubject<Integer> complexC = PublishSubject.create();
    PublishSubject<Exercise> complexS = PublishSubject.create();
    PublishSubject<Complex> complexE = PublishSubject.create();
    Complex currentComplex;
    Exercise currentExercise;
    int cCount = 0;
    int setCount = 0;
    int exCount = 0;

    Disposable dC;
    Disposable dE;
    Disposable dS;
    private void createMock() {
        aC.set(0);
        complexC.subscribe(integer -> {
            System.out.println("ComplexScreenPresenter created complex start");
            name = "complex #" + aC.incrementAndGet();
            dC = mComplexUseCase.createNewComplex(name).subscribe(() -> {
                mComplexUseCase.getComplexByName(name).subscribe(complex -> {
                    System.out.println("ComplexScreenPresenter created complex " + name);
                    cCount++;
                    aE.set(0);
                    currentComplex = complex;
                    if (cCount <= 10)
                        complexE.onNext(complex);
                    if (dC != null && !dC.isDisposed())
                        dC.dispose();
                }, throwable -> System.out.println("ComplexScreenPresenter.createMock error c " + throwable));
            });
        });
        complexE.subscribe(complex -> {
            createE(complex);
        }, throwable -> System.out.println("ComplexScreenPresenter.createMock error e " + throwable));
        complexS.subscribe(exercise1 -> {
            createS(exercise1);
        }, throwable -> System.out.println("ComplexScreenPresenter.createMock error s " + throwable));
        complexC.onNext(0);
    }

    private void createE(Complex complex) {
        uuid = UUID.randomUUID();
        ExerciseSum exercise = new ExerciseSum();
        exercise.setUuid(UUID.randomUUID());
        exercise.setName("exercise #" + aE.incrementAndGet());
        exercise.setSetsCount(1);
        exercise.setWeekDay(WeekDay.MONDAY);
        exercise.setRepeatCount(new RangeInt(random.nextInt(9) + 1, random.nextInt(9) + 1));
        exercise.setComplexUuid(complex.getUuid());
        currentExercise = exercise;
        editExerciseUseCase.saveExercise(exercise).subscribe(() -> {
            System.out.println("ComplexScreenPresenter created exercise " + exercise.getName());
            exCount++;
            if (exCount >= 10) {
                exCount = 0;
                complexC.onNext(0);
            } else {
                complexS.onNext(currentExercise);
            }

            if (dE != null && !dE.isDisposed())
                dE.dispose();
        });
    }


    private void createS(Exercise exercise) {
        set = new SetSum(ValueType.COUNT);
        set.setUuid(UUID.randomUUID());
        set.setExerciseUuid(exercise.getUuid());
        set.setRepeat(random.nextInt(15) + 1);
        System.out.println("ComplexScreenPresenter created set " + set.getExerciseUuid());
        setUseCase.saveSet(set).subscribe(() -> {

            setCount++;
            if (setCount == 10) {
                setCount = 0;
                complexE.onNext(currentComplex);
            } else {
                complexS.onNext(currentExercise);
            }
            if (dS != null && !dS.isDisposed())
                dS.dispose();
        });
    }

    private void setCurrentComplex(Optional<Complex> complex) {
        if (complex.isPresent()) {
            mCurrentComplex = complex.get();
            getViewState().showCurrentComplex(mCurrentComplex);
            if (isCreateNewComplex) {
                isCreateNewComplex = false;
                handler.postDelayed(() -> getViewState().hideComplexSheet(), 500);
            }
        } else {
            getViewState().showSelectCurrentComplex();
        }

    }

    private void onGetComplexes(List<Complex> complexes) {
        if (complexes.size() == 0)
            getViewState().addLastComplexes(complexes);
        else
            getViewState().addComplexes(complexes);
    }

    public ComplexScreenPresenter() {
        App.getAppComponent().inject(this);
    }

    public void onClickTile(final WeekDay weekDay) {
        if (!validateOpenExercises())
            return;
        switch (weekDay) {
            case FRIDAY:
            case MONDAY:
            case SUNDAY:
            case TUESDAY:
            case SATURDAY:
            case THURSDAY:
            case WEDNESDAY:
                getViewState().showExerciseForWeekday(mCurrentComplex.getUuid(), weekDay);
                break;
        }
    }

    private boolean validateOpenExercises() {
        boolean hasCurrentComplex = mCurrentComplex != null;
        if (!hasCurrentComplex) {
            getViewState().showSelectCurrentComplex();
        }
        return hasCurrentComplex;
    }

    private boolean checkUserLevel() {
        if (!userLevelManager.isFullVersion() && complexesCount.get() > 0) {
            getViewState().showUserLevelError();
            return true;
        }
        return false;
    }

    public void onSelectComplex(final Complex complex) {
        if (mCurrentComplex != null && mCurrentComplex.getUuid().equals(complex.getUuid())) {
            getViewState().hideComplexSheet();
            return;
        }

        addDisposable(scheduling(mComplexUseCase.setCurrentComplex(complex))
                .subscribe(this::getCurrentComplex,
                        throwable -> getViewState().showSelectCurrentComplex()));
    }

    private void getCurrentComplex() {
        addDisposable(scheduling(mComplexUseCase.getCurrentComplex())
                .subscribe(this::setCurrentComplex, throwable ->
                        getViewState().showSelectCurrentComplex()));
    }

    public void onClickDeleteComplex(final Complex complex) {
        mCandidateDeleteComplex = complex;
        getViewState().showDeleteComplexConfirm(mCandidateDeleteComplex);
    }

    public void onDeleteComplexConfirmed() {
        if (mCandidateDeleteComplex != null) {
            addDisposable(scheduling(mComplexUseCase.deleteComplex(mCandidateDeleteComplex))
                    .subscribe(this::onGetDeleteComplex, throwable -> showNotifyMessage(throwable, R.string.delete_complex_failed)));
        }
    }

    private void onGetDeleteComplex() {
        getViewState().onDeletedComplexItem(mCandidateDeleteComplex);
    }

    public void onClickEditComplex(final Complex complex) {
        getViewState().showEditComplex(complex);
    }

    public void onClickNewComplex() {
        if (checkUserLevel()) {
            return;
        }
        getViewState().showCreateNewComplex();
    }

    public void onGetLastItem(final int position) {
        addDisposable(scheduling(mComplexUseCase.getComplexes(position))
                .doOnNext(complexes -> complexesCount.addAndGet(complexes.size()))
                .subscribe(this::onGetComplexes, throwable ->
                    showNotifyMessage(throwable, R.string.get_complex_failed)));
    }

    public void onCreateNewComplexClick(final String name) {
        isCreateNewComplex = true;
        addDisposable(scheduling(mComplexUseCase.createNewComplex(name))
                .doOnComplete(() -> setCurrentByName(name))
                .subscribe(this::onGetCreateNewComplex, throwable ->
                    showNotifyMessage(throwable, R.string.create_complex_failed)));
    }

    private void onGetCreateNewComplex() {
        getViewState().clearComplexes();
        getCurrentComplex();
    }

    private void setCurrentByName(String name) {
        mComplexUseCase.getComplexByName(name)
                .flatMapCompletable(complex -> mComplexUseCase.setCurrentComplex(complex))
                .andThen(mComplexUseCase.getCurrentComplex())
                .subscribe(this::setCurrentComplex, throwable -> {});
    }

    public void onChangeNavigationMenuState() {
        getViewState().hideComplexSheet();
    }

    private void purchase(Purchase purchase) {
        addDisposable(scheduling(mComplexUseCase.purchase(purchase)).subscribe(() -> {
            Log.d(getClass().getSimpleName(), "purchase: success");
        }, throwable -> {
            //if (throwable instanceof SerjException)
            //    getViewState().showNotifyMessage("Серега сделай покупки");
            //else
                getViewState().showNotifyMessage(throwable.getMessage());
        }));
    }

    @Override
    public void onBillingClientSetupFinished() {

    }

    @Override
    public void onConsumeFinished(String token, int result) {
        Purchase purchase = billingManager.findTransactionInfoByToken(token);
        if (purchase != null) {
            purchase(purchase);
        }
    }

    @Override
    public void onBillingError(BillingManager.BillingResponse resultCode) {
        @StringRes int stringRes;
        switch (resultCode) {
            case BILLING_UNAVAILABLE:
                stringRes = R.string.error_billing_unavailable;
                break;
            case ITEM_UNAVAILABLE:
                stringRes = R.string.error_billing_item_unavailable;
                break;
            case SERVICE_UNAVAILABLE:
                stringRes = R.string.error_billing_service_unavailable;
                break;
            case FEATURE_NOT_SUPPORTED:
                stringRes = R.string.error_billing_feature_not_supported;
                break;
            case SERVICE_DISCONNECTED:
                stringRes = R.string.error_billing_service_disconnected;
                break;
            case USER_CANCELED:
                stringRes = R.string.error_billing_user_canceled;
                break;
            case ITEM_ALREADY_OWNED:
                return;
            default:
                stringRes = R.string.error_billing;
                break;
        }
        Log.d(getClass().getSimpleName(), "onBillingError: " + resultCode);
        getViewState().showNotifyMessage(stringRes);
    }

    @Override
    public void onPurchasesUpdated(List<Purchase> purchases) {
        if (purchases != null) {
            addDisposable(scheduling(mComplexUseCase.savePurchases(purchases)).subscribe(() -> {
                Log.d(getClass().getSimpleName(), "onPurchasesUpdated: savePurchases ");
            }));
            for (Purchase purchase : purchases) {
                if (purchase != null)
                    purchase(purchase);
            }
        }
        getViewState().sync();
    }

    public void onEditedComplexClick(Complex complex, String name) {
        complex.setName(name);
        addDisposable(scheduling(mComplexUseCase.editComplex(complex)).subscribe(() -> {

        }, throwable -> {}));
    }
}