package com.alphabit.sportnotes.ui.screens.base.fragment;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.databinding.DialogNewComplexBinding;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.alphabit.sportnotes.ui.screens.base.view.BaseMvpView;
import com.alphabit.sportnotes.view.customdialog.CustomDialogBuilder;

import java.util.UUID;

/**
 * Created by Fly on 08.09.2017.
 */

public abstract class BaseMvpFragment<Binding extends ViewDataBinding> extends BaseFragment<Binding> implements BaseMvpView {


    @Override
    public void showNotifyMessage(final int stringRes) {
        Toast.makeText(getContext(), getString(stringRes), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNotifyMessage(String string) {
        Toast.makeText(getContext(), string, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setTitle(final int titleRes) {
        getActivity().setTitle(titleRes);
    }

    @Override
    public void showLoadingIndicator() {

    }

    @Override
    public void refreshAdState() {
        if (getActivity() instanceof BaseMvpView)
            ((BaseMvpView) getActivity()).refreshAdState();
    }

    @Override
    public void hideLoadingIndicator() {

    }

    @Override
    public void showUserLevelError() {
        showNotifyMessage(getString(R.string.user_level_error));
    }
}
