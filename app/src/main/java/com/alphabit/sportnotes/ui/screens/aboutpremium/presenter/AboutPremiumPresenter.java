package com.alphabit.sportnotes.ui.screens.aboutpremium.presenter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.StringRes;
import android.util.Log;

import com.alphabit.sportnotes.App;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.ui.screens.aboutpremium.view.AboutPremiumView;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.alphabit.sportnotes.utils.billing.BillingManager;
import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import sportnotes.alphabit.com.business.exceptions.SerjException;
import sportnotes.alphabit.com.business.model.Purchase;
import sportnotes.alphabit.com.business.usecase.AuthorizationUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IAuthorizationUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IBillingUseCase;

/**
 * Created by fly12 on 19.11.2017.
 */
@InjectViewState
public class AboutPremiumPresenter extends BasePresenter<AboutPremiumView> implements BillingManager.BillingUpdatesListener {

    @Inject
    Context context;
    @Inject
    IBillingUseCase useCase;
    @Inject
    IAuthorizationUseCase authorizationUseCase;

    boolean isClickBuy = false;

    private final BillingManager billingManager;

    public AboutPremiumPresenter(BillingManager billingManager) {
        App.getAppComponent().inject(this);
        this.billingManager = billingManager;
        Log.d(getClass().getSimpleName(), "AboutPremiumPresenter: constructor isReady = " + billingManager.isReady());
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().disableBuy();
        getViewState().showBillingConnecting();
        billingManager.startClient(this);
        if (billingManager.isReady()) {
            getViewState().enableBuy();
            getViewState().showBillingConnected();
        }
        addDisposable(scheduling(authorizationUseCase.getAuthEvents()
                .filter(authEvent -> isClickBuy)
                .filter(authEvent -> authEvent == AuthorizationUseCase.AuthEvent.LOGIN))
                .subscribe(authEvent -> onClickBuy()));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        billingManager.releaseClient(this);
    }

    public void onClickBuy() {
        Log.d(getClass().getSimpleName(), "onClickBuy: ");
        addDisposable(scheduling(authorizationUseCase.getUser())
        .subscribe(userOptional -> {
            if (userOptional.isPresent()) {
                billingManager.initiatePurchaseFlow("sportnotes_month_2", BillingManager.SkuType.SUBS);
            } else {
                isClickBuy = true;
                getViewState().requestLogin();
            }
        }));
    }

    @Override
    public void onBillingClientSetupFinished() {
        Log.d(getClass().getSimpleName(), "onBillingClientSetupFinished isReady = " + billingManager.isReady());
        getViewState().enableBuy();
        getViewState().showBillingConnected();
    }

    @Override
    public void onConsumeFinished(String token, int result) {
        getViewState().sync();
        getViewState().enableBuy();
        getViewState().showPurchased();
        Log.d(getClass().getSimpleName(), "onConsumeFinished token = " + token + " result = " + result);
    }



    @Override
    public void onPurchasesUpdated(List<Purchase> purchases) {
        getViewState().enableBuy();
        if (purchases.size() > 0)
            getViewState().showPurchased();
        Log.d(getClass().getSimpleName(), "onPurchasesUpdated purchases = " + new Gson().toJson(purchases));

    }

    @Override
    public void onBillingError(BillingManager.BillingResponse resultCode) {
        Log.d(getClass().getSimpleName(), "onBillingError resultCode = " + resultCode);
        getViewState().enableBuy();
        switch (resultCode) {
            case ITEM_ALREADY_OWNED:
                getViewState().showPurchased();
                break;
        }
    }
}
