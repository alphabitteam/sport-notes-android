package com.alphabit.sportnotes.ui.screens.editexercise.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.alphabit.sportnotes.App;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.alphabit.sportnotes.ui.screens.editexercise.view.IEditExerciseView;
import com.alphabit.sportnotes.utils.NumberUtils;
import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.jakewharton.rxbinding2.InitialValueObservable;

import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Observable;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.ExerciseSum;
import sportnotes.alphabit.com.business.model.ExerciseTime;
import sportnotes.alphabit.com.business.model.RangeInt;
import sportnotes.alphabit.com.business.model.RepeatType;
import sportnotes.alphabit.com.business.model.WeekDay;
import sportnotes.alphabit.com.business.usecase.contract.IEditExerciseUseCase;

/**
 * Created by Fly on 11.09.2017.
 */
@InjectViewState
public class EditExercisePresenter extends BasePresenter<IEditExerciseView> {

    private WeekDay mWeekDay;
    private Exercise mExercise;

    public static final int EMPTY = -1;

    @Inject
    IEditExerciseUseCase mUseCase;

    @Inject
    Context mContext;

    private UUID mUuid;
    private UUID mComplexUuid;

    private String weightSuf;

    private String mName;
    private String mDescr;
    private int mSetsCount;
    private int mRepeatCountStart;
    private int mRepeatCountEnd = EMPTY;
    private int mMinutes;
    private int mSeconds;
    private int mSetsValue = EMPTY;
    private RepeatType mRepeatType;
    private boolean mHasWeight;

    private final String mMinutesSuf;
    private final String mSecondsSuf;

    public EditExercisePresenter() {
        App.getAppComponent().inject(this);
        mMinutesSuf = mContext.getString(R.string.m);
        mSecondsSuf = mContext.getString(R.string.s);
    }

    public void setArguments(final WeekDay weekDay, final UUID complexUuid, final UUID exerciseUuid) {
        mWeekDay = weekDay;
        mComplexUuid = complexUuid;
        mUuid = exerciseUuid;
    }

    @Override
    public void attachView(final IEditExerciseView view) {
        super.attachView(view);
        if (mWeekDay != null && mUuid == null)
            getViewState().setTitle(R.string.menu_create_exercise);
        else if (mWeekDay == null && mUuid != null) {
            getViewState().setTitle(R.string.menu_edit_exercise);
            addDisposable(scheduling(mUseCase.getExercise(mUuid)).subscribe(this::bind,
                    throwable -> getViewState().showNotifyMessage(R.string.error_loading_exercise)));
        }
        onClickWeightCheckButton(true);
    }

    private void bind(Exercise exercise) {
        mExercise = exercise;
        mComplexUuid = exercise.getComplexUuid();
        mWeekDay = exercise.getWeekDay();
        if (exercise instanceof ExerciseSum) {
            getViewState().bindExerciseSum((ExerciseSum)exercise);
            getViewState().showCountState();
        } else if (exercise instanceof ExerciseTime) {
            getViewState().bindExerciseTime((ExerciseTime) exercise);
            getViewState().showTimeState();
        }
        //видете ли пользователи тупые и лучше им дать возможность всегда
        //редактировать вес
        //getViewState().disableWeightChange();
        getViewState().disableTypeChange();
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        setupCountState();
        getViewState().hideWeightField();

        addDisposable(scheduling(mUseCase.getWeightUnit())
                .subscribe(this::onGetWeightUnit, throwable -> {}));
        onRepeatCountTextChanged("");
        onRepeatCount2TextChanged(false, "");

    }

    private void onGetWeightUnit(String s) {
        weightSuf = s;
        Log.d(getClass().getSimpleName(), "onGetWeightUnit: " + weightSuf);
        getViewState().updateSetsValueText();
    }

    public void onClickRepeatCheckButton() {
        setupCountState();
    }

    public void onClickTimeCheckButton() {
        setupTimeState();
    }

    private void setupCountState() {
        mRepeatType = RepeatType.SUM;
        getViewState().showCountState();
    }

    private void setupTimeState() {
        mRepeatType = RepeatType.TIME;
        getViewState().showTimeState();
    }

    public void onClickWeightCheckButton(final boolean isChecked) {
        mHasWeight = isChecked;
        if (isChecked)
            getViewState().showWeightField();
        else getViewState().hideWeightField();
    }

    private String clearZeros(String s) {
        boolean isNumeric = NumberUtils.isNumeric(s);
        if (isNumeric)
            return String.valueOf(Integer.valueOf(s));
        else return s;
    }

    public void onSetsValueTextChanged(final CharSequence s) {
        mSetsValue = getIntFromString(String.valueOf(s));
        if (s.length() == 0)
            return;
        String text = String.valueOf(s);
        if (TextUtils.isEmpty(weightSuf))
            return;
        if (!text.contains(weightSuf)) {
            String newText = mSetsValue + " " + weightSuf;
            getViewState().setSetsValueText(newText);
            getViewState().setSetsValueCursor(newText.length());
        } else if (s.length() <= (weightSuf.length() + 1)) {
            getViewState().setSetsValueText("");
        }
    }

    public void onRepeatCountTextChanged(final CharSequence s) {
        mRepeatCountStart = getIntFromString(String.valueOf(s));
        if (s.length() > 0) {
            getViewState().showRepeatCount2();
        } else {
            getViewState().hideRepeatCount2();
        }
    }

    public void onRepeatCount2TextChanged(boolean hasFocus, final CharSequence s) {
        mRepeatCountEnd = getIntFromString(String.valueOf(s));
        if (s.length() > 0 || hasFocus) {
            getViewState().setRepeatCount2Primary();
        } else {
            getViewState().setRepeatCount2Secondary();
        }
    }

    public void onSetsCountTextChanged(final CharSequence charSequence) {
        mSetsCount = getIntFromString(String.valueOf(charSequence));
    }

    public void onNameTextChanged(final CharSequence charSequence) {
        mName = String.valueOf(charSequence);
    }

    public void onDescriptionTextChanged(final CharSequence charSequence) {
        mDescr = String.valueOf(charSequence);
    }

    //public void onNameChanged(Observable<CharSequence> observable) {
    //    observable.subscribe(charSequence -> mName = String.valueOf(charSequence), throwable -> {});
    //}

    //public void onDescriptionChanged(Observable<CharSequence> observable) {
    //    observable.subscribe(charSequence -> mDescr = String.valueOf(charSequence), throwable -> {});
    //}

    //public void onSetsCountChanged(Observable<CharSequence> observable) {
    //    observable.subscribe(charSequence -> mSetsCount = getIntFromString(String.valueOf(charSequence)), throwable -> {});
    //}

    //public void onRepeatCountChanged(Observable<CharSequence> observable) {
    //    observable.subscribe(charSequence ->
    //            mRepeatCountStart = getIntFromString(String.valueOf(charSequence)), throwable -> {});
    //}

    //public void onRepeatCount2Changed(Observable<CharSequence> observable) {
    //    observable.subscribe(charSequence ->
    //            mRepeatCountEnd = getIntFromString(String.valueOf(charSequence)), throwable -> {});
    //}

    //public void onSetsValueChanged(Observable<CharSequence> observable) {
    //    observable.subscribe(charSequence ->
    //            mSetsValue = getIntFromString(String.valueOf(charSequence)), throwable -> {});
    //}

    public void onMinutesPickerChanged(int newVal) {
        mMinutes = newVal;
    }

    public void onSecondsPickerChanged(int newVal) {
        mSeconds = newVal;
    }

    public String formatMinutesPicker(final int value) {
        return NumberUtils.zeroPrefix(value) + mMinutesSuf;
    }

    public String formatSecondsPicker(final int value) {
        return NumberUtils.zeroPrefix(value) + mSecondsSuf;
    }

    private int getIntFromString(String s) {
        if (s == null)
            return EMPTY;
        String tmp = s.replaceAll(String.valueOf(weightSuf), "").trim();
        if (NumberUtils.isNumeric(tmp))
            return Integer.valueOf(tmp);
        else return EMPTY;
    }

    public void onRepeatCount2FocusChanged(final boolean hasFocus, final CharSequence text) {
        if (hasFocus || !TextUtils.isEmpty(text)) {
            getViewState().setRepeatCount2Primary();
        } else {
            getViewState().setRepeatCount2Secondary();
        }
    }

    public void onSetsValueSelectionChanged(final CharSequence charSequence, final int selEnd) {
        String text = String.valueOf(charSequence);
        int lengthInt = text.length() - (weightSuf.length() == 0 ? 0 : weightSuf.length() + 1);
        if (text.contains(weightSuf) && (selEnd > lengthInt)) {
            getViewState().setSetsValueCursor(lengthInt);
        }
    }

    public void onClickSave() {
        if (!validate())
            return;

        Exercise exercise = null;

        switch (mRepeatType) {
            case TIME:
                final ExerciseTime exerciseTime = new ExerciseTime();
                long millis = ((mMinutes * 60) + mSeconds) * 1000;
                exerciseTime.setSetsValue(millis);
                exercise = exerciseTime;
                break;
            case SUM:
                final ExerciseSum exerciseSum = new ExerciseSum();
                if (mSetsValue != EMPTY && mHasWeight)
                    exerciseSum.setSetsValue(mSetsValue);
                exerciseSum.setRepeatCount(new RangeInt(mRepeatCountStart,
                        //TODO убрать такой говнокод
                        mRepeatCountEnd == EMPTY ? 0 : mRepeatCountEnd));
                exerciseSum.setHasWeight(mHasWeight);
                exercise = exerciseSum;
                break;
        }

        System.out.println("EditExercisePresenter.onClickSave = " + mExercise);
        if (mExercise != null) {
            exercise.setCreatedAt(mExercise.getCreatedAt());
            exercise.setPosition(mExercise.getPosition());
            exercise.setComplexUuid(mExercise.getComplexUuid());
            exercise.setWeekDay(mExercise.getWeekDay());
            exercise.setUuid(mExercise.getUuid());
        } else {
            exercise.setComplexUuid(mComplexUuid);
            exercise.setWeekDay(mWeekDay);
            if (mUuid != null)
                exercise.setUuid(mUuid);
        }

        exercise.setName(mName);
        exercise.setDescription(mDescr);
        exercise.setSetsCount(mSetsCount);

        addDisposable(scheduling(mUseCase.saveExercise(exercise))
                .doOnSubscribe(subscription -> getViewState().showLoadingIndicator())
                .doOnComplete(() -> getViewState().hideLoadingIndicator())
                .subscribe(() -> getViewState().terminate()
                        , throwable -> getViewState().terminate()));
    }

    private boolean validate() {
        boolean isValid = true;
        if (TextUtils.isEmpty(mName)) {
            getViewState().showNameEmptyError();
            isValid = false;
        }
        if (mSetsCount == 0 || mSetsCount == EMPTY) {
            getViewState().showSetsCountEmptyError();
            isValid = false;
        }


        if (mRepeatType == RepeatType.SUM && ((mRepeatCountStart == 0 || mRepeatCountStart == EMPTY))) {
            getViewState().showRepeatCountStartEmptyError();
            isValid = false;
        }

        if (mRepeatType == RepeatType.SUM && ((mRepeatCountEnd == 0))) {
            getViewState().showRepeatCountEndEmptyError();
            isValid = false;
        }

        if (mRepeatType == RepeatType.SUM  && mHasWeight && (mSetsValue == 0 || mSetsValue == EMPTY)) {
            getViewState().showSetsValueEmptyError();
            isValid = false;
        }
        if (mComplexUuid == null) {
            getViewState().showPopupError();
            getViewState().terminate();
            isValid = false;
        }

        getViewState().hideKeyboard();

        return isValid;
    }

}
