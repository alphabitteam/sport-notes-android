package com.alphabit.sportnotes.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.alphabit.sportnotes.ui.screens.complex.view.ComplexActivity;

/**
 * Created by Fly on 27.08.2017.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent activityIntent = new Intent(this, ComplexActivity.class);
        Intent intent = getIntent();
        if (intent != null) {
            activityIntent.setAction(intent.getAction());
            activityIntent.putExtras(getIntent());
        }
        startActivity(activityIntent);
        finish();
    }

}
