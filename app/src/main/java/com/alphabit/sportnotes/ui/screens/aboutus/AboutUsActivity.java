package com.alphabit.sportnotes.ui.screens.aboutus;

import android.content.Intent;
import android.net.Uri;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.databinding.ActivityAboutusBinding;
import com.alphabit.sportnotes.ui.screens.base.BaseActivity;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 18.04.18.
 */
@Layout(id = R.layout.activity_aboutus)
public class AboutUsActivity extends BaseActivity<ActivityAboutusBinding> {
    @Override
    protected void initView() {
        getBinding().estimate.setOnClickListener(v -> {
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                anfe.printStackTrace();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        });
    }
}
