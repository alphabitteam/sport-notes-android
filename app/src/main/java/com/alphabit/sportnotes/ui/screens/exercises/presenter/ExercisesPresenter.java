package com.alphabit.sportnotes.ui.screens.exercises.presenter;

import android.util.Log;
import android.util.Pair;

import com.alphabit.sportnotes.App;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.alphabit.sportnotes.ui.screens.exercises.view.IExerciseMvpView;
import com.arellomobile.mvp.InjectViewState;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Flowable;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.WeekDay;
import sportnotes.alphabit.com.business.usecase.contract.IExerciseListUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISetUseCase;

/**
 * Created by Fly on 04.09.2017.
 */
@InjectViewState
public class ExercisesPresenter extends BasePresenter<IExerciseMvpView> {

    private UUID mComplexUuid;
    private WeekDay mWeekDay;

    private Exercise mDraggingExercise;
    private Exercise candidateDeleteExercise;

    @Inject
    IExerciseListUseCase mIExerciseListUseCase;
    @Inject
    ISetUseCase setUseCase;

    public ExercisesPresenter() {
        App.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    @Override
    public void attachView(final IExerciseMvpView view) {
        super.attachView(view);
        getViewState().clearExercise();
        onGetLastItem(0);
    }

    private void addExercises(final List<Exercise> exercises) {
        if (exercises.size() == 0)
            getViewState().addLastExercises(exercises);
        else getViewState().addExercises(exercises);
    }

    public void setParams(final UUID complexUuid, final WeekDay weekDay) {
        mComplexUuid = complexUuid;
        mWeekDay = weekDay;
    }

    public void onGetLastItem(final int lastPosition) {
        addDisposable(scheduling(Flowable.combineLatest(onGetLastItemObs(lastPosition), setUseCase.getWeightUnit(), Pair::new))
                .subscribe(listStringPair -> {
                    getViewState().applyWeightUnit(listStringPair.second);
                    addExercises(listStringPair.first);
                }, throwable -> showNotifyMessage(throwable, R.string.get_exercise_failed)));
    }

    public Flowable<List<Exercise>> onGetLastItemObs(final int lastPosition) {
        return scheduling(mIExerciseListUseCase.getExercises(lastPosition, mComplexUuid, mWeekDay))
                .take(1);
    }

    public void onSortClickMenu(List<Exercise> data) {
        addDisposable(scheduling(mIExerciseListUseCase.updatePositions(data, 0))
                .subscribe(getViewState()::showDragModeList));

    }

    public void onSortCompleteClickMenu(List<Exercise> data) {
        addDisposable(scheduling(mIExerciseListUseCase.updatePositions(data, 0))
                .andThen(onGetLastItemObs(0))
                .subscribe(exercises -> {
                    getViewState().showNormalModeList();
                    getViewState().clearExercise();
                    getViewState().addExercises(exercises);
                }));
    }

    public void onChangeListPosition(final int oldPosition, final int newPosition) {
        //ignored реализован более производительный и стабильный способ: отправка состояния списка при старте и завершении сортировки
    }

    public void onClickDelete(final Exercise exercise) {
        candidateDeleteExercise = exercise;
        getViewState().showDeleteExerciseConfirm();

    }
    public void onDeleteExerciseConfirmed() {
        if (candidateDeleteExercise == null)
            return;
        addDisposable(scheduling(mIExerciseListUseCase.deleteExercise(candidateDeleteExercise))
                .subscribe(() -> onGetDeletedExercise(candidateDeleteExercise)
                        , throwable -> showNotifyMessage(throwable, R.string.delete_exercise_failed)));
    }

    private void onGetDeletedExercise(final Exercise exercise) {
        getViewState().onDeletedComplexItem(exercise);
    }

    public void onOrderChanged() {
    }

    public void onAddClickMenu() {
        getViewState().showCreateExercise(mWeekDay, mComplexUuid);
    }

    public void onItemClick(final Exercise exercise) {
        getViewState().openExerciseDetail(exercise.getUuid());
    }

    public void onStartDrag(final Exercise exercise) {
        mDraggingExercise = exercise;
    }
}
