package com.alphabit.sportnotes.ui.screens.base;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.ui.screens.base.view.BaseMvpView;
import com.arellomobile.mvp.MvpAppCompatDialogFragment;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 26.11.2017.
 */

public abstract class BaseMvpDialogFragment extends MvpAppCompatDialogFragment implements BaseMvpView {

    @Override
    public void showLoadingIndicator() {

    }

    @Override
    public void hideLoadingIndicator() {

    }

    @Override
    public void refreshAdState() {

    }

    @Override
    public void showUserLevelError() {
        showNotifyMessage(getString(R.string.user_level_error));
    }
}
