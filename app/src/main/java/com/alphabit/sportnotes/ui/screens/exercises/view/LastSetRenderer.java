package com.alphabit.sportnotes.ui.screens.exercises.view;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.databinding.ItemExerciseLastSetsBinding;
import com.alphabit.sportnotes.timer.TimerPrinter;
import com.alphabit.sportnotes.ui.adapter.BaseRenderer;
import com.alphabit.sportnotes.ui.adapter.BaseHolder;
import com.alphabit.sportnotes.view.DatePrinter;

import java.util.List;

import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.SetSum;
import sportnotes.alphabit.com.business.model.SetTime;

public class LastSetRenderer extends BaseRenderer<LastSetRenderer.ViewHolder, Set> {

    private DatePrinter datePrinter;

    private boolean withWeight;
    private String weightUnit;


    public LastSetRenderer(DatePrinter datePrinter) {
        this.datePrinter = datePrinter;
    }

    @NonNull
    @Override
    public BaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_exercise_last_sets, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @NonNull List<Set> items, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public void onRecycled(ViewHolder holder) {

    }


    @Override
    public boolean isForViewType(@NonNull List items, int position) {
        return items.get(position) instanceof Set;
    }

    public void setDatePrinter(DatePrinter datePrinter) {
        this.datePrinter = datePrinter;
    }

    public void setWeight(boolean withWeight, String weightUnit) {
        this.withWeight = withWeight;
        this.weightUnit = weightUnit;
    }

    public class ViewHolder extends BaseHolder<Set, ItemExerciseLastSetsBinding> {

        public ViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bind(Set item) {
            if (item instanceof SetSum)
                bindSum((SetSum)item);
            else if (item instanceof SetTime)
                bindTime((SetTime)item);
        }

        private void bindTime(final SetTime item) {
            getBinding().value.setText(TimerPrinter.printWithoutMillis(item.getValue()));
            getBinding().date.setText(datePrinter.printLastSets(item.getCreatedAt()));
        }

        private void bindSum(final SetSum item) {
            StringBuilder value = new StringBuilder();
            value.append(item.getValue());
            if (withWeight)
                value.append(" ").append(weightUnit);
            getBinding().value.setText(value);

            getBinding().date.setText(datePrinter.printLastSets(item.getCreatedAt()));
        }

    }
}
