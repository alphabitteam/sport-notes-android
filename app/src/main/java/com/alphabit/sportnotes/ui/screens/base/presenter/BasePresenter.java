package com.alphabit.sportnotes.ui.screens.base.presenter;

import android.support.annotation.StringRes;
import android.util.Log;

import com.alphabit.sportnotes.ui.screens.base.view.BaseMvpView;
import com.alphabit.sportnotes.utils.SchedulersInjector;
import com.arellomobile.mvp.MvpPresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import sportnotes.alphabit.com.business.SyncEvent;

/**
 * Created by Fly on 27.08.2017.
 */

public abstract class BasePresenter<View extends BaseMvpView> extends MvpPresenter<View> {

    private final SchedulersInjector mSchedulersInjector;
    private boolean isLoading;

    private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public BasePresenter() {
        mSchedulersInjector = new SchedulersInjector();
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        EventBus.getDefault().register(this);
    }

    @Override
    public void attachView(View view) {
        super.attachView(view);
        getViewState().hideLoadingIndicator();
    }

    protected void addDisposable(Disposable disposable) {
        mCompositeDisposable.add(disposable);
    }

    protected <T> Single<T> scheduling(final Single<T> single) {
        return single.observeOn(mSchedulersInjector.getMainThread());
    }

    protected <T> Observable<T> scheduling(final Observable<T> observable) {
        return observable.observeOn(mSchedulersInjector.getMainThread());
    }

    protected Completable scheduling(final Completable completable) {
        return completable.observeOn(mSchedulersInjector.getMainThread());
    }

    protected <T> Flowable<T> scheduling(final Flowable<T> flowable) {
        return flowable.observeOn(mSchedulersInjector.getMainThread());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getAttachedViews().size() == 0)
            mCompositeDisposable.clear();
        EventBus.getDefault().unregister(this);
    }

    protected void clearObserve() {
        mCompositeDisposable.clear();
        onSyncEvent(new SyncEvent(SyncEvent.SyncStatus.END));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSyncEvent(SyncEvent event) {
        System.out.println("BasePresenter.onSyncEvent" + event.getSyncStatus());
        switch (event.getSyncStatus()) {
            case START:
                if (!isLoading)
                    getViewState().showLoadingIndicator();
                isLoading = true;
                break;
            case END:
                getViewState().hideLoadingIndicator();
                getViewState().refreshAdState();
                isLoading = false;
                break;
        }
    }

    @Override
    public void detachView(View view) {
        super.detachView(view);
        getViewState().hideLoadingIndicator();
    }

    protected void showThrowable(Throwable throwable) {
        Log.d("BasePresenter", "showNotifyMessage: " + throwable);
    }

    protected void showNotifyMessage(Throwable throwable, @StringRes int stringRes) {
        showThrowable(throwable);
        getViewState().showNotifyMessage(stringRes);
    }

    protected void showDialog(String title, String text) {

    }
}
