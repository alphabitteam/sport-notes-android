package com.alphabit.sportnotes.ui.adapter;

import android.support.v7.util.DiffUtil;

import java.util.ArrayList;
import java.util.List;

import sportnotes.alphabit.com.business.model.UuidModel;

/**
 * Created by fly12 on 03.12.2017.
 */
public class DiffCallback<M extends UuidModel> extends DiffUtil.Callback {

    private final List<M> mOldItems;
    private final List<M> mNewItems;

    public DiffCallback(List<M> mOldItems, List<M> mNewItems) {
        this.mOldItems = mOldItems;
        this.mNewItems = mNewItems;
    }

    @Override
    public int getOldListSize() {
        return mOldItems.size();
    }

    @Override
    public int getNewListSize() {
        return mNewItems.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldItems.get(oldItemPosition).getUuid().equals(mNewItems.get(newItemPosition).getUuid());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldItems.get(oldItemPosition).hashCode() == mNewItems.get(newItemPosition).hashCode();
    }
}
