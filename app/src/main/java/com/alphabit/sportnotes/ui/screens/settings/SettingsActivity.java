package com.alphabit.sportnotes.ui.screens.settings;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.Preference;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alphabit.sportnotes.App;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.ui.screens.base.BaseActivity;
import com.alphabit.sportnotes.view.customdialog.SimpleCustomDialog;
import com.codevscolor.materialpreference.activity.MaterialPreferenceActivity;
import com.codevscolor.materialpreference.callback.MaterialPreferenceCallback;
import com.codevscolor.materialpreference.util.MaterialPrefUtil;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.CompositePermissionListener;
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.karumi.dexter.listener.single.SnackbarOnDeniedPermissionListener;
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat;
import com.takisoft.fix.support.v7.preference.RingtonePreference;

import javax.inject.Inject;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import sportnotes.alphabit.com.business.usecase.contract.IEditExerciseUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISettingsUseCase;

import static com.codevscolor.materialpreference.util.MaterialPrefUtil.useDarkTheme;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 16.04.18.
 */
public class SettingsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        if (savedInstanceState == null)
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new MyPreferenceFragment())
                    .commit();
    }

    /** @Override
    public void init(@Nullable Bundle savedInstanceState) {
        //register this class as listener for preference change
        setPreferenceChangedListener(this);

        App.getAppComponent().inject(this);

        //use dark theme or not . Default is light theme
        useDarkTheme(true);

        //set toolbar title
        setToolbarTitle(getString(R.string.settings));

        //set primary color
        setPrimaryColor(MaterialPrefUtil.COLOR_BLUE_GREY);

        //default secondary color for tinting widgets, if no secondary color is used yet
        setDefaultSecondaryColor(this, MaterialPrefUtil.COLOR_BLUE_GREY);

        //set application package name and xml resource name of preference
        setAppPackageName(getPackageName());
        //set xml resource name
        setXmlResourceName("settingspreference");
    }**/

    public void onPreferenceSettingsChanged1(SharedPreferences sharedPreferences, String name) {
        System.out.println("SettingsActivity.onPreferenceSettingsChanged name = " + name);

    }

    @Override
    public boolean moveSharedPreferencesFrom(Context sourceContext, String name) {
        System.out.println("SettingsActivity.moveSharedPreferencesFrom = " + name);
        return super.moveSharedPreferencesFrom(sourceContext, name);
    }

    @Override
    public boolean deleteSharedPreferences(String name) {
        System.out.println("SettingsActivity.deleteSharedPreferences = " + name);
        return super.deleteSharedPreferences(name);
    }

    public static class MyPreferenceFragment extends PreferenceFragmentCompat {

        @Inject
        ISettingsUseCase useCase;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            App.getAppComponent().inject(this);

        }

        @Override
        public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.settingspreference, rootKey);

            Preference unit = findPreference("unit");
            if (unit != null) {
                unit.setOnPreferenceChangeListener((preference, newValue) -> {
                    useCase.saveWeightUnit((String) newValue)
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> {}, throwable -> {});
                    return true;
                });
            }

            RingtonePreference ringtone = (RingtonePreference) findPreference("ringtone");

            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                ringtone.setEnabled(false);
            }


            if (ringtone != null) {
                ringtone.setOnPreferenceChangeListener((preference, newValue) -> {
                    useCase.saveRingtone(String.valueOf(newValue))
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> {}, throwable -> {});
                    return true;
                });
            }
        }


        @Override
        protected void displayPreferenceDialog(@NonNull Fragment fragment, @NonNull String key) {
            super.displayPreferenceDialog(fragment, key);
            System.out.println("MyPreferenceFragment.displayPreferenceDialog");
        }

        @Override
        protected void displayPreferenceDialog(@NonNull Fragment fragment, @NonNull String key, @Nullable Bundle bundle) {
            super.displayPreferenceDialog(fragment, key, bundle);
            System.out.println("MyPreferenceFragment.displayPreferenceDialog with bundle");
        }
    }
}
