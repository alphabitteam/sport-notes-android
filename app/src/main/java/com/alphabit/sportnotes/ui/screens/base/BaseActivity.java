package com.alphabit.sportnotes.ui.screens.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alphabit.sportnotes.App;
import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.annotations.Layout;
import com.alphabit.sportnotes.notification.NotificationHandler;
import com.alphabit.sportnotes.notification.Notifications;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.lang.annotation.Annotation;

import javax.inject.Inject;

import butterknife.ButterKnife;
import sportnotes.alphabit.com.data.utils.UserLevelManager;

/**
 * Created by Fly on 27.08.2017.
 */

public abstract class BaseActivity<Binding extends ViewDataBinding> extends MvpAppCompatActivity {

    private Toolbar mToolbar;
    private TextView mToolbarTitle;
    private MenuItem indicatorMenuItem;
    private Binding mBinding;

    private Injector injector;

    private BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onNewIntent(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Class cls = getClass();
        if (!cls.isAnnotationPresent(Layout.class)) return;
        Annotation annotation = cls.getAnnotation(Layout.class);
        Layout layout = (Layout) annotation;
        mBinding = DataBindingUtil.setContentView(this, layout.id());
        ButterKnife.bind(this);

        injector = new Injector();

        setupToolbar();
        setTitle("");
        initView();
        onNewIntent(getIntent());
    }

    protected void initAd() {
        AdView adView = findViewById(R.id.adView);
        if (adView == null)
            return;
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    public void invalidateAd() {
        AdView adView = findViewById(R.id.adView);

        if (injector.getUserLevelManager().isFullVersion()) {
            adView.setVisibility(View.GONE);
            if (adView.getVideoController() != null)
                adView.getVideoController().pause();
            return;
        } else {
            adView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        new NotificationHandler(this).handle(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.setCurrentActivity(this);
        register(notificationReceiver, Notifications.EXERCISE_TIMER_END_ACTION);
        initAd();
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.setCurrentActivity(null);
        unregister(notificationReceiver);
    }

    protected abstract void initView();

    public void setupToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbarTitle = findViewById(R.id.toolbarTitle);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        indicatorMenuItem = menu.findItem(R.id.action_refresh);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public MenuItem getIndicatorMenuItem() {
        return indicatorMenuItem;
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public void setTitle(final int titleId) {
        if (mToolbar != null) {
            super.setTitle("");
            if (mToolbarTitle != null) {
                mToolbar.setTitle("");
                mToolbarTitle.setText(titleId);
            } else
                mToolbar.setTitle(titleId);
        } else super.setTitle(titleId);
    }

    @Override
    public void setTitle(final CharSequence title) {
        if (mToolbar != null) {
            super.setTitle("");
            mToolbar.setTitle(title);
        } else super.setTitle(title);
    }


    public final Binding getBinding() {
        return mBinding;
    }

    protected void register(BroadcastReceiver receiver, String... actions) {
        IntentFilter intentFilter = new IntentFilter();
        for (String action : actions) {
            intentFilter.addAction(action);
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,  intentFilter);
    }

    protected void unregister(BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    public static class Injector {

        @Inject
        UserLevelManager userLevelManager;

        public Injector() {
            App.getAppComponent().inject(this);
        }

        public UserLevelManager getUserLevelManager() {
            return userLevelManager;
        }
    }
}
