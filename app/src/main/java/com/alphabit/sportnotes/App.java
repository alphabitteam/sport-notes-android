package com.alphabit.sportnotes;

import android.app.Application;
import android.support.multidex.MultiDexApplication;
import com.alphabit.logsender.LogSender;
import com.alphabit.sportnotes.di.BaseComponent;
import com.alphabit.sportnotes.di.components.DaggerAppComponent;
import com.alphabit.sportnotes.di.components.DaggerAppMockComponent;
import com.alphabit.sportnotes.di.components.DaggerAppTestComponent;
import com.alphabit.sportnotes.di.modules.features.AppModule;
import com.alphabit.sportnotes.di.modules.features.RepositoryModule;
import com.alphabit.sportnotes.di.modules.features.RxModule;
import com.alphabit.sportnotes.di.modules.features.UseCaseModule;
import com.alphabit.sportnotes.di.modules.mock.RepositoryMockModule;
import com.alphabit.sportnotes.di.modules.test.RxTestModule;
import com.alphabit.sportnotes.ui.screens.base.BaseActivity;
import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.google.android.gms.ads.MobileAds;
import io.fabric.sdk.android.Fabric;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import sportnotes.alphabit.com.business.usecase.contract.ISynchronizationUseCase;
import net.danlew.android.joda.JodaTimeAndroid;
import javax.annotation.Nullable;
import javax.inject.Inject;

/**
 * Created by Fly on 27.08.2017.
 */

public class App extends MultiDexApplication {

    @Inject
    ISynchronizationUseCase useCase;

    Disposable syncD;

    private static BaseComponent appComponent;

    private static BaseActivity currentActivity;

    private static Application sApplication;

    public App() {
        sApplication = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        buildComponent();
        //buildMockComponent();
        //buildTestComponent();
        appComponent.inject(this);

        if (BuildConfig.DEBUG) {
            // Create an InitializerBuilder
            Stetho.InitializerBuilder initializerBuilder =
                    Stetho.newInitializerBuilder(this);

            // Enable Chrome DevTools
            initializerBuilder.enableWebKitInspector(
                    Stetho.defaultInspectorModulesProvider(this)
            );

            // Enable command line interface
            initializerBuilder.enableDumpapp(
                    Stetho.defaultDumperPluginsProvider(this)
            );

            // Use the InitializerBuilder to generate an Initializer
            Stetho.Initializer initializer = initializerBuilder.build();

            // Initialize Stetho with the Initializer
            Stetho.initialize(initializer);
        }
        JodaTimeAndroid.init(this);

        LogSender.init(this);

        syncD = useCase.synchronize()
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {}, throwable -> {});

        MobileAds.initialize(this, "ca-app-pub-4667278565281772~4609494043");
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        useCase.synchronize()
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {}, throwable -> {});
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (syncD != null && !syncD.isDisposed())
            syncD.dispose();
    }

    public static void buildComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(sApplication))
                .repositoryModule(new RepositoryModule())
                .rxModule(new RxModule())
                .useCaseModule(new UseCaseModule())
                .build();
    }

    public static void buildMockComponent() {
        appComponent = DaggerAppMockComponent.builder()
                .appModule(new AppModule(sApplication))
                .repositoryMockModule(new RepositoryMockModule())
                .rxModule(new RxModule())
                .useCaseModule(new UseCaseModule())
                .build();
    }

    public static void buildTestComponent() {
        appComponent = DaggerAppTestComponent.builder()
                .appModule(new AppModule(sApplication))
                .repositoryMockModule(new RepositoryMockModule())
                .rxTestModule(new RxTestModule())
                .useCaseModule(new UseCaseModule())
                .build();
    }

    public static @Nullable
    BaseActivity getCurrentActivity() {
        return currentActivity;
    }

    public static void setCurrentActivity(BaseActivity currentActivity) {
        App.currentActivity = currentActivity;
    }

    public static BaseComponent getAppComponent() {
        return appComponent;
    }
}

