package com.alphabit.sportnotes.utils;
import java.util.concurrent.TimeUnit;

/**
 * Created by fly12 on 19.11.2017.
 */

public class DateUtils {

    public static class Formatter {
        long millis;

        public Formatter(long millis) {
            this.millis = millis;
        }

        public int getMinutes() {
            return (int) TimeUnit.MILLISECONDS.toMinutes(millis);
        }

        public int getSeconds() {
            return (int) (TimeUnit.MILLISECONDS.toSeconds(millis) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        }
    }
}
