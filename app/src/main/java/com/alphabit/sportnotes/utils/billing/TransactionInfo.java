package com.alphabit.sportnotes.utils.billing;

/**
 * Created by fly12 on 19.11.2017.
 */

public class TransactionInfo {
    private final String productId;
    private final String purchaseToken;
    private final boolean autoRenewing;
    private final String developerPayload;

    public TransactionInfo(String productId, String purchaseToken, boolean autoRenewing, String developerPayload) {
        this.productId = productId;
        this.purchaseToken = purchaseToken;
        this.autoRenewing = autoRenewing;
        this.developerPayload = developerPayload;
    }

    public String getProductId() {
        return productId;
    }

    public String getPurchaseToken() {
        return purchaseToken;
    }

    public boolean isAutoRenewing() {
        return autoRenewing;
    }

    public String getDeveloperPayload() {
        return developerPayload;
    }

}
