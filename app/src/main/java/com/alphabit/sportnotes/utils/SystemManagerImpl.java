package com.alphabit.sportnotes.utils;

import org.joda.time.DateTime;

import sportnotes.alphabit.com.business.utils.SystemManager;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 24.12.17.
 */
public class SystemManagerImpl implements SystemManager {

    @Override
    public DateTime getCurrentTime() {
        return new DateTime(System.currentTimeMillis());
    }
}
