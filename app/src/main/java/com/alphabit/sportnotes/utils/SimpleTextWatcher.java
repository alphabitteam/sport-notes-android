package com.alphabit.sportnotes.utils;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Yuri Zigunov on 30.09.2017.
 */

public abstract class SimpleTextWatcher implements TextWatcher {
    @Override
    public final void beforeTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {

    }

    @Override
    public final void onTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {
        onChanged(charSequence);
    }

    @Override
    public final void afterTextChanged(final Editable editable) {

    }

    public abstract void onChanged(final CharSequence charSequence);
}
