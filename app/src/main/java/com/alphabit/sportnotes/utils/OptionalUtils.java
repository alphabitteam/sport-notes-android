package com.alphabit.sportnotes.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.annimon.stream.Optional;

/**
 * Created by Yuri Zigunov on 12.10.2017.
 */

public class OptionalUtils {

    @Nullable
    public static<T> T getObject(@NonNull Optional<T> optional) {
        return optional.isPresent() ? optional.get() : null;
    }
}
