package com.alphabit.sportnotes.utils;

import android.support.v7.util.DiffUtil;

import java.util.List;

import sportnotes.alphabit.com.business.model.UuidModel;

/**
 * Created by Yuri Zigunov on 22.10.2017.
 */

public class UuidItemDiffCallback<T extends UuidModel> extends DiffUtil.Callback {

    private List<T> oldList;
    private List<T> newList;

    public UuidItemDiffCallback() {
    }

    public UuidItemDiffCallback(final List<T> oldList, final List<T> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(final int oldItemPosition, final int newItemPosition) {
        final T oldItem = oldList.get(oldItemPosition);
        final T newItem = newList.get(newItemPosition);
        return oldItem.getUuid().equals(newItem.getUuid());
    }

    @Override
    public boolean areContentsTheSame(final int oldItemPosition, final int newItemPosition) {
        T oldItem = oldList.get(oldItemPosition);
        T newItem = newList.get(newItemPosition);
        return oldItem.getUuid().equals(newItem.getUuid());
    }
}
