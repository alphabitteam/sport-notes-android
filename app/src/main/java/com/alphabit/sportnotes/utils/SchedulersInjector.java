package com.alphabit.sportnotes.utils;


import com.alphabit.sportnotes.App;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;

/**
 * Created by Yuri Zigunov on 15.10.2017.
 */

public class SchedulersInjector {

    @Inject
    @Named("MainThreadScheduler")
    Scheduler mainThread;
    @Inject
    @Named("IOScheduler")
    Scheduler io;
    @Inject
    @Named("ComputationScheduler")
    Scheduler computation;

    public SchedulersInjector() {
        App.getAppComponent().inject(this);
    }

    public Scheduler getMainThread() {
        return mainThread;
    }

    public Scheduler getIo() {
        return io;
    }

    public Scheduler getComputation() {
        return computation;
    }
}