package com.alphabit.sportnotes.utils;

import android.util.Log;

import com.alphabit.logsender.LogSenderPublisher;
import com.alphabit.logsender.LogSenderRegister;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * Created by fly12 on 19.11.2017.
 */

public class LoggingInterceptor implements Interceptor {
    @Override
    public okhttp3.Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        long t1 = System.nanoTime();
        String requestLog = "method " + request.method() + "\n" + String.format("Sending request %s on %s%n%s",
                request.url(), chain.connection(), request.headers());
        //YLog.d(String.format("Sending request %s on %s%n%s",
        //        request.url(), chain.connection(), request.headers()));
        if (request.method().compareToIgnoreCase("post") == 0) {
            requestLog = "method " + request.method() + "\n" + requestLog + "\n" + bodyToString(request);
        }
        Log.d("networkLogging", "request" + "\n" + requestLog);
        LogSenderRegister.registerLog("networkLogging", "request" + "\n" + requestLog);

        okhttp3.Response response = chain.proceed(request);
        long t2 = System.nanoTime();

        String responseLog = String.format("Received response for %s in %.1fms%n%s code %d",
                response.request().url(), (t2 - t1) / 1e6d, response.headers(), response.code());

        String bodyString = response.body().string();

        Log.d("networkLogging", "response" + "\n" + responseLog + "\n" + bodyString);
        LogSenderRegister.registerLog("networkLogging", "response" + "\n" + responseLog + "\n" + bodyString);

        return response.newBuilder()
                .body(ResponseBody.create(response.body().contentType(), bodyString))
                .build();
    }

    public String bodyToString(final Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}