package com.alphabit.sportnotes.utils;

import android.content.Intent;
import android.os.Bundle;

/**
 * Created by fly12 on 13.11.2017.
 */

public class IntentUtils {

    public static String print(Intent intent) {
        Bundle bundle = intent.getExtras();
        StringBuilder sb = new StringBuilder();
        if (bundle != null) {
            for (String key : bundle.keySet()) {
                Object value = bundle.get(key);
                sb.append(String.format("%s %s (%s)", key,
                        value.toString(), value.getClass().getName())).append(" ");
            }
        }
        return sb.toString();
    }
}
