package com.alphabit.sportnotes.utils;

import android.util.Log;

import com.alphabit.logsender.LogSenderRegister;
import com.crashlytics.android.Crashlytics;

import sportnotes.alphabit.com.business.CrushLogger;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 23.12.17.
 */
public class CrushLoggerImpl implements CrushLogger {
    private static final String TAG = "CrushLogger";

    @Override
    public void logException(Throwable throwable) {
        Log.e(TAG, "logException: ", throwable);
        Crashlytics.logException(throwable);
        LogSenderRegister.registerLog("", throwable);
    }
}
