package com.alphabit.sportnotes.utils;

public interface Action<T> {
    void apply(T t);
}
