package com.alphabit.sportnotes.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.alphabit.sportnotes.App;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import sportnotes.alphabit.com.business.usecase.contract.ISynchronizationUseCase;
import sportnotes.alphabit.com.business.utils.IAutoSyncManager;
import sportnotes.alphabit.com.business.utils.SyncStateRegistrator;

/**
 * Created by fly12 on 03.12.2017.
 */
public class AutoSyncManager implements IAutoSyncManager {

    private static long DELAY = TimeUnit.MINUTES.toMillis(5);

    private final Context context;
    private final SyncStateRegistrator syncStateRegistrator;

    public AutoSyncManager(Context context, SyncStateRegistrator syncStateRegistrator) {
        this.context = context;
        this.syncStateRegistrator = syncStateRegistrator;
    }

    public void registerChange() {
        Log.d(getClass().getSimpleName(), "registerChange: ");
        syncStateRegistrator.dbChanged();
        removeAlarm();
        createAlarm();
    }

    private void createAlarm() {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SyncAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 2302, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + DELAY, pendingIntent);
    }

    private void removeAlarm() {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SyncAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 2302, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);
    }

    public static class SyncAlarmReceiver extends BroadcastReceiver {

        @Inject
        ISynchronizationUseCase synchronizationUseCase;

        public SyncAlarmReceiver() {
            App.getAppComponent().inject(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("AutoSyncManager", "onReceive: !!! = " + synchronizationUseCase);
            synchronizationUseCase.liteSynchronize()
                    .subscribe(() -> {}, throwable -> {});
        }
    }

}
