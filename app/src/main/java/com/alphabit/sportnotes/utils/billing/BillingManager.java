package com.alphabit.sportnotes.utils.billing;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.alphabit.sportnotes.utils.Action;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.subjects.PublishSubject;
import sportnotes.alphabit.com.business.model.PurchaseResponseData;
import sportnotes.alphabit.com.business.model.Purchases;

/**
 * Created by fly12 on 19.11.2017.
 */

public class BillingManager implements PurchasesUpdatedListener {
    // Default value of mBillingClientResponseCode until BillingManager was not yeat initialized
    public static final int BILLING_MANAGER_NOT_INITIALIZED  = -1;

    private static final String TAG = "BillingManager";

    /** A reference to BillingClient **/
    private BillingClient mBillingClient;

    /**
     * True if billing service is connected now.
     */
    private boolean mIsServiceConnected;

    private Set<BillingUpdatesListener> mBillingUpdatesListeners = new HashSet<>();

    private final Activity mActivity;

    //private final List<Purchase> mPurchases = new ArrayList<>();
    private final Map<String, Purchase> mPurchases = new HashMap<>();

    private Set<String> mTokensToBeConsumed;

    private int mBillingClientResponseCode = BILLING_MANAGER_NOT_INITIALIZED;

    private final PublishSubject<Boolean> updateEvents = PublishSubject.create();

    /* BASE_64_ENCODED_PUBLIC_KEY should be YOUR APPLICATION'S PUBLIC KEY
     * (that you got from the Google Play developer console). This is not your
     * developer public key, it's the *app-specific* public key.
     *
     * Instead of just storing the entire literal string here embedded in the
     * program,  construct the key at runtime from pieces or
     * use bit manipulation (for example, XOR with some other string) to hide
     * the actual key.  The key itself is not secret information, but we don't
     * want to make it easy for an attacker to replace the public key with one
     * of their own and then fake messages from the server.
     */
    private static final String BASE_64_ENCODED_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlxwWsbOjgOcAoQ0FBTfUtpFmKqgcer3fKIwDvAiafEMdhULaMPPyaaSmkgCIVyzKRgUEQFzPXQVtbWIChYHxjO1sLmArNBf0/acX8wmpSJmf1oXBShdQPGsPNSZ08kAvm5+CUmyaaRyMgVSvioyor5+lDlUu4YxpjK+FkFRT+JVIR8EP8imhXCrgs+t8W5xPNf6Rlgybp3cjZAR6iU3f7ubiSQ02ac6Q1cV9jV3yE7jct5Hud8LKcR7B9+qr7idKY1ujAI0BiANKSGtlblGvIaxuuXv49H3Vlo//5BC4tO48KD4aFXG2wNX8wKOP34xuCYGCI4B1f4+Tlj0SKjJ8xQIDAQAB";

    public boolean isSubscribed(String productId) {
        return findTransactionInfo(productId) != null;
    }

    public List<String> getPurchaseIds() {
        return Collections.list(Collections.enumeration(mPurchases.keySet()));
    }

    public String getDeveloperPayloadFrom(Purchase purchase) {
        try {
            return new JSONObject(purchase.getOriginalJson()).optString("developerPayload");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public PublishSubject<Boolean> getUpdateEvents() {
        return updateEvents;
    }

    public sportnotes.alphabit.com.business.model.Purchase findTransactionInfoByToken(String token) {
        for (Map.Entry<String, Purchase> item : mPurchases.entrySet()) {
            if (item.getValue().getPurchaseToken().equals(token))
                return convert(item.getValue());
        }
        return null;
    }

    /**
     * Listener to the updates that happen when purchases list was updated or consumption of the
     * item was finished
     */
    public interface BillingUpdatesListener {
        void onBillingClientSetupFinished();
        void onConsumeFinished(String token, @BillingClient.BillingResponse int result);
        void onBillingError(BillingResponse resultCode);

        void onPurchasesUpdated(List<sportnotes.alphabit.com.business.model.Purchase> list);
    }

    /**
     * Listener for the Billing client state to become connected
     */
    public interface ServiceConnectedListener {
        void onServiceConnected(@BillingClient.BillingResponse int resultCode);
    }

    public BillingManager(Activity activity) {
        mActivity = activity;
    }

    public void startClient(final BillingUpdatesListener updatesListener) {
        if (updatesListener != null)
            mBillingUpdatesListeners.add(updatesListener);
        if (mBillingClient != null) {
            return;
        }
        Log.d(TAG, "Creating Billing client.");
        mBillingClient = BillingClient.newBuilder(mActivity).setListener(this).build();
        Log.d(TAG, "Starting setup.");

        // Start setup. This is asynchronous and the specified listener will be called
        // once setup completes.
        // It also starts to report all the new purchases through onPurchasesUpdated() callback.
        startServiceConnection(new Runnable() {
            @Override
            public void run() {
                // Notifying the listener that billing client is ready
                call(BillingUpdatesListener::onBillingClientSetupFinished);
                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.d(TAG, "Setup successful. Querying inventory.");
                queryPurchases();
            }
        });
    }

    private void call(Action<BillingUpdatesListener> action) {
        updateEvents.onNext(true);
        for (BillingUpdatesListener listener : mBillingUpdatesListeners) {
            action.apply(listener);
        }
    }

    public void releaseClient(final BillingUpdatesListener updatesListener) {
        mBillingUpdatesListeners.remove(updatesListener);
    }

    public List<Purchase> getPurchaseList() {
        return new ArrayList<>(mPurchases.values());
    }

    sportnotes.alphabit.com.business.model.Purchase convert(Purchase src) {
        sportnotes.alphabit.com.business.model.Purchase purchase = new sportnotes.alphabit.com.business.model.Purchase();
        purchase.setOrderId(src.getOrderId());
        purchase.setSku(src.getSku());
        purchase.setPurchaseTime(src.getPurchaseTime());
        purchase.setPurchaseToken(src.getPurchaseToken());
        purchase.setOriginalJson(src.getOriginalJson());
        purchase.setSignature(src.getSignature());
        return purchase;
    }

    /**
     * Handle a callback that purchases were updated from the Billing library
     */
    @Override
    public void onPurchasesUpdated(int resultCode, List<Purchase> purchases) {
        if (resultCode == BillingClient.BillingResponse.OK) {
            for (Purchase purchase : purchases) {
                handlePurchase(purchase);
            }
            List<sportnotes.alphabit.com.business.model.Purchase> list = new ArrayList<>();
            List<Purchase> src = getPurchaseList();
            for (Purchase item : src) {
                list.add(convert(item));
            }
            call(billingUpdatesListener -> billingUpdatesListener.onPurchasesUpdated(list));
        } else if (resultCode == BillingClient.BillingResponse.USER_CANCELED) {
            Log.i(TAG, "onPurchasesUpdated() - user cancelled the purchase flow - skipping");
            onBillingError(resultCode);
        } else {
            Log.w(TAG, "onPurchasesUpdated() got unknown resultCode: " + resultCode);
            onBillingError(resultCode);
        }
    }

    public boolean isReady() {
        if (mBillingClient == null)
            return false;
        return mBillingClient.isReady();
    }

    private void onBillingError(@BillingClient.BillingResponse int resultCode) {
        call(billingUpdatesListener -> billingUpdatesListener.onBillingError(getBillingResponse(resultCode)));
    }

    /**
     * Start a purchase flow
     */
    public void initiatePurchaseFlow(final String skuId, final SkuType billingType) {
        initiatePurchaseFlow(skuId, null, billingType);
    }

    private @BillingClient.SkuType String getSkuType(SkuType skuType) {
        switch (skuType) {
            case SUBS: return BillingClient.SkuType.SUBS;
            case INAPP: return BillingClient.SkuType.INAPP;
            default: throw new RuntimeException("not found skuType = " + skuType);
        }
    }

    /**
     * Start a purchase or subscription replace flow
     */
    public void initiatePurchaseFlow(final String skuId, final ArrayList<String> oldSkus,
                                     final SkuType billingType) {
        Runnable purchaseFlowRequest = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Launching in-app purchase flow. Replace old SKU? " + (oldSkus != null));
                BillingFlowParams purchaseParams = BillingFlowParams.newBuilder()
                        .setSku(skuId).setType(getSkuType(billingType)).setOldSkus(oldSkus).build();
                mBillingClient.launchBillingFlow(mActivity, purchaseParams);
            }
        };

        executeServiceRequest(purchaseFlowRequest);
    }

    public Context getContext() {
        return mActivity;
    }

    /**
     * Clear the resources
     */
    public void destroy() {
        Log.d(TAG, "Destroying the manager.");

        if (mBillingClient != null && mBillingClient.isReady()) {
            mBillingClient.endConnection();
            mBillingClient = null;
        }
    }

    public void restart() {
        destroy();
        startClient(null);
    }

    public void querySkuDetailsAsync(@BillingClient.SkuType final String itemType, final List<String> skuList,
                                     final SkuDetailsResponseListener listener) {
        // Creating a runnable from the request to use it inside our connection retry policy below
        Runnable queryRequest = new Runnable() {
            @Override
            public void run() {
                // Query the purchase async
                SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                params.setSkusList(skuList).setType(itemType);
                mBillingClient.querySkuDetailsAsync(params.build(),
                        new SkuDetailsResponseListener() {
                            @Override
                            public void onSkuDetailsResponse(int responseCode,
                                                             List<SkuDetails> skuDetailsList) {
                                listener.onSkuDetailsResponse(responseCode, skuDetailsList);
                            }
                        });
            }
        };

        executeServiceRequest(queryRequest);
    }

    public void consumeAsync(final String purchaseToken) {
        // If we've already scheduled to consume this token - no action is needed (this could happen
        // if you received the token when querying purchases inside onReceive() and later from
        // onActivityResult()
        if (mTokensToBeConsumed == null) {
            mTokensToBeConsumed = new HashSet<>();
        } else if (mTokensToBeConsumed.contains(purchaseToken)) {
            Log.i(TAG, "Token was already scheduled to be consumed - skipping...");
            return;
        }
        mTokensToBeConsumed.add(purchaseToken);

        // Generating Consume Response listener
        final ConsumeResponseListener onConsumeListener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(@BillingClient.BillingResponse int responseCode, String purchaseToken) {
                // If billing service was disconnected, we try to reconnect 1 time
                // (feel free to introduce your retry policy here).
                call(billingUpdatesListener -> billingUpdatesListener.onConsumeFinished(purchaseToken, responseCode));
            }
        };

        // Creating a runnable from the request to use it inside our connection retry policy below
        Runnable consumeRequest = new Runnable() {
            @Override
            public void run() {
                // Consume the purchase async
                mBillingClient.consumeAsync(purchaseToken, onConsumeListener);
            }
        };

        executeServiceRequest(consumeRequest);
    }

    /**
     * Returns the value Billing client response code or BILLING_MANAGER_NOT_INITIALIZED if the
     * clien connection response was not received yet.
     */
    public int getBillingClientResponseCode() {
        return mBillingClientResponseCode;
    }

    /**
     * Handles the purchase
     * <p>Note: Notice that for each purchase, we check if signature is valid on the client.
     * It's recommended to move this check into your backend.
     * See {@link Security#verifyPurchase(String, String, String)}
     * </p>
     * @param purchase Purchase to be handled
     */
    private void handlePurchase(Purchase purchase) {
        if (!verifyValidSignature(purchase.getOriginalJson(), purchase.getSignature())) {
            Log.i(TAG, "Got a purchase: " + purchase + "; but signature is bad. Skipping...");
            return;
        }

        Log.d(TAG, "Got a verified purchase: " + purchase);

        mPurchases.put(purchase.getOrderId(), purchase);
    }

    public TransactionInfo findTransactionInfo(String productId) {
        for (Map.Entry<String, Purchase> purchase : mPurchases.entrySet()) {
            if (purchase.getKey().equals(productId)) {
                return new TransactionInfo(purchase.getKey(), purchase.getValue().getPurchaseToken(),
                        purchase.getValue().isAutoRenewing(), getDeveloperPayloadFrom(purchase.getValue()));
            }
        }
        return null;
    }

    /**
     * Handle a result from querying of purchases and report an updated list to the listener
     */
    private void onQueryPurchasesFinished(Purchase.PurchasesResult result) {
        // Have we been disposed of in the meantime? If so, or bad result code, then quit
        if (mBillingClient == null || result.getResponseCode() != BillingClient.BillingResponse.OK) {
            Log.w(TAG, "Billing client was null or result code (" + result.getResponseCode()
                    + ") was bad - quitting");
            return;
        }

        Log.d(TAG, "Query inventory was successful.");

        // Update the UI and purchases inventory with new list of purchases
        mPurchases.clear();
        onPurchasesUpdated(BillingClient.BillingResponse.OK, result.getPurchasesList());
    }

    /**
     * Checks if subscriptions are supported for current client
     * <p>Note: This method does not automatically retry for RESULT_SERVICE_DISCONNECTED.
     * It is only used in unit tests and after queryPurchases execution, which already has
     * a retry-mechanism implemented.
     * </p>
     */
    public boolean areSubscriptionsSupported() {
        int responseCode = mBillingClient.isFeatureSupported(BillingClient.FeatureType.SUBSCRIPTIONS);
        if (responseCode != BillingClient.BillingResponse.OK) {
            Log.w(TAG, "areSubscriptionsSupported() got an error response: " + responseCode);
        }
        return responseCode == BillingClient.BillingResponse.OK;
    }

    /**
     * Query purchases across various use cases and deliver the result in a formalized way through
     * a listener
     */
    public void queryPurchases() {
        Runnable queryToExecute = new Runnable() {
            @Override
            public void run() {
                long time = System.currentTimeMillis();
                Purchase.PurchasesResult purchasesResult = mBillingClient.queryPurchases(BillingClient.SkuType.INAPP);
                Log.i(TAG, "Querying purchases elapsed time: " + (System.currentTimeMillis() - time)
                        + "ms");
                // If there are subscriptions supported, we add subscription rows as well
                if (areSubscriptionsSupported()) {
                    Purchase.PurchasesResult subscriptionResult
                            = mBillingClient.queryPurchases(BillingClient.SkuType.SUBS);
                    Log.i(TAG, "Querying purchases and subscriptions elapsed time: "
                            + (System.currentTimeMillis() - time) + "ms");
                    Log.i(TAG, "Querying subscriptions result code: "
                            + subscriptionResult.getResponseCode()
                            + " res: " + subscriptionResult.getPurchasesList().size());

                    if (subscriptionResult.getResponseCode() == BillingClient.BillingResponse.OK) {
                        purchasesResult.getPurchasesList().addAll(
                                subscriptionResult.getPurchasesList());
                    } else {
                        Log.e(TAG, "Got an error response trying to query subscription purchases", new RuntimeException());
                    }
                } else if (purchasesResult.getResponseCode() == BillingClient.BillingResponse.OK) {
                    Log.i(TAG, "Skipped subscription purchases query since they are not supported");
                } else {
                    Log.w(TAG, "queryPurchases() got an error response code: "
                            + purchasesResult.getResponseCode());
                }
                onQueryPurchasesFinished(purchasesResult);
            }
        };

        executeServiceRequest(queryToExecute);
    }

    public void startServiceConnection(final Runnable executeOnSuccess) {
        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
                Log.d(TAG, "Setup finished. Response code: " + billingResponseCode);

                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    mIsServiceConnected = true;
                    if (executeOnSuccess != null) {
                        executeOnSuccess.run();
                    }
                }
                mBillingClientResponseCode = billingResponseCode;
            }

            @Override
            public void onBillingServiceDisconnected() {
                mIsServiceConnected = false;
            }
        });
    }

    private void executeServiceRequest(Runnable runnable) {
        if (mIsServiceConnected) {
            runnable.run();
        } else {
            // If billing service was disconnected, we try to reconnect 1 time.
            // (feel free to introduce your retry policy here).
            startServiceConnection(runnable);
        }
    }

    /**
     * Verifies that the purchase was signed correctly for this developer's public key.
     * <p>Note: It's strongly recommended to perform such check on your backend since hackers can
     * replace this method with "constant true" if they decompile/rebuild your app.
     * </p>
     */
    private boolean verifyValidSignature(String signedData, String signature) {
        // Some sanity checks to see if the developer (that's you!) really followed the
        // instructions to run this sample (don't put these checks on your app!)
        if (BASE_64_ENCODED_PUBLIC_KEY.contains("CONSTRUCT_YOUR")) {
            throw new RuntimeException("Please update your app's public key at: "
                    + "BASE_64_ENCODED_PUBLIC_KEY");
        }

        try {
            return Security.verifyPurchase(BASE_64_ENCODED_PUBLIC_KEY, signedData, signature);
        } catch (IOException e) {
            Log.e(TAG, "Got an exception trying to validate a purchase: ", e);
            return false;
        }
    }

    private BillingResponse getBillingResponse(@BillingClient.BillingResponse int code) {
        switch (code) {
            case BillingClient.BillingResponse.OK: return BillingResponse.OK;
            case BillingClient.BillingResponse.FEATURE_NOT_SUPPORTED: return BillingResponse.FEATURE_NOT_SUPPORTED;
            case BillingClient.BillingResponse.SERVICE_DISCONNECTED: return BillingResponse.SERVICE_DISCONNECTED;
            case BillingClient.BillingResponse.USER_CANCELED: return BillingResponse.USER_CANCELED;
            case BillingClient.BillingResponse.SERVICE_UNAVAILABLE: return BillingResponse.SERVICE_UNAVAILABLE;
            case BillingClient.BillingResponse.BILLING_UNAVAILABLE: return BillingResponse.BILLING_UNAVAILABLE;
            case BillingClient.BillingResponse.ITEM_UNAVAILABLE: return BillingResponse.ITEM_UNAVAILABLE;
            case BillingClient.BillingResponse.DEVELOPER_ERROR: return BillingResponse.DEVELOPER_ERROR;
            case BillingClient.BillingResponse.ERROR: return BillingResponse.ERROR;
            case BillingClient.BillingResponse.ITEM_ALREADY_OWNED: return BillingResponse.ITEM_ALREADY_OWNED;
            case BillingClient.BillingResponse.ITEM_NOT_OWNED: return BillingResponse.ITEM_NOT_OWNED;
            default: throw new RuntimeException("not found billing responce code = " + code);
        }
    }

    public enum SkuType {
        INAPP, SUBS
    }

    public enum BillingResponse {
        FEATURE_NOT_SUPPORTED, SERVICE_DISCONNECTED, OK, USER_CANCELED, SERVICE_UNAVAILABLE,
        BILLING_UNAVAILABLE, ITEM_UNAVAILABLE, DEVELOPER_ERROR, ERROR, ITEM_ALREADY_OWNED,
        ITEM_NOT_OWNED
    }
}