package com.alphabit.sportnotes.utils;

/**
 * Created by Yuri Zigunov on 01.10.2017.
 */

public class NumberUtils {

    private static NumberUtils instance;

    public static void newInstance(NumberUtils instance) {
        instance = new NumberUtils();
    }

    public static NumberUtils getInstance() {
        return instance;
    }

    public static boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }

    public static String zeroPrefix(Number value) {
        if (value == null)
            return "00";
        if (value.intValue() < 10)
            return "0" + value;
        return String.valueOf(value);
    }

}