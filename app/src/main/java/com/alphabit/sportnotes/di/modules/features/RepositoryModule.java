package com.alphabit.sportnotes.di.modules.features;

import android.content.Context;

import com.alphabit.sportnotes.di.modules.contract.IRepositoryModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import sportnotes.alphabit.com.business.repository.IAuthorizationRepository;
import sportnotes.alphabit.com.business.repository.IBillingRepository;
import sportnotes.alphabit.com.business.repository.IComplexesNetworkRepository;
import sportnotes.alphabit.com.business.repository.IComplexesRepository;
import sportnotes.alphabit.com.business.repository.IExercisesNetworkRepository;
import sportnotes.alphabit.com.business.repository.IExercisesRepository;
import sportnotes.alphabit.com.business.repository.IPreferenceRepository;
import sportnotes.alphabit.com.business.repository.ISetsNetworkRepository;
import sportnotes.alphabit.com.business.repository.ISetsRepository;
import sportnotes.alphabit.com.business.repository.ISyncRepository;
import sportnotes.alphabit.com.data.repository.AuthorizationRepository;
import sportnotes.alphabit.com.data.repository.BillingRepository;
import sportnotes.alphabit.com.data.repository.ComplexesNetworkRepository;
import sportnotes.alphabit.com.data.repository.ComplexesRepository;
import sportnotes.alphabit.com.data.repository.ExercisesNetworkRepository;
import sportnotes.alphabit.com.data.repository.ExercisesRepository;
import sportnotes.alphabit.com.data.repository.PreferenceRepository;
import sportnotes.alphabit.com.data.repository.SetsNetworkRepository;
import sportnotes.alphabit.com.data.repository.SetsRepository;
import sportnotes.alphabit.com.data.repository.SyncRepository;
import sportnotes.alphabit.com.data.storages.alarm.AlarmStorage;
import sportnotes.alphabit.com.data.storages.auth.AuthLocalStorage;
import sportnotes.alphabit.com.data.storages.auth.AuthNetworkStorage;
import sportnotes.alphabit.com.data.storages.base.cores.PreferenceCore;
import sportnotes.alphabit.com.data.storages.complex.ComplexLocalStorage;
import sportnotes.alphabit.com.data.storages.complex.ComplexLocalSyncStorage;
import sportnotes.alphabit.com.data.storages.complex.ComplexNetworkStorage;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseLocalStorage;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseLocalSyncStorage;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseNetworkStorage;
import sportnotes.alphabit.com.data.storages.purchase.PurchaseNetworkStorage;
import sportnotes.alphabit.com.data.storages.sets.SetLocalSyncStorage;
import sportnotes.alphabit.com.data.storages.sets.SetsLocalStorage;
import sportnotes.alphabit.com.data.storages.sets.SetsNetworkStorage;
import sportnotes.alphabit.com.data.storages.settings.SettingsStorage;
import sportnotes.alphabit.com.data.utils.UserLevelManager;

/**
 * Created by Fly on 29.08.2017.
 */
@Singleton
@Module
public class RepositoryModule implements IRepositoryModule {

    @Provides
    @Singleton
    @Override
    public IAuthorizationRepository provideAuthorizationRepository(AuthLocalStorage localStorage, AuthNetworkStorage networkStorage) {
        return new AuthorizationRepository(localStorage, networkStorage);
    }

    @Provides
    @Singleton
    @Override
    public IComplexesRepository provideComplexesRepository(ComplexLocalStorage storage,
                                                           SettingsStorage settingsStorage,
                                                           ExerciseLocalStorage exerciseLocalStorage,
                                                           UserLevelManager userLevelManager) {
        return new ComplexesRepository(storage, settingsStorage, exerciseLocalStorage, userLevelManager);
    }

    @Provides
    @Singleton
    @Override
    public IExercisesRepository provideIExercisesRepository(Context context, ExerciseLocalStorage storage, SetsLocalStorage setsLocalStorage,
                                                            UserLevelManager userLevelManager) {
        return new ExercisesRepository(context, storage, setsLocalStorage, userLevelManager);
    }

    @Provides
    @Singleton
    @Override
    public IPreferenceRepository provideIPreferenceRepository(AlarmStorage alarmStorage,
                                                              SettingsStorage settingsStorage) {
        return new PreferenceRepository(alarmStorage, settingsStorage);
    }

    @Provides
    @Singleton
    @Override
    public ISetsRepository provideISetsRepository(SetsLocalStorage storage, UserLevelManager userLevelManager) {
        return new SetsRepository(storage, userLevelManager);
    }

    @Provides
    @Singleton
    @Override
    public IComplexesNetworkRepository provideIComplexesNetworkRepository(final ComplexNetworkStorage storage) {
        return new ComplexesNetworkRepository(storage);
    }

    @Provides
    @Singleton
    @Override
    public IExercisesNetworkRepository provideIExercisesNetworkRepository(final ExerciseNetworkStorage storage) {
        return new ExercisesNetworkRepository(storage);
    }

    @Provides
    @Singleton
    @Override
    public ISetsNetworkRepository provideISetsNetworkRepository(final SetsNetworkStorage storage) {
        return new SetsNetworkRepository(storage);
    }

    @Override
    @Provides
    @Singleton
    public IBillingRepository provideBillingRepository(PurchaseNetworkStorage networkStorage, PreferenceCore core) {
        return new BillingRepository(networkStorage, core);
    }

    @Override
    @Provides
    @Singleton
    public ISyncRepository provideISyncRepository(ComplexLocalSyncStorage complexLocalSyncStorage,
                                                  ExerciseLocalSyncStorage exerciseLocalSyncStorage,
                                                  SetLocalSyncStorage setLocalSyncStorage, UserLevelManager userLevelManager) {
        return new SyncRepository(complexLocalSyncStorage, exerciseLocalSyncStorage, setLocalSyncStorage, userLevelManager);
    }
}
