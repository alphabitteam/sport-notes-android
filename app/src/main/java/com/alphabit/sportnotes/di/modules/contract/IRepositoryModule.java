package com.alphabit.sportnotes.di.modules.contract;

import android.content.Context;

import sportnotes.alphabit.com.business.repository.IAuthorizationRepository;
import sportnotes.alphabit.com.business.repository.IBillingRepository;
import sportnotes.alphabit.com.business.repository.IComplexesNetworkRepository;
import sportnotes.alphabit.com.business.repository.IComplexesRepository;
import sportnotes.alphabit.com.business.repository.IExercisesNetworkRepository;
import sportnotes.alphabit.com.business.repository.IExercisesRepository;
import sportnotes.alphabit.com.business.repository.IPreferenceRepository;
import sportnotes.alphabit.com.business.repository.ISetsNetworkRepository;
import sportnotes.alphabit.com.business.repository.ISetsRepository;
import sportnotes.alphabit.com.business.repository.ISyncRepository;
import sportnotes.alphabit.com.data.storages.alarm.AlarmStorage;
import sportnotes.alphabit.com.data.storages.auth.AuthLocalStorage;
import sportnotes.alphabit.com.data.storages.auth.AuthNetworkStorage;
import sportnotes.alphabit.com.data.storages.base.cores.PreferenceCore;
import sportnotes.alphabit.com.data.storages.complex.ComplexLocalStorage;
import sportnotes.alphabit.com.data.storages.complex.ComplexLocalSyncStorage;
import sportnotes.alphabit.com.data.storages.complex.ComplexNetworkStorage;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseLocalStorage;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseLocalSyncStorage;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseNetworkStorage;
import sportnotes.alphabit.com.data.storages.purchase.PurchaseNetworkStorage;
import sportnotes.alphabit.com.data.storages.sets.SetLocalSyncStorage;
import sportnotes.alphabit.com.data.storages.sets.SetsLocalStorage;
import sportnotes.alphabit.com.data.storages.sets.SetsNetworkStorage;
import sportnotes.alphabit.com.data.storages.settings.SettingsStorage;
import sportnotes.alphabit.com.data.utils.UserLevelManager;

/**
 * Created by Fly on 29.08.2017.
 */

public interface IRepositoryModule {

    IAuthorizationRepository provideAuthorizationRepository(AuthLocalStorage localStorage, AuthNetworkStorage networkStorage);

    IComplexesRepository provideComplexesRepository(ComplexLocalStorage storage, SettingsStorage settingsStorage, ExerciseLocalStorage exerciseLocalStorage, UserLevelManager userLevelManager);

    IExercisesRepository provideIExercisesRepository(Context context, ExerciseLocalStorage storage, SetsLocalStorage setsLocalStorage, UserLevelManager userLevelManager);

    IPreferenceRepository provideIPreferenceRepository(AlarmStorage alarmStorage,
                                                       SettingsStorage settingsStorage);

    ISetsRepository provideISetsRepository(SetsLocalStorage storage, UserLevelManager userLevelManager);

    IComplexesNetworkRepository provideIComplexesNetworkRepository(ComplexNetworkStorage storage);

    IExercisesNetworkRepository provideIExercisesNetworkRepository(ExerciseNetworkStorage storage);

    ISetsNetworkRepository provideISetsNetworkRepository(SetsNetworkStorage storage);

    IBillingRepository provideBillingRepository(PurchaseNetworkStorage networkStorage, PreferenceCore core);

    ISyncRepository provideISyncRepository(ComplexLocalSyncStorage complexLocalSyncStorage, ExerciseLocalSyncStorage exerciseLocalSyncStorage,
                                           SetLocalSyncStorage setLocalSyncStorage, UserLevelManager userLevelManager);

}
