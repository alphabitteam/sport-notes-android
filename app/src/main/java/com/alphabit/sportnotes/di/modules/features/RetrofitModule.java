package com.alphabit.sportnotes.di.modules.features;

import sportnotes.alphabit.com.data.utils.JodoDateTimeConverter;

import com.alphabit.sportnotes.BuildConfig;
import com.alphabit.sportnotes.utils.LoggingInterceptor;
import com.alphabit.sportnotes.utils.OptionalUtils;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Authenticator;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import sportnotes.alphabit.com.business.model.Provider;
import sportnotes.alphabit.com.data.Configuration;
import sportnotes.alphabit.com.data.api.ServerApi;
import sportnotes.alphabit.com.data.api.dto.AuthDto;
import sportnotes.alphabit.com.data.api.results.AuthResult;
import sportnotes.alphabit.com.data.storages.auth.AuthLocalStorage;
import sportnotes.alphabit.com.data.storages.auth.AuthNetworkStorage;

/**
 * Created by Yuri Zigunov on 22.10.2017.
 */
@Module
public class RetrofitModule {

    @Provides
    @Singleton
    public ServerApi provideAuthorizationUseCase(Retrofit retrofit, AuthNetworkStorage networkStorage) {
        ServerApi serverApi = retrofit.create(ServerApi.class);
        networkStorage.setServerApi(serverApi);
        return serverApi;
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(Retrofit.Builder builder) {
        return builder.baseUrl(String.valueOf(Configuration.HOST)).build();
    }

    @Provides
    @Singleton
    @SuppressWarnings("ThrowableNotThrown")
    public Authenticator provideAuthenticator(AuthLocalStorage localStorage, AuthNetworkStorage networkStorage) {
        return (route, response) -> {
            //retoken

            AuthDto auth = OptionalUtils.getObject(localStorage.getAuth().blockingGet());
            if (auth == null) {
                return null;
            }

            String refreshToken = auth.getRefreshToken();

            AuthResult responce = networkStorage.retoken(String.valueOf(Provider.G), refreshToken).blockingGet();

            if (responce != null && responce.getResult() != null) {
                String token = responce.getResult().getAccessToken();
                localStorage.setAuth(responce.getResult()).blockingGet();

                return response.request().newBuilder()
                        .header("Authorization", "Bearer " + token)
                        .build();
            }
            return null;
        };
    }

    @Provides
    @Singleton
    public List<Interceptor> provideInterceptors(AuthLocalStorage localStorage) {
        List<Interceptor> interceptors = new ArrayList<>();

        if (BuildConfig.DEBUG)
            interceptors.add(new StethoInterceptor());
        interceptors.add(chain -> {
            Request.Builder request = chain.request().newBuilder();
            request.addHeader("Host", "sport-note.dev");
            AuthDto auth = OptionalUtils.getObject(localStorage.getAuth().blockingGet());
            if (auth != null && !chain.request().headers().names().contains("Authorization"))
                request.addHeader("Authorization", "Bearer " + auth.getAccessToken());
            return chain.proceed(request.build());
        });
        return interceptors;
    }

    @Provides
    @Singleton
    public Retrofit.Builder provideRetrofitBuilder(Converter.Factory converterFactory, Authenticator authenticator, List<Interceptor> interceptors) {

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder()
                .authenticator(authenticator);

        for (Interceptor interceptor : interceptors) {
            builder.addInterceptor(interceptor);
        }
        builder.addNetworkInterceptor(new LoggingInterceptor());

        OkHttpClient okHttpClient = builder.build();

        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(converterFactory)
                .client(okHttpClient);

    }

    @Provides
    @Singleton
    public Converter.Factory provideConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .registerTypeAdapter(DateTime.class, new JodoDateTimeConverter())
                .setLenient()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setFieldNamingStrategy(new CustomFieldNamingPolicy())
                .setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
    }

    private static class CustomFieldNamingPolicy implements FieldNamingStrategy {
        @Override
        public String translateName(Field field) {
            String name = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES.translateName(field);
            name = name.substring(2, name.length()).toLowerCase();
            return name;
        }
    }
}
