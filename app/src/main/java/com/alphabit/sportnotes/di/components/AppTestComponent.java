package com.alphabit.sportnotes.di.components;

import com.alphabit.sportnotes.di.BaseComponent;
import com.alphabit.sportnotes.di.modules.features.AppModule;
import com.alphabit.sportnotes.di.modules.features.RetrofitModule;
import com.alphabit.sportnotes.di.modules.features.StorageModule;
import com.alphabit.sportnotes.di.modules.features.UseCaseModule;
import com.alphabit.sportnotes.di.modules.mock.RepositoryMockModule;
import com.alphabit.sportnotes.di.modules.test.RxTestModule;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Fly on 27.08.2017.
 */
@Singleton
@Component(modules = {AppModule.class, RxTestModule.class,
        RepositoryMockModule.class, UseCaseModule.class, StorageModule.class,
        RetrofitModule.class})
public interface AppTestComponent extends BaseComponent {

}
