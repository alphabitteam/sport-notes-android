package com.alphabit.sportnotes.di;

import android.databinding.ViewDataBinding;

import com.alphabit.sportnotes.App;
import com.alphabit.sportnotes.notification.NotificationPresenter;
import com.alphabit.sportnotes.timer.AlarmReceiver;
import com.alphabit.sportnotes.ui.screens.aboutpremium.presenter.AboutPremiumPresenter;
import com.alphabit.sportnotes.ui.screens.base.BaseActivity;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.alphabit.sportnotes.ui.screens.complex.presenter.ComplexScreenPresenter;
import com.alphabit.sportnotes.ui.screens.editexercise.presenter.EditExercisePresenter;
import com.alphabit.sportnotes.ui.screens.exercisedetail.presenter.ExerciseDetailPresenter;
import com.alphabit.sportnotes.ui.screens.exercises.presenter.ExercisesPresenter;
import com.alphabit.sportnotes.ui.screens.navigation.presenter.NavigationPresenter;
import com.alphabit.sportnotes.ui.screens.sets.presenter.SetsPresenter;
import com.alphabit.sportnotes.ui.screens.settings.SettingsActivity;
import com.alphabit.sportnotes.utils.AutoSyncManager;
import com.alphabit.sportnotes.utils.SchedulersInjector;

/**
 * Created by Fly on 27.08.2017.
 */

public interface BaseComponent {

    void inject(ComplexScreenPresenter complexScreenPresenter);

    void inject(ExercisesPresenter exercisesPresenter);

    void inject(EditExercisePresenter editExercisePresenter);

    void inject(ExerciseDetailPresenter exerciseDetailPresenter);

    void inject(SetsPresenter setsPresenter);

    void inject(SchedulersInjector schedulersInjector);

    void inject(NavigationPresenter navigationPresenter);

    void inject(NotificationPresenter notificationPresenter);

    void inject(AlarmReceiver alarmReceiver);

    void inject(AboutPremiumPresenter aboutPremiumPresenter);

    void inject(AutoSyncManager.SyncAlarmReceiver syncAlarmReceiver);

    void inject(App app);

    void inject(BaseActivity.Injector injector);

    void inject(SettingsActivity.MyPreferenceFragment myPreferenceFragment);
}
