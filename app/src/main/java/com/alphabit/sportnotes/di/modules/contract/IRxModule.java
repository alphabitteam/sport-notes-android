package com.alphabit.sportnotes.di.modules.contract;

import io.reactivex.Scheduler;

/**
 * Created by Fly on 29.08.2017.
 */

public interface IRxModule {
    Scheduler provideMainThreadScheduler();

    Scheduler provideIOScheduler();

    Scheduler provideComputationScheduler();
}
