package com.alphabit.sportnotes.di.modules.features;

import com.alphabit.sportnotes.di.modules.contract.IRxModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Fly on 27.08.2017.
 */
@Singleton
@Module
public class RxModule implements IRxModule {

    @Provides
    @Singleton
    @Named("MainThreadScheduler")
    public Scheduler provideMainThreadScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    @Named("IOScheduler")
    public Scheduler provideIOScheduler() {
        return Schedulers.io();
    }

    @Provides
    @Singleton
    @Named("ComputationScheduler")
    public Scheduler provideComputationScheduler() {
        return Schedulers.computation();
    }
}
