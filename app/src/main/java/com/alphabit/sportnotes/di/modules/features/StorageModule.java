package com.alphabit.sportnotes.di.modules.features;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import sportnotes.alphabit.com.data.api.ServerApi;
import sportnotes.alphabit.com.data.storages.alarm.AlarmStorage;
import sportnotes.alphabit.com.data.storages.alarm.AlarmStorageImpl;
import sportnotes.alphabit.com.data.storages.auth.AuthLocalStorage;
import sportnotes.alphabit.com.data.storages.auth.AuthLocalStorageImpl;
import sportnotes.alphabit.com.data.storages.auth.AuthNetworkStorage;
import sportnotes.alphabit.com.data.storages.auth.AuthNetworkStorageImpl;
import sportnotes.alphabit.com.data.storages.base.cores.PreferenceCore;
import sportnotes.alphabit.com.data.storages.complex.ComplexLocalStorage;
import sportnotes.alphabit.com.data.storages.complex.ComplexLocalStorageImpl;
import sportnotes.alphabit.com.data.storages.complex.ComplexLocalSyncStorage;
import sportnotes.alphabit.com.data.storages.complex.ComplexLocalSyncStorageImpl;
import sportnotes.alphabit.com.data.storages.complex.ComplexNetworkStorage;
import sportnotes.alphabit.com.data.storages.complex.ComplexNetworkStorageImpl;
import sportnotes.alphabit.com.data.storages.database.DatabaseCore;
import sportnotes.alphabit.com.data.storages.database.IDatabaseCore;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseLocalStorage;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseLocalStorageImpl;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseLocalSyncStorage;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseLocalSyncStorageImpl;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseNetworkStorage;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseNetworkStorageImpl;
import sportnotes.alphabit.com.data.storages.purchase.PurchaseNetworkStorage;
import sportnotes.alphabit.com.data.storages.purchase.PurchaseNetworkStorageImpl;
import sportnotes.alphabit.com.data.storages.sets.SetLocalSyncStorage;
import sportnotes.alphabit.com.data.storages.sets.SetLocalSyncStorageImpl;
import sportnotes.alphabit.com.data.storages.sets.SetsLocalStorage;
import sportnotes.alphabit.com.data.storages.sets.SetsLocalStorageImpl;
import sportnotes.alphabit.com.data.storages.sets.SetsNetworkStorage;
import sportnotes.alphabit.com.data.storages.sets.SetsNetworkStorageImpl;
import sportnotes.alphabit.com.data.storages.settings.SettingStorageImpl;
import sportnotes.alphabit.com.data.storages.settings.SettingsStorage;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */
@Module
public class StorageModule {
    @Provides
    @Singleton
    IDatabaseCore provideDatabaseCore(Context context) {
        return new DatabaseCore(context);
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Provides
    @Singleton
    AuthLocalStorage provideAuthLocalStorage(SharedPreferences sharedPreferences) {
        return new AuthLocalStorageImpl(sharedPreferences);
    }

    @Provides
    @Singleton
    AuthNetworkStorage provideAuthNetworkStorage() {
        return new AuthNetworkStorageImpl();
    }

    @Provides
    @Singleton
    AlarmStorage provideAlarmStorage(PreferenceCore preferenceCore) {
        return new AlarmStorageImpl(preferenceCore);
    }

    @Provides
    @Singleton
    SettingsStorage provideSettingsStorage(PreferenceCore preferenceCore) {
        return new SettingStorageImpl(preferenceCore);
    }

    @Provides
    @Singleton
    ComplexLocalStorage provideComplexLocalStorage(IDatabaseCore databaseCore) {
        return new ComplexLocalStorageImpl(databaseCore.getDatabase().getComplexDao());
    }

    @Provides
    @Singleton
    ComplexNetworkStorage provideComplexNetworkStorage(ServerApi serverApi) {
        return new ComplexNetworkStorageImpl(serverApi);
    }

    @Provides
    @Singleton
    ExerciseLocalStorage provideExerciseLocalStorage(IDatabaseCore databaseCore) {
        return new ExerciseLocalStorageImpl(databaseCore.getDatabase().getExerciseDao(),
                databaseCore.getDatabase());
    }

    @Provides
    @Singleton
    ExerciseNetworkStorage provideExerciseNetworkStorage(ServerApi serverApi) {
        return new ExerciseNetworkStorageImpl(serverApi);
    }

    @Provides
    @Singleton
    SetsLocalStorage provideSetsLocalStorage(IDatabaseCore databaseCore) {
        return new SetsLocalStorageImpl(databaseCore.getDatabase().getSetDao());
    }

    @Provides
    @Singleton
    SetsNetworkStorage provideSetsNetworkStorage(ServerApi serverApi) {
        return new SetsNetworkStorageImpl(serverApi);
    }

    @Provides
    @Singleton
    PurchaseNetworkStorage providePurchaseNetworkStorage(ServerApi serverApi) {
        return new PurchaseNetworkStorageImpl(serverApi);
    }

    @Provides
    @Singleton
    ComplexLocalSyncStorage provideComplexLocalSyncStorage(IDatabaseCore databaseCore) {
        return new ComplexLocalSyncStorageImpl(databaseCore.getDatabase().getComplexSyncDao());
    }

    @Provides
    @Singleton
    ExerciseLocalSyncStorage provideExerciseLocalSyncStorage(IDatabaseCore databaseCore) {
        return new ExerciseLocalSyncStorageImpl(databaseCore.getDatabase().getExerciseSyncDao());
    }

    @Provides
    @Singleton
    SetLocalSyncStorage provideSetLocalSyncStorage(IDatabaseCore databaseCore) {
        return new SetLocalSyncStorageImpl(databaseCore.getDatabase().getSetSyncDao());
    }

}
