package com.alphabit.sportnotes.di.components;


import com.alphabit.sportnotes.di.BaseComponent;
import com.alphabit.sportnotes.di.modules.features.AppModule;
import com.alphabit.sportnotes.di.modules.features.RepositoryModule;
import com.alphabit.sportnotes.di.modules.features.RetrofitModule;
import com.alphabit.sportnotes.di.modules.features.RxModule;
import com.alphabit.sportnotes.di.modules.features.StorageModule;
import com.alphabit.sportnotes.di.modules.features.UseCaseModule;
import com.alphabit.sportnotes.ui.screens.base.presenter.BasePresenter;
import com.arellomobile.mvp.MvpView;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Fly on 27.08.2017.
 */
@Singleton
@Component(modules = {AppModule.class, RxModule.class,
        RepositoryModule.class, UseCaseModule.class, StorageModule.class,
        RetrofitModule.class})
public interface AppComponent extends BaseComponent {

}
