package com.alphabit.sportnotes.di.modules.features;

import com.alphabit.sportnotes.di.modules.contract.IUseCaseModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import sportnotes.alphabit.com.business.CrushLogger;
import sportnotes.alphabit.com.business.repository.IAuthorizationRepository;
import sportnotes.alphabit.com.business.repository.IBillingRepository;
import sportnotes.alphabit.com.business.repository.IComplexesNetworkRepository;
import sportnotes.alphabit.com.business.repository.IComplexesRepository;
import sportnotes.alphabit.com.business.repository.IExercisesNetworkRepository;
import sportnotes.alphabit.com.business.repository.IExercisesRepository;
import sportnotes.alphabit.com.business.repository.IPreferenceRepository;
import sportnotes.alphabit.com.business.repository.ISetsNetworkRepository;
import sportnotes.alphabit.com.business.repository.ISetsRepository;
import sportnotes.alphabit.com.business.repository.ISyncRepository;
import sportnotes.alphabit.com.business.usecase.AuthorizationUseCase;
import sportnotes.alphabit.com.business.usecase.BillingUseCase;
import sportnotes.alphabit.com.business.usecase.ComplexUseCase;
import sportnotes.alphabit.com.business.usecase.EditExerciseUseCase;
import sportnotes.alphabit.com.business.usecase.ExerciseDetailUseCase;
import sportnotes.alphabit.com.business.usecase.ExerciseListUseCase;
import sportnotes.alphabit.com.business.usecase.SetUseCase;
import sportnotes.alphabit.com.business.usecase.SettingsUseCase;
import sportnotes.alphabit.com.business.usecase.SynchronizationUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IAuthorizationUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IBillingUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IComplexUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IEditExerciseUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IExerciseDetailUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IExerciseListUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISetUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISettingsUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISynchronizationUseCase;
import sportnotes.alphabit.com.business.utils.IAutoSyncManager;
import sportnotes.alphabit.com.business.utils.SyncStateRegistrator;

/**
 * Created by Fly on 27.08.2017.
 */
@Singleton
@Module
public class UseCaseModule implements IUseCaseModule {

    @Provides
    @Singleton
    @Override
    public IAuthorizationUseCase provideAuthorizationUseCase(final IAuthorizationRepository repository, CrushLogger crushLogger) {
        return new AuthorizationUseCase(repository, crushLogger);
    }

    @Provides
    @Singleton
    @Override
    public IComplexUseCase provideComplexUseCase(IComplexesRepository complexesRepository,
                                                 IAutoSyncManager autoSyncManager, CrushLogger crushLogger,
                                                 IBillingUseCase billingUseCase) {
        return new ComplexUseCase(complexesRepository, autoSyncManager, crushLogger, billingUseCase);
    }

    @Provides
    @Singleton
    @Override
    public IExerciseListUseCase provideIExerciseListUseCase(final IExercisesRepository exercisesRepository,
                                                            IAutoSyncManager autoSyncManager, CrushLogger crushLogger) {
        return new ExerciseListUseCase(exercisesRepository, autoSyncManager, crushLogger);
    }

    @Provides
    @Singleton
    @Override
    public IEditExerciseUseCase provideIEditExerciseUseCase(final IPreferenceRepository repository,
                                                            IExercisesRepository exercisesRepository,
                                                            IAutoSyncManager autoSyncManager, CrushLogger crushLogger) {
        return new EditExerciseUseCase(repository, exercisesRepository, autoSyncManager, crushLogger);
    }

    @Provides
    @Singleton
    @Override
    public IExerciseDetailUseCase provideExerciseDetailUseCase(final IExercisesRepository repository,
                                                               final IPreferenceRepository preferenceRepository,
                                                               final ISetsRepository setsRepository,
                                                               IAutoSyncManager autoSyncManager, CrushLogger crushLogger) {
        return new ExerciseDetailUseCase(repository, preferenceRepository, setsRepository, autoSyncManager, crushLogger);
    }

    @Provides
    @Singleton
    @Override
    public ISetUseCase provideISetUseCase(final ISetsRepository repository, IAutoSyncManager autoSyncManager, CrushLogger crushLogger, IPreferenceRepository preferenceRepository) {
        return new SetUseCase(repository, autoSyncManager, crushLogger, preferenceRepository);
    }

    @Provides
    @Singleton
    @Override
    public ISynchronizationUseCase provideISynchronizationUseCase(IComplexesRepository complexesRepository,
                                                                  IExercisesRepository exercisesRepository,
                                                                  ISetsRepository setsRepository,
                                                                  IComplexesNetworkRepository complexesNetworkRepository,
                                                                  IExercisesNetworkRepository exercisesNetworkRepository,
                                                                  ISetsNetworkRepository setsNetworkRepository,
                                                                  ISyncRepository syncRepository, CrushLogger crushLogger,
                                                                  SyncStateRegistrator syncStateRegistrator) {
        return new SynchronizationUseCase(complexesRepository, exercisesRepository,
                setsRepository, complexesNetworkRepository, exercisesNetworkRepository, setsNetworkRepository, syncRepository, crushLogger, syncStateRegistrator);
    }

    @Provides
    @Singleton
    @Override
    public ISettingsUseCase provideSettingsUseCase(CrushLogger crushLogger, IPreferenceRepository repository) {
        return new SettingsUseCase(crushLogger, repository);
    }

    @Provides
    @Singleton
    @Override
    public IBillingUseCase provideBillingUseCase(IBillingRepository billingRepository, CrushLogger crushLogger) {
        return new BillingUseCase(billingRepository, crushLogger);
    }

}
