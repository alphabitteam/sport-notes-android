package com.alphabit.sportnotes.di.modules.features;

import android.app.Application;
import android.content.Context;

import com.alphabit.sportnotes.utils.AutoSyncManager;
import com.alphabit.sportnotes.utils.CrushLoggerImpl;
import com.alphabit.sportnotes.utils.SystemManagerImpl;
import com.alphabit.sportnotes.view.DatePrinter;
import com.alphabit.sportnotes.view.date.DatePrintable;
import com.alphabit.sportnotes.view.date.DatePrintableImpl;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import sportnotes.alphabit.com.business.CrushLogger;
import sportnotes.alphabit.com.business.repository.ISyncRepository;
import sportnotes.alphabit.com.business.utils.IAutoSyncManager;
import sportnotes.alphabit.com.business.utils.SyncStateRegistrator;
import sportnotes.alphabit.com.business.utils.SystemManager;
import sportnotes.alphabit.com.data.storages.auth.AuthLocalStorage;
import sportnotes.alphabit.com.data.storages.base.cores.PreferenceCore;
import sportnotes.alphabit.com.data.utils.UserLevelManager;

/**
 * Created by Fly on 27.08.2017.
 */
@Singleton
@Module
public class AppModule {
   private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    UserLevelManager provideUserLevelManager(AuthLocalStorage authLocalStorage, PreferenceCore preferenceCore, SystemManager systemManager) {
        return new UserLevelManager(authLocalStorage, preferenceCore, systemManager);
    }

    @Provides
    @Singleton
    @Named(DatePrinter.LAST_SETS_FORMATTER)
    DateTimeFormatter provideDateTimeFormatter() {
        return DateTimeFormat.forPattern("MM.dd");
    }

    @Provides
    @Singleton
    DatePrintable provideDatePrintable(Context context) {
        return new DatePrintableImpl(context.getResources());
    }

    @Provides
    @Singleton
    DatePrinter provideDatePrinter(DatePrintable datePrintable,
                                   @Named(DatePrinter.LAST_SETS_FORMATTER) DateTimeFormatter lastSetsFormatter) {
        return new DatePrinter(datePrintable, lastSetsFormatter);
    }

    @Provides
    @Singleton
    IAutoSyncManager provideIAutoSyncManager(Context context,
                                             SyncStateRegistrator syncStateRegistrator) {
        return new AutoSyncManager(context, syncStateRegistrator);
    }

    @Provides
    @Singleton
    SyncStateRegistrator provideSyncStateRegistrator(ISyncRepository syncRepository) {
        return new SyncStateRegistrator(syncRepository);
    }

    @Provides
    @Singleton
    CrushLogger provideCrushLogger() {
        return new CrushLoggerImpl();
    }

    @Provides
    @Singleton
    SystemManager provideSystemManager() {
        return new SystemManagerImpl();
    }
}
