package com.alphabit.sportnotes.di.modules.contract;

import sportnotes.alphabit.com.business.CrushLogger;
import sportnotes.alphabit.com.business.repository.IAuthorizationRepository;
import sportnotes.alphabit.com.business.repository.IBillingRepository;
import sportnotes.alphabit.com.business.repository.IComplexesNetworkRepository;
import sportnotes.alphabit.com.business.repository.IComplexesRepository;
import sportnotes.alphabit.com.business.repository.IExercisesNetworkRepository;
import sportnotes.alphabit.com.business.repository.IExercisesRepository;
import sportnotes.alphabit.com.business.repository.IPreferenceRepository;
import sportnotes.alphabit.com.business.repository.ISetsNetworkRepository;
import sportnotes.alphabit.com.business.repository.ISetsRepository;
import sportnotes.alphabit.com.business.repository.ISyncRepository;
import sportnotes.alphabit.com.business.usecase.BillingUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IAuthorizationUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IBillingUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IComplexUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IEditExerciseUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IExerciseDetailUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IExerciseListUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISetUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISettingsUseCase;
import sportnotes.alphabit.com.business.usecase.contract.ISynchronizationUseCase;
import sportnotes.alphabit.com.business.utils.IAutoSyncManager;
import sportnotes.alphabit.com.business.utils.SyncStateRegistrator;

/**
 * Created by Fly on 29.08.2017.
 */

public interface IUseCaseModule {

    IAuthorizationUseCase provideAuthorizationUseCase(final IAuthorizationRepository repository, CrushLogger crushLogger);

    IComplexUseCase provideComplexUseCase(IComplexesRepository repository,
                                          IAutoSyncManager autoSyncManager, CrushLogger crushLogger,
                                          IBillingUseCase billingUseCase);

    IExerciseListUseCase provideIExerciseListUseCase(IExercisesRepository repository,
                                                     IAutoSyncManager autoSyncManager, CrushLogger crushLogger);

    IEditExerciseUseCase provideIEditExerciseUseCase(IPreferenceRepository repository,
                                                     IExercisesRepository exercisesRepository,
                                                     IAutoSyncManager autoSyncManager, CrushLogger crushLogger);

    IExerciseDetailUseCase provideExerciseDetailUseCase(IExercisesRepository repository,
                                                        IPreferenceRepository preferenceRepository,
                                                        ISetsRepository setsRepository,
                                                        IAutoSyncManager autoSyncManager, CrushLogger crushLogger);

    ISetUseCase provideISetUseCase(ISetsRepository repository, IAutoSyncManager autoSyncManager, CrushLogger crushLogger, IPreferenceRepository preferenceRepository);

    ISynchronizationUseCase provideISynchronizationUseCase(IComplexesRepository complexesRepository,
                                                           IExercisesRepository exercisesRepository,
                                                           ISetsRepository setsRepository,
                                                           IComplexesNetworkRepository complexesNetworkRepository,
                                                           IExercisesNetworkRepository exercisesNetworkRepository,
                                                           ISetsNetworkRepository setsNetworkRepository,
                                                           ISyncRepository syncRepository, CrushLogger crushLogger,
                                                           SyncStateRegistrator syncStateRegistrator);

    IBillingUseCase provideBillingUseCase(IBillingRepository billingRepository, CrushLogger crushLogger);

    ISettingsUseCase provideSettingsUseCase(CrushLogger crushLogger, IPreferenceRepository repository);
}
