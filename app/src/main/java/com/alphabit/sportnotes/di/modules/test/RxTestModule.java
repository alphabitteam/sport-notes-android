package com.alphabit.sportnotes.di.modules.test;

import com.alphabit.sportnotes.di.modules.contract.IRxModule;
import com.alphabit.sportnotes.di.modules.features.RxModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Fly on 27.08.2017.
 */
@Module
public class RxTestModule implements IRxModule {

    @Provides
    @Singleton
    @Named("MainThreadScheduler")
    public Scheduler provideMainThreadScheduler() {
        return Schedulers.trampoline();
    }

    @Provides
    @Singleton
    @Named("IOScheduler")
    public Scheduler provideIOScheduler() {
        return Schedulers.trampoline();
    }

    @Provides
    @Singleton
    @Named("ComputationScheduler")
    public Scheduler provideComputationScheduler() {
        return Schedulers.trampoline();
    }
}
