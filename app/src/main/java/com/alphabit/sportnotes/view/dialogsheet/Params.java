package com.alphabit.sportnotes.view.dialogsheet;

import android.graphics.Rect;
import android.view.Gravity;
import android.view.ViewGroup;

/**
 * Created by Fly on 29.08.2017.
 */

public class Params {
    private int mGravity = Gravity.BOTTOM;
    private int mWidth = ViewGroup.LayoutParams.MATCH_PARENT;
    private int mHeight = ViewGroup.LayoutParams.WRAP_CONTENT;;
    private Rect mMargin = new Rect(0, 0, 0, 0);

    public Params setGravity(final int gravity) {
        this.mGravity = gravity;
        return this;
    }

    public Params setHeight(final int height) {
        this.mHeight = height;
        return this;
    }

    public void setWidth(final int width) {
        this.mWidth = width;
    }

    public Params setMargin(final int left, final int top, final int right, final int bottom) {
        mMargin = new Rect(left, top, right, bottom);
        return this;
    }

    public int getGravity() {
        return mGravity;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getWidth() {
        return mWidth;
    }

    public Rect getMargin() {
        return mMargin;
    }
}
