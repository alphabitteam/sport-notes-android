package com.alphabit.sportnotes.view.dialogsheet;

/**
 * Created by Fly on 06.09.2017.
 */

public class DialogSheetCoreFactory {

    public static DialogSheetCore create(DialogSheetBuilder.Type type) {
        switch (type) {
            case SHEET:
                return new DialogSheetCoreSheetImpl();
            case DIALOG:
                return new DialogSheetCoreDialogImpl();
        }
        throw new IllegalStateException("no implementation core for type " + type);
    }
}
