package com.alphabit.sportnotes.view.dialogsheet;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.alphabit.sportnotes.view.DialogSheet2;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.DialogPlusBuilder;
import com.orhanobut.dialogplus.Holder;

/**
 * Created by Fly on 29.08.2017.
 */

public class DialogSheetCoreSheetImpl extends DialogSheetCore {

    @Override
    public DialogSheetView build(@NonNull final DialogSheetView dialogSheetView,
                                 @NonNull final DialogSheetLifecycle dialogSheetLifecycle,
                                 @NonNull final Params params) {
        final DialogPlus dialog = DialogPlus.newDialog(getContext())
                .setGravity(params.getGravity())
                .setOutMostMargin(params.getMargin().left, params.getMargin().top,
                        params.getMargin().right, params.getMargin().bottom)
                .setContentHeight(params.getHeight())
                .setContentWidth(params.getWidth())
                .setOnDismissListener(dialog1 -> dialogSheetLifecycle.onHide())
                .setContentHolder(new Holder() {
                    @Override
                    public void addHeader(final View view) {
                    }

                    @Override
                    public void addFooter(final View view) {
                    }

                    @Override
                    public void setBackgroundResource(final int colorResource) {
                        dialogSheetView.getView().setBackgroundColor(
                                ContextCompat.getColor(getContext(), colorResource));
                    }

                    @Override
                    public View getView(final LayoutInflater inflater, final ViewGroup parent) {
                        return dialogSheetView.getView();
                    }

                    @Override
                    public void setOnKeyListener(final View.OnKeyListener keyListener) {
                    }

                    @Override
                    public View getInflatedView() {
                        return dialogSheetView.getView();
                    }

                    @Override
                    public View getHeader() {
                        return null;
                    }

                    @Override
                    public View getFooter() {
                        return null;
                    }
                })
                .create();

        dialogSheetView.setDialogSheetDelegate(new DialogSheet() {
            @Override
            public void show() {
                if (!dialog.isShowing()) {
                    dialog.show();
                    dialogSheetLifecycle.onShow();
                }
            }

            @Override
            public void dismiss() {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        });
        return dialogSheetView;
    }
}
