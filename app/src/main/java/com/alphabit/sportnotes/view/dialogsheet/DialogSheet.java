package com.alphabit.sportnotes.view.dialogsheet;

/**
 * Created by Fly on 29.08.2017.
 */

public interface DialogSheet {

    void show();

    void dismiss();

}
