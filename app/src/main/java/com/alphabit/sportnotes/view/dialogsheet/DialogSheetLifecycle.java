package com.alphabit.sportnotes.view.dialogsheet;

import android.support.annotation.CallSuper;

/**
 * Created by Fly on 29.08.2017.
 */

public abstract class DialogSheetLifecycle {

    @CallSuper
    protected void onCreate() {

    }

    @CallSuper
    protected void onViewCreated() {

    }

    @CallSuper
    protected void onDestroy() {

    }

    @CallSuper
    protected void onShow() {

    }

    @CallSuper
    protected void onHide() {

    }
}
