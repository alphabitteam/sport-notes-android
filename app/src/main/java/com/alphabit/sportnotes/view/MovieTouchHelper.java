package com.alphabit.sportnotes.view;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;

import com.alphabit.sportnotes.ui.adapter.MoveAdapter;

/**
 * Created by Fly on 05.09.2017.
 */

public class MovieTouchHelper extends ItemTouchHelper.SimpleCallback {
    private final MoveAdapter mMovieAdapter;
    private final OnChangePositionListener onChangePositionListener;
    private boolean mOrderChanged;

    public MovieTouchHelper(MoveAdapter movieAdapter, OnChangePositionListener onChangePositionListener) {
        super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0);
        this.mMovieAdapter = movieAdapter;
        this.onChangePositionListener = onChangePositionListener;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        super.onSelectedChanged(viewHolder, actionState);
        if (actionState == ItemTouchHelper.ACTION_STATE_IDLE && mOrderChanged) {
            mOrderChanged = false;
            onChangePositionListener.onOrderChanged();
        }
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                          RecyclerView.ViewHolder target) {
        mMovieAdapter.swap(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        onChangePositionListener.onChangePosition(viewHolder.getLayoutPosition(),
                target.getLayoutPosition());
        mOrderChanged = true;
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        mMovieAdapter.remove(viewHolder.getAdapterPosition());
        Log.d(getClass().getSimpleName(), "onSwiped: ");
    }

    public interface OnChangePositionListener {
        void onChangePosition(int oldPosition, int newPosition);
        void onOrderChanged();
    }
}