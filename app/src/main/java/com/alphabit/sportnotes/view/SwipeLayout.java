package com.alphabit.sportnotes.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.chauthai.swipereveallayout.SwipeRevealLayout;

public class SwipeLayout extends SwipeRevealLayout {
    private static final int CLICK_ACTION_THRESHOLD = 50;
    private float startX;
    private float startY;
    private boolean isClick;

    public SwipeLayout(Context context) {
        super(context);
    }

    public SwipeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SwipeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    boolean isis;
    /*@Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN)
            isis = false;
        if (isis)
            return super.onInterceptTouchEvent(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                startY = event.getY();
                isClick = true;
                isis = false;
                return false;
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
            default:
                float endX = event.getX();
                float endY = event.getY();
                if (isClick)
                    isClick = isAClick(startX, endX, startY, endY);
                if (isClick)
                    return true;
        }
        isis = true;
        Log.d(getClass().getSimpleName(), "onInterceptTouchEvent: " + isClick);
        return super.onInterceptTouchEvent(event);
    }*/

    private boolean isAClick(float startX, float endX, float startY, float endY) {
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        return !(differenceX > CLICK_ACTION_THRESHOLD|| differenceY > CLICK_ACTION_THRESHOLD);
    }
}
