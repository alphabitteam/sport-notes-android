package com.alphabit.sportnotes.view.dialogsheet;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.CallSuper;
import android.util.Log;
import android.view.View;

/**
 * Created by Fly on 28.08.2017.
 */

public abstract class DialogSheetView<Binding extends ViewDataBinding> extends DialogSheetLifecycle
        implements DialogSheet {

    private DialogSheet mDialogSheetDelegate = new DialogSheet() {
        @Override
        public void show() {

        }

        @Override
        public void dismiss() {

        }
    };

    private OnOpenListener mOnOpenListener = () -> {};
    private OnCloseListener mOnCloseListener = () -> {};

    private View mView;
    private Context mContext;
    private Binding mBinding;


    @Override
    protected void onCreate() {
        super.onCreate();
        mView = onViewCreate();
        mBinding = DataBindingUtil.bind(mView);
        onViewCreated();
    }

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
    }

    public Binding getBinding() {
        return mBinding;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public View getView() {
        if (mView == null)
            onCreate();
        return mView;
    }

    public Context getContext() {
        return mContext;
    }

    void setContext(final Context context) {
        mContext = context;
    }

    public abstract View onViewCreate();

    public abstract Params withParams(Params params);

    public void setOnOpenListener(final OnOpenListener onOpenListener) {
        mOnOpenListener = onOpenListener;
    }

    public void setOnCloseListener(final OnCloseListener onCloseListener) {
        mOnCloseListener = onCloseListener;
    }

    @Override
    protected void onHide() {
        super.onHide();
        if (mOnCloseListener != null) mOnCloseListener.onClose();
    }

    @Override
    protected void onShow() {
        super.onShow();
        if (mOnOpenListener != null) mOnOpenListener.onOpen();
    }

    @Override
    public final void show() {
        mDialogSheetDelegate.show();
    }

    public void setDialogSheetDelegate(final DialogSheet dialogSheetDelegate) {
        mDialogSheetDelegate = dialogSheetDelegate;
    }

    public DialogSheet getDialogSheetDelegate() {
        return mDialogSheetDelegate;
    }

    @Override
    @CallSuper
    public final void dismiss() {
        mDialogSheetDelegate.dismiss();
    }
}
