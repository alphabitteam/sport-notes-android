package com.alphabit.sportnotes.view.customdialog;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alphabit.sportnotes.R;

/**
 * Created by Fly on 06.09.2017.
 */

public class CustomDialog {
    private String mTitle;
    private View mView;
    private String mPositiveName;
    private String mNegativeName;
    private String mNeutralName;
    private CustomDialogBuilder.OnPositiveClickListener mOnPositiveClickListener;
    private CustomDialogBuilder.OnNegativeClickListener mOnNegativeClickListener;
    private CustomDialogBuilder.OnNeutralClickListener onNeutralClickListener;

    private final Handler handler = new Handler(Looper.getMainLooper());

    private AlertDialog mAlertDialog;
    private boolean cancelable = true;

    void setTitle(final String title) {
        mTitle = title;
    }

    void setView(final View view) {
        mView = view;
    }

    void setOnPositiveClickListener(final String positiveName, final CustomDialogBuilder.OnPositiveClickListener onPositiveClickListener) {
        mOnPositiveClickListener = onPositiveClickListener;
        mPositiveName = positiveName;
    }

    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }

    void setOnNegativeClickListener(final String negativeName, final CustomDialogBuilder.OnNegativeClickListener onNegativeClickListener) {
        mOnNegativeClickListener = onNegativeClickListener;
        mNegativeName = negativeName;
    }

    void setOnNeutralClickListener(final String neutralName, final CustomDialogBuilder.OnNeutralClickListener onNeutralClickListener) {
        this.onNeutralClickListener = onNeutralClickListener;
        mNeutralName = neutralName;
    }

    void onCreateDialog(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogStyle);
        builder.setTitle(mTitle);
        builder.setView(mView);
        builder.setCancelable(cancelable);
        if (mOnPositiveClickListener != null)
            builder.setPositiveButton(mPositiveName, (dialogInterface, i) ->
                    mOnPositiveClickListener.onPositiveClick(this));
        if (mOnNegativeClickListener != null)
            builder.setNegativeButton(mNegativeName, (dialogInterface, i) ->
                    mOnNegativeClickListener.onNegativeClick(this));
        if (onNeutralClickListener != null)
            builder.setNeutralButton(mNeutralName, (dialogInterface, i) ->
                    onNeutralClickListener.onNeutralClick(this));
        mAlertDialog = builder.create();
        fillFont(context);
    }

    private void fillFont(final Context context) {
        TextView alertTitle = mAlertDialog.getWindow().findViewById(R.id.alertTitle);
        Button button1 = mAlertDialog.getWindow().findViewById(android.R.id.button1);
        Button button2 = mAlertDialog.getWindow().findViewById(android.R.id.button2);

        Typeface face= Typeface.createFromAsset(context.getAssets(),"fonts/HARMONIASANSPROCYR-REGULAR.OTF");

        if (alertTitle != null)
            alertTitle.setTypeface(face);
        if (button1 != null)
            button1.setTypeface(face);
        if (button2 != null)
            button2.setTypeface(face);
    }

    public void show() {
        handler.post(() -> {
            try {
                mAlertDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void cancel() {
        handler.post(() -> {
            try {
                mAlertDialog.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
