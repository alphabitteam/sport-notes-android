package com.alphabit.sportnotes.view.date;

import android.content.Context;
import android.content.res.Resources;

import com.alphabit.sportnotes.R;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by fly12 on 12.11.2017.
 */

public class DatePrintableImpl extends DatePrintable {

    private final Resources resources;

    public DatePrintableImpl(Resources resources) {
        this.resources = resources;
    }

    @Override
    public String now(DateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        return resources.getString(R.string.today);
    }

    @Override
    public String aMinuteAgo(DateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        return resources.getString(R.string.today);
    }

    @Override
    public String today(DateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        return resources.getString(R.string.today);
    }

    @Override
    public String aDayAgo(DateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        return resources.getString(R.string.yesterday);
    }
}
