package com.alphabit.sportnotes.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.alphabit.sportnotes.R;

/**
 * Created by Fly on 25.04.2017.
 */

public class StyleableNumberPicker extends CustomNumberPicker {
    private Typeface type;

    public StyleableNumberPicker(Context context) {
        super(context);
        setCustomFontAttr(context, null);
    }

    public StyleableNumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFontAttr(context, attrs);
    }

    public StyleableNumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFontAttr(context, attrs);
    }

    private void setCustomFontAttr(Context context, AttributeSet attrs) {
        String customFont = "fonts/HARMONIASANSPROCYR-LIGHT.OTF";

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.StyleableTextView);

            int value = -1;

            if (a.hasValue(R.styleable.StyleableTextView_font_type)) {
                value = a.getInt(R.styleable.StyleableTextView_font_type, 0);
            }

            switch (value) {
                case 1:
                    customFont = "fonts/HARMONIASANSPROCYR-REGULAR.OTF";
                    break;
                case 2:
                    customFont = "fonts/HARMONIASANSPROCYR-LIGHT.OTF";
                    break;
                default:
                    customFont = "fonts/HARMONIASANSPROCYR-LIGHT.OTF";
            }

            a.recycle();
        }

        try {
            type = Typeface.createFromAsset(context.getAssets(), customFont);
            setTypeface(type);
        } catch (Exception e) {
        }
    }
}
