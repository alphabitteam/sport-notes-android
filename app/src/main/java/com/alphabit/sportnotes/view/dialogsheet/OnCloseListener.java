package com.alphabit.sportnotes.view.dialogsheet;

/**
 * Created by Fly on 28.08.2017.
 */

public interface OnCloseListener {

    void onClose();

}
