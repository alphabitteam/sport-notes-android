package com.alphabit.sportnotes.view;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import android.widget.BaseAdapter;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.DialogPlusBuilder;

/**
 * Created by Fly on 28.08.2017.
 */

public class DialogSheet2 {

    private Context mContext;
    private int mGravity;
    private View mHeaderView;
    private BaseAdapter mAdapter;
    private OnItemClickListener mOnItemClickListener = item -> {};
    private int mContentHeight;
    private Rect mMargin;
    private Rect mPadding;

    private DialogPlus mDialog;
    private OnShowListener mOnShowListener = () -> {};
    private OnHideListener mOnHideListener = () -> {};

    public static Builder newBuilder() {
        return new DialogSheet2.Builder();
    }

    private DialogSheet2(Context context) {
        mContext = context;
    }

    public DialogSheet2 setOnItemClickListener(final OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
        return this;
    }

    public DialogSheet2 setAdapter(final BaseAdapter adapter) {
        mAdapter = adapter;
        return this;
    }

    public DialogSheet2 setGravity(final int gravity) {
        mGravity = gravity;
        return this;
    }

    public DialogSheet2 setHeaderView(final View headerView) {
        mHeaderView = headerView;
        return this;
    }

    public DialogSheet2 setContentHeight(final int contentHeight) {
        mContentHeight = contentHeight;
        return this;
    }

    public DialogSheet2 setMargin(final Rect margin) {
        mMargin = margin;
        return this;
    }

    public DialogSheet2 setPadding(final Rect padding) {
        mPadding = padding;
        return this;
    }

    public void update() {
        create();
    }

    private DialogSheet2 create() {
        final DialogPlusBuilder builder = DialogPlus.newDialog(mContext);
        if (mAdapter != null)
            builder.setAdapter(mAdapter);
        if (mGravity != 0)
            builder.setGravity(mGravity);
        if (mHeaderView != null)
            builder.setHeader(mHeaderView);
        if (mContentHeight != 0)
            builder.setContentHeight(mContentHeight);
        if (mMargin != null)
            builder.setOutMostMargin(mMargin.left, mMargin.top, mMargin.right, mMargin.bottom);
        if (mPadding != null)
            builder.setPadding(mPadding.left, mPadding.top, mPadding.right, mPadding.bottom);

        builder.setOnDismissListener(dialog -> mOnHideListener.onHide());
        builder.setOnItemClickListener((dialog1, item, view, position) ->
                mOnItemClickListener.onItemClick(item));


        builder.setExpanded(false);  // This will enable the expand feature, (similar to android L share dialog)
        mDialog = builder.create();
        return this;
    }

    public void show() {
        mOnShowListener.onShow();
        mDialog.show();
    }

    public void dismiss() {
        mDialog.dismiss();
    }

    public final static class Builder {

        private Builder() {
        }

        public Params with(Context context) {
            return new Params(context);
        }
    }

    public final static class Params {

        private Context context;
        private int gravity;
        private View headerView;
        private BaseAdapter adapter;
        private OnItemClickListener onItemClickListener;
        private OnShowListener onShowListener;
        private OnHideListener onHideListener;
        private int height;
        private Rect margin;
        private Rect padding;

        private Params(final Context context) {
            this.context = context;
        }

        public Params setGravity(int gravity) {
            this.gravity = gravity;
            return this;
        }

        public Params setAdapter(BaseAdapter adapter) {
            this.adapter = adapter;
            return this;
        }

        public Params setHeader(View view) {
            this.headerView = view;
            return this;
        }

        public Params setOnItemClickListener(OnItemClickListener listener) {
            this.onItemClickListener = listener;
            return this;
        }

        public Params setContentHeight(int height) {
            this.height = height;
            return this;
        }

        public Params setOnShowListener(final OnShowListener listener) {
            this.onShowListener = listener;
            return this;
        }

        public Params setOnHideListener(final OnHideListener listener) {
            this.onHideListener = listener;
            return this;
        }

        public DialogSheet2 build() {
            return new DialogSheet2(context)
                    .setAdapter(adapter)
                    .setGravity(gravity)
                    .setOnShowListener(onShowListener)
                    .setOnHideListener(onHideListener)
                    .setHeaderView(headerView)
                    .setMargin(margin)
                    .setPadding(padding)
                    .setOnItemClickListener(onItemClickListener)
                    .setContentHeight(height)
                    .create();
        }

        public Params setMargin(final int left, final int top, final int right, final int bottom) {
            margin = new Rect(left, top, right, bottom);
            return this;
        }

        public Params setPadding(final int left, final int top, final int right, final int bottom) {
            padding = new Rect(left, top, right, bottom);
            return this;
        }
    }

    public DialogSheet2 setOnShowListener(final OnShowListener listener) {
        mOnShowListener = listener;
        return this;
    }

    public DialogSheet2 setOnHideListener(final OnHideListener listener) {
        mOnHideListener = listener;
        return this;
    }

    public interface OnItemClickListener {
        void onItemClick(Object item);
    }

    public interface OnShowListener {
        void onShow();
    }

    public interface OnHideListener {
        void onHide();
    }
}
