package com.alphabit.sportnotes.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ToggleButton;

/**
 * Created by Fly on 01.05.2017.
 */

public class StyleableToggleTextView extends ToggleButton {

    public StyleableToggleTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public StyleableToggleTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

}
