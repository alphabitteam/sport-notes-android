package com.alphabit.sportnotes.view.dialogsheet;

import android.content.Context;

/**
 * Created by Fly on 28.08.2017.
 */

public class DialogSheetBuilder {

    private DialogSheetBuilder() {
    }

    public static Builder newDialogSheet(Context context) {
        return new Builder(context, DialogSheetCoreFactory.create(Type.SHEET));
    }

    public static class Builder {

        private final DialogSheetCore dialogSheetCore;
        private final Context context;
        private DialogSheetView dialogSheetView;

        private Builder(Context context, final DialogSheetCore dialogSheetCore) {
            this.context = context;
            this.dialogSheetCore = dialogSheetCore;
        }

        public BuildParams dialog(DialogSheetView dialogSheetView) {
            if (dialogSheetView == null)
                throw new RuntimeException("низя показать диалог null");
            this.dialogSheetView = dialogSheetView;
            return new BuildParams(this);
        }

        private <T extends DialogSheet> T buildWithParams(BuildParams buildParams) {
            dialogSheetCore.setContext(context);
            dialogSheetView.setContext(context);
            dialogSheetView.setOnOpenListener(buildParams.onOpenListener);
            dialogSheetView.setOnCloseListener(buildParams.onCloseListener);

            Params params = dialogSheetView.withParams(new Params());
            if (params == null)
                params = new Params();

            DialogSheetView dialog = dialogSheetCore.build(dialogSheetView, dialogSheetView, params);
            if (dialog.getDialogSheetDelegate() == null)
                throw new RuntimeException("For build " + dialog.getClass().getSimpleName()
                        + " need call setDialogSheetDelegate");
            return (T) dialog;
        }
    }

    public static class BuildParams {

        private final Builder builder;
        private OnOpenListener onOpenListener;
        private OnCloseListener onCloseListener;

        private BuildParams(Builder builder) {
            this.builder = builder;
        }

        public BuildParams withOnOpenListener(OnOpenListener listener) {
            this.onOpenListener = listener;
            return this;
        }

        public BuildParams withOnCloseListener(OnCloseListener listener) {
            this.onCloseListener = listener;
            return this;
        }

        public <T extends DialogSheet> T build() {
            return builder.buildWithParams(this);
        }
    }

    public enum Type {
        DIALOG, SHEET
    }
}
