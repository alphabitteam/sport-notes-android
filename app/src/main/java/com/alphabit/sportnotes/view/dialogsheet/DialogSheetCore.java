package com.alphabit.sportnotes.view.dialogsheet;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by Fly on 29.08.2017.
 */

public abstract class DialogSheetCore {

    private Context mContext;

    public abstract DialogSheetView build(@NonNull final DialogSheetView dialogSheetView,
                                      @NonNull final DialogSheetLifecycle dialogSheetLifecycle,
                                      @NonNull final Params params);

    void setContext(final Context context) {
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

}
