package com.alphabit.sportnotes.view.customdialog;

import android.content.Context;
import android.view.View;

/**
 * Created by Fly on 06.09.2017.
 */

public class CustomDialogBuilder {

    private CustomDialogBuilder() {
    }

    public static final Builder newBuilder(Context context) {
        return new Builder(context);
    }

    public static class Builder {
        private String mTitle;
        private View mView;
        private String mPositiveName;
        private String mNegativeName;
        private String neutralName;
        private OnPositiveClickListener mOnPositiveClickListener;
        private OnNegativeClickListener mOnNegativeClickListener;
        private OnNeutralClickListener onNeutralClickListener;
        private final Context mContext;
        private boolean cancelable = true;

        public Builder(final Context context) {
            mContext = context;
        }

        public Builder setTitle(final String title) {
            mTitle = title;
            return this;
        }

        public Builder setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public Builder setView(final View view) {
            mView = view;
            return this;
        }

        public Builder setOnPositiveClickListener(String positiveName, final OnPositiveClickListener onPositiveClickListener) {
            mOnPositiveClickListener = onPositiveClickListener;
            mPositiveName = positiveName;
            return this;
        }

        public Builder setOnNegativeClickListener(String negativeName, final OnNegativeClickListener onNegativeClickListener) {
            mOnNegativeClickListener = onNegativeClickListener;
            mNegativeName = negativeName;
            return this;
        }

        public Builder setOnNeutralClickListener(String neutralName, final OnNeutralClickListener onNeutralClickListener) {
            this.onNeutralClickListener = onNeutralClickListener;
            this.neutralName = neutralName;
            return this;
        }

        public CustomDialog build() {
            CustomDialog dialog = new CustomDialog();
            dialog.setTitle(mTitle);
            dialog.setView(mView);
            dialog.setCancelable(cancelable);
            dialog.setOnPositiveClickListener(mPositiveName, mOnPositiveClickListener);
            dialog.setOnNegativeClickListener(mNegativeName, mOnNegativeClickListener);
            dialog.setOnNeutralClickListener(neutralName, onNeutralClickListener);
            dialog.onCreateDialog(mContext);
            return dialog;
        }
    }

    public interface OnPositiveClickListener {
        void onPositiveClick(CustomDialog customDialog);
    }

    public interface OnNegativeClickListener {
        void onNegativeClick(CustomDialog customDialog);
    }

    public interface OnNeutralClickListener {
        void onNeutralClick(CustomDialog customDialog);
    }
}
