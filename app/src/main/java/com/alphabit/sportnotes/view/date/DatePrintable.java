package com.alphabit.sportnotes.view.date;

import android.text.TextUtils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by fly12 on 12.11.2017.
 */

public abstract class DatePrintable {

    public String now(DateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        return aMinuteAgo(dateTime, dateTimeFormatter);
    }

    public String aMinuteAgo(DateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        return today(dateTime, dateTimeFormatter);
    }

    public String today(DateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        return aDayAgo(dateTime, dateTimeFormatter);
    }

    public String aDayAgo(DateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        return twoDayAgo(dateTime, dateTimeFormatter);
    }

    public String twoDayAgo(DateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        return aWeekAgo(dateTime, dateTimeFormatter);
    }

    public String aWeekAgo(DateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        return aMonthAgo(dateTime, dateTimeFormatter);
    }

    public String aMonthAgo(DateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        return otherDate(dateTime, dateTimeFormatter);
    }

    public String otherDate(DateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        return dateTimeFormatter.print(dateTime);
    }
}
