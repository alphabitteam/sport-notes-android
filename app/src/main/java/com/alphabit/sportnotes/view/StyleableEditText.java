package com.alphabit.sportnotes.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.util.Log;

import com.alphabit.sportnotes.R;


/**
 * Created by Fly on 27.11.2016.
 */

public class StyleableEditText extends AppCompatEditText {

    OnSelectionChangedListener onSelectionChangedListener;

    public StyleableEditText(Context context) {
        super(context);
        setCustomFontAttr(context, null);
    }

    public StyleableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFontAttr(context, attrs);
    }

    public StyleableEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFontAttr(context, attrs);
    }

    private void setCustomFontAttr(Context context, AttributeSet attrs) {
        String customFont = "fonts/HARMONIASANSPROCYR-LIGHT.OTF";

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.StyleableTextView);

            int value = -1;

            if (a.hasValue(R.styleable.StyleableTextView_font_type)) {
                value = a.getInt(R.styleable.StyleableTextView_font_type, 0);
            }

            switch (value) {
                case 1:
                    customFont = "fonts/HARMONIASANSPROCYR-REGULAR.OTF";
                    break;
                case 2:
                    customFont = "fonts/HARMONIASANSPROCYR-LIGHT.OTF";
                    break;
                default:
                    customFont = "fonts/HARMONIASANSPROCYR-LIGHT.OTF";
            }

            a.recycle();
        }
        setCustomFont(context, customFont);
    }

    public boolean setCustomFont(Context context, String asset) {
        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(context.getAssets(), asset);
        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), "Could not get typeface: " + e.getMessage());
            return false;
        }

        setTypeface(tf);
        return true;
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        super.onSelectionChanged(selStart, selEnd);
        if (onSelectionChangedListener != null)
            onSelectionChangedListener.onSelectionChanged(selStart, selEnd);
    }

    public OnSelectionChangedListener getOnSelectionChangedListener() {
        return onSelectionChangedListener;
    }

    public void setOnSelectionChangedListener(OnSelectionChangedListener onSelectionChangedListener) {
        this.onSelectionChangedListener = onSelectionChangedListener;
    }

    public interface OnSelectionChangedListener {
        void onSelectionChanged(int selStart, int selEnd);
    }
}
