package com.alphabit.sportnotes.view;

import com.alphabit.sportnotes.view.date.DatePrintable;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import sportnotes.alphabit.com.data.utils.DateTimeUtils;

/**
 * Created by fly12 on 12.11.2017.
 */

public class DatePrinter {

    public static final String LAST_SETS_FORMATTER = "last_sets_formatter";

    private final DatePrintable datePrintable;
    private final DateTimeFormatter lastSetsFormatter;

    public DatePrinter(DatePrintable datePrintable, DateTimeFormatter lastSetsFormatter) {
        this.datePrintable = datePrintable;
        this.lastSetsFormatter = lastSetsFormatter;
    }

    public String printLastSets(DateTime dateTime) {
        DateTime current = DateTimeUtils.getStartDay(dateTime);
        current = DateTime.now();
        if (dateTime.isBefore(current)) {
            if (dateTime.isAfter(current.minusMinutes(1)))
                return datePrintable.now(dateTime, lastSetsFormatter);
            if (dateTime.isAfter(current.minusMinutes(2)))
                return datePrintable.aMinuteAgo(dateTime, lastSetsFormatter);
            if (dateTime.isAfter(startOfDay(current)))
                return datePrintable.today(dateTime, lastSetsFormatter);
            if (dateTime.isAfter(startOfDay(current.minusDays(1))))
                return datePrintable.aDayAgo(dateTime, lastSetsFormatter);
            else if (dateTime.isAfter(startOfDay(current.minusDays(2))))
                return datePrintable.twoDayAgo(dateTime, lastSetsFormatter);
            else if (dateTime.isAfter(startOfDay(current.minusWeeks(1))))
                return datePrintable.aWeekAgo(dateTime, lastSetsFormatter);
            else if (dateTime.isAfter(startOfDay(current.minusMonths(1))))
                return datePrintable.aMonthAgo(dateTime, lastSetsFormatter);
        }
        return datePrintable.otherDate(dateTime, lastSetsFormatter);
    }

    private DateTime startOfDay(DateTime dateTime) {
        return dateTime.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
    }

}
