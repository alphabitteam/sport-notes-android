package com.alphabit.sportnotes.view.customdialog;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;

import com.alphabit.sportnotes.R;
import com.alphabit.sportnotes.databinding.DialogSimpleBinding;

/**
 * Created by Fly on 09.09.2017.
 */

public class SimpleCustomDialog extends CustomDialog {

    public static CustomDialog create(Context context, @StringRes int titleRes, @StringRes int textRes) {
        return create(context, context.getString(titleRes), context.getString(textRes));
    }

    public static CustomDialog create(Context context, String title, String text) {
        DialogSimpleBinding view = DataBindingUtil.bind(LayoutInflater.from(context).inflate(R.layout.dialog_simple, null, false));
        view.text.setText(text);
        return CustomDialogBuilder.newBuilder(context)
                .setTitle(title)
                .setView(view.getRoot())
                .setOnPositiveClickListener(context.getString(R.string.ok), customDialog -> {})
                .build();
    }
}
