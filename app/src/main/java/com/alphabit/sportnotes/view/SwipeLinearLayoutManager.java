package com.alphabit.sportnotes.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.swipe.SwipeLayout;

/**
 * Created by Fly on 31.08.2017.
 */

public class SwipeLinearLayoutManager extends LinearLayoutManager {
    public SwipeLinearLayoutManager(final Context context) {
        super(context);
    }

    public SwipeLinearLayoutManager(final Context context, final int orientation) {
        super(context, orientation, false);
        collapseSwipeMenu();
    }

    public SwipeLinearLayoutManager(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        collapseSwipeMenu();
    }

    @Override
    public void onItemsRemoved(final RecyclerView recyclerView, final int positionStart, final int itemCount) {
        super.onItemsRemoved(recyclerView, positionStart, itemCount);
        collapseSwipeMenu();
    }

    public SwipeLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
        collapseSwipeMenu();
    }

    @Override
    public void onScrollStateChanged(final int state) {
        super.onScrollStateChanged(state);
        if (state == 1)
            collapseSwipeMenu();
    }

    @Override
    public void onItemsChanged(final RecyclerView recyclerView) {
        super.onItemsChanged(recyclerView);
        collapseSwipeMenu();
    }



    private void collapseSwipeMenu() {
        for (int i = 0; i < getChildCount(); i++) {
            View view = getChildAt(i);
            SwipeLayout swipeLayout = findSwipeLayout(view);
            if (swipeLayout != null)
                swipeLayout.close(true);
        }
    }

    private SwipeLayout findSwipeLayout(View view) {
        if (view == null)
            return null;
        if (view instanceof SwipeLayout)
            return (SwipeLayout) view;
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View child = ((ViewGroup) view).getChildAt(i);
                SwipeLayout swipeLayout = findSwipeLayout(child);
                if (swipeLayout != null)
                    return swipeLayout;
            }
        }
        return null;
    }
}
