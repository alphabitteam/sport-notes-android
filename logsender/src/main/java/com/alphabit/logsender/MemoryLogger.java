package com.alphabit.logsender;

import com.alphabit.logsender.ActionWriter;
import com.alphabit.logsender.LogWriter;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;

class MemoryLogger implements ActionWriter, LogWriter {

    private StringWriter swLog = new StringWriter();
    private StringWriter swAction = new StringWriter();

    @Override
    public void clearLog() {
        try {
            swLog.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        swLog = new StringWriter();
    }

    @Override
    public void writeLog(String tag, String msg) {
        swLog.append(tag).append(" ").append(msg).append('\n');
    }

    @Override
    public void writeLog(String tag, Throwable throwable) {
        swLog.append(tag).append(" ");
        throwable.printStackTrace(new PrintWriter(swLog));
        swLog.append('\n');
    }

    @Override
    public void writeLog(String tag, String msg, Throwable throwable) {
        swLog.append(tag).append(" ").append(msg).append(" ");
        throwable.printStackTrace(new PrintWriter(swLog));
        swLog.append('\n');
    }

    @Override
    public void clearActions() {
        try {
            swAction.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        swAction = new StringWriter();
    }

    @Override
    public void writeActions(String msg) {
        swAction.append(msg).append('\n');
    }

    StringWriter getSbLog() {
        return swLog;
    }

    StringWriter getSbAction() {
        return swAction;
    }
}
