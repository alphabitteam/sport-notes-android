package com.alphabit.logsender;

public interface ActionWriter {

    void clearActions();

    void writeActions(String msg);

}
