package com.alphabit.logsender;

public class LogSenderRegister {

    private static StreamHolder streamHolder;

    static void init(StreamHolder streamHolder) {
        LogSenderRegister.streamHolder = streamHolder;
    }

    public static void registerAction(String action) {
        checkInit();
        streamHolder.getActionWriter().writeActions(action);
    }

    public static void registerLog(String tag, String msg) {
        checkInit();
        streamHolder.getLogWriter().writeLog(tag, msg);
    }

    public static void registerLog(String tag, Throwable throwable) {
        checkInit();
        streamHolder.getLogWriter().writeLog(tag, throwable);
    }

    public static void registerLog(String tag, String msg, Throwable throwable) {
        checkInit();
        streamHolder.getLogWriter().writeLog(tag, msg, throwable);
    }

    private static void checkInit() {
        if (streamHolder == null)
            throw new IllegalStateException(" LogSender is not initialized. Call LogSender.init() on Application class");
    }

    public static void clear() {
        streamHolder.getLogWriter().clearLog();
        streamHolder.getActionWriter().clearActions();
    }
}
