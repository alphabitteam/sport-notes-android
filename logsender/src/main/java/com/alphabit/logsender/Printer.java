package com.alphabit.logsender;

import java.io.StringWriter;
import java.util.concurrent.TimeUnit;

public interface Printer {

    CharSequence printAll();

    CharSequence printLatest(TimeUnit timeUnit, int count);

}
