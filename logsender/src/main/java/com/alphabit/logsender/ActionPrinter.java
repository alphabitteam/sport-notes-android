package com.alphabit.logsender;

import java.util.concurrent.TimeUnit;

class ActionPrinter implements Printer {
    private final MemoryLogger memoryLogger;
    public ActionPrinter(MemoryLogger memoryLogger) {
        this.memoryLogger = memoryLogger;
    }

    @Override
    public CharSequence printAll() {
        return memoryLogger.getSbAction().toString();
    }

    @Override
    public CharSequence printLatest(TimeUnit timeUnit, int count) {
        return memoryLogger.getSbAction().toString();
    }
}
