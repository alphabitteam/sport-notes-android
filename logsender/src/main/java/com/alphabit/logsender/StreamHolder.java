package com.alphabit.logsender;

class StreamHolder {

    private final Printer logPrinter;
    private final Printer actionPrinter;
    private final LogWriter logWriter;
    private final ActionWriter actionWriter;

    public StreamHolder(Printer logPrinter, Printer actionPrinter, LogWriter logWriter, ActionWriter actionWriter) {
        this.logPrinter = logPrinter;
        this.actionPrinter = actionPrinter;
        this.logWriter = logWriter;
        this.actionWriter = actionWriter;
    }

    public Printer getLogPrinter() {
        return logPrinter;
    }

    public Printer getActionPrinter() {
        return actionPrinter;
    }

    public LogWriter getLogWriter() {
        return logWriter;
    }

    public ActionWriter getActionWriter() {
        return actionWriter;
    }

}
