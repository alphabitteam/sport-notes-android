package com.alphabit.logsender;

public interface LogWriter {

    void clearLog();

    void writeLog(String tag, String msg);

    void writeLog(String tag, Throwable throwable);

    void writeLog(String tag, String msg, Throwable throwable);

}
