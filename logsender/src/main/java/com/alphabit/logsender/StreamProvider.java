package com.alphabit.logsender;

class StreamProvider {

    private final MemoryLogger memoryLogger = new MemoryLogger();
    private final ActionPrinter actionPrinter = new ActionPrinter(memoryLogger);
    private final LogPrinter logPrinter = new LogPrinter(memoryLogger);

    public Printer provideLogPrinter() {
        return logPrinter;
    }

    public Printer provideActionPrinter() {
        return actionPrinter;
    }

    public LogWriter provideLogWriter() {
        return memoryLogger;
    }

    public ActionWriter provideActionWriter() {
        return memoryLogger;
    }

}
