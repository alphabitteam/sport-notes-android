package com.alphabit.logsender;

import java.util.concurrent.TimeUnit;

class LogPrinter implements Printer {
    private final MemoryLogger memoryLogger;
    public LogPrinter(MemoryLogger memoryLogger) {
        this.memoryLogger = memoryLogger;
    }

    @Override
    public CharSequence printAll() {
        return memoryLogger.getSbLog().toString();
    }

    @Override
    public CharSequence printLatest(TimeUnit timeUnit, int count) {
        return memoryLogger.getSbLog().toString();
    }
}
