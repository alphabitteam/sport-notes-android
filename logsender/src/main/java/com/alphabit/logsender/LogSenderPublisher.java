package com.alphabit.logsender;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LogSenderPublisher {

    private static StreamHolder streamHolder;

    static void init(StreamHolder streamHolder) {
        LogSenderPublisher.streamHolder = streamHolder;
    }

    public static void showLogSenderDialog(final Context context) {

        checkInit();

        final StringBuffer sb = new StringBuffer();
        sb.append(streamHolder.getActionPrinter().printAll().toString());
        sb.append("\n");
        sb.append(streamHolder.getLogPrinter().printAll().toString());

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_logsender, null, false);
        TextView text = view.findViewById(R.id.text);
        Button button = view.findViewById(R.id.copy);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendToEmail(context, sb.toString());
            }
        });
        text.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        text.setSingleLine(false);
        if (sb.length() > 1)
            text.setText(sb.toString());
        builder.setView(view);
        builder.setTitle("Log");
        builder.setPositiveButton("окей", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LogSenderRegister.clear();
                Toast.makeText(context, "лог очищен", Toast.LENGTH_LONG).show();

            }
        });
        builder.show();
    }

    public static Printer getActions() {
        checkInit();
        return streamHolder.getActionPrinter();
    }

    public static Printer getLog() {
        checkInit();
        return streamHolder.getLogPrinter();
    }

    private static void checkInit() {
        if (streamHolder == null)
            throw new IllegalStateException(" LogSender is not initialized. Call LogSender.init() on Application class");
    }

    private static final void sendToEmail(Context context, String text) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"fly1232008@yandex.ru"});
        i.putExtra(Intent.EXTRA_SUBJECT, "logcat SportNotes");
        i.putExtra(Intent.EXTRA_TEXT   , text);
        try {
            context.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
