package com.alphabit.logsender;

import android.app.Application;

public class LogSender {

    private static StreamProvider streamProvider;

    public static void init(Application application) {
        streamProvider = new StreamProvider();
        StreamHolder streamHolder = new StreamHolder(
                streamProvider.provideLogPrinter(),
                streamProvider.provideActionPrinter(),
                streamProvider.provideLogWriter(),
                streamProvider.provideActionWriter());
        LogSenderRegister.init(streamHolder);
        LogSenderPublisher.init(streamHolder);
    }

}
