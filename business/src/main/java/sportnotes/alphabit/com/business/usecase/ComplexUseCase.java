package sportnotes.alphabit.com.business.usecase;

import com.annimon.stream.Optional;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import sportnotes.alphabit.com.business.CrushLogger;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.model.Purchase;
import sportnotes.alphabit.com.business.repository.IComplexesRepository;
import sportnotes.alphabit.com.business.usecase.contract.IBillingUseCase;
import sportnotes.alphabit.com.business.usecase.contract.IComplexUseCase;
import sportnotes.alphabit.com.business.utils.IAutoSyncManager;

/**
 * Created by Fly on 27.08.2017.
 */

public class ComplexUseCase extends UseCase implements IComplexUseCase {

    private final IComplexesRepository mComplexesRepository;
    private final IAutoSyncManager autoSyncManager;
    private final IBillingUseCase billingUseCase;

    public ComplexUseCase(final IComplexesRepository complexesRepository, IAutoSyncManager autoSyncManager, CrushLogger crushLogger, IBillingUseCase billingUseCase) {
        super(crushLogger);
        mComplexesRepository = complexesRepository;
        this.autoSyncManager = autoSyncManager;
        this.billingUseCase = billingUseCase;
    }

    @Override
    public Flowable<Optional<Complex>> getCurrentComplex() {
        return execute(mComplexesRepository.getCurrentComplex());
    }

    @Override
    public Flowable<List<Complex>> getComplexes(final int offset) {
        return execute(mComplexesRepository.getComplexes(PART_COUNT, offset));
    }

    @Override
    public Flowable<Complex> getComplexByName(final String name) {
        return execute(mComplexesRepository.getComplexByName(name));
    }

    @Override
    public Completable setCurrentComplex(final Complex complex) {
        return execute(mComplexesRepository.setCurrentComplex(complex));
    }

    @Override
    public Completable deleteComplex(final Complex complex) {
        autoSyncManager.registerChange();
        return execute(mComplexesRepository.deleteComplex(complex));
    }

    @Override
    public Completable createNewComplex(final String name) {
        autoSyncManager.registerChange();
        return execute(mComplexesRepository.createNewComplex(name));
    }

    @Override
    public Completable purchase(Purchase purchase) {
        return billingUseCase.purchase(purchase);
    }

    @Override
    public Completable savePurchases(List<Purchase> list) {
        return billingUseCase.savePurchases(list);
    }

    @Override
    public Completable editComplex(Complex complex) {
        return execute(mComplexesRepository.editComplex(complex));
    }
}
