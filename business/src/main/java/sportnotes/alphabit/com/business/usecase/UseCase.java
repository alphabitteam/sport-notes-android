package sportnotes.alphabit.com.business.usecase;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.schedulers.Schedulers;
import sportnotes.alphabit.com.business.CrushLogger;

/**
 * Created by Fly on 17.07.2017.
 */

public abstract class UseCase {
    public static final int PART_COUNT = 100;
    private final CrushLogger crushLogger;

    protected UseCase(CrushLogger crushLogger) {
        this.crushLogger = crushLogger;
    }

    protected <T> Single<T> execute(final Executor<T> executor) {
        return Single.create(emitter -> emitting(emitter, executor));
    }

    <T>Observable<T> execute(Observable<T> observable) {
        return observable
                .subscribeOn(Schedulers.io())
                .doOnError(this::logException);
    }

    <T>Flowable<T> execute(Flowable<T> observable) {
        return observable
                .subscribeOn(Schedulers.io())
                .doOnError(this::logException);
    }

    <T>Single<T> execute(Single<T> observable) {
        return observable.subscribeOn(Schedulers.computation())
                .doOnError(this::logException);
    }

    Completable execute(Completable observable) {
        return observable.subscribeOn(Schedulers.computation())
                .doOnError(this::logException);
    }

    protected Completable execute(final CompletableExecutor executor) {
        return Completable.fromCallable(() -> {
            executor.execute();
            return true;
        }).doOnError(this::logException);
    }

    private static <T> void emitting(final SingleEmitter<T> emitter, final Executor<T> executor) throws Exception {
        if (!emitter.isDisposed())
            emitter.onSuccess(executor.execute());
    }

    public interface Executor<T> {
        T execute();
    }

    public interface CompletableExecutor {
        void execute();
    }

    private void logException(Throwable throwable) {
        System.out.println("UseCase.logException = " + throwable);
        crushLogger.logException(throwable);
    }
}
