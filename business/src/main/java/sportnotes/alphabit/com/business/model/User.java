package sportnotes.alphabit.com.business.model;

import org.joda.time.DateTime;

import java.util.UUID;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 26.11.2017.
 */

public class User {

    private UUID uuid;
    private String email;
    private Object authKey;
    private String passwordHash;
    private Object passwordResetToken;
    private DateTime createdAt;
    private DateTime updatedAt;
    private String provider;
    private String firstname;
    private String lastname;
    private String externalId;
    private String avatarUrl;
    private DateTime subscribedTo;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getAuthKey() {
        return authKey;
    }

    public void setAuthKey(Object authKey) {
        this.authKey = authKey;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Object getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(Object passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(DateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public DateTime getSubscribedTo() {
        return subscribedTo;
    }

    public void setSubscribedTo(DateTime subscribedTo) {
        this.subscribedTo = subscribedTo;
    }
}
