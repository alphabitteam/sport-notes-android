package sportnotes.alphabit.com.business.utils;

import io.reactivex.annotations.NonNull;

/**
 * Created by Yuri Zigunov on 21.10.2017.
 */

public interface SimpleFunction<T, R> {

    R apply(@NonNull T t);

}