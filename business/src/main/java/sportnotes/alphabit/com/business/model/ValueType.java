package sportnotes.alphabit.com.business.model;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 25.12.17.
 */
public enum  ValueType {
    WEIGHT, COUNT
}
