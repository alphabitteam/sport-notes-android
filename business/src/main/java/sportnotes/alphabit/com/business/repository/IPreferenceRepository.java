package sportnotes.alphabit.com.business.repository;

import com.annimon.stream.Optional;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.TimerTask;
import sportnotes.alphabit.com.business.usecase.UseCase;

/**
 * Created by Yuri Zigunov on 30.09.2017.
 */

public interface IPreferenceRepository extends Repository {
    Flowable<String> getWeightUnit();

    Single<Optional<TimerTask>> getTimerTask();

    Completable setTimerTask(TimerTask timerTask);

    Completable saveWeightUnit(String unit);

    Completable saveRingtone(String uri);

    Flowable<String> getRingtone();

}
