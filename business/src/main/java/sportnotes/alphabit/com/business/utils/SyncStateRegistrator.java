package sportnotes.alphabit.com.business.utils;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import sportnotes.alphabit.com.business.model.SyncState;
import sportnotes.alphabit.com.business.repository.ISyncRepository;

public class SyncStateRegistrator {

    private final BehaviorSubject<SyncState> statusObservable = BehaviorSubject.create();
    private final ISyncRepository syncRepository;

    public SyncStateRegistrator(ISyncRepository syncRepository) {
        this.syncRepository = syncRepository;
    }

    public void dbChanged() {
        statusObservable.onNext(SyncState.NOT_SYNCHRONIZED);
    }

    public void syncComplete() {
        statusObservable.onNext(SyncState.SYNCHRONIZED);
    }

    public Observable<SyncState> getSyncStatus() {
        Observable.just(true)
                .map(aBoolean -> syncRepository.getSyncState())
                .subscribeOn(Schedulers.computation())
                .doOnNext(statusObservable::onNext);
        return statusObservable;
    }
}
