package sportnotes.alphabit.com.business.usecase.contract;

import com.annimon.stream.Optional;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.model.Purchase;

/**
 * Created by Fly on 27.08.2017.
 */

public interface IComplexUseCase {

    Flowable<Optional<Complex>> getCurrentComplex();

    Flowable<List<Complex>> getComplexes(int offset);

    Flowable<Complex> getComplexByName(String name);

    Completable setCurrentComplex(Complex complex);

    Completable deleteComplex(Complex complex);

    Completable createNewComplex(String name);

    Completable purchase(Purchase purchase);

    Completable savePurchases(List<Purchase> list);

    Completable editComplex(Complex complex);
}
