package sportnotes.alphabit.com.business.usecase;

import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import sportnotes.alphabit.com.business.CrushLogger;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.repository.IExercisesRepository;
import sportnotes.alphabit.com.business.repository.IPreferenceRepository;
import sportnotes.alphabit.com.business.usecase.contract.IEditExerciseUseCase;
import sportnotes.alphabit.com.business.utils.IAutoSyncManager;

/**
 * Created by Yuri Zigunov on 30.09.2017.
 */

public class EditExerciseUseCase extends UseCase implements IEditExerciseUseCase {

    private final IPreferenceRepository mRepository;
    private final IExercisesRepository mExercisesRepository;
    private final IAutoSyncManager autoSyncManager;

    public EditExerciseUseCase(final IPreferenceRepository repository, IExercisesRepository exercisesRepository, IAutoSyncManager autoSyncManager, CrushLogger crushLogger) {
        super(crushLogger);
        mRepository = repository;
        mExercisesRepository = exercisesRepository;
        this.autoSyncManager = autoSyncManager;
    }

    @Override
    public Flowable<String> getWeightUnit() {
        return execute(mRepository.getWeightUnit());
    }

    @Override
    public Flowable<Exercise> getExercise(UUID uuid) {
        return execute(mExercisesRepository.getExercise(uuid));
    }

    @Override
    public Completable saveExercise(final Exercise exercise) {
        autoSyncManager.registerChange();
        return execute(mExercisesRepository.saveExercise(exercise));
    }
}
