package sportnotes.alphabit.com.business.model;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by Fly on 29.08.2017.
 */

public class Complex extends UuidModel implements Serializable {

    private long position;

    private String name;

    public long getPosition() {
        return position;
    }

    public Complex setPosition(long position) {
        this.position = position;
        return this;
    }

    public String getName() {
        return name;
    }

    public Complex setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Complex) {
            return getUuid().equals(((Complex) o).getUuid());
        }
        return super.equals(o);
    }
}
