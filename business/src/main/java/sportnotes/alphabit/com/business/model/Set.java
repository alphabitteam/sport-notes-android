package sportnotes.alphabit.com.business.model;

import org.joda.time.DateTime;

import java.util.UUID;

/**
 * Created by Fly on 31.07.2017.
 */

public abstract class Set extends UuidModel {

    private UUID exerciseUuid;
    private RepeatType repeatType;
    private int number;
    private int repeat;
    private String comment;

    public Set(final RepeatType repeatType) {
        this.repeatType = repeatType;
    }

    public RepeatType getRepeatType() {
        return repeatType;
    }

    public UUID getExerciseUuid() {
        return exerciseUuid;
    }

    public void setExerciseUuid(UUID exerciseUuid) {
        this.exerciseUuid = exerciseUuid;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(final int number) {
        this.number = number;
    }

    public int getRepeat() {
        return repeat;
    }

    public void setRepeat(final int repeat) {
        this.repeat = repeat;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(final String comment) {
        this.comment = comment;
    }
}
