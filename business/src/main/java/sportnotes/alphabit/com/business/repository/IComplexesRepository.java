package sportnotes.alphabit.com.business.repository;

import com.annimon.stream.Optional;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.usecase.UseCase;

/**
 * Created by Fly on 29.08.2017.
 */

public interface IComplexesRepository extends Repository {
    Flowable<List<Complex>> getComplexes(final int count, final long offset);

    Completable setComplexes(List<Complex> complexes);

    Flowable<List<Complex>> getNotSyncComplexes(final int count, final long offset);

    Flowable<List<Complex>> getDeletedComplexes(final int count, final long offset);

    Flowable<Optional<Complex>> getCurrentComplex();

    Completable setCurrentComplex(final Complex complex);

    Completable deleteComplex(Complex complex);

    Completable moveAllToCandidateDelete();

    Completable removeAllCandidateDelete();

    Completable createNewComplex(String name);

    Flowable<Complex> getComplexByName(String name);

    Completable editComplex(Complex complex);

    Completable removeComplexes(List<Complex> complexes);
}
