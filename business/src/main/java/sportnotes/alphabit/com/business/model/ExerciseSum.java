package sportnotes.alphabit.com.business.model;

import java.io.Serializable;

/**
 * Created by Fly on 04.09.2017.
 */

public class ExerciseSum extends Exercise implements Serializable {

    private boolean hasWeight;
    private int setsValue;

    private RangeInt repeatCount = RangeInt.EMPTY;

    public ExerciseSum() {
        super(RepeatType.SUM);
    }

    public boolean hasWeight() {
        return hasWeight;
    }

    public void setHasWeight(boolean hasWeight) {
        this.hasWeight = hasWeight;
    }

    public RangeInt getRepeatCount() {
        return repeatCount;
    }

    public void setRepeatCount(final RangeInt repeatCount) {
        this.repeatCount = repeatCount;
    }

    public int getSetsValue() {
        return setsValue;
    }

    public void setSetsValue(final int setsValue) {
        this.setsValue = setsValue;
    }
}
