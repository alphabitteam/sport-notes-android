package sportnotes.alphabit.com.business.usecase.contract;

import io.reactivex.Completable;
import io.reactivex.Observable;
import sportnotes.alphabit.com.business.model.SyncState;

/**
 * Created by Yuri Zigunov on 15.10.2017.
 */

public interface ISynchronizationUseCase {

    Completable liteSynchronize();

    Completable synchronize();

    Observable<SyncState> getSyncState();

}
