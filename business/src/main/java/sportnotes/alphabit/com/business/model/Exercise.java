package sportnotes.alphabit.com.business.model;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * Created by Fly on 23.07.2017.
 */

public abstract class Exercise extends UuidModel implements Serializable {

    private UUID complexUuid;
    private String name;
    private String description;
    private WeekDay weekDay;
    private long position = -1;

    private int setsCount;
    private final RepeatType repeatType;
    private List<Set> sets;

    public long getPosition() {
        return position;
    }

    public void setPosition(final long position) {
        this.position = position;
    }

    public Exercise(final RepeatType repeatType) {
        this.repeatType = repeatType;
    }

    public UUID getComplexUuid() {
        return complexUuid;
    }

    public void setComplexUuid(UUID complexUuid) {
        this.complexUuid = complexUuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public WeekDay getWeekDay() {
        return weekDay;
    }

    public Exercise setWeekDay(WeekDay weekDay) {
        this.weekDay = weekDay;
        return this;
    }

    public int getSetsCount() {
        return setsCount;
    }

    public void setSetsCount(int setsCount) {
        this.setsCount = setsCount;
    }

    public RepeatType getRepeatType() {
        return repeatType;
    }

    public List<Set> getSets() {
        return sets;
    }

    public void setSets(List<Set> sets) {
        this.sets = sets;
    }
}
