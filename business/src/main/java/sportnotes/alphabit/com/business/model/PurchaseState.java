package sportnotes.alphabit.com.business.model;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 04.02.18.
 */
public enum PurchaseState {
    PurchasedSuccessfully,
    Canceled,
    Refunded,
    SubscriptionExpired
}