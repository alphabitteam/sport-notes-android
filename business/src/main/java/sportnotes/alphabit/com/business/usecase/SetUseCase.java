package sportnotes.alphabit.com.business.usecase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.CrushLogger;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.repository.IPreferenceRepository;
import sportnotes.alphabit.com.business.repository.ISetsRepository;
import sportnotes.alphabit.com.business.usecase.contract.ISetUseCase;
import sportnotes.alphabit.com.business.utils.IAutoSyncManager;

/**
 * Created by Yuri Zigunov on 07.10.2017.
 */

public class SetUseCase extends UseCase implements ISetUseCase {

    private final ISetsRepository mRepository;
    private final IAutoSyncManager autoSyncManager;
    private final IPreferenceRepository preferenceRepository;

    public SetUseCase(final ISetsRepository repository, IAutoSyncManager autoSyncManager, CrushLogger crushLogger, IPreferenceRepository preferenceRepository) {
        super(crushLogger);
        mRepository = repository;
        this.autoSyncManager = autoSyncManager;
        this.preferenceRepository = preferenceRepository;
    }

    @Override
    public Flowable<List<Set>> getSets(final UUID exerciseUuid, long offset) {
        return execute(mRepository.getSets(exerciseUuid, PART_COUNT, offset)
                .flatMap(sets -> {
                    List<Set> sortedList = new ArrayList<>(sets.size());
                    sortedList.addAll(sets);
                    Collections.sort(sortedList, (o1, o2) -> {
                        if (o1.getCreatedAt().getDayOfYear() < o2.getCreatedAt().getDayOfYear()) {
                            return 1;
                        } else if (o1.getNumber() == o2.getNumber()
                                && o1.getCreatedAt().getDayOfYear() == o2.getCreatedAt().getDayOfYear()) {
                            return o1.getCreatedAt().isBefore(o2.getCreatedAt()) ? 1 : 0;
                        } else {
                            return o1.getNumber() < o2.getNumber()
                                    && o1.getCreatedAt().getDayOfYear() == o2.getCreatedAt().getDayOfYear()
                                    ? 1 : -1;
                        }
                    });
                    return Flowable.just(sortedList);
                }));
    }

    @Override
    public Flowable<List<Set>> getLastSets(UUID exerciseUuid) {
        return execute(mRepository.getSets(exerciseUuid, 3, 0));
    }

    @Override
    public Flowable<List<Set>> getDetailLastSets(UUID exerciseUuid) {
        return execute(mRepository.getSets(exerciseUuid, 10, 0));
    }

    @Override
    public Completable deleteSet(final Set set) {
        autoSyncManager.registerChange();
        return execute(mRepository.deleteSet(set));
    }

    @Override
    public Flowable<String> getWeightUnit() {
        return execute(preferenceRepository.getWeightUnit());
    }
}
