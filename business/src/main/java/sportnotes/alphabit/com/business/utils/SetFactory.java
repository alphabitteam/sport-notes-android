package sportnotes.alphabit.com.business.utils;

import sportnotes.alphabit.com.business.exceptions.UnsupportedSetTypeException;
import sportnotes.alphabit.com.business.model.RepeatType;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.SetSum;
import sportnotes.alphabit.com.business.model.SetTime;
import sportnotes.alphabit.com.business.model.ValueType;

/**
 * Created by Yuri Zigunov on 15.10.2017.
 */

public class SetFactory {

    public static Set createSet(RepeatType repeatType, int value, ValueType valueType) {
        return createSet(repeatType, Integer.valueOf(value).longValue(), valueType);
    }

    public static Set createSet(RepeatType repeatType, long value, ValueType valueType) {
        switch (repeatType) {
            case SUM:
                SetSum setSum = new SetSum(valueType);
                setSum.setValue(Long.valueOf(value).intValue());
                return setSum;
            case TIME:
                SetTime setTime = new SetTime();
                setTime.setValue(value);
                return setTime;
            default: throw new UnsupportedSetTypeException();
        }
    }
}
