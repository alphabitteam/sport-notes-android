package sportnotes.alphabit.com.business.model;

/**
 * Created by fly12 on 20.11.2017.
 */

public class Purchases {

    private PurchaseResponseData responseData;

    private String originJson;

    private String signature;

    public Purchases(PurchaseResponseData responseData, String originJson, String signature) {
        this.responseData = responseData;
        this.signature = signature;
        this.originJson = originJson;
    }

    public String getOriginJson() {
        return originJson;
    }

    public void setOriginJson(String originJson) {
        this.originJson = originJson;
    }

    public PurchaseResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(PurchaseResponseData responseData) {
        this.responseData = responseData;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

}