package sportnotes.alphabit.com.business.model;

import org.joda.time.DateTime;

import java.util.UUID;

/**
 * Created by Fly on 24.07.2017.
 */

public abstract class UuidModel {

    private UUID uuid;
    private Status status;
    private boolean deleted;
    private DateTime createdAt;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof UuidModel)
            return uuid.equals(((UuidModel) o).getUuid());
        return super.equals(o);
    }

    public boolean equalsContent(UuidModel uuidModel) {
        return uuidModel.hashCode() == hashCode();
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public int hashCode() {
        System.out.println("UuidModel.hashCode");
        return super.hashCode();
    }
}
