package sportnotes.alphabit.com.business.model;

import java.io.Serializable;

/**
 * Created by Fly on 04.09.2017.
 */

public class ExerciseTime extends Exercise implements Serializable {

    private long setsValue;

    public ExerciseTime() {
        super(RepeatType.TIME);
    }

    public long getSetsValue() {
        return setsValue;
    }

    public void setSetsValue(final long setsValue) {
        this.setsValue = setsValue;
    }
}
