package sportnotes.alphabit.com.business.repository;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.WeekDay;

/**
 * Created by Fly on 04.09.2017.
 */

public interface IExercisesRepository extends Repository {
    Flowable<List<Exercise>> getExercises(final int count, final long offset, UUID complexUuid, WeekDay weekDay);

    Completable setExercises(List<Exercise> exercises);

    Flowable<List<Exercise>> getNotSyncExercises(final int count, final long offset);

    Flowable<List<Exercise>> getDeletedExercises(final int count, final long offset);

    Completable onChangeExercisePosition(Exercise exercise, int oldPosition, int newPosition);

    Completable deleteExercise(Exercise exercise);

    Completable moveAllToCandidateDelete();

    Completable removeAllCandidateDelete();

    Completable saveExercise(Exercise exercise);

    Flowable<Exercise> getExercise(UUID uuid);

    Completable updatePositions(List<Exercise> exercises, long offset);

    Completable removeExercises(List<Exercise> exercises);
}
