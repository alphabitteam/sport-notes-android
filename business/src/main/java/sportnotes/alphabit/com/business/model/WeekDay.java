package sportnotes.alphabit.com.business.model;

/**
 * Created by Fly on 14.04.2017.
 */

public enum WeekDay {
    MONDAY("monday"), TUESDAY("tuesday"), WEDNESDAY("wednesday"), THURSDAY("thursday"), FRIDAY("friday"), SATURDAY("saturday"), SUNDAY("sunday");

    String value;

    WeekDay(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static WeekDay getEnum(String value) {
        for (WeekDay v : values())
            if (v.value.equalsIgnoreCase(value)) return v;
        return null;
    }
}
