package sportnotes.alphabit.com.business.usecase.contract;

import com.annimon.stream.Optional;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.subjects.PublishSubject;
import sportnotes.alphabit.com.business.model.Provider;
import sportnotes.alphabit.com.business.model.User;
import sportnotes.alphabit.com.business.usecase.AuthorizationUseCase;

/**
 * Created by fly12 on 19.11.2017.
 */

public interface IAuthorizationUseCase {

    Completable login(Provider provider, String token);

    Single<Optional<User>> getUser();

    Completable logout();

    PublishSubject<AuthorizationUseCase.AuthEvent> getAuthEvents();
}
