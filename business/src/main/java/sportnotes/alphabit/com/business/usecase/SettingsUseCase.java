package sportnotes.alphabit.com.business.usecase;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import sportnotes.alphabit.com.business.CrushLogger;
import sportnotes.alphabit.com.business.repository.IPreferenceRepository;
import sportnotes.alphabit.com.business.usecase.contract.ISettingsUseCase;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 16.04.18.
 */
public class SettingsUseCase extends UseCase implements ISettingsUseCase {

    private final IPreferenceRepository mRepository;

    public SettingsUseCase(CrushLogger crushLogger, IPreferenceRepository mRepository) {
        super(crushLogger);
        this.mRepository = mRepository;
    }

    @Override
    public Completable saveWeightUnit(String unit) {
        return execute(mRepository.saveWeightUnit(unit));
    }

    @Override
    public Completable saveRingtone(String uri) {
        return execute(mRepository.saveRingtone(uri));
    }

    @Override
    public Flowable<String> getRingtone() {
        return execute(mRepository.getRingtone());
    }
}
