package sportnotes.alphabit.com.business.model;

public enum SyncState {
    SYNCHRONIZED, NOT_SYNCHRONIZED
}
