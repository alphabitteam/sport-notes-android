package sportnotes.alphabit.com.business.usecase;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.CrushLogger;
import sportnotes.alphabit.com.business.Serializer;
import sportnotes.alphabit.com.business.SyncEvent;
import sportnotes.alphabit.com.business.exceptions.UserLevelException;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.Status;
import sportnotes.alphabit.com.business.model.SyncState;
import sportnotes.alphabit.com.business.repository.IComplexesNetworkRepository;
import sportnotes.alphabit.com.business.repository.IComplexesRepository;
import sportnotes.alphabit.com.business.repository.IExercisesNetworkRepository;
import sportnotes.alphabit.com.business.repository.IExercisesRepository;
import sportnotes.alphabit.com.business.repository.ISetsNetworkRepository;
import sportnotes.alphabit.com.business.repository.ISetsRepository;
import sportnotes.alphabit.com.business.repository.ISyncRepository;
import sportnotes.alphabit.com.business.usecase.contract.ISynchronizationUseCase;
import sportnotes.alphabit.com.business.utils.Pager;
import sportnotes.alphabit.com.business.utils.SyncStateRegistrator;

/**
 * Created by Yuri Zigunov on 15.10.2017.
 */

public class SynchronizationUseCase extends UseCase implements ISynchronizationUseCase {

    private final IComplexesRepository mComplexesRepository;
    private final IExercisesRepository mExercisesRepository;
    private final ISetsRepository mSetsRepository;
    private final IComplexesNetworkRepository complexesNetworkRepository;
    private final IExercisesNetworkRepository exercisesNetworkRepository;
    private final ISetsNetworkRepository setsNetworkRepository;
    private final ISyncRepository syncRepository;
    private final Serializer serializer = new Serializer();
    private final SyncStateRegistrator syncStateRegistrator;

    private boolean syncIsProcess = false;

    public SynchronizationUseCase(final IComplexesRepository complexesRepository,
                                  final IExercisesRepository exercisesRepository,
                                  final ISetsRepository setsRepository,
                                  final IComplexesNetworkRepository complexesNetworkRepository,
                                  final IExercisesNetworkRepository exercisesNetworkRepository,
                                  final ISetsNetworkRepository setsNetworkRepository,
                                  final ISyncRepository syncRepository, CrushLogger crushLogger,
                                  final SyncStateRegistrator syncStateRegistrator) {
        super(crushLogger);
        mComplexesRepository = complexesRepository;
        mExercisesRepository = exercisesRepository;
        mSetsRepository = setsRepository;
        this.syncRepository = syncRepository;
        this.complexesNetworkRepository = complexesNetworkRepository;
        this.exercisesNetworkRepository = exercisesNetworkRepository;
        this.setsNetworkRepository = setsNetworkRepository;
        this.syncStateRegistrator = syncStateRegistrator;
    }


    @Override
    public Completable liteSynchronize() {
        System.out.println("SynchronizationUseCase.liteSynchronize start");
        return lite()
                .doOnComplete(syncStateRegistrator::syncComplete)
                .doOnComplete(() ->
                        EventBus.getDefault().post(new SyncEvent(SyncEvent.SyncStatus.END)));
    }

    private Completable lite() {
        return execute(syncRepository::isFullVersion)
                .flatMapCompletable(aBoolean -> aBoolean ? Completable.complete() : Completable.error(new UserLevelException()))
                .doOnComplete(() ->
                        EventBus.getDefault().post(new SyncEvent(SyncEvent.SyncStatus.START)))
                .andThen(execute(Completable.create(e ->
                        new Pager<Complex>(e, page -> Single.fromCallable(() -> syncRepository.getNotSyncComplexes(PART_COUNT, PART_COUNT * page)),
                                complexesNetworkRepository::setComplexes,
                                complexes -> Flowable.fromIterable(complexes).map(complex -> {
                                    complex.setStatus(Status.SENDED);
                                    return complex;
                                }).toList()
                                        .doAfterSuccess(complexes1 -> {
                                            System.out.println("SynchronizationUseCase.liteSynchronize getNotSyncComplexes and send size = " + complexes1.size() + " data = " + serializer.serialize(complexes1));
                                        }).flatMapCompletable(mComplexesRepository::setComplexes)))
                        .doOnError(throwable -> System.out.println("SynchronizationUseCase.liteSynchronize getNotSyncComplexes " + throwable))
                        .andThen(Completable.create(e ->
                                new Pager<Complex>(e, page -> Single.fromCallable(() -> syncRepository.getDeletedComplexes(PART_COUNT, PART_COUNT * page)),
                                        complexesNetworkRepository::deleteComplexes,
                                        mComplexesRepository::removeComplexes)))
                        .doOnComplete(() -> System.out.println("SynchronizationUseCase.liteSynchronize getDeletedComplexes complete"))
                        .doOnError(throwable -> System.out.println("SynchronizationUseCase.liteSynchronize getDeletedComplexes " + throwable))
                        .andThen(Completable.create(e ->
                                new Pager<Exercise>(e, page -> Single.fromCallable(() -> syncRepository.getNotSyncExercises(PART_COUNT, PART_COUNT * page)),
                                        exercisesNetworkRepository::setExercises,
                                        exercises -> Flowable.fromIterable(exercises).map(exercise -> {
                                            exercise.setStatus(Status.SENDED);
                                            return exercise;
                                        }).toList().doAfterSuccess(complexes1 -> {
                                            System.out.println("SynchronizationUseCase.liteSynchronize getNotSyncExercises and send size = " + complexes1.size() + " data = " + serializer.serialize(complexes1));
                                        }).flatMapCompletable(mExercisesRepository::setExercises)))
                                .andThen(Completable.create(e ->
                                        new Pager<Exercise>(e, page -> Single.fromCallable(() -> syncRepository.getDeletedExercises(PART_COUNT, PART_COUNT * page)),
                                                exercisesNetworkRepository::deleteExercise,
                                                mExercisesRepository::removeExercises))))
                        .doOnComplete(() -> System.out.println("SynchronizationUseCase.liteSynchronize getDeletedExercises complete"))
                        .doOnError(throwable -> System.out.println("SynchronizationUseCase.liteSynchronize getNotSyncExercises " + throwable))
                        .andThen(Completable.create(e ->
                                new Pager<Set>(e, page -> Single.fromCallable(() -> syncRepository.getNotSyncSets(PART_COUNT, PART_COUNT * page)),
                                        setsNetworkRepository::setSets,
                                        sets -> Flowable.fromIterable(sets).map(set -> {
                                            set.setStatus(Status.SENDED);
                                            return set;
                                        }).toList().doAfterSuccess(complexes1 -> {
                                            System.out.println("SynchronizationUseCase.liteSynchronize getNotSyncSets and send = " + complexes1.size() + " data = " + serializer.serialize(complexes1));
                                        }).flatMapCompletable(mSetsRepository::setSets)))
                                .andThen(Completable.create(e ->
                                        new Pager<Set>(e, page -> Single.fromCallable(() -> syncRepository.getDeletedSets(PART_COUNT, PART_COUNT * page)),
                                                setsNetworkRepository::deleteSets,
                                                mSetsRepository::removeSets))))
                        .doOnComplete(() -> System.out.println("SynchronizationUseCase.liteSynchronize getDeletedSets complete"))
                        .doOnError(throwable -> System.out.println("SynchronizationUseCase.liteSynchronize getNotSyncSets " + throwable))))
                .doOnComplete(() -> {
                    System.out.println("SynchronizationUseCase.liteSynchronize  end");
                })
                .doOnError(throwable -> {
                    System.out.println("SynchronizationUseCase.lite = " + throwable);
                });
    }

    @Override
    public Completable synchronize() {
        System.out.println("SynchronizationUseCase.synchronize start");
        return execute(syncRepository::isFullVersion)
                .flatMapCompletable(aBoolean -> aBoolean ? Completable.complete() : Completable.error(new UserLevelException()))
                .andThen(execute(lite()
                        .doOnComplete(() -> System.out.println("SynchronizationUseCase.synchronize"))
                        .andThen(mComplexesRepository.moveAllToCandidateDelete()
                                .andThen(Completable.create(e ->
                                        new Pager<Complex>(e, page -> complexesNetworkRepository.getComplexes(PART_COUNT, page),
                                                complexes -> Completable.complete(),
                                                complexes -> Flowable.fromIterable(complexes).map(complex -> {
                                                    complex.setStatus(Status.SENDED);
                                                    return complex;
                                                }).toList().doAfterSuccess(complexes1 -> {
                                                    System.out.println("SynchronizationUseCase.liteSynchronize to db = " + serializer.serialize(complexes1));
                                                }).flatMapCompletable(mComplexesRepository::setComplexes))))
                                .andThen(mComplexesRepository.removeAllCandidateDelete()))
                        .andThen(mExercisesRepository.moveAllToCandidateDelete()
                                .andThen(Completable.create(e ->
                                        new Pager<Exercise>(e, page -> exercisesNetworkRepository.getExercise(PART_COUNT, page),
                                                exercises -> Completable.complete(),
                                                exercises -> Flowable.fromIterable(exercises).map(exercise -> {
                                                    exercise.setStatus(Status.SENDED);
                                                    return exercise;
                                                }).toList().doAfterSuccess(complexes1 -> {
                                                    System.out.println("SynchronizationUseCase.liteSynchronize to db = " + serializer.serialize(complexes1));
                                                }).flatMapCompletable(mExercisesRepository::setExercises))))
                                .andThen(mExercisesRepository.removeAllCandidateDelete()))
                        .andThen(mSetsRepository.moveAllToCandidateDelete()
                                .andThen(Completable.create(e ->
                                        new Pager<Set>(e, page -> setsNetworkRepository.getSets(PART_COUNT, page),
                                                sets -> Completable.complete(),
                                                sets -> Flowable.fromIterable(sets).map(set -> {
                                                    set.setStatus(Status.SENDED);
                                                    return set;
                                                }).toList().doAfterSuccess(complexes1 -> {
                                                    System.out.println("SynchronizationUseCase.liteSynchronize to db = " + serializer.serialize(complexes1));
                                                }).flatMapCompletable(mSetsRepository::setSets))))
                                .andThen(mSetsRepository.removeAllCandidateDelete()))))
                .doOnComplete(() -> {
                    System.out.println("SynchronizationUseCase.synchronize end");
                })
                .doOnComplete(syncStateRegistrator::syncComplete)
                .doOnComplete(() ->
                        EventBus.getDefault().post(new SyncEvent(SyncEvent.SyncStatus.END)));
    }

    @Override
    public Observable<SyncState> getSyncState() {
        return syncStateRegistrator.getSyncStatus();
    }
}
