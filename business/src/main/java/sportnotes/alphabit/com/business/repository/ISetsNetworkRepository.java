package sportnotes.alphabit.com.business.repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Set;

/**
 * Created by Yuri Zigunov on 21.10.2017.
 */

public interface ISetsNetworkRepository extends Repository{

    Completable setSets(List<Set> exercises);

    Completable deleteSets(List<Set> exercises);

    Single<List<Set>> getSets(int count, long offset);
}
