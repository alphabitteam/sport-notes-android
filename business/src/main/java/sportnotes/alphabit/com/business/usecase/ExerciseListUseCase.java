package sportnotes.alphabit.com.business.usecase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import sportnotes.alphabit.com.business.CrushLogger;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.WeekDay;
import sportnotes.alphabit.com.business.repository.IExercisesRepository;
import sportnotes.alphabit.com.business.usecase.contract.IExerciseListUseCase;
import sportnotes.alphabit.com.business.utils.IAutoSyncManager;

/**
 * Created by Fly on 04.09.2017.
 */

public class ExerciseListUseCase extends UseCase implements IExerciseListUseCase {

    private final IExercisesRepository mExercisesRepository;
    private final IAutoSyncManager autoSyncManager;

    public ExerciseListUseCase(final IExercisesRepository exercisesRepository, IAutoSyncManager autoSyncManager, CrushLogger crushLogger) {
        super(crushLogger);
        mExercisesRepository = exercisesRepository;
        this.autoSyncManager = autoSyncManager;
    }

    @Override
    public Flowable<List<Exercise>> getExercises(final int offset, final UUID complexUuid, final WeekDay weekDay) {
        return execute(mExercisesRepository.getExercises(PART_COUNT, offset, complexUuid, weekDay)
                .flatMap(exercises -> {
                    List<Exercise> sortedList = new ArrayList<>(exercises.size());
                    sortedList.addAll(exercises);
                    //TODO: вынести в dagger
                    Collections.sort(sortedList, (o1, o2) -> {
                        boolean replase;
                        if (o1.getPosition() == o2.getPosition())
                            replase = o1.getCreatedAt().isAfter(o2.getCreatedAt());
                        else replase = o1.getPosition() > o2.getPosition();
                        return replase ? 1 : -1;
                    });
                    for (Exercise item : sortedList) {
                        System.out.println("ExerciseListUseCase.getExercises " + item.getPosition() + " created = " + item.getCreatedAt());
                    }

                    for (Exercise item : exercises) {
                        System.out.println("ExerciseListUseCase.getExercises src " + item.getPosition() + " created = " + item.getCreatedAt());
                    }
                    return Flowable.just(sortedList);
                }));
    }



    @Override
    public Completable onChangeExercisePosition(Exercise exercise, final int oldPosition, final int newPosition) {
        autoSyncManager.registerChange();
        return execute(mExercisesRepository.onChangeExercisePosition(exercise, oldPosition, newPosition));
    }

    @Override
    public Completable deleteExercise(final Exercise exercise) {
        autoSyncManager.registerChange();
        return execute(mExercisesRepository.deleteExercise(exercise));
    }

    @Override
    public Completable updatePositions(List<Exercise> exercises, long offset) {
        return execute(mExercisesRepository.updatePositions(exercises, offset));
    }
}
