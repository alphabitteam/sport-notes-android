package sportnotes.alphabit.com.business.model;

import java.util.UUID;

/**
 * Created by Yuri Zigunov on 07.10.2017.
 */

public class SetSum extends Set {

    private int value;
    @Deprecated
    private final ValueType valueType;

    public SetSum(ValueType valueType) {
        super(RepeatType.SUM);
        this.valueType = valueType;
    }

    @Deprecated
    public ValueType getValueType() {
        return valueType;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}
