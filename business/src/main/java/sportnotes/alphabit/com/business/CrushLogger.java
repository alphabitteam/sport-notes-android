package sportnotes.alphabit.com.business;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 23.12.17.
 */
public interface CrushLogger {

    void logException(Throwable throwable);
}
