package sportnotes.alphabit.com.business.model;

/**
 * Created by fly12 on 19.11.2017.
 */

public enum  Provider {
    G("google");
    String name;

    Provider(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
