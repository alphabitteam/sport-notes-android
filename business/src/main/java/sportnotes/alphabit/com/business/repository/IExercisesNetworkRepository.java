package sportnotes.alphabit.com.business.repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Exercise;

/**
 * Created by Yuri Zigunov on 21.10.2017.
 */

public interface IExercisesNetworkRepository extends Repository {
    Completable setExercises(List<Exercise> exercises);

    Completable deleteExercise(List<Exercise> exercises);

    Single<List<Exercise>> getExercise(int count, long offset);
}
