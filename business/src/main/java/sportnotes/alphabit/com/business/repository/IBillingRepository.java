package sportnotes.alphabit.com.business.repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Purchase;
import sportnotes.alphabit.com.business.model.Purchases;
import sportnotes.alphabit.com.business.usecase.UseCase;

/**
 * Created by fly12 on 20.11.2017.
 */

public interface IBillingRepository extends Repository {
    Completable setPurchase(Purchase purchase);

    Completable savePurchase(List<Purchase> list);
}
