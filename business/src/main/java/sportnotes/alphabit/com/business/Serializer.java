package sportnotes.alphabit.com.business;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;

import sportnotes.alphabit.com.business.utils.JodoDateTimeConverter;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */

public class Serializer {

    private final Gson gson;

    public Serializer() {
        gson = new GsonBuilder()
                .registerTypeAdapter(DateTime.class, new JodoDateTimeConverter())
                .create();
    }

    public String serialize(Object object, Class clazz) {
        return gson.toJson(object, clazz);
    }

    public String serialize(Object object) {
        return gson.toJson(object);
    }

    public <T> T deserialize(String string, Class<T> clazz) {
        return gson.fromJson(string, clazz);
    }
}
