package sportnotes.alphabit.com.business.model;

/**
 * Created by Fly on 19.04.2017.
 */

public enum Status {
    SENDING(0), SENDED(1);
    int value;

    Status(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public int getValue() {
        return value;
    }

    public static Status getEnum(int value) {
        for (Status v : values())
            if (v.value == value) return v;
        return null;
    }
}
