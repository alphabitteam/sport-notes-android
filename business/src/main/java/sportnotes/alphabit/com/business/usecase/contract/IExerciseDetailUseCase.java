package sportnotes.alphabit.com.business.usecase.contract;

import java.util.List;
import java.util.UUID;
import com.annimon.stream.Optional;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.TimerTask;

/**
 * Created by Yuri Zigunov on 02.10.2017.
 */

public interface IExerciseDetailUseCase {

    Flowable<Exercise> getExercise(UUID exerciseUuid);

    Flowable<String> getWeightUnit();

    Single<Optional<TimerTask>> getTimerTask();

    Completable saveTimerTask(TimerTask timerTask);

    Completable saveSet(Set set);

    Flowable<List<Set>> getLastSets(final UUID exerciseUuid);

    Flowable<Integer> getCountSetsToday(UUID mExerciseUuid);
}
