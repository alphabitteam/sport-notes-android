package sportnotes.alphabit.com.business.usecase.contract;

import io.reactivex.Completable;
import io.reactivex.Flowable;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 16.04.18.
 */
public interface ISettingsUseCase {

    Completable saveWeightUnit(String unit);

    Completable saveRingtone(String newValue);

    Flowable<String> getRingtone();
}
