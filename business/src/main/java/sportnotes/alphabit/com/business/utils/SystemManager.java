package sportnotes.alphabit.com.business.utils;

import org.joda.time.DateTime;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 24.12.17.
 */
public interface SystemManager {

    DateTime getCurrentTime();
}
