package sportnotes.alphabit.com.business.utils;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

/**
 * Created by Yuri Zigunov on 21.10.2017.
 */

public class Pager<T> {

    private final SimpleFunction<Integer, Single<List<T>>> srcFunction;

    private final SimpleFunction<List<T>, Completable> resFunction;

    private final SimpleFunction<List<T>, Completable> res2Function;

    private AtomicInteger page = new AtomicInteger();
    private boolean isLast;
    private Disposable d;
    private final CompletableEmitter emitter;

    public Pager(CompletableEmitter emitter, SimpleFunction<Integer, Single<List<T>>> srcFunction,
                 SimpleFunction<List<T>, Completable> resFunction,
                 SimpleFunction<List<T>, Completable> res2Function) {
        this.srcFunction = srcFunction;
        this.resFunction = resFunction;
        this.res2Function = res2Function;
        this.emitter = emitter;
        run();
    }

    private void run() {
        d = Single.fromCallable(() -> page.get())
                .doOnSuccess(aLong -> System.out.println("Pager.run start page = " + aLong))
                .flatMap(srcFunction::apply)
                .doOnSuccess((d) -> System.out.println("Pager.run src success"))
                .doOnError(throwable -> System.out.println("Pager.run src = " + throwable))
                .doOnSuccess(ts -> page.incrementAndGet())
                .doOnSuccess(list -> isLast = list.size() == 0)
                .flatMapCompletable(ts -> resFunction.apply(ts)
                        .concatWith(observer -> res2Function.apply(ts).subscribe(observer)))
                .doOnComplete(() -> System.out.println("Pager.run res success"))
                .doOnError(throwable -> System.out.println("Pager.run res = " + throwable))
                .subscribe(this::onGetData, emitter::onError);
    }

    private void onGetData() {
        System.out.println("Pager.onGetData isLast = " + isLast + " offset = " + page.get());
        if (d != null && !d.isDisposed())
            d.dispose();
        if (isLast)
            completePager();
        else
            run();
    }

    private void completePager() {
        if (emitter != null)
            emitter.onComplete();
    }
}
