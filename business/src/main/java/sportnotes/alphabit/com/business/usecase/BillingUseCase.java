package sportnotes.alphabit.com.business.usecase;

import java.util.List;

import io.reactivex.Completable;
import sportnotes.alphabit.com.business.CrushLogger;
import sportnotes.alphabit.com.business.model.Purchase;
import sportnotes.alphabit.com.business.repository.IBillingRepository;
import sportnotes.alphabit.com.business.usecase.contract.IBillingUseCase;

/**
 * Created by fly12 on 20.11.2017.
 */

public class BillingUseCase extends UseCase implements IBillingUseCase {

    private final IBillingRepository billingRepository;

    public BillingUseCase(IBillingRepository billingRepository, CrushLogger crushLogger) {
        super(crushLogger);
        this.billingRepository = billingRepository;
    }


    @Override
    public Completable purchase(Purchase purchase) {
        return execute(billingRepository.setPurchase(purchase));
    }

    @Override
    public Completable savePurchases(List<Purchase> list) {
        return execute(billingRepository.savePurchase(list));
    }
}
