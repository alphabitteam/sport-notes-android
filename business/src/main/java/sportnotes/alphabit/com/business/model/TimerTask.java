package sportnotes.alphabit.com.business.model;

import java.util.UUID;

/**
 * Created by Yuri Zigunov on 02.10.2017.
 */

public class TimerTask {

    private long mMillis;
    private long startValue;
    private boolean mIsRun;
    private UUID mUuid;

    public TimerTask(final long startValue, final long millis, final boolean isRun, final UUID uuid) {
        mMillis = millis;
        mIsRun = isRun;
        mUuid = uuid;
        this.startValue = startValue;
    }

    public long getStartValue() {
        return startValue;
    }

    public long getMillis() {
        return mMillis;
    }

    public void setMillis(final long millis) {
        mMillis = millis;
    }

    public boolean isRun() {
        return mIsRun;
    }

    public void setRun(final boolean run) {
        mIsRun = run;
    }

    public UUID getUuid() {
        return mUuid;
    }

    public void setUuid(final UUID uuid) {
        mUuid = uuid;
    }
}
