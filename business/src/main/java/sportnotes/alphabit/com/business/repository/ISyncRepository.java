package sportnotes.alphabit.com.business.repository;

import java.util.List;

import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.SyncState;

/**
 * Created by fly12 on 03.12.2017.
 */
public interface ISyncRepository {

    boolean isFullVersion();

    List<Complex> getNotSyncComplexes(final int count, final long skip);

    List<Complex> getDeletedComplexes(final int count, final long skip);

    List<Exercise> getNotSyncExercises(int count, long skip);

    List<Exercise> getDeletedExercises(final int count, final long skip);

    List<Set> getNotSyncSets(final int count, final long skip);

    List<Set> getDeletedSets(final int count, final long skip);

    SyncState getSyncState();
}
