package sportnotes.alphabit.com.business.usecase.contract;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.WeekDay;

/**
 * Created by Fly on 04.09.2017.
 */

public interface IExerciseListUseCase {
    Flowable<List<Exercise>> getExercises(final int offset, UUID complexUuid, WeekDay weekDay);

    Completable onChangeExercisePosition(Exercise exercise, int oldPosition, int newPosition);

    Completable deleteExercise(Exercise exercise);

    Completable updatePositions(final List<Exercise> exercises, long offset);
}
