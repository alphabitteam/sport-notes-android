package sportnotes.alphabit.com.business.repository;

import com.annimon.stream.Optional;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Provider;
import sportnotes.alphabit.com.business.model.User;
import sportnotes.alphabit.com.business.usecase.UseCase;

/**
 * Created by fly12 on 19.11.2017.
 */

public interface IAuthorizationRepository {
    Completable login(Provider provider, String token);
    Single<Optional<User>> getUser();

    Completable logout();
}
