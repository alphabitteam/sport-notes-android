package sportnotes.alphabit.com.business.usecase.contract;

import java.util.List;

import io.reactivex.Completable;
import sportnotes.alphabit.com.business.model.Purchase;

/**
 * Created by fly12 on 20.11.2017.
 */

public interface IBillingUseCase {

    Completable purchase(Purchase purchase);

    Completable savePurchases(List<Purchase> list);
}
