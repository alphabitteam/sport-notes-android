package sportnotes.alphabit.com.business.model;

/**
 * Created by Yuri Zigunov on 07.10.2017.
 */

public class SetTime extends Set {

    private long value;

    public SetTime() {
        super(RepeatType.TIME);
    }

    public long getValue() {
        return value;
    }

    public void setValue(final long value) {
        this.value = value;
    }
}
