package sportnotes.alphabit.com.business;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 22.04.18.
 */
public class SyncEvent {

    private final SyncStatus syncStatus;


    public SyncEvent(SyncStatus syncStatus) {
        this.syncStatus = syncStatus;
    }

    public SyncStatus getSyncStatus() {
        return syncStatus;
    }

    public enum SyncStatus {
        START, END
    }
}
