package sportnotes.alphabit.com.business.utils;

import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

/**
 * Created by fly12 on 18.11.2017.
 */

public class HandleUtils<T> {

    private final Function<Throwable, T> function;

    public HandleUtils(Function<Throwable, T> function) {
        this.function = function;
    }

    public Single<T> handle(Single<T> single) {
        return single.onErrorResumeNext(throwable -> Single.just(function.apply(throwable)));
    }

    public Completable handle(Completable single) {
        return single.onErrorResumeNext(throwable -> {
            try {
                function.apply(throwable);
                return Completable.complete();
            } catch (Throwable e) {
                return Completable.error(e);
            }
        });
    }
}
