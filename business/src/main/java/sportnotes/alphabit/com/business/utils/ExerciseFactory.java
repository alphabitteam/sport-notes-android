package sportnotes.alphabit.com.business.utils;

import sportnotes.alphabit.com.business.exceptions.UnsupportedExerciseTypeException;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.ExerciseSum;
import sportnotes.alphabit.com.business.model.ExerciseTime;
import sportnotes.alphabit.com.business.model.RepeatType;

/**
 * Created by Yuri Zigunov on 14.10.2017.
 */

public class ExerciseFactory {

    public static Exercise createExercise(RepeatType repeatType, int value) {
        return createExercise(repeatType, Integer.valueOf(value).longValue());
    }

    public static Exercise createExercise(RepeatType repeatType, long value) {
        switch (repeatType) {
            case SUM:
                ExerciseSum exerciseSum = new ExerciseSum();
                exerciseSum.setSetsValue(Long.valueOf(value).intValue());
                return exerciseSum;
            case TIME:
                ExerciseTime exerciseTime = new ExerciseTime();
                exerciseTime.setSetsValue(value);
                return exerciseTime;
            default: throw new UnsupportedExerciseTypeException();
        }
    }

}
