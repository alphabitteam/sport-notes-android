package sportnotes.alphabit.com.business.usecase;

import com.annimon.stream.Optional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.CrushLogger;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.TimerTask;
import sportnotes.alphabit.com.business.repository.IExercisesRepository;
import sportnotes.alphabit.com.business.repository.IPreferenceRepository;
import sportnotes.alphabit.com.business.repository.ISetsRepository;
import sportnotes.alphabit.com.business.usecase.contract.IExerciseDetailUseCase;
import sportnotes.alphabit.com.business.utils.IAutoSyncManager;

/**
 * Created by Yuri Zigunov on 02.10.2017.
 */

public class ExerciseDetailUseCase extends UseCase implements IExerciseDetailUseCase {

    private final IExercisesRepository mRepository;
    private final IPreferenceRepository mPreferenceRepository;
    private final ISetsRepository mSetsRepository;
    private final IAutoSyncManager autoSyncManager;

    public ExerciseDetailUseCase(final IExercisesRepository repository,
                                 IPreferenceRepository preferenceRepository,
                                 final ISetsRepository setsRepository, IAutoSyncManager autoSyncManager, CrushLogger crushLogger) {
        super(crushLogger);
        mRepository = repository;
        mPreferenceRepository = preferenceRepository;
        mSetsRepository = setsRepository;
        this.autoSyncManager = autoSyncManager;
    }

    @Override
    public Flowable<Exercise> getExercise(final UUID exerciseUuid) {
        return execute(mRepository.getExercise(exerciseUuid));
    }

    @Override
    public Flowable<String> getWeightUnit() {
        return execute(mPreferenceRepository.getWeightUnit());
    }

    @Override
    public Single<Optional<TimerTask>> getTimerTask() {
        return execute(mPreferenceRepository.getTimerTask());
    }

    @Override
    public Completable saveTimerTask(final TimerTask timerTask) {
        return execute(mPreferenceRepository.setTimerTask(timerTask));
    }

    @Override
    public Completable saveSet(final Set set) {
        autoSyncManager.registerChange();
        return execute(mSetsRepository.setSet(set));
    }

    @Override
    public Flowable<List<Set>> getLastSets(final UUID exerciseUuid) {
        return execute(mSetsRepository.getLastSets(exerciseUuid, 100, 0)
        .flatMap(sets -> {
            List<Set> sortedList = new ArrayList<>(sets.size());
            sortedList.addAll(sets);
            //TODO: вынести в dagger
            Collections.sort(sortedList, (o1, o2) -> {
                if (o1.getCreatedAt().getDayOfYear() < o2.getCreatedAt().getDayOfYear()) {
                    return 1;
                } else if (o1.getNumber() == o2.getNumber()
                        && o1.getCreatedAt().getDayOfYear() == o2.getCreatedAt().getDayOfYear()) {
                    return o1.getCreatedAt().isBefore(o2.getCreatedAt()) ? 1 : 0;
                } else {
                    return o1.getNumber() < o2.getNumber()
                            && o1.getCreatedAt().getDayOfYear() == o2.getCreatedAt().getDayOfYear()
                            ? 1 : -1;
                }
            });
            return Flowable.just(sortedList);
        }));
    }

    @Override
    public Flowable<Integer> getCountSetsToday(UUID mExerciseUuid) {
        return execute(mSetsRepository.getCountSetsToday(mExerciseUuid));
    }
}
