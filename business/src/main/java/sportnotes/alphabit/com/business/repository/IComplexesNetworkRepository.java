package sportnotes.alphabit.com.business.repository;

import org.reactivestreams.Publisher;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Complex;

/**
 * Created by Yuri Zigunov on 21.10.2017.
 */

public interface IComplexesNetworkRepository extends Repository {
    Completable setComplexes(List<Complex> complexes);

    Completable deleteComplexes(List<Complex> complexes);

    Single<List<Complex>> getComplexes(int count, long offset);
}
