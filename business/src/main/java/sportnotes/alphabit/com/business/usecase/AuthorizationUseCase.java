package sportnotes.alphabit.com.business.usecase;

import com.annimon.stream.Optional;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.subjects.PublishSubject;
import sportnotes.alphabit.com.business.CrushLogger;
import sportnotes.alphabit.com.business.model.Provider;
import sportnotes.alphabit.com.business.model.User;
import sportnotes.alphabit.com.business.repository.IAuthorizationRepository;
import sportnotes.alphabit.com.business.usecase.contract.IAuthorizationUseCase;

/**
 * Created by fly12 on 19.11.2017.
 */

public class AuthorizationUseCase extends UseCase implements IAuthorizationUseCase {

    private final IAuthorizationRepository repository;
    private final PublishSubject<AuthEvent> authEvents = PublishSubject.create();

    public AuthorizationUseCase(IAuthorizationRepository repository, CrushLogger crushLogger) {
        super(crushLogger);
        this.repository = repository;
    }

    @Override
    public Completable login(Provider provider, String token) {
        return execute(repository.login(provider, token))
                .doOnComplete(() -> authEvents.onNext(AuthEvent.LOGIN));
    }

    @Override
    public Single<Optional<User>> getUser() {
        return execute(repository.getUser());
    }

    @Override
    public Completable logout() {
        return execute(repository.logout())
                .doOnComplete(() -> authEvents.onNext(AuthEvent.LOGIN));
    }

    @Override
    public PublishSubject<AuthEvent> getAuthEvents() {
        return authEvents;
    }

    public enum AuthEvent {
        LOGIN, LOGOUT
    }
}
