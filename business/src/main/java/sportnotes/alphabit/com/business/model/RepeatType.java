package sportnotes.alphabit.com.business.model;

/**
 * Created by Fly on 21.04.2017.
 */

public enum RepeatType {
    SUM, TIME
}
