package sportnotes.alphabit.com.business.usecase.contract;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Set;

/**
 * Created by Yuri Zigunov on 07.10.2017.
 */

public interface ISetUseCase {
    Flowable<List<Set>> getSets(UUID exerciseUuid, long offset);

    Flowable<List<Set>> getLastSets(UUID exerciseUuid);

    Flowable<List<Set>> getDetailLastSets(UUID exerciseUuid);

    Completable deleteSet(Set set);

    Flowable<String> getWeightUnit();
}
