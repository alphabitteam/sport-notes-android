package sportnotes.alphabit.com.business.model;

/**
 * Created by Fly on 24.07.2017.
 */

public class RangeInt {
    private static final int EMPTY_VALUE = 0;
    private int start;
    private int end;

    public RangeInt(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public String getStartString() {
        return start == EMPTY_VALUE ? "" : String.valueOf(start);
    }

    public int getEnd() {
        return end;
    }

    public String getEndString() {
        return end == EMPTY_VALUE ? "" : String.valueOf(end);
    }

    public void setStart(int start) {
        this.start = start;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public static final RangeInt EMPTY = new RangeInt(EMPTY_VALUE, EMPTY_VALUE);

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (start != EMPTY_VALUE)
            stringBuilder.append(start);
        if (start != EMPTY_VALUE && end != EMPTY_VALUE)
            stringBuilder.append("-");
        if (end != EMPTY_VALUE)
            stringBuilder.append(end);
        return stringBuilder.toString();
    }

    public static RangeInt valueOf(String s) {
        String[] arr = s.split("-");
        int start = EMPTY_VALUE;
        int end = EMPTY_VALUE;
        if (arr.length > 0)
            start = Integer.valueOf(arr[0]);
        if (arr.length > 1)
            end = Integer.valueOf(arr[1]);
        return new RangeInt(start, end);
    }
}
