package sportnotes.alphabit.com.business.usecase.contract;

import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Exercise;

/**
 * Created by Yuri Zigunov on 30.09.2017.
 */

public interface IEditExerciseUseCase {
    Flowable<String> getWeightUnit();

    Flowable<Exercise> getExercise(UUID uuid);

    Completable saveExercise(Exercise exercise);
}
