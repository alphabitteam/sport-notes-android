package sportnotes.alphabit.com.business.utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 25.11.2017.
 */

public class JodoDateTimeConverter implements JsonDeserializer<DateTime>, JsonSerializer<DateTime> {

    private final static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public DateTime deserialize(final JsonElement je, final Type type, final JsonDeserializationContext jdc) throws JsonParseException {
        final String dateAsString = je.getAsString();
        return dateAsString.length() == 0 ? null : DATE_TIME_FORMATTER.parseDateTime(dateAsString);
    }

    @Override
    public JsonElement serialize(final DateTime src, final Type typeOfSrc,
                                 final JsonSerializationContext context) {
        return new JsonPrimitive(src == null ? "" : DATE_TIME_FORMATTER.print(src));
    }
}
