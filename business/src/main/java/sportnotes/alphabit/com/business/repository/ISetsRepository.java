package sportnotes.alphabit.com.business.repository;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Set;

/**
 * Created by Yuri Zigunov on 07.10.2017.
 */

public interface ISetsRepository extends Repository {
    Completable setSet(final Set set);

    Flowable<List<Set>> getSets(final UUID exerciseUuid, int count, long skip);

    Flowable<List<Set>> getLastSets(final UUID exerciseUuid, final int count, final long skip);

    Completable deleteSet(Set set);

    Completable setSets(List<Set> exercises);

    Flowable<List<Set>> getNotSyncSets(final int count, final long offset);

    Flowable<List<Set>> getDeletedSets(final int count, final long offset);

    Completable moveAllToCandidateDelete();

    Completable removeAllCandidateDelete();

    Flowable<Integer> getCountSetsToday(UUID mExerciseUuid);

    Completable removeSets(List<Set> sets);
}
