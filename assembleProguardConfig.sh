#!/bin/sh
out="./app/proguard-rules.pro"
echo "# don't modify this file directly, add modular rules to /proguard from https://github.com/krschultz/android-proguard-snippets">$out
for inp in ./proguard/*.pro; do
    echo "# --- $inp" >> $out
    cat $inp >> $out
done
