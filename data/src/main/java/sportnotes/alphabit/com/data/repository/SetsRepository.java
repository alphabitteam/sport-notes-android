package sportnotes.alphabit.com.data.repository;

import android.util.Log;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.Status;
import sportnotes.alphabit.com.business.repository.ISetsRepository;
import sportnotes.alphabit.com.data.storages.mappers.db.SetMapper;
import sportnotes.alphabit.com.data.storages.sets.SetsLocalStorage;
import sportnotes.alphabit.com.data.storages.sets.db.SetEntity;
import sportnotes.alphabit.com.data.utils.DateTimeUtils;
import sportnotes.alphabit.com.data.utils.UserLevelManager;

/**
 * Created by Yuri Zigunov on 07.10.2017.
 */

public class SetsRepository extends Repository implements ISetsRepository {

    private final SetsLocalStorage mSetsLocalStorage;
    private final UserLevelManager userLevelManager;
    private final SetMapper setMapper = new SetMapper();

    public SetsRepository(SetsLocalStorage setsLocalStorage, UserLevelManager userLevelManager) {
        mSetsLocalStorage = setsLocalStorage;
        this.userLevelManager = userLevelManager;
    }

    @Override
    public Completable setSet(final Set set) {
        return mSetsLocalStorage.insertSet(setMapper.to(set));
    }

    @Override
    public Flowable<List<Set>> getSets(final UUID exerciseUuid, final int count, final long skip) {
        return getSets(exerciseUuid, count, skip, SetsLocalStorage.Vector.START);
    }

    @Override
    public Flowable<List<Set>> getLastSets(final UUID exerciseUuid, final int count, final long skip) {
        return getSets(exerciseUuid, count, skip, SetsLocalStorage.Vector.END);
    }

    private Flowable<List<Set>> getSets(final UUID exerciseUuid, final int count, final long skip, SetsLocalStorage.Vector vector) {
        return execute(mSetsLocalStorage.getSets(exerciseUuid, count, skip, vector)
                .map(setEntities -> {
                    List<Set> res = new ArrayList<>();
                    for (SetEntity item : setEntities) {
                        res.add(setMapper.from(item));
                    }
                    return res;
                }));
    }

    @Override
    public Completable deleteSet(final Set set) {
        System.out.println("SetsRepository.deleteSet isFullVersion = " + userLevelManager.isFullVersion());
        if (!userLevelManager.isFullVersion())
            return execute(mSetsLocalStorage.removeSet(setMapper.to(set)));
        return execute(mSetsLocalStorage.deleteSet(set.getUuid()));
    }

    @Override
    public Completable removeSets(List<Set> sets) {
        return execute(Completable.fromCallable(() -> {
            SetEntity[] res = new SetEntity[sets.size()];
            for (int i = 0; i < res.length; i++) {
                res[i] = setMapper.to(sets.get(i));
            }
            return mSetsLocalStorage.removeSet(res);
        }));
    }

    @Override
    public Completable setSets(final List<Set> sets) {
        List<SetEntity> list = new ArrayList<>();
        for (Set set: sets) {
            list.add(setMapper.to(set));
        }
        return execute(mSetsLocalStorage.setSets(list));
    }

    @Override
    public Flowable<List<Set>> getNotSyncSets(final int count, final long offset) {
        return execute(mSetsLocalStorage.getNotSyncSets(count, offset)
                .map(setEntities -> {
                    List<Set> res = new ArrayList<>();
                    for (SetEntity item : setEntities) {
                        res.add(setMapper.from(item));
                    }
                    return res;
                }));
    }

    @Override
    public Flowable<List<Set>> getDeletedSets(final int count, final long offset) {
        Log.d(getClass().getSimpleName(), "getDeletedSets: " + count + " offset = " + offset);
        return execute(mSetsLocalStorage.getDeletedSets(count, offset)
                .doOnError(throwable -> Log.d(getClass().getSimpleName(), "getDeletedSets: error = " + throwable))
                .map(setEntities -> {
                    Log.d(getClass().getSimpleName(), "getDeletedSets: " + setEntities.size());
                    List<Set> res = new ArrayList<>();
                    for (SetEntity item : setEntities) {
                        res.add(setMapper.from(item));
                    }
                    return res;
                }));
    }

    @Override
    public Completable moveAllToCandidateDelete() {
        return execute(mSetsLocalStorage.moveAllToCandidateDelete());
    }

    @Override
    public Completable removeAllCandidateDelete() {
        return execute(mSetsLocalStorage.removeAllCandidateDelete());
    }

    @Override
    public Flowable<Integer> getCountSetsToday(UUID mExerciseUuid) {
        DateTime dateTime = DateTime.now();
        return execute(mSetsLocalStorage.getCountSetsToday(mExerciseUuid,
                DateTimeUtils.getStartDay(dateTime), DateTimeUtils.getEndDay(dateTime)));
    }

}
