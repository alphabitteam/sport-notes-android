package sportnotes.alphabit.com.data.repository;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.SetSum;
import sportnotes.alphabit.com.business.model.SetTime;
import sportnotes.alphabit.com.business.model.WeekDay;
import sportnotes.alphabit.com.business.repository.IExercisesRepository;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseLocalStorage;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseEntity;
import sportnotes.alphabit.com.data.storages.mappers.db.ExerciseMapper;
import sportnotes.alphabit.com.data.storages.mappers.db.SetMapper;
import sportnotes.alphabit.com.data.storages.sets.SetsLocalStorage;
import sportnotes.alphabit.com.data.storages.sets.db.SetEntity;
import sportnotes.alphabit.com.data.utils.UserLevelManager;

/**
 * Created by Fly on 04.09.2017.
 */

public class ExercisesRepository extends Repository implements IExercisesRepository {

    private final ExerciseLocalStorage mExerciseLocalStorage;
    private final SetsLocalStorage setsLocalStorage;
    private final ExerciseMapper exerciseMapper = new ExerciseMapper();
    private final SetMapper setMapper = new SetMapper();
    private final UserLevelManager userLevelManager;

    public ExercisesRepository(Context context, final ExerciseLocalStorage exerciseLocalStorage, SetsLocalStorage setsLocalStorage,
                               UserLevelManager userLevelManager) {
        mExerciseLocalStorage = exerciseLocalStorage;
        this.setsLocalStorage = setsLocalStorage;
        this.userLevelManager = userLevelManager;
    }

    @Override
    public Flowable<List<Exercise>> getExercises(final int count, final long offset, final UUID complexUuid, final WeekDay weekDay) {
        return execute(mExerciseLocalStorage.getWeekDayExercise(complexUuid, weekDay, count, offset)
                .map(exerciseEntities -> {
                    List<Exercise> res = new ArrayList<>();
                    for (ExerciseEntity item : exerciseEntities) {
                        res.add(exerciseMapper.from(item));
                    }
                    List<List<SetEntity>> sets = setsLocalStorage.getSets(exerciseEntities, 100, 0, SetsLocalStorage.Vector.END);
                    List<SetEntity> setEntities;

                    for (int i = 0; i < res.size(); i++) {
                        setEntities = sets.get(i);
                        List<Set> resSets = new ArrayList<>();
                        for (SetEntity item : setEntities) {
                            resSets.add(setMapper.from(item));
                        }
                        Collections.sort(resSets, (o1, o2) -> {
                            if (o1.getCreatedAt().getDayOfYear() < o2.getCreatedAt().getDayOfYear()) {
                                return 1;
                            } else if (o1.getNumber() == o2.getNumber()
                                    && o1.getCreatedAt().getDayOfYear() == o2.getCreatedAt().getDayOfYear()) {
                                return o1.getCreatedAt().isBefore(o2.getCreatedAt()) ? 1 : 0;
                            } else {
                                return o1.getNumber() < o2.getNumber()
                                        && o1.getCreatedAt().getDayOfYear() == o2.getCreatedAt().getDayOfYear()
                                        ? 1 : -1;
                            }
                        });
                        if (resSets.size() >= 3)
                            resSets = resSets.subList(0, 3);
                        res.get(i).setSets(resSets);
                    }
                    return res;
                }));

    }

    @Override
    public Completable setExercises(final List<Exercise> exercises) {
        List<ExerciseEntity> list = new ArrayList<>();

        for (Exercise exercise : exercises) {
            System.out.println("ExercisesRepository.setExercises = " + exercise.getName() + " pos = " + exercise.getPosition());
            list.add(exerciseMapper.to(exercise));
        }
        return execute(mExerciseLocalStorage.setExercises(list));
    }

    @Override
    public Flowable<List<Exercise>> getNotSyncExercises(final int count, final long offset) {
        return execute(mExerciseLocalStorage.getNotSyncExercises(count, offset)
                .map(exerciseEntities -> {
                    List<Exercise> res = new ArrayList<>();
                    for (ExerciseEntity item : exerciseEntities) {
                        res.add(exerciseMapper.from(item));
                    }
                    return res;
                }));
    }

    @Override
    public Flowable<List<Exercise>> getDeletedExercises(final int count, final long offset) {
        return execute(mExerciseLocalStorage.getDeletedExercises(count, offset)
                .map(exerciseEntities -> {
                    List<Exercise> res = new ArrayList<>();
                    for (ExerciseEntity item : exerciseEntities) {
                        res.add(exerciseMapper.from(item));
                    }
                    return res;
                }));
    }

    @Override
    public Completable removeExercises(List<Exercise> exercises) {
        return execute(Completable.fromCallable(() -> {
            ExerciseEntity[] res = new ExerciseEntity[exercises.size()];
            for (int i = 0; i < res.length; i++) {
                res[i] = exerciseMapper.to(exercises.get(i));
            }
            return mExerciseLocalStorage.removeExercise(res);
        }));
    }

    @Override
    public Completable onChangeExercisePosition(final Exercise exercise, final int oldPosition, final int newPosition) {
        if (exercise == null)
            return Completable.complete();
        if (newPosition > oldPosition) {
            return mExerciseLocalStorage.changePositionToDown(exercise.getUuid(), oldPosition, newPosition);
        } else if (newPosition < oldPosition) {
            return mExerciseLocalStorage.changePositionToUp(exercise.getUuid(), oldPosition, newPosition);
        }
        return Completable.complete();
    }

    @Override
    public Completable deleteExercise(final Exercise exercise) {
        if (!userLevelManager.isFullVersion())
            return execute(mExerciseLocalStorage.removeExercise(exerciseMapper.to(exercise)))
                    .andThen(setsLocalStorage.removeSetsByExerciseUuid(exercise.getUuid()));
        return execute(mExerciseLocalStorage.deleteExercise(exercise.getUuid()))
                .andThen(setsLocalStorage.removeSetsByExerciseUuid(exercise.getUuid()));
    }

    @Override
    public Completable moveAllToCandidateDelete() {
        return execute(mExerciseLocalStorage.moveAllToCandidateDelete());
    }

    @Override
    public Completable removeAllCandidateDelete() {
        return execute(mExerciseLocalStorage.removeAllCandidateDelete());
    }

    @Override
    public Completable saveExercise(final Exercise exercise) {
        return execute(mExerciseLocalStorage.insertExercise(exerciseMapper.to(exercise)));
    }

    @Override
    public Flowable<Exercise> getExercise(final UUID uuid) {
        return execute(mExerciseLocalStorage.getExercise(uuid)
                .map(exerciseMapper::from));
    }

    @Override
    public Completable updatePositions(final List<Exercise> exercises, long offset) {
        List<ExerciseEntity> list = new ArrayList<>();

        for (Exercise exercise : exercises) {
            list.add(exerciseMapper.to(exercise));
        }
        return execute(mExerciseLocalStorage.updatePositions(list, offset));
    }
}
