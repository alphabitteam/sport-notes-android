package sportnotes.alphabit.com.data.storages.exercise.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by fly12 on 03.12.2017.
 */
@Dao
public interface ExerciseSyncDao {

    @Query("SELECT * FROM exercise WHERE status=0 AND deleted=0 LIMIT :count OFFSET :skip")
    public abstract List<ExerciseEntity> getNotSyncExercises(int count, long skip);

    @Query("SELECT * FROM exercise WHERE deleted=1 LIMIT :count OFFSET :skip")
    public abstract List<ExerciseEntity> getDeletedExercises(final int count, final long skip);

}
