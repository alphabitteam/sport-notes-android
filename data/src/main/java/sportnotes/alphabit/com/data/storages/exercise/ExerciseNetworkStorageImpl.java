package sportnotes.alphabit.com.data.storages.exercise;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.data.api.ServerApi;
import sportnotes.alphabit.com.data.api.results.ExerciseResult;
import sportnotes.alphabit.com.data.storages.exercise.network.ExerciseDto;
import sportnotes.alphabit.com.data.storages.mappers.network.ExerciseMapper;

/**
 * Created by Yuri Zigunov on 07.10.2017.
 */

public class ExerciseNetworkStorageImpl implements ExerciseNetworkStorage {

    private final ServerApi serverApi;

    public ExerciseNetworkStorageImpl(final ServerApi serverApi) {
        this.serverApi = serverApi;
    }

    @Override
    public Completable setExercises(final List<ExerciseDto> list) {
        return serverApi.setExercises(list).toCompletable();
    }

    @Override
    public Completable deleteExercise(final List<ExerciseDto> list) {
        return Observable.fromIterable(list)
                .map(ExerciseDto::getUuid)
                .map(UUID::toString)
                .toList()
                .flatMap(serverApi::deleteExercise).toCompletable();
    }

    @Override
    public Single<ExerciseResult> getExercise(final int count, final long offset) {
        return serverApi.getExercise(count, offset);
    }
}
