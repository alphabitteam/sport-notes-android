package sportnotes.alphabit.com.data.storages.mappers.network;

import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.SetSum;
import sportnotes.alphabit.com.business.model.SetTime;
import sportnotes.alphabit.com.business.model.ValueType;
import sportnotes.alphabit.com.business.utils.SetFactory;
import sportnotes.alphabit.com.data.storages.mappers.db.PairMapper;
import sportnotes.alphabit.com.data.storages.sets.network.SetDto;

/**
 * Created by Yuri Zigunov on 22.10.2017.
 */

public class SetMapper implements PairMapper<SetDto, Set> {
    @Override
    public Set from(final SetDto setDto) {
        //TODO: fix hardcode ValueType.COUNT
        Set set = SetFactory.createSet(setDto.getRepeatType(), setDto.getValue(), ValueType.COUNT);
        set.setUuid(setDto.getUuid());
        set.setRepeat(setDto.getRepeat());
        set.setCreatedAt(setDto.getCreatedAt());
        set.setExerciseUuid(setDto.getExerciseUuid());
        set.setComment(setDto.getComment());
        set.setNumber(setDto.getNumber());
        return set;
    }

    @Override
    public SetDto to(final Set set) {
        SetDto setDto = new SetDto();
        setDto.setUuid(set.getUuid());
        setDto.setRepeat(set.getRepeat());
        setDto.setCreatedAt(set.getCreatedAt());
        setDto.setExerciseUuid(set.getExerciseUuid());
        setDto.setComment(set.getComment());
        setDto.setNumber(set.getNumber());
        setDto.setRepeatType(set.getRepeatType());

        if (set instanceof SetSum) {
            setDto.setValueType(((SetSum) set).getValueType());
            setDto.setValue(Integer.valueOf(((SetSum) set).getValue()).longValue());
        } else if (set instanceof SetTime)
            setDto.setValue(((SetTime) set).getValue());
        return setDto;
    }
}
