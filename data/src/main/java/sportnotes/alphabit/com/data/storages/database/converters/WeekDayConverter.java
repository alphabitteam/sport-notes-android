package sportnotes.alphabit.com.data.storages.database.converters;

import android.arch.persistence.room.TypeConverter;

import sportnotes.alphabit.com.business.model.RepeatType;
import sportnotes.alphabit.com.business.model.WeekDay;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public class WeekDayConverter {

    @TypeConverter
    public static WeekDay stringToType(String s) {
        if (s == null)
            return null;
        return WeekDay.getEnum(s);
    }

    @TypeConverter
    public static String typeToString(WeekDay weekDay) {
        if (weekDay == null)
            return null;
        return weekDay.toString();
    }
}
