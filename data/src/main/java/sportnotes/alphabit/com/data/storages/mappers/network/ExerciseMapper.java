package sportnotes.alphabit.com.data.storages.mappers.network;

import android.util.Log;

import com.google.gson.Gson;

import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.ExerciseSum;
import sportnotes.alphabit.com.business.model.ExerciseTime;
import sportnotes.alphabit.com.business.model.RangeInt;
import sportnotes.alphabit.com.business.utils.ExerciseFactory;
import sportnotes.alphabit.com.data.storages.exercise.network.ExerciseDto;
import sportnotes.alphabit.com.data.storages.mappers.db.PairMapper;

/**
 * Created by Yuri Zigunov on 22.10.2017.
 */

public class ExerciseMapper implements PairMapper<ExerciseDto, Exercise> {
    @Override
    public Exercise from(final ExerciseDto exerciseDto) {
        Log.d(getClass().getSimpleName(), "from: " + new Gson().toJson(exerciseDto));
        Exercise exercise = ExerciseFactory.createExercise(exerciseDto.getRepeatType(), exerciseDto.getSetsValue());
        try {
            exercise.setUuid(exerciseDto.getUuid());
            exercise.setComplexUuid(exerciseDto.getComplexUuid());
            exercise.setName(exerciseDto.getName());
            exercise.setDescription(exerciseDto.getDescription());
            exercise.setSetsCount(exerciseDto.getSetsCount());
            exercise.setWeekDay(exerciseDto.getWeekDay());
            exercise.setPosition(exerciseDto.getPosition());
            exercise.setCreatedAt(exerciseDto.getCreatedAt());

            if (exercise instanceof ExerciseSum) {
                ((ExerciseSum) exercise).setRepeatCount(RangeInt.valueOf(exerciseDto.getRepeatCount()));
                ((ExerciseSum) exercise).setHasWeight(exerciseDto.getSetsValue() != 0);
            }
        } catch (Exception e) {
            Log.d(getClass().getSimpleName(), "from: " + e);
        }
        return exercise;
    }

    @Override
    public ExerciseDto to(final Exercise exercise) {
        ExerciseDto exerciseDto = new ExerciseDto();
        exerciseDto.setUuid(exercise.getUuid());
        exerciseDto.setComplexUuid(exercise.getComplexUuid());
        exerciseDto.setName(exercise.getName());
        exerciseDto.setDescription(exercise.getDescription());
        exerciseDto.setSetsCount(exercise.getSetsCount());
        exerciseDto.setWeekDay(exercise.getWeekDay());
        exerciseDto.setRepeatType(exercise.getRepeatType());
        exerciseDto.setPosition(exercise.getPosition());
        exerciseDto.setCreatedAt(exercise.getCreatedAt());

        if (exercise instanceof ExerciseSum) {
            exerciseDto.setSetsValue((long) ((ExerciseSum) exercise).getSetsValue());
            exerciseDto.setRepeatCount(String.valueOf(((ExerciseSum) exercise).getRepeatCount()));
        } else if (exercise instanceof ExerciseTime)
            exerciseDto.setSetsValue(exerciseDto.getSetsValue());
        return exerciseDto;
    }
}
