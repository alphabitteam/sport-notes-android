package sportnotes.alphabit.com.data.api.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.UUID;

/**
 * Created by fly12 on 19.11.2017.
 */

public class UserDto {

    @SerializedName("uuid")
    @Expose
    private UUID uuid;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("auth_key")
    @Expose
    private Object authKey;
    @SerializedName("password_hash")
    @Expose
    private String passwordHash;
    @SerializedName("password_reset_token")
    @Expose
    private Object passwordResetToken;
    @SerializedName("created_at")
    @Expose
    private DateTime createdAt;
    @SerializedName("updated_at")
    @Expose
    private DateTime updatedAt;
    @SerializedName("provider")
    @Expose
    private String provider;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("external_id")
    @Expose
    private String externalId;
    @SerializedName("avatar_url")
    @Expose
    private String avatarUrl;
    @SerializedName("subscribed_to")
    @Expose
    private DateTime subscribedTo;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getAuthKey() {
        return authKey;
    }

    public void setAuthKey(Object authKey) {
        this.authKey = authKey;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Object getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(Object passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(DateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public DateTime getSubscribedTo() {
        return subscribedTo;
    }

    public void setSubscribedTo(DateTime subscribedTo) {
        this.subscribedTo = subscribedTo;
    }

}