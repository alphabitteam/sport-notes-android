package sportnotes.alphabit.com.data.storages.complex.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import sportnotes.alphabit.com.data.storages.base.db.BaseDao;
import sportnotes.alphabit.com.data.storages.complex.ComplexLocalStorage;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */
@Dao
public interface ComplexDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertComplex(ComplexEntity... complexEntity);

    @Delete
    void removeComplex(ComplexEntity... complexEntity);

    @Query("UPDATE complex SET deleted=1, position=0, createdAt=0, updatedAt=0, status=0 WHERE uuid=:uuid")
    void deleteComplex(UUID uuid);

    @Query("SELECT * FROM complex WHERE deleted=0 LIMIT :count OFFSET :skip")
    Flowable<List<ComplexEntity>> getComplexes(int count, long skip);

    @Query("SELECT * FROM complex WHERE deleted=0 AND uuid=:uuid")
    Flowable<ComplexEntity> getComplex(UUID uuid);

    @Query("SELECT * FROM complex WHERE status=0 AND deleted=0 LIMIT :count OFFSET :skip")
    Flowable<List<ComplexEntity>> getNotSyncComplexes(final int count, final long skip);

    @Query("SELECT * FROM complex WHERE deleted=1 AND status=0 LIMIT :count OFFSET :skip")
    Flowable<List<ComplexEntity>> getDeletedComplexes(final int count, final long skip);

    @Query("UPDATE complex SET candidateDelete=1")
    void moveAllToCandidateDelete();

    @Query("DELETE FROM complex WHERE candidateDelete=1")
    void removeAllCandidateDelete();

    @Query("SELECT * FROM complex WHERE name=:name")
    Flowable<ComplexEntity> getComplexByName(String name);

    @Query("SELECT count(*) FROM complex")
    int getCountComplex();

    @Query("UPDATE complex SET name=:name, status=0 WHERE uuid=:uuid")
    void editComplex(UUID uuid, String name);
}
