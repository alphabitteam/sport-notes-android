package sportnotes.alphabit.com.data.api.results;

import java.util.List;

import sportnotes.alphabit.com.data.api.dto.ErrorDto;
import sportnotes.alphabit.com.data.storages.sets.network.SetDto;

/**
 * Created by fly12 on 19.11.2017.
 */

public class SetResult extends BaseResult<List<SetDto>, List<ErrorDto>> {

}
