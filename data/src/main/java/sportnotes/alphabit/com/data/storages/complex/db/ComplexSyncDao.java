package sportnotes.alphabit.com.data.storages.complex.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by fly12 on 03.12.2017.
 */
@Dao
public interface ComplexSyncDao {

    @Query("SELECT * FROM complex WHERE status=0 AND deleted=0 LIMIT :count OFFSET :skip")
    List<ComplexEntity> getNotSyncComplexes(final int count, final long skip);

    @Query("SELECT * FROM complex WHERE deleted=1 LIMIT :count OFFSET :skip")
    List<ComplexEntity> getDeletedComplexes(final int count, final long skip);

}
