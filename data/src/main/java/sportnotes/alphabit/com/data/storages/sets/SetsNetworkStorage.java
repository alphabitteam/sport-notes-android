package sportnotes.alphabit.com.data.storages.sets;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.data.api.results.SetResult;
import sportnotes.alphabit.com.data.storages.base.NetworkStorage;
import sportnotes.alphabit.com.data.storages.sets.network.SetDto;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */

public interface SetsNetworkStorage extends NetworkStorage {
    Completable setSets(List<SetDto> list);

    Completable deleteSets(List<SetDto> list);

    Single<SetResult> getSets(int count, long offset);
}
