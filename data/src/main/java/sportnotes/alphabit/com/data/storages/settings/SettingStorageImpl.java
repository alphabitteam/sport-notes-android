package sportnotes.alphabit.com.data.storages.settings;

import android.util.Log;

import com.annimon.stream.Optional;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Purchase;
import sportnotes.alphabit.com.data.storages.base.cores.PreferenceCore;
import sportnotes.alphabit.com.data.storages.database.DatabaseCore;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */

public class SettingStorageImpl implements SettingsStorage {

    private final PreferenceCore mPreferenceCore;

    @Inject
    public SettingStorageImpl(final PreferenceCore preferenceCore) {
        mPreferenceCore = preferenceCore;
    }

    @Override
    public Flowable<String> getWeightUnit() {
        return Flowable.fromCallable(mPreferenceCore::getWeightUnit);
    }

    @Override
    public Completable setWeightUnit(final String unit) {
        mPreferenceCore.setWeightUnit(unit);
        return Completable.complete();
    }

    @Override
    public Flowable<Optional<UUID>> getCurrentComplex() {
        return Flowable.fromCallable(() ->
            Optional.ofNullable(mPreferenceCore.getCurrentComplex()));
    }

    @Override
    public Completable setCurrentComplex(final UUID complexUuid) {
        mPreferenceCore.setCurrentComplex(complexUuid);
        return Completable.complete();
    }

    @Override
    public Completable setRingtone(String uri) {
        mPreferenceCore.setRingtone(uri);
        return Completable.complete();
    }

    @Override
    public Flowable<String> getRingtone() {
        return Flowable.fromCallable(mPreferenceCore::getRingtone);
    }

}
