package sportnotes.alphabit.com.data.storages.database.converters;

import android.arch.persistence.room.TypeConverter;

import java.util.UUID;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public class UuidConverter {

    @TypeConverter
    public static String uuidToString(UUID uuid) {
        return uuid.toString();
    }

    @TypeConverter
    public static UUID stringToUuid(String uuidStr) {
        return UUID.fromString(uuidStr);
    }
}
