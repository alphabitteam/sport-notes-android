package sportnotes.alphabit.com.data.storages.exercise;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.Flowable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.WeekDay;
import sportnotes.alphabit.com.data.storages.base.LocalStorage;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseEntity;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */

public interface ExerciseLocalStorage extends LocalStorage {

    Completable insertExercise(ExerciseEntity... exerciseEntity);

    Completable deleteExercise(UUID exerciseUuid);

    Flowable<ExerciseEntity> getExercise(UUID exerciseUuid);

    Flowable<List<ExerciseEntity>> getAllExercise(int count, long skip);

    Completable setExercises(final List<ExerciseEntity> exercises);

    Flowable<List<ExerciseEntity>> getWeekDayExercise(UUID complexUuid, WeekDay weekDay, int count, long skip);

    Flowable<List<ExerciseEntity>> getNotSyncExercises(final int count, final long offset);

    Flowable<List<ExerciseEntity>> getDeletedExercises(final int count, final long offset);

    Completable removeExercise(ExerciseEntity... exerciseEntity);

    Completable moveAllToCandidateDelete();

    Completable removeAllCandidateDelete();

    Completable changePositionToUp(UUID exerciseUuid, int oldPosition, int newPosition);

    Completable changePositionToDown(UUID exerciseUuid, int oldPosition, int newPosition);

    Completable updatePositions(List<ExerciseEntity> exercises, long offset);

    Completable removeExercisesByComplexUuid(UUID uuid);
}
