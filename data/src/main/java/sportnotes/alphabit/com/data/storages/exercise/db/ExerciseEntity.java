package sportnotes.alphabit.com.data.storages.exercise.db;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import sportnotes.alphabit.com.business.model.RepeatType;
import sportnotes.alphabit.com.business.model.WeekDay;
import sportnotes.alphabit.com.data.storages.base.db.DbEntity;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */
@Entity(tableName = "exercise")
public class ExerciseEntity extends DbEntity {

    @SerializedName("complexUuid")
    @Expose
    private UUID complexUuid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("weekDay")
    @Expose
    private WeekDay weekDay;
    @SerializedName("setsCount")
    @Expose
    private int setsCount;
    @SerializedName("repeatType")
    @Expose
    private RepeatType repeatType;
    @SerializedName("repeatCount")
    @Expose
    private String repeatCount;
    @SerializedName("setsValue")
    @Expose
    private long setsValue;
    @SerializedName("hasWeight")
    @Expose
    private boolean hasWeight;

    public boolean isHasWeight() {
        return hasWeight;
    }

    public void setHasWeight(boolean hasWeight) {
        this.hasWeight = hasWeight;
    }

    public long getSetsValue() {
        return setsValue;
    }

    public void setSetsValue(final long setsValue) {
        this.setsValue = setsValue;
    }

    public String getRepeatCount() {
        return repeatCount;
    }

    public void setRepeatCount(final String repeatCount) {
        this.repeatCount = repeatCount;
    }

    public UUID getComplexUuid() {
        return complexUuid;
    }

    public void setComplexUuid(final UUID complexUuid) {
        this.complexUuid = complexUuid;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public WeekDay getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(final WeekDay weekDay) {
        this.weekDay = weekDay;
    }

    public int getSetsCount() {
        return setsCount;
    }

    public void setSetsCount(final int setsCount) {
        this.setsCount = setsCount;
    }

    public RepeatType getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(final RepeatType repeatType) {
        this.repeatType = repeatType;
    }
}
