package sportnotes.alphabit.com.data.storages.database.keepers;

import sportnotes.alphabit.com.data.storages.sets.db.SetDao;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public interface SetDaoKeeper extends DaoKeeper {

    SetDao getSetDao();

}
