package sportnotes.alphabit.com.data.utils;

import org.joda.time.DateTime;

public class DateTimeUtils {

    public static DateTime getStartDay(DateTime src) {
        return src.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
    }

    public static DateTime getEndDay(DateTime src) {
        return src.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999);
    }
}
