package sportnotes.alphabit.com.data.storages.auth;

import android.util.Log;

import com.google.gson.Gson;

import io.reactivex.Single;
import sportnotes.alphabit.com.data.api.ServerApi;
import sportnotes.alphabit.com.data.api.results.AuthResult;
import sportnotes.alphabit.com.data.api.results.UserResult;

/**
 * Created by fly12 on 19.11.2017.
 */

public class AuthNetworkStorageImpl implements AuthNetworkStorage {

    private ServerApi serverApi;

    @Override
    public void setServerApi(ServerApi serverApi) {
        this.serverApi = serverApi;
    }

    @Override
    public Single<AuthResult> login(String provider, String token) {
        return serverApi.login(provider, token).map(authDtoErrorDtoBaseResult -> {
            Log.d(getClass().getSimpleName(), "login: " + new Gson().toJson(authDtoErrorDtoBaseResult));
            AuthResult authResult = new AuthResult();
            authResult.setError(authDtoErrorDtoBaseResult.getError());
            authResult.setResult(authDtoErrorDtoBaseResult.getResult());
            return authResult;
        });
    }

    @Override
    public Single<AuthResult> retoken(String provider, String refresh_token) {
        return serverApi.retoken(provider, refresh_token);
    }

    @Override
    public Single<UserResult> getUser() {
        return serverApi.getUser();
    }
}
