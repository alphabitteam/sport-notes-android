package sportnotes.alphabit.com.data.storages.sets;

import java.util.List;

import sportnotes.alphabit.com.data.storages.sets.db.SetEntity;
import sportnotes.alphabit.com.data.storages.sets.db.SetSyncDao;

/**
 * Created by fly12 on 03.12.2017.
 */
public class SetLocalSyncStorageImpl implements SetLocalSyncStorage {

    private final SetSyncDao dao;

    public SetLocalSyncStorageImpl(SetSyncDao dao) {
        this.dao = dao;
    }

    public List<SetEntity> getNotSyncSets(final int count, final long skip) {
        return dao.getNotSyncSets(count, skip);
    }

    public List<SetEntity> getDeletedSets(final int count, final long skip) {
        return dao.getDeletedSets(count, skip);
    }
}
