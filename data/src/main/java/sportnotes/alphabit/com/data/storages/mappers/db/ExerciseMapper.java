package sportnotes.alphabit.com.data.storages.mappers.db;

import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.ExerciseSum;
import sportnotes.alphabit.com.business.model.ExerciseTime;
import sportnotes.alphabit.com.business.model.RangeInt;
import sportnotes.alphabit.com.business.utils.ExerciseFactory;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseEntity;

/**
 * Created by Yuri Zigunov on 14.10.2017.
 */

public class ExerciseMapper implements PairMapper<ExerciseEntity, Exercise> {
    @Override
    public Exercise from(final ExerciseEntity exerciseEntity) {
        final Exercise exercise = ExerciseFactory.createExercise(exerciseEntity.getRepeatType(), exerciseEntity.getSetsValue());
        exercise.setUuid(exerciseEntity.getUuid());
        exercise.setComplexUuid(exerciseEntity.getComplexUuid());
        exercise.setWeekDay(exerciseEntity.getWeekDay());
        exercise.setSetsCount(exerciseEntity.getSetsCount());
        exercise.setDescription(exerciseEntity.getDescription());
        exercise.setName(exerciseEntity.getName());
        exercise.setPosition(exerciseEntity.getPosition());
        exercise.setStatus(exerciseEntity.getStatus());
        exercise.setDeleted(exerciseEntity.isDeleted());
        exercise.setCreatedAt(exerciseEntity.getCreatedAt());

        if (exercise instanceof ExerciseSum) {
            ((ExerciseSum) exercise).setRepeatCount(RangeInt.valueOf(exerciseEntity.getRepeatCount()));
            ((ExerciseSum) exercise).setHasWeight(exerciseEntity.isHasWeight());
        }
        return exercise;
    }

    @Override
    public ExerciseEntity to(final Exercise exercise) {
        ExerciseEntity exerciseEntity = new ExerciseEntity();
        exerciseEntity.setRepeatType(exercise.getRepeatType());
        exerciseEntity.setUuid(exercise.getUuid());
        exerciseEntity.setComplexUuid(exercise.getComplexUuid());
        exerciseEntity.setWeekDay(exercise.getWeekDay());
        exerciseEntity.setSetsCount(exercise.getSetsCount());
        exerciseEntity.setDescription(exercise.getDescription());
        exerciseEntity.setName(exercise.getName());
        exerciseEntity.setPosition(exercise.getPosition());
        exerciseEntity.setStatus(exercise.getStatus());
        exerciseEntity.setDeleted(exercise.isDeleted());
        exerciseEntity.setCreatedAt(exercise.getCreatedAt());

        if (exercise instanceof ExerciseSum) {
            exerciseEntity.setRepeatCount(String.valueOf(((ExerciseSum) exercise).getRepeatCount()));
            exerciseEntity.setSetsValue((long)((ExerciseSum) exercise).getSetsValue());
            exerciseEntity.setHasWeight(((ExerciseSum) exercise).hasWeight());
        } else if (exercise instanceof ExerciseTime) {
            exerciseEntity.setSetsValue(((ExerciseTime) exercise).getSetsValue());
        }
        return exerciseEntity;
    }
}
