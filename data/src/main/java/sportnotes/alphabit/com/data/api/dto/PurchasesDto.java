package sportnotes.alphabit.com.data.api.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fly12 on 19.11.2017.
 */

public class PurchasesDto {

    @SerializedName("responseData")
    @Expose
    private String responseData;
    @SerializedName("purchaseData")
    @Expose
    private ResponseDataDto purchaseData;
    @SerializedName("signature")
    @Expose
    private String signature;

    public PurchasesDto(ResponseDataDto purchaseData, String responseData,  String signature) {
        this.purchaseData = purchaseData;
        this.responseData = responseData;
        this.signature = signature;
    }

    public String getResponseData() {
        return responseData;
    }

    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }

    public ResponseDataDto getPurchaseData() {
        return purchaseData;
    }

    public void setPurchaseData(ResponseDataDto purchaseData) {
        this.purchaseData = purchaseData;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

}