package sportnotes.alphabit.com.data.storages.auth;

import com.annimon.stream.Optional;

import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import sportnotes.alphabit.com.business.model.User;
import sportnotes.alphabit.com.data.api.dto.AuthDto;

/**
 * Created by fly12 on 19.11.2017.
 */

public interface AuthLocalStorage {
    Completable setAuth(AuthDto authDto);

    Single<Optional<AuthDto>> getAuth();

    Completable setUser(User user);

    Single<Optional<User>> getUser();

}
