package sportnotes.alphabit.com.data.storages.purchase;

import android.util.Log;

import com.google.gson.Gson;

import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Purchases;
import sportnotes.alphabit.com.data.api.ServerApi;
import sportnotes.alphabit.com.data.api.dto.PurchasesDto;
import sportnotes.alphabit.com.data.api.results.PurchasesResult;

/**
 * Created by fly12 on 20.11.2017.
 */

public class PurchaseNetworkStorageImpl implements PurchaseNetworkStorage {

    private final ServerApi serverApi;

    public PurchaseNetworkStorageImpl(ServerApi serverApi) {
        this.serverApi = serverApi;
    }

    @Override
    public Single<PurchasesResult> setPurchase(PurchasesDto purchase) {
        Log.d(getClass().getSimpleName(), "setPurchase: " + purchase);
        return serverApi.setPurchase(purchase).doOnSuccess(purchasesResult -> {
            Log.d(getClass().getSimpleName(), "setPurchase: result " + new Gson().toJson(purchasesResult));
        });
    }
}
