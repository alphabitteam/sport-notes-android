package sportnotes.alphabit.com.data.storages.sets;

import org.joda.time.DateTime;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import sportnotes.alphabit.com.data.storages.base.LocalStorage;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseEntity;
import sportnotes.alphabit.com.data.storages.sets.db.SetEntity;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */

public interface SetsLocalStorage extends LocalStorage {

    Completable insertSet(SetEntity setEntity);

    Completable deleteSet(UUID uuid);

    Flowable<List<SetEntity>> getSets(UUID exerciseUuid, int count, long skip, Vector vector);

    List<List<SetEntity>> getSets(List<ExerciseEntity> exerciseEntities, int count, long skip, Vector vector);

    Completable setSets(final List<SetEntity> exercises);

    Flowable<List<SetEntity>> getNotSyncSets(final int count, final long offset);

    Flowable<List<SetEntity>> getDeletedSets(final int count, final long offset);

    Completable removeSet(SetEntity... setEntity);

    Completable moveAllToCandidateDelete();

    Completable removeAllCandidateDelete();

    Flowable<List<SetEntity>> getAllSet(int count, long skip);

    Flowable<SetEntity> getSet(UUID uuid);

    Completable removeSetsByExerciseUuid(UUID uuid);

    Flowable<Integer> getCountSetsToday(UUID mExerciseUuid, DateTime startDay, DateTime endDay);

    enum Vector {
        START, END
    }
}
