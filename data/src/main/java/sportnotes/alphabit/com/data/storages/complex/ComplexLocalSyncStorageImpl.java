package sportnotes.alphabit.com.data.storages.complex;

import java.util.List;

import sportnotes.alphabit.com.data.storages.complex.db.ComplexDao;
import sportnotes.alphabit.com.data.storages.complex.db.ComplexEntity;
import sportnotes.alphabit.com.data.storages.complex.db.ComplexSyncDao;

/**
 * Created by fly12 on 03.12.2017.
 */
public class ComplexLocalSyncStorageImpl implements ComplexLocalSyncStorage {

    private final ComplexSyncDao complexSyncDao;

    public ComplexLocalSyncStorageImpl(ComplexSyncDao complexSyncDao) {
        this.complexSyncDao = complexSyncDao;
    }

    public List<ComplexEntity> getNotSyncComplexes(final int count, final long skip) {
        return complexSyncDao.getNotSyncComplexes(count, skip);
    }

    public List<ComplexEntity> getDeletedComplexes(final int count, final long skip) {
        return complexSyncDao.getDeletedComplexes(count, skip);
    }
}
