package sportnotes.alphabit.com.data.storages.exercise;

import java.util.List;

import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseEntity;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseSyncDao;

/**
 * Created by fly12 on 03.12.2017.
 */
public class ExerciseLocalSyncStorageImpl implements ExerciseLocalSyncStorage {

    private final ExerciseSyncDao dao;

    public ExerciseLocalSyncStorageImpl(ExerciseSyncDao dao) {
        this.dao = dao;
    }

    public List<ExerciseEntity> getNotSyncExercises(int count, long skip) {
        return dao.getNotSyncExercises(count, skip);
    }

    public List<ExerciseEntity> getDeletedExercises(final int count, final long skip) {
        return dao.getDeletedExercises(count, skip);
    }
}
