package sportnotes.alphabit.com.data.storages.complex;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.data.api.ServerApi;
import sportnotes.alphabit.com.data.api.results.ComplexResult;
import sportnotes.alphabit.com.data.api.results.DeleteResult;
import sportnotes.alphabit.com.data.storages.complex.network.ComplexDto;

/**
 * Created by Yuri Zigunov on 07.10.2017.
 */

public class ComplexNetworkStorageImpl implements ComplexNetworkStorage {

    private final ServerApi serverApi;

    public ComplexNetworkStorageImpl(final ServerApi serverApi) {
        this.serverApi = serverApi;
    }

    @Override
    public Single<ComplexResult> getComplexes(final int count, final long offset) {
        return serverApi.getComplexes(count, offset);
    }

    @Override
    public Single<DeleteResult> deleteComplexes(final List<ComplexDto> list) {
        return Observable.fromIterable(list)
                .map(ComplexDto::getUuid)
                .map(UUID::toString)
                .toList()
                .flatMap(serverApi::deleteComplexes);
    }

    @Override
    public Single<ComplexResult> setComplexes(final List<ComplexDto> list) {
        Log.d(getClass().getSimpleName(), "setComplexes: " + new Gson().toJson(list));
        return serverApi.setComplexes(list).doOnSuccess(complexResult -> Log.d(getClass().getSimpleName(), "setComplexes: " + new Gson().toJson(complexResult)));
    }
}
