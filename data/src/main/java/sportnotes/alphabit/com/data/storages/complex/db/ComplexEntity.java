package sportnotes.alphabit.com.data.storages.complex.db;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import sportnotes.alphabit.com.data.storages.base.db.DbEntity;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */
@Entity(tableName = "complex")
public class ComplexEntity extends DbEntity {

    @SerializedName("name")
    @Expose
    private String name;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
