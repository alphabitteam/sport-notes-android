package sportnotes.alphabit.com.data.repositorymock;

import android.content.Context;
import android.util.Log;

import com.annimon.stream.Optional;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.model.Status;
import sportnotes.alphabit.com.business.repository.IComplexesRepository;

/**
 * Created by Fly on 29.08.2017.
 */
public class ComplexesMockRepository implements IComplexesRepository {

    private final ArrayList<Complex> mList;
    private Complex mCurrentComplex;

    public ComplexesMockRepository() {
        mList = new ArrayList<>();
        mList.add(generateComplex("Сила", DateTime.parse("10.03.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Сжигание жира", DateTime.parse("16.05.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Подготовка к Iron man", DateTime.parse("20.06.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("рельеф", DateTime.parse("19.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("сушка", DateTime.parse("30.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));

        mList.add(generateComplex("Сила 2", DateTime.parse("10.03.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Сжигание жира 2", DateTime.parse("16.05.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Подготовка к Iron man 2", DateTime.parse("20.06.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("рельеф 2", DateTime.parse("19.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("сушка 2", DateTime.parse("30.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));

        mList.add(generateComplex("Сила 3", DateTime.parse("10.03.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Сжигание жира 3", DateTime.parse("16.05.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Подготовка к Iron man 3", DateTime.parse("20.06.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("рельеф 3", DateTime.parse("19.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("сушка 3", DateTime.parse("30.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));

        mList.add(generateComplex("Сила 4", DateTime.parse("10.03.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Сжигание жира 4", DateTime.parse("16.05.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Подготовка к Iron man 4", DateTime.parse("20.06.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("рельеф 4", DateTime.parse("19.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("сушка 4", DateTime.parse("30.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));

        mList.add(generateComplex("Сила 5", DateTime.parse("10.03.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Сжигание жира 5", DateTime.parse("16.05.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Подготовка к Iron man 5", DateTime.parse("20.06.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("рельеф 5", DateTime.parse("19.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("сушка 5", DateTime.parse("30.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));

        mList.add(generateComplex("Сила 6", DateTime.parse("10.03.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Сжигание жира 6", DateTime.parse("16.05.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Подготовка к Iron man 6", DateTime.parse("20.06.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("рельеф 6", DateTime.parse("19.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("сушка 6", DateTime.parse("30.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));

        mList.add(generateComplex("Сила 7", DateTime.parse("10.03.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Сжигание жира 7", DateTime.parse("16.05.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Подготовка к Iron man 7", DateTime.parse("20.06.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("рельеф 7", DateTime.parse("19.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("сушка 7", DateTime.parse("30.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));

        mList.add(generateComplex("Сила 8", DateTime.parse("10.03.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Сжигание жира 8", DateTime.parse("16.05.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("Подготовка к Iron man 8", DateTime.parse("20.06.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("рельеф 8", DateTime.parse("19.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));
        mList.add(generateComplex("сушка 8", DateTime.parse("30.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy"))));

    }

    @Override
    public Flowable<List<Complex>> getComplexes(final int count, final long offset) {
        Log.d(getClass().getSimpleName(), "getComplexes: " + mList.size());
        try {
            int endIndex = (int) (offset + count);
            if (endIndex > mList.size())
                endIndex = mList.size();
            return Flowable.just(mList.subList((int) offset, endIndex));
        } catch (IndexOutOfBoundsException ex) {
            Log.d(getClass().getSimpleName(), "getComplexes: " + ex);
            List<Complex> list = new ArrayList<>();
            return Flowable.just(list);
        }
    }

    @Override
    public Completable setComplexes(final List<Complex> complexes) {
        return null;
    }

    @Override
    public Flowable<List<Complex>> getNotSyncComplexes(final int count, final long offset) {
        return Flowable.just(mList);
    }

    @Override
    public Flowable<List<Complex>> getDeletedComplexes(final int count, final long offset) {
        return null;
    }

    @Override
    public Flowable<Optional<Complex>> getCurrentComplex() {
        if (mCurrentComplex == null)
            mCurrentComplex = mList.get(0);
        Log.d(getClass().getSimpleName(), "getCurrentComplex: " + mCurrentComplex.getName());
        return Flowable.just(Optional.ofNullable(mCurrentComplex));
    }

    @Override
    public Completable setCurrentComplex(final Complex complex) {
        mCurrentComplex = complex;
        Log.d(getClass().getSimpleName(), "setCurrentComplex: " + mCurrentComplex.getName());
        return Completable.complete();
    }

    @Override
    public Completable deleteComplex(final Complex complex) {
        mList.remove(complex);
        Log.d(getClass().getSimpleName(), "deleteComplex: " + mList.size());
        return Completable.complete();
    }

    @Override
    public Completable moveAllToCandidateDelete() {
        return null;
    }

    @Override
    public Completable removeAllCandidateDelete() {
        return null;
    }

    @Override
    public Completable createNewComplex(final String name) {
        final Complex complex = generateComplex(name, DateTime.parse("30.08.2017", DateTimeFormat.forPattern("dd.MM.yyyy")));
        mList.add(complex);
        Log.d(getClass().getSimpleName(), "createNewComplex: " + mList.size());
        return Completable.complete();
    }

    @Override
    public Flowable<Complex> getComplexByName(final String name) {
        return null;
    }

    @Override
    public Completable editComplex(Complex complex) {
        return null;
    }

    @Override
    public Completable removeComplexes(List<Complex> complexes) {
        return null;
    }

    public Complex generateComplex(String name, DateTime dateTime) {
        Complex complex = new Complex();
        complex.setUuid(UUID.randomUUID());
        complex.setPosition(0);
        complex.setName(name);
        return complex;
    }
}
