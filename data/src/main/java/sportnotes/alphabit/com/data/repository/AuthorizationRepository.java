package sportnotes.alphabit.com.data.repository;

import android.util.Log;

import com.annimon.stream.Optional;
import com.google.gson.Gson;

import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Provider;
import sportnotes.alphabit.com.business.model.User;
import sportnotes.alphabit.com.business.repository.IAuthorizationRepository;
import sportnotes.alphabit.com.data.api.results.BaseResult;
import sportnotes.alphabit.com.data.storages.auth.AuthLocalStorage;
import sportnotes.alphabit.com.data.storages.auth.AuthNetworkStorage;
import sportnotes.alphabit.com.data.storages.mappers.network.UserMapper;

/**
 * Created by fly12 on 19.11.2017.
 */

public class AuthorizationRepository implements IAuthorizationRepository {

    private final AuthLocalStorage localStorage;
    private final AuthNetworkStorage networkStorage;
    private final UserMapper userMapper = new UserMapper();

    public AuthorizationRepository(AuthLocalStorage localStorage, AuthNetworkStorage networkStorage) {
        this.localStorage = localStorage;
        this.networkStorage = networkStorage;
    }

    @Override
    public Completable login(Provider provider, String token) {
        Log.d(getClass().getSimpleName(), "login: " + provider + " token = " + token);
        return networkStorage.login(String.valueOf(provider), token)
                .flatMap(complexResult -> {
                    Log.d(getClass().getSimpleName(), "login: complexResult" + new Gson().toJson(complexResult));
                    if (complexResult.getResult() != null)
                        return Single.just(complexResult.getResult());
                    else {
                        return Single.error(new RuntimeException(complexResult.getError().getMessage()));
                    }
                })
                .doOnSuccess(authDto -> {
                    Log.d(getClass().getSimpleName(), "login: " + new Gson().toJson(authDto));
                })
                .flatMapCompletable(localStorage::setAuth)
                .andThen(getServerUser())
                .flatMapCompletable(localStorage::setUser);
    }

    @Override
    public Single<Optional<User>> getUser() {
        return localStorage.getUser();
    }

    @Override
    public Completable logout() {
        return localStorage.setAuth(null)
                .andThen(localStorage.setUser(null));
    }

    private Single<User> getServerUser() {
        return networkStorage.getUser()
                .flatMap(userResult -> {
                    if (userResult.getResult() != null)
                        return Single.just(userResult.getResult());
                    else {
                        return Single.error(new RuntimeException(userResult.getError().getMessage()));
                    }
                })
                .map(userMapper::from);
    }
}
