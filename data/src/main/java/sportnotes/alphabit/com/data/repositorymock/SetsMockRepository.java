package sportnotes.alphabit.com.data.repositorymock;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.repository.ISetsRepository;

/**
 * Created by Yuri Zigunov on 07.10.2017.
 */

public class SetsMockRepository implements ISetsRepository {

    private final List<Set> mList;

    public SetsMockRepository() {
        mList = new ArrayList<>();
    }

    @Override
    public Completable setSet(final Set set) {
        mList.add(set);
        return Completable.complete();
    }

    @Override
    public Flowable<List<Set>> getSets(final UUID exerciseUuid, final int count, final long skip) {
        return Flowable.just(mList);
    }

    @Override
    public Flowable<List<Set>> getLastSets(UUID exerciseUuid, int count, long skip) {
        return null;
    }

    @Override
    public Completable deleteSet(final Set set) {
        mList.remove(set);
        return Completable.complete();
    }

    @Override
    public Completable setSets(final List<Set> exercises) {
        return null;
    }

    @Override
    public Flowable<List<Set>> getNotSyncSets(final int count, final long offset) {
        return null;
    }

    @Override
    public Flowable<List<Set>> getDeletedSets(final int count, final long offset) {
        return null;
    }

    @Override
    public Completable moveAllToCandidateDelete() {
        return null;
    }

    @Override
    public Completable removeAllCandidateDelete() {
        return null;
    }

    @Override
    public Flowable<Integer> getCountSetsToday(UUID mExerciseUuid) {
        return null;
    }

    @Override
    public Completable removeSets(List<Set> sets) {
        return null;
    }

}
