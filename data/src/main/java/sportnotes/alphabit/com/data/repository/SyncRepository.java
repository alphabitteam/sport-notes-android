package sportnotes.alphabit.com.data.repository;

import java.util.ArrayList;
import java.util.List;

import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.SyncState;
import sportnotes.alphabit.com.business.repository.ISyncRepository;
import sportnotes.alphabit.com.data.storages.complex.ComplexLocalSyncStorage;
import sportnotes.alphabit.com.data.storages.complex.db.ComplexEntity;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseLocalSyncStorage;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseEntity;
import sportnotes.alphabit.com.data.storages.mappers.db.ComplexMapper;
import sportnotes.alphabit.com.data.storages.mappers.db.ExerciseMapper;
import sportnotes.alphabit.com.data.storages.mappers.db.SetMapper;
import sportnotes.alphabit.com.data.storages.sets.SetLocalSyncStorage;
import sportnotes.alphabit.com.data.storages.sets.db.SetEntity;
import sportnotes.alphabit.com.data.utils.UserLevelManager;

/**
 * Created by fly12 on 03.12.2017.
 */
public class SyncRepository implements ISyncRepository {

    private final ComplexLocalSyncStorage complexLocalSyncStorage;
    private final ExerciseLocalSyncStorage exerciseLocalSyncStorage;
    private final SetLocalSyncStorage setLocalSyncStorage;
    private final ComplexMapper complexMapper = new ComplexMapper();
    private final ExerciseMapper exerciseMapper = new ExerciseMapper();
    private final SetMapper setMapper = new SetMapper();
    private final UserLevelManager userLevelManager;

    public SyncRepository(ComplexLocalSyncStorage complexLocalSyncStorage, ExerciseLocalSyncStorage exerciseLocalSyncStorage,
                          SetLocalSyncStorage setLocalSyncStorage, UserLevelManager userLevelManager) {
        this.complexLocalSyncStorage = complexLocalSyncStorage;
        this.exerciseLocalSyncStorage = exerciseLocalSyncStorage;
        this.setLocalSyncStorage = setLocalSyncStorage;
        this.userLevelManager = userLevelManager;
    }

    @Override
    public boolean isFullVersion() {
        return userLevelManager.isFullVersion();
    }

    public List<Complex> getNotSyncComplexes(final int count, final long skip) {
        List<ComplexEntity> list = complexLocalSyncStorage.getNotSyncComplexes(count, skip);
        List<Complex> res = new ArrayList<>();
        for (ComplexEntity entity : list) {
            res.add(complexMapper.from(entity));
        }
        return res;
    }

    public List<Complex> getDeletedComplexes(final int count, final long skip) {
        List<ComplexEntity> list = complexLocalSyncStorage.getDeletedComplexes(count, skip);
        List<Complex> res = new ArrayList<>();
        for (ComplexEntity entity : list) {
            res.add(complexMapper.from(entity));
        }
        return res;
    }


    public List<Exercise> getNotSyncExercises(int count, long skip) {
        List<ExerciseEntity> list = exerciseLocalSyncStorage.getNotSyncExercises(count, skip);
        List<Exercise> res = new ArrayList<>();
        for (ExerciseEntity entity : list) {
            res.add(exerciseMapper.from(entity));
        }
        return res;
    }

    public List<Exercise> getDeletedExercises(final int count, final long skip) {
        List<ExerciseEntity> list = exerciseLocalSyncStorage.getDeletedExercises(count, skip);
        List<Exercise> res = new ArrayList<>();
        for (ExerciseEntity entity : list) {
            res.add(exerciseMapper.from(entity));
        }
        return res;
    }

    public List<Set> getNotSyncSets(final int count, final long skip) {
        List<SetEntity> list = setLocalSyncStorage.getNotSyncSets(count, skip);
        List<Set> res = new ArrayList<>();
        for (SetEntity entity : list) {
            res.add(setMapper.from(entity));
        }
        return res;
    }

    public List<Set> getDeletedSets(final int count, final long skip) {
        List<SetEntity> list = setLocalSyncStorage.getDeletedSets(count, skip);
        List<Set> res = new ArrayList<>();
        for (SetEntity entity : list) {
            res.add(setMapper.from(entity));
        }
        return res;
    }

    @Override
    public SyncState getSyncState() {
        List<ComplexEntity> complexes = complexLocalSyncStorage.getNotSyncComplexes(1, 0);
        List<ExerciseEntity> exercises = exerciseLocalSyncStorage.getNotSyncExercises(1, 0);
        List<SetEntity> sets = setLocalSyncStorage.getNotSyncSets(1, 0);
        if (complexes.size() == 0 && exercises.size() == 0 && sets.size() == 0)
            return SyncState.SYNCHRONIZED;
        else return SyncState.NOT_SYNCHRONIZED;
    }
}
