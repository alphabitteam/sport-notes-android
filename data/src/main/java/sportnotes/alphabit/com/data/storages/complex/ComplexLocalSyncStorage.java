package sportnotes.alphabit.com.data.storages.complex;

import java.util.List;

import sportnotes.alphabit.com.data.storages.complex.db.ComplexEntity;

/**
 * Created by fly12 on 03.12.2017.
 */
public interface ComplexLocalSyncStorage {

    List<ComplexEntity> getNotSyncComplexes(final int count, final long skip);

    List<ComplexEntity> getDeletedComplexes(final int count, final long skip);
}
