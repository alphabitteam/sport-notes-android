package sportnotes.alphabit.com.data.storages.sets;

import com.google.gson.Gson;

import org.joda.time.DateTime;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import sportnotes.alphabit.com.data.Serializer;
import sportnotes.alphabit.com.data.storages.database.DbHelper;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseEntity;
import sportnotes.alphabit.com.data.storages.sets.db.SetDao;
import sportnotes.alphabit.com.data.storages.sets.db.SetEntity;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public class SetsLocalStorageImpl implements SetsLocalStorage {

    private final SetDao mSetDao;
    private final DbHelper mDbHelper = new DbHelper();

    public SetsLocalStorageImpl(final SetDao setDao) {
        mSetDao = setDao;
    }

    @Override
    public Completable insertSet(final SetEntity setEntity) {
        return Completable.fromCallable(() -> {
            mDbHelper.prepare(setEntity);
            mSetDao.insertOrUpdateSetBySetNumber(setEntity);
            return Completable.complete();
        });
    }

    @Override
    public List<List<SetEntity>> getSets(List<ExerciseEntity> exerciseEntities, int count, long skip, Vector vector) {
        return mSetDao.getSets(exerciseEntities, count, skip, vector);
    }

    @Override
    public Completable setSets(final List<SetEntity> setEntities) {
        return Completable.fromCallable(() -> {
            mSetDao.insertSet(setEntities.toArray(new SetEntity[setEntities.size()]));
            return Completable.complete();
        });
    }

    @Override
    public Flowable<List<SetEntity>> getNotSyncSets(final int count, final long offset) {
        return mSetDao.getNotSyncSets(count, offset);
    }

    @Override
    public Flowable<List<SetEntity>> getDeletedSets(final int count, final long offset) {
        return mSetDao.getDeletedSets(count, offset);
    }

    @Override
    public Completable deleteSet(final UUID uuid) {
        return Completable.fromCallable(() -> {
            mSetDao.deleteSet(uuid);
            return Completable.complete();
        });
    }

    @Override
    public Flowable<List<SetEntity>> getSets(UUID exerciseUuid, int count, long skip, Vector vector) {
        if (vector == Vector.START)
            return mSetDao.getSets(exerciseUuid, count, skip);
        else return mSetDao.getLastSets(exerciseUuid, count, skip);
    }

    @Override
    public Completable removeSet(final SetEntity... setEntity) {
        return Completable.fromCallable(() -> {
            mSetDao.removeSet(setEntity);
            return Completable.complete();
        });
    }

    @Override
    public Completable moveAllToCandidateDelete() {
        return Completable.fromCallable(() -> {
            mSetDao.moveAllToCandidateDelete();
            return Completable.complete();
        });
    }

    @Override
    public Completable removeAllCandidateDelete() {
        return Completable.fromCallable(() -> {
            mSetDao.removeAllCandidateDelete();
            return Completable.complete();
        });
    }

    @Override
    public Flowable<List<SetEntity>> getAllSet(final int count, final long skip) {
        return mSetDao.getAllSet(count, skip);
    }

    @Override
    public Flowable<SetEntity> getSet(final UUID uuid) {
        return mSetDao.getSet(uuid);
    }

    @Override
    public Completable removeSetsByExerciseUuid(UUID uuid) {
        return Completable.fromCallable(() -> {
            mSetDao.removeSetsByExerciseUuid(uuid);
            return Completable.complete();
        });
    }

    @Override
    public Flowable<Integer> getCountSetsToday(UUID mExerciseUuid, DateTime startDay, DateTime endDay) {
        return mSetDao.getCountSetsToday(mExerciseUuid, startDay, endDay);
    }
}
