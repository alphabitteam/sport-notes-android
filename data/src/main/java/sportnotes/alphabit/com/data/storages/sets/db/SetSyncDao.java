package sportnotes.alphabit.com.data.storages.sets.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by fly12 on 03.12.2017.
 */
@Dao
public interface SetSyncDao {

    @Query("SELECT * FROM sets WHERE status=0 AND deleted=0 LIMIT :count OFFSET :skip")
    List<SetEntity> getNotSyncSets(final int count, final long skip);

    @Query("SELECT * FROM sets WHERE deleted=1 AND status=0 LIMIT :count OFFSET :skip")
    List<SetEntity> getDeletedSets(final int count, final long skip);
}
