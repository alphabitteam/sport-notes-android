package sportnotes.alphabit.com.data.storages.base.db;

import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.UUID;

import sportnotes.alphabit.com.business.model.Status;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public class DbEntity {
    @PrimaryKey
    @NonNull
    @SerializedName("uuid")
    @Expose
    private UUID uuid;
    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("candidateDelete")
    @Expose
    private boolean candidateDelete;
    @SerializedName("deleted")
    @Expose
    private boolean deleted;
    @SerializedName("position")
    @Expose
    private long position = -1;
    @SerializedName("createdAt")
    @Expose
    private DateTime createdAt;
    @SerializedName("updatedAt")
    @Expose
    private DateTime updatedAt;

    public boolean isCandidateDelete() {
        return candidateDelete;
    }

    public void setCandidateDelete(final boolean candidateDelete) {
        this.candidateDelete = candidateDelete;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(final boolean deleted) {
        this.deleted = deleted;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(final long position) {
        this.position = position;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(final DateTime createdAt) {
        this.createdAt = createdAt;
    }

    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(final DateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
