package sportnotes.alphabit.com.data.repositorymock;

import android.support.annotation.StringRes;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.ExerciseSum;
import sportnotes.alphabit.com.business.model.ExerciseTime;
import sportnotes.alphabit.com.business.model.RangeInt;
import sportnotes.alphabit.com.business.model.RepeatType;
import sportnotes.alphabit.com.business.model.WeekDay;
import sportnotes.alphabit.com.business.repository.IExercisesRepository;

/**
 * Created by Fly on 04.09.2017.
 */

public class ExercisesMockRepository implements IExercisesRepository {

    private final ArrayList<Exercise> mArrayList;

    public ExercisesMockRepository() {
        mArrayList = new ArrayList<>();
        mArrayList.add(generateExercise("упр 1", "описание упражнений 1", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 1", "описание упражнений 1", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 1", "описание упражнений 1", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 1", "описание упражнений 1", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 1", "описание упражнений 1", WeekDay.MONDAY));

        mArrayList.add(generateExercise("упр 2", "описание упражнений 2", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 2", "описание упражнений 2", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 2", "описание упражнений 2", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 2", "описание упражнений 2", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 2", "описание упражнений 2", WeekDay.MONDAY));

        mArrayList.add(generateExercise("упр 3", "описание упражнений 3", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 3", "описание упражнений 3", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 3", "описание упражнений 3", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 3", "описание упражнений 3", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 3", "описание упражнений 3", WeekDay.MONDAY));

        mArrayList.add(generateExercise("упр 4", "описание упражнений 4", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 4", "описание упражнений 4", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 4", "описание упражнений 4", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 4", "описание упражнений 4", WeekDay.MONDAY));
        mArrayList.add(generateExercise("упр 4", "описание упражнений 4", WeekDay.MONDAY));
    }

    @Override
    public Flowable<List<Exercise>> getExercises(final int count, final long offset, final UUID complexUuid, final WeekDay weekDay) {
        try {
            int endIndex = (int) (offset + count);
            if (endIndex > mArrayList.size())
                endIndex = mArrayList.size();
            return Flowable.just(mArrayList.subList((int) offset, endIndex));
        } catch (IndexOutOfBoundsException ex) {
            List<Exercise> list = new ArrayList<>();
            return Flowable.just(list);
        }
    }

    @Override
    public Completable setExercises(final List<Exercise> exercises) {
        return null;
    }

    @Override
    public Flowable<List<Exercise>> getNotSyncExercises(final int count, final long offset) {
        return Flowable.just(mArrayList);
    }

    @Override
    public Flowable<List<Exercise>> getDeletedExercises(final int count, final long offset) {
        return null;
    }

    @Override
    public Completable onChangeExercisePosition(final Exercise exercise, final int oldPosition, final int newPosition) {
        Exercise oldO = mArrayList.get(newPosition);
        Exercise newO = mArrayList.get(oldPosition);
        mArrayList.set(oldPosition, oldO);
        mArrayList.set(newPosition, newO);
        return Completable.complete();
    }

    @Override
    public Completable deleteExercise(final Exercise exercise) {
        mArrayList.remove(exercise);
        return Completable.complete();
    }

    @Override
    public Completable moveAllToCandidateDelete() {
        return null;
    }

    @Override
    public Completable removeAllCandidateDelete() {
        return null;
    }

    @Override
    public Completable saveExercise(final Exercise exercise) {
        Log.d(getClass().getSimpleName(), "saveExercise: " + exercise);
        mArrayList.add(exercise);
        return Completable.complete();
    }

    @Override
    public Flowable<Exercise> getExercise(final UUID uuid) {
        return Flowable.just(findExerciseByUuid(uuid));
    }

    @Override
    public Completable updatePositions(final List<Exercise> exercises, final long offset) {
        return null;
    }

    @Override
    public Completable removeExercises(List<Exercise> exercises) {
        return null;
    }

    private Exercise findExerciseByUuid(final UUID uuid) {
        for (Exercise item : mArrayList) {
            if (item.getUuid().equals(uuid))
                return item;
        }
        return null;
    }

    private Exercise generateExercise(String name, String descr, WeekDay weekDay) {
        int r = new Random().nextInt(2);
        switch (r) {
            case 0:
                return generateSumExercise(name, descr, weekDay);
            default:
                return generateTimeExercise(name, descr, weekDay);
        }
    }

    private Exercise generateSumExercise(String name, String descr, WeekDay weekDay) {
        ExerciseSum exercise = new ExerciseSum();
        exercise.setRepeatCount(new RangeInt(8, 10));
        exercise.setSetsValue(new Random().nextInt(50) + 70);
        return fillExercise(exercise, name, descr, weekDay);
    }

    private Exercise generateTimeExercise(String name, String descr, WeekDay weekDay) {
        ExerciseTime exercise = new ExerciseTime();
        exercise.setSetsValue(600000);
        return fillExercise(exercise, name, descr, weekDay);
    }

    private Exercise fillExercise(Exercise exercise, String name, String descr, WeekDay weekDay) {
        exercise.setUuid(UUID.randomUUID());
        exercise.setComplexUuid(UUID.randomUUID());
        exercise.setName(name);
        exercise.setDescription(descr);

        RepeatType repeatType;
        int random = new Random().nextInt(2);
        switch (random) {
            case 0:
                repeatType = RepeatType.SUM;
                break;
            default:
                repeatType = RepeatType.TIME;
                break;
        }

        exercise.setSetsCount(new Random().nextInt(5));
        exercise.setWeekDay(weekDay);
        return exercise;
    }
}
