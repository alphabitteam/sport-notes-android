package sportnotes.alphabit.com.data.api.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fly12 on 19.11.2017.
 */

public class ErrorDto {
    @SerializedName("code")
    @Expose
    int code;
    @SerializedName("message")
    @Expose
    String message;

    public ErrorDto(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ErrorDto() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}