package sportnotes.alphabit.com.data.storages.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import sportnotes.alphabit.com.data.storages.base.db.BaseDao;
import sportnotes.alphabit.com.data.storages.complex.db.ComplexDao;
import sportnotes.alphabit.com.data.storages.complex.db.ComplexEntity;
import sportnotes.alphabit.com.data.storages.database.converters.DateTimeConverter;
import sportnotes.alphabit.com.data.storages.database.converters.RepeatTypeConverter;
import sportnotes.alphabit.com.data.storages.database.converters.StatusConverter;
import sportnotes.alphabit.com.data.storages.database.converters.UuidConverter;
import sportnotes.alphabit.com.data.storages.database.converters.ValueTypeConverter;
import sportnotes.alphabit.com.data.storages.database.converters.WeekDayConverter;
import sportnotes.alphabit.com.data.storages.database.keepers.ComplexDaoKeeper;
import sportnotes.alphabit.com.data.storages.database.keepers.ExerciseDaoKeeper;
import sportnotes.alphabit.com.data.storages.database.keepers.SetDaoKeeper;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseDao;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseEntity;
import sportnotes.alphabit.com.data.storages.sets.db.SetDao;
import sportnotes.alphabit.com.data.storages.sets.db.SetEntity;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */
@Database(entities = {ComplexEntity.class, ExerciseEntity.class, SetEntity.class}, version = 1, exportSchema = false)
@TypeConverters({UuidConverter.class, StatusConverter.class, DateTimeConverter.class,
        RepeatTypeConverter.class, WeekDayConverter.class, ValueTypeConverter.class})
public abstract class AppDatabase extends RoomDatabase implements IAppDatabase {

    public abstract ComplexDao getComplexDao();

    public abstract ExerciseDao getExerciseDao();

    public abstract SetDao getSetDao();

    @Override
    public void beginTransaction() {
        super.beginTransaction();
    }

    @Override
    public void endTransaction() {
        super.endTransaction();
    }
}
