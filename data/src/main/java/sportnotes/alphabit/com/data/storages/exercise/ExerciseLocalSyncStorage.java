package sportnotes.alphabit.com.data.storages.exercise;

import java.util.List;

import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseEntity;

/**
 * Created by fly12 on 03.12.2017.
 */
public interface ExerciseLocalSyncStorage {

    List<ExerciseEntity> getNotSyncExercises(int count, long skip);

    List<ExerciseEntity> getDeletedExercises(final int count, final long skip);

}
