package sportnotes.alphabit.com.data.repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Purchase;
import sportnotes.alphabit.com.business.repository.IBillingRepository;
import sportnotes.alphabit.com.data.storages.base.cores.PreferenceCore;
import sportnotes.alphabit.com.data.storages.mappers.network.PurchaseMapper;
import sportnotes.alphabit.com.data.storages.mappers.network.PurchasesMapper;
import sportnotes.alphabit.com.data.storages.purchase.PurchaseNetworkStorage;

/**
 * Created by fly12 on 20.11.2017.
 */
public class BillingRepository implements IBillingRepository {

    private final PurchaseMapper purchaseMapper = new PurchaseMapper();
    private final PurchasesMapper purchasesMapper = new PurchasesMapper();
    private final PurchaseNetworkStorage networkStorage;
    private final PreferenceCore core;

    public BillingRepository(PurchaseNetworkStorage networkStorage, PreferenceCore core) {
        this.networkStorage = networkStorage;
        this.core = core;
    }


    @Override
    public Completable setPurchase(Purchase purchase) {
        return networkStorage.setPurchase(purchaseMapper.to(purchasesMapper.from(purchase)))
                .flatMap(purchasesResult -> {
                    if (purchasesResult.getResult() != null)
                        return Single.just(purchasesResult.getResult());
                    else {
                        return Single.error(new RuntimeException(purchasesResult.getError().getMessage()));
                    }
                }).toCompletable();
    }

    @Override
    public Completable savePurchase(List<Purchase> list) {
        return Completable.fromCallable(() -> {
            core.setPurchases(list);
            return Completable.complete();
        });
    }

}
