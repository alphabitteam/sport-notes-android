package sportnotes.alphabit.com.data.storages.alarm;

import com.annimon.stream.Optional;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.TimerTask;
import sportnotes.alphabit.com.data.storages.base.cores.PreferenceCore;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */

public class AlarmStorageImpl implements AlarmStorage {

    private final PreferenceCore mPreferenceCore;

    @Inject
    public AlarmStorageImpl(final PreferenceCore preferenceCore) {
        mPreferenceCore = preferenceCore;
    }


    @Override
    public Single<Optional<TimerTask>> getTimerTask() {
        return Single.fromCallable(() -> Optional.ofNullable(mPreferenceCore.getTimerTask()));
    }

    @Override
    public Completable setTimerTask(final TimerTask timerTask) {
        mPreferenceCore.setTimerTask(timerTask);
        return Completable.complete();
    }
}
