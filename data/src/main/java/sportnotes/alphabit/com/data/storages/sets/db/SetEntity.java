package sportnotes.alphabit.com.data.storages.sets.db;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import sportnotes.alphabit.com.business.model.RepeatType;
import sportnotes.alphabit.com.business.model.ValueType;
import sportnotes.alphabit.com.data.storages.base.db.DbEntity;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */
@Entity(tableName = "sets")
public class SetEntity extends DbEntity {

    @SerializedName("exerciseUuid")
    @Expose
    private UUID exerciseUuid;
    @SerializedName("repeatType")
    @Expose
    private RepeatType repeatType;
    @SerializedName("valueType")
    @Expose
    private ValueType valueType;
    @SerializedName("number")
    @Expose
    private int number;
    @SerializedName("repeat")
    @Expose
    private int repeat;
    @SerializedName("value")
    @Expose
    private long value;
    @SerializedName("comment")
    @Expose
    private String comment;

    public ValueType getValueType() {
        return valueType;
    }

    public void setValueType(ValueType valueType) {
        this.valueType = valueType;
    }

    public long getValue() {
        return value;
    }

    public void setValue(final long value) {
        this.value = value;
    }

    public UUID getExerciseUuid() {
        return exerciseUuid;
    }

    public void setExerciseUuid(final UUID exerciseUuid) {
        this.exerciseUuid = exerciseUuid;
    }

    public RepeatType getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(final RepeatType repeatType) {
        this.repeatType = repeatType;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(final int number) {
        this.number = number;
    }

    public int getRepeat() {
        return repeat;
    }

    public void setRepeat(final int repeat) {
        this.repeat = repeat;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(final String comment) {
        this.comment = comment;
    }
}
