package sportnotes.alphabit.com.data;

import sportnotes.alphabit.com.data.utils.Host;

/**
 * Created by Fly on 03.05.2017.
 */

public class Configuration {

    public static final Host HOST = Host.PRODUCTION;
    public static final String API_VERSION_NAME = "v1";

}
