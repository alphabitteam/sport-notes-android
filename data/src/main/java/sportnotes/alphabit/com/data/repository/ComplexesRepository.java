package sportnotes.alphabit.com.data.repository;


import android.util.Log;

import com.annimon.stream.Optional;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.repository.IComplexesRepository;
import sportnotes.alphabit.com.business.exceptions.UserLevelException;
import sportnotes.alphabit.com.data.Serializer;
import sportnotes.alphabit.com.data.storages.complex.ComplexLocalStorage;
import sportnotes.alphabit.com.data.storages.complex.db.ComplexEntity;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseLocalStorage;
import sportnotes.alphabit.com.data.storages.mappers.db.ComplexMapper;
import sportnotes.alphabit.com.data.storages.settings.SettingsStorage;
import sportnotes.alphabit.com.data.utils.UserLevelManager;

/**
 * Created by Fly on 29.08.2017.
 */

public class ComplexesRepository extends Repository implements IComplexesRepository {

    private final SettingsStorage mSettingsStorage;
    private final ComplexLocalStorage mComplexLocalStorage;
    private final ExerciseLocalStorage exerciseLocalStorage;
    private final UserLevelManager userLevelManager;
    private final ComplexMapper complexMapper = new ComplexMapper();

    public ComplexesRepository(final ComplexLocalStorage complexLocalStorage, SettingsStorage settingsStorage, ExerciseLocalStorage exerciseLocalStorage, UserLevelManager userLevelManager) {
        mComplexLocalStorage = complexLocalStorage;
        mSettingsStorage = settingsStorage;
        this.exerciseLocalStorage = exerciseLocalStorage;
        this.userLevelManager = userLevelManager;
    }

    @Override
    public Flowable<List<Complex>> getComplexes(final int count, final long skip) {
        return execute(mComplexLocalStorage.getComplexes(count, skip)
                .map(complexEntity -> {
                    List<Complex> res = new ArrayList<>();
                    for (ComplexEntity item : complexEntity) {
                        res.add(complexMapper.from(item));
                    }
                    return res;
                }));
    }

    @Override
    public Completable setComplexes(final List<Complex> complexes) {
        List<ComplexEntity> list = new ArrayList<>();
        for (Complex item : complexes) {
            list.add(complexMapper.to(item));
        }
        return execute(mComplexLocalStorage.insertComplex(list.toArray(new ComplexEntity[list.size()])));
    }

    @Override
    public Flowable<List<Complex>> getNotSyncComplexes(final int count, final long offset) {
        return execute(mComplexLocalStorage.getNotSyncComplexes(count, offset)
                .map(complexEntity -> {
                    List<Complex> res = new ArrayList<>();
                    for (ComplexEntity item : complexEntity) {
                        res.add(complexMapper.from(item));
                    }
                    return res;
                }));
    }

    @Override
    public Flowable<List<Complex>> getDeletedComplexes(final int count, final long offset) {
        return execute(mComplexLocalStorage.getDeletedComplexes(count, offset)
                .map(complexEntity -> {
                    List<Complex> res = new ArrayList<>();
                    for (ComplexEntity item : complexEntity) {
                        res.add(complexMapper.from(item));
                    }
                    return res;
                }));
    }

    @Override
    public Completable removeComplexes(List<Complex> complexes) {
        return execute(Completable.fromCallable(() -> {
            ComplexEntity[] res = new ComplexEntity[complexes.size()];
            for (int i = 0; i < res.length; i++) {
                res[i] = complexMapper.to(complexes.get(i));
            }
            return mComplexLocalStorage.removeComplex(res);
        }));
    }

    @Override
    public Flowable<Optional<Complex>> getCurrentComplex() {
        return execute(mSettingsStorage.getCurrentComplex()
                .flatMap(uuidOptional -> {
                    if (uuidOptional.isPresent())
                        return mComplexLocalStorage.getComplex(uuidOptional.get())
                                .map(complexMapper::from)
                                .map(Optional::ofNullable);
                    else return Flowable.just(Optional.empty());
                }));
    }

    @Override
    public Completable setCurrentComplex(final Complex complex) {
        return execute(mSettingsStorage.setCurrentComplex(complex.getUuid()));
    }

    @Override
    public Completable deleteComplex(final Complex complex) {
        if (!userLevelManager.isFullVersion())
            return execute(mComplexLocalStorage.removeComplex(complexMapper.to(complex)))
                    .andThen(exerciseLocalStorage.removeExercisesByComplexUuid(complex.getUuid()));
        return execute(mComplexLocalStorage.deleteComplex(complex.getUuid()))
                .andThen(exerciseLocalStorage.removeExercisesByComplexUuid(complex.getUuid()));
    }

    @Override
    public Completable moveAllToCandidateDelete() {
        return execute(mComplexLocalStorage.moveAllToCandidateDelete());
    }

    @Override
    public Completable removeAllCandidateDelete() {
        return execute(mComplexLocalStorage.removeAllCandidateDelete());
    }

    @Override
    public Completable createNewComplex(final String name) {
        return execute(Completable.fromCallable(() -> {
            int size = mComplexLocalStorage.getCountComplex();
            if (!userLevelManager.isFullVersion() && size > 0)
                return Completable.error(new UserLevelException());
            ComplexEntity entity = new ComplexEntity();
            entity.setName(name);
            return mComplexLocalStorage.insertComplex(entity);
        }));
    }

    @Override
    public Flowable<Complex> getComplexByName(final String name) {
        return mComplexLocalStorage.getComplexByName(name)
                .map(complexMapper::from);
    }

    @Override
    public Completable editComplex(Complex complex) {
        return execute(mComplexLocalStorage.editComplex(complex));
    }
}
