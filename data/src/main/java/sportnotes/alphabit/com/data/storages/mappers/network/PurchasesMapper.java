package sportnotes.alphabit.com.data.storages.mappers.network;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import sportnotes.alphabit.com.business.model.Purchase;
import sportnotes.alphabit.com.business.model.PurchaseResponseData;
import sportnotes.alphabit.com.business.model.PurchaseState;
import sportnotes.alphabit.com.business.model.Purchases;
import sportnotes.alphabit.com.data.Serializer;
import sportnotes.alphabit.com.data.storages.mappers.db.PairMapper;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 04.02.18.
 */
public class PurchasesMapper implements PairMapper<Purchase, Purchases> {

    private final Serializer serializer = new Serializer();
    private final DateTimeFormatter dtf = DateTimeFormat.forPattern("MMM dd, yyyy K:mm:ss a").withLocale(Locale.ENGLISH); //Jun 26, 2017 5:58:09 PM

    @Override
    public Purchases from(Purchase purchase) {
        PurchaseResponseData prd;
        prd = serializer.deserialize(purchase.getOriginalJson(), PurchaseResponseData.class);

        long time = 0;
        try {
            int tmpPurchaseState;
            JSONObject json = new JSONObject(purchase.getOriginalJson());
            tmpPurchaseState = json.optInt("purchaseState", 1);
            prd.setPurchaseState(tmpPurchaseState == -1 ? null : PurchaseState.values()[tmpPurchaseState]);

            time = json.optLong("purchaseTime", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String jodatime = dtf.print(time);
        prd.setPurchaseTime(jodatime);


        Purchases res = new Purchases(prd, purchase.getOriginalJson(), purchase.getSignature());
        return res;
    }

    @Override
    public Purchase to(Purchases purchases) {
        Purchase res;
        res = serializer.deserialize(purchases.getOriginJson(), Purchase.class);
        res.setOriginalJson(purchases.getOriginJson());
        return res;
    }
}
