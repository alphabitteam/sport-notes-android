package sportnotes.alphabit.com.data.utils;

import android.text.TextUtils;
import android.util.Log;

import com.annimon.stream.Optional;
import com.google.gson.Gson;

import org.joda.time.DateTime;

import java.util.List;

import sportnotes.alphabit.com.business.model.Purchase;
import sportnotes.alphabit.com.business.model.User;
import sportnotes.alphabit.com.business.utils.SystemManager;
import sportnotes.alphabit.com.data.Serializer;
import sportnotes.alphabit.com.data.api.dto.AuthDto;
import sportnotes.alphabit.com.data.storages.auth.AuthLocalStorage;
import sportnotes.alphabit.com.data.storages.base.PreferenceStorage;
import sportnotes.alphabit.com.data.storages.base.cores.PreferenceCore;
import sportnotes.alphabit.com.data.storages.settings.SettingsStorage;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 18.12.17.
 */
public class UserLevelManager {

    private final AuthLocalStorage authLocalStorage;
    private final PreferenceCore preferenceCore;
    private final Serializer serializer = new Serializer();
    private final SystemManager systemManager;


    public UserLevelManager(AuthLocalStorage authLocalStorage,
                            PreferenceCore preferenceCore, SystemManager systemManager) {
        this.authLocalStorage = authLocalStorage;
        this.preferenceCore = preferenceCore;
        this.systemManager = systemManager;
    }

    public boolean isFullVersion() {
        Optional<AuthDto> auth = authLocalStorage.getAuth().blockingGet();
        Optional<User> user = authLocalStorage.getUser().blockingGet();
        List<Purchase> purchases = preferenceCore.getPurchases();
        Log.d(getClass().getSimpleName(), "isFullVersion: purchases " + purchases.size() + " auth = " + serializer.serialize(auth, Optional.class) + " user = " + serializer.serialize(user, Optional.class));
        if (auth.isPresent() && user.isPresent()) {
            if (purchases.size() > 0 || actualSubscribe(user.get().getSubscribedTo())) {
                return true;
            }
        }
        return false;
    }

    public boolean hasAuth() {
        Optional<AuthDto> auth = authLocalStorage.getAuth().blockingGet();
        return auth.isPresent();
    }

    private boolean actualSubscribe(DateTime date) {
        if (date == null)
            return false;
        DateTime current = systemManager.getCurrentTime();
        return current.isBefore(date);
    }
}
