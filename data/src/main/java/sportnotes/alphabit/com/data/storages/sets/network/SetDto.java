package sportnotes.alphabit.com.data.storages.sets.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.UUID;

import sportnotes.alphabit.com.business.model.RepeatType;
import sportnotes.alphabit.com.business.model.ValueType;

/**
 * Created by Yuri Zigunov on 22.10.2017.
 */

public class SetDto {

    @SerializedName("uuid")
    @Expose
    private UUID uuid;
    @SerializedName("exercise_uuid")
    @Expose
    private UUID exerciseUuid;
    @SerializedName("repeat_type")
    @Expose
    private RepeatType repeatType;
    @SerializedName("number")
    @Expose
    private int number;
    @SerializedName("repeat")
    @Expose
    private int repeat;
    @SerializedName("value")
    @Expose
    private long value;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("created_at")
    @Expose
    private DateTime createdAt;
    @SerializedName("valueType")
    @Expose
    private ValueType valueType;

    public ValueType getValueType() {
        return valueType;
    }

    public void setValueType(ValueType valueType) {
        this.valueType = valueType;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getExerciseUuid() {
        return exerciseUuid;
    }

    public void setExerciseUuid(final UUID exerciseUuid) {
        this.exerciseUuid = exerciseUuid;
    }

    public RepeatType getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(final RepeatType repeatType) {
        this.repeatType = repeatType;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(final int number) {
        this.number = number;
    }

    public int getRepeat() {
        return repeat;
    }

    public void setRepeat(final int repeat) {
        this.repeat = repeat;
    }

    public long getValue() {
        return value;
    }

    public void setValue(final long value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(final String comment) {
        this.comment = comment;
    }
}
