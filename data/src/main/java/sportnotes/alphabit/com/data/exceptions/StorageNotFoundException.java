package sportnotes.alphabit.com.data.exceptions;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public class StorageNotFoundException extends RuntimeException {
    public StorageNotFoundException(final String message) {
        super(message);
    }
}
