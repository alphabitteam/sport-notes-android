package sportnotes.alphabit.com.data.repository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.repository.ISetsNetworkRepository;
import sportnotes.alphabit.com.data.storages.mappers.network.SetMapper;
import sportnotes.alphabit.com.data.storages.sets.SetsNetworkStorage;
import sportnotes.alphabit.com.data.storages.sets.network.SetDto;

/**
 * Created by Yuri Zigunov on 21.10.2017.
 */

public class SetsNetworkRepository implements ISetsNetworkRepository {

    private final SetsNetworkStorage storage;
    private final SetMapper setMapper = new SetMapper();

    //TODO: вставить реалные данные
    public SetsNetworkRepository(final SetsNetworkStorage storage) {
        this.storage = storage;
    }

    @Override
    public Completable setSets(final List<Set> sets) {
        List<SetDto> list = new ArrayList<>();
        for (Set set : sets) {
            list.add(setMapper.to(set));
        }
        return storage.setSets(list);
    }

    @Override
    public Completable deleteSets(final List<Set> sets) {
        List<SetDto> list = new ArrayList<>();
        for (Set set : sets) {
            list.add(setMapper.to(set));
        }
        return storage.deleteSets(list);
    }

    @Override
    public Single<List<Set>> getSets(final int count, final long offset) {
        return storage.getSets(count, offset)
                .flatMap(setResult -> {
                    if (setResult.getResult() != null)
                        return Single.just(setResult.getResult());
                    else {
                        return Single.error(new RuntimeException(String.valueOf(setResult.getError())));
                    }
                }).flatMap(exerciseDto ->
                        Observable.fromIterable(exerciseDto)
                                .map(setMapper::from)
                                .toList());
    }
}
