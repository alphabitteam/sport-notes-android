package sportnotes.alphabit.com.data.storages.exercise.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.UUID;

import sportnotes.alphabit.com.business.model.RepeatType;
import sportnotes.alphabit.com.business.model.WeekDay;

/**
 * Created by Yuri Zigunov on 22.10.2017.
 */

public class ExerciseDto {

    @SerializedName("uuid")
    @Expose
    private UUID uuid;
    @SerializedName("position")
    @Expose
    private long position = -1;
    @SerializedName("complex_uuid")
    @Expose
    private UUID complexUuid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("week_day")
    @Expose
    private WeekDay weekDay;
    @SerializedName("sets_count")
    @Expose
    private int setsCount;
    @SerializedName("repeat_type")
    @Expose
    private RepeatType repeatType;
    @SerializedName("repeat_count")
    @Expose
    private String repeatCount;
    @SerializedName("sets_value")
    @Expose
    private long setsValue;
    @SerializedName("created_at")
    @Expose
    private DateTime createdAt;

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getRepeatCount() {
        return repeatCount;
    }

    public void setRepeatCount(final String repeatCount) {
        this.repeatCount = repeatCount;
    }

    public long getSetsValue() {
        return setsValue;
    }

    public void setSetsValue(final long setsValue) {
        this.setsValue = setsValue;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(final long position) {
        this.position = position;
    }

    public UUID getComplexUuid() {
        return complexUuid;
    }

    public void setComplexUuid(final UUID complexUuid) {
        this.complexUuid = complexUuid;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public WeekDay getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(final WeekDay weekDay) {
        this.weekDay = weekDay;
    }

    public int getSetsCount() {
        return setsCount;
    }

    public void setSetsCount(final int setsCount) {
        this.setsCount = setsCount;
    }

    public RepeatType getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(final RepeatType repeatType) {
        this.repeatType = repeatType;
    }
}
