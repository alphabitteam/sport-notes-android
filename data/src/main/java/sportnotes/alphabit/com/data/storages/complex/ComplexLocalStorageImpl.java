package sportnotes.alphabit.com.data.storages.complex;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.data.storages.complex.db.ComplexDao;
import sportnotes.alphabit.com.data.storages.complex.db.ComplexEntity;
import sportnotes.alphabit.com.data.storages.database.DbHelper;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public class ComplexLocalStorageImpl implements ComplexLocalStorage {

    private final ComplexDao mComplexDao;
    private final DbHelper mDbHelper = new DbHelper();

    public ComplexLocalStorageImpl(ComplexDao complexDao) {
        mComplexDao = complexDao;
    }

    @Override
    public Completable insertComplex(final ComplexEntity... complexEntity) {
        Log.d(getClass().getSimpleName(), "insertComplex: ");
        for (ComplexEntity item : complexEntity) {
            mDbHelper.prepare(item);
        }
        Log.d(getClass().getSimpleName(), "insertComplex: insertComplex");
        mComplexDao.insertComplex(complexEntity);
        return Completable.complete();
    }


    @Override
    public Completable deleteComplex(final UUID uuid) {
        Log.d(getClass().getSimpleName(), "deleteComplex: " + uuid);
        return Completable.fromCallable(() -> {
            mComplexDao.deleteComplex(uuid);
            return Completable.complete();
        }).doOnComplete(() -> {
            Log.d(getClass().getSimpleName(), "deleteComplex: doOnComplete");
        }).doOnError(throwable -> {
            Log.d(getClass().getSimpleName(), "deleteComplex: " + throwable);
        });
    }

    @Override
    public Flowable<ComplexEntity> getComplex(final UUID uuid) {
        Log.d(getClass().getSimpleName(), "getComplex: " + uuid);
        return mComplexDao.getComplex(uuid);
    }

    @Override
    public Flowable<List<ComplexEntity>> getComplexes(final int count, final long skip) {
        Log.d(getClass().getSimpleName(), "getComplexes: " + count + " skip = " + skip);
        return mComplexDao.getComplexes(count, skip);
    }

    @Override
    public int getCountComplex() {
        return mComplexDao.getCountComplex();
    }

    @Override
    public Flowable<List<ComplexEntity>> getNotSyncComplexes(final int count, final long offset) {
        Log.d(getClass().getSimpleName(), "getNotSyncComplexes: start");
        return mComplexDao.getNotSyncComplexes(count, offset)
                .doOnNext(complexEntities -> Log.d(getClass().getSimpleName(), "getNotSyncComplexes: " + complexEntities.size()));
    }

    @Override
    public Flowable<List<ComplexEntity>> getDeletedComplexes(final int count, final long skip) {
        Log.d(getClass().getSimpleName(), "getDeletedComplexes: " + count + " skip = " + skip);
        return mComplexDao.getDeletedComplexes(count, skip);
    }

    @Override
    public Completable moveAllToCandidateDelete() {
        Log.d(getClass().getSimpleName(), "moveAllToCandidateDelete: ");
        return Completable.fromCallable(() -> {
            mComplexDao.moveAllToCandidateDelete();
            return Completable.complete();
        });
    }

    @Override
    public Completable removeAllCandidateDelete() {
        return Completable.fromCallable(() -> {
            mComplexDao.removeAllCandidateDelete();
            return Completable.complete();
        });
    }

    @Override
    public Flowable<ComplexEntity> getComplexByName(final String name) {
        return mComplexDao.getComplexByName(name);
    }

    @Override
    public Completable removeComplex(ComplexEntity... complexEntity) {
        return Completable.fromCallable(() -> {
            mComplexDao.removeComplex(complexEntity);
            return Completable.complete();
        });

    }

    @Override
    public Completable editComplex(Complex complex) {
        return Completable.fromCallable(() -> {
            mComplexDao.editComplex(complex.getUuid(), complex.getName());
            return Completable.complete();
        });
    }
}
