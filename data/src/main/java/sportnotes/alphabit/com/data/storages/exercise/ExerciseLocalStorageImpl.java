package sportnotes.alphabit.com.data.storages.exercise;

import android.arch.persistence.room.RoomDatabase;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.model.WeekDay;
import sportnotes.alphabit.com.data.Serializer;
import sportnotes.alphabit.com.data.storages.database.DbHelper;
import sportnotes.alphabit.com.data.storages.database.IAppDatabase;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseDao;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseEntity;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public class ExerciseLocalStorageImpl implements ExerciseLocalStorage {

    private final ExerciseDao mExerciseDao;
    private final DbHelper mDbHelper = new DbHelper();
    private final IAppDatabase appDatabase;

    public ExerciseLocalStorageImpl(final ExerciseDao exerciseDao, IAppDatabase appDatabase) {
        mExerciseDao = exerciseDao;
        this.appDatabase = appDatabase;
    }

    @Override
    public Completable insertExercise(final ExerciseEntity... exerciseEntity) {
        return Completable.fromCallable(() -> {
            for (ExerciseEntity item : exerciseEntity) {
                mDbHelper.prepare(item);
                mExerciseDao.insertExercise(item);
            }
            return Completable.complete();
        });
    }

    @Override
    public Completable deleteExercise(final UUID exerciseUuid) {
        return Completable.fromCallable(() -> {
            mExerciseDao.deleteExercise(exerciseUuid);
            return Completable.complete();
        });
    }

    @Override
    public Flowable<ExerciseEntity> getExercise(final UUID exerciseUuid) {
        return mExerciseDao.getExercise(exerciseUuid);
    }

    @Override
    public Flowable<List<ExerciseEntity>> getAllExercise(final int count, final long skip) {
        return mExerciseDao.getAllExercise(count, skip);
    }

    @Override
    public Completable setExercises(final List<ExerciseEntity> exercises) {
        return Completable.fromCallable(() -> {
            mExerciseDao.insert(exercises.toArray(new ExerciseEntity[exercises.size()]));
            return Completable.complete();
        });
    }

    @Override
    public Flowable<List<ExerciseEntity>> getWeekDayExercise(final UUID complexUuid, final WeekDay weekDay, final int count, final long skip) {
        return mExerciseDao.getWeekDayExercise(complexUuid, weekDay, count, skip);
    }

    @Override
    public Flowable<List<ExerciseEntity>> getNotSyncExercises(final int count, final long offset) {
        return mExerciseDao.getNotSyncExercises(count, offset);
    }

    @Override
    public Flowable<List<ExerciseEntity>> getDeletedExercises(final int count, final long offset) {
        return mExerciseDao.getDeletedExercises(count, offset);
    }

    @Override
    public Completable removeExercise(final ExerciseEntity... exerciseEntity) {
        return Completable.fromCallable(() -> {
            mExerciseDao.removeExercise(exerciseEntity);
            return Completable.complete();
        });
    }

    @Override
    public Completable moveAllToCandidateDelete() {
        return Completable.fromCallable(() -> {
            mExerciseDao.moveAllToCandidateDelete();
            return Completable.complete();
        });
    }

    @Override
    public Completable removeAllCandidateDelete() {
        return Completable.fromCallable(() -> {
            mExerciseDao.removeAllCandidateDelete();
            return Completable.complete();
        });
    }

    @Override
    public Completable changePositionToUp(final UUID exerciseUuid, final int oldPosition, final int newPosition) {
        return Completable.fromCallable(() -> {
            //mExerciseDao.changePosition(oldPosition, newPosition);
            return Completable.complete();
        });
    }

    @Override
    public Completable changePositionToDown(final UUID exerciseUuid, final int oldPosition, final int newPosition) {
        return Completable.fromCallable(() -> {
            //mExerciseDao.changePosition(oldPosition, newPosition);
            return Completable.complete();
        });
    }

    @Override
    public Completable updatePositions(final List<ExerciseEntity> exercises, long offset) {
        return Completable.fromCallable(() -> {
            mExerciseDao.updatePositionTransaction(exercises, offset);
            return Completable.complete();
        });
    }

    @Override
    public Completable removeExercisesByComplexUuid(UUID uuid) {
        return Completable.fromCallable(() -> {
            mExerciseDao.removeExercisesByComplexUuid(uuid);
            return Completable.complete();
        });
    }
}
