package sportnotes.alphabit.com.data.storages.complex;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.data.storages.base.LocalStorage;
import sportnotes.alphabit.com.data.storages.complex.db.ComplexEntity;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */

public interface ComplexLocalStorage extends LocalStorage {
    Completable insertComplex(ComplexEntity... complexEntity);

    Completable deleteComplex(UUID uuid);

    Flowable<ComplexEntity> getComplex(UUID uuid);

    Flowable<List<ComplexEntity>> getComplexes(int count, long skip);

    int getCountComplex();

    Flowable<List<ComplexEntity>> getNotSyncComplexes(final int count, final long offset);

    Flowable<List<ComplexEntity>> getDeletedComplexes(final int count, final long offset);

    Completable moveAllToCandidateDelete();

    Completable removeAllCandidateDelete();

    Flowable<ComplexEntity> getComplexByName(String name);

    Completable removeComplex(ComplexEntity... complexEntity);

    Completable editComplex(Complex complex);
}
