package sportnotes.alphabit.com.data.storages.purchase;

import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Purchases;
import sportnotes.alphabit.com.data.api.dto.PurchasesDto;
import sportnotes.alphabit.com.data.api.results.PurchasesResult;

/**
 * Created by fly12 on 20.11.2017.
 */

public interface PurchaseNetworkStorage {
    Single<PurchasesResult> setPurchase(PurchasesDto purchase);
}
