package sportnotes.alphabit.com.data.api.results;

import sportnotes.alphabit.com.data.api.dto.ErrorDto;
import sportnotes.alphabit.com.data.api.dto.PurchaseResultDto;

/**
 * Created by fly12 on 20.11.2017.
 */

public class PurchasesResult extends BaseResult<PurchaseResultDto, ErrorDto> {
}
