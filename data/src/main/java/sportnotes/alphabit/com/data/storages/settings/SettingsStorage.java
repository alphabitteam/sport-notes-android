package sportnotes.alphabit.com.data.storages.settings;

import com.annimon.stream.Optional;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Purchase;
import sportnotes.alphabit.com.data.storages.base.PreferenceStorage;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */

public interface SettingsStorage extends PreferenceStorage {

    Flowable<String> getWeightUnit();

    Completable setWeightUnit(String unit);

    Flowable<Optional<UUID>> getCurrentComplex();

    Completable setCurrentComplex(UUID complexUuid);

    Completable setRingtone(String uri);

    Flowable<String> getRingtone();

}
