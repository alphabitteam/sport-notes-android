package sportnotes.alphabit.com.data.storages.exercise;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.data.api.results.ExerciseResult;
import sportnotes.alphabit.com.data.storages.base.NetworkStorage;
import sportnotes.alphabit.com.data.storages.exercise.network.ExerciseDto;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */

public interface ExerciseNetworkStorage extends NetworkStorage {
    Completable setExercises(List<ExerciseDto> list);

    Completable deleteExercise(List<ExerciseDto> list);

    Single<ExerciseResult> getExercise(int count, long offset);
}
