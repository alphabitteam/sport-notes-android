package sportnotes.alphabit.com.data.storages.database.converters;

import android.arch.persistence.room.TypeConverter;

import sportnotes.alphabit.com.business.model.RepeatType;
import sportnotes.alphabit.com.business.model.ValueType;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 25.12.17.
 */
public class ValueTypeConverter {

    @TypeConverter
    public static ValueType stringToType(String s) {
        if (s == null)
            return null;
        return ValueType.valueOf(s);
    }

    @TypeConverter
    public static String typeToString(ValueType repeatType) {
        if (repeatType == null)
            return null;
        return repeatType.toString();
    }

}
