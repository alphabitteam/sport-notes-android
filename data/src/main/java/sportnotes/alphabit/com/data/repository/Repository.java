package sportnotes.alphabit.com.data.repository;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Yuri Zigunov on 15.10.2017.
 */

public class Repository {

    protected <T> Observable<T> execute(Observable<T> observable) {
        return observable.subscribeOn(Schedulers.io());
    }

    protected <T> Flowable<T> execute(Flowable<T> observable) {
        return observable.subscribeOn(Schedulers.io());
    }

    protected <T> Single<T> execute(Single<T> observable) {
        return observable.subscribeOn(Schedulers.io());
    }

    protected Completable execute(Completable observable) {
        return observable.subscribeOn(Schedulers.io());
    }
}
