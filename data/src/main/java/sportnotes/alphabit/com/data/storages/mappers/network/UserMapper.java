package sportnotes.alphabit.com.data.storages.mappers.network;

import sportnotes.alphabit.com.business.model.User;
import sportnotes.alphabit.com.data.api.dto.UserDto;
import sportnotes.alphabit.com.data.storages.mappers.db.PairMapper;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 26.11.2017.
 */

public class UserMapper implements PairMapper<UserDto, User> {
    @Override
    public User from(UserDto userDto) {
        User user = new User();
        user.setAuthKey(userDto.getAuthKey());
        user.setAvatarUrl(userDto.getAvatarUrl());
        user.setCreatedAt(userDto.getCreatedAt());
        user.setEmail(userDto.getEmail());
        user.setExternalId(userDto.getExternalId());
        user.setFirstname(userDto.getFirstname());
        user.setUuid(userDto.getUuid());
        user.setLastname(userDto.getLastname());
        user.setPasswordHash(userDto.getPasswordHash());
        user.setPasswordResetToken(userDto.getPasswordResetToken());
        user.setProvider(userDto.getProvider());
        user.setSubscribedTo(userDto.getSubscribedTo());
        user.setUpdatedAt(userDto.getUpdatedAt());
        return user;
    }

    @Override
    public UserDto to(User user) {
        UserDto userDto = new UserDto();
        userDto.setAuthKey(user.getAuthKey());
        userDto.setAvatarUrl(user.getAvatarUrl());
        userDto.setCreatedAt(user.getCreatedAt());
        userDto.setEmail(user.getEmail());
        userDto.setExternalId(user.getExternalId());
        userDto.setFirstname(user.getFirstname());
        userDto.setUuid(user.getUuid());
        userDto.setLastname(user.getLastname());
        userDto.setPasswordHash(user.getPasswordHash());
        userDto.setPasswordResetToken(user.getPasswordResetToken());
        userDto.setProvider(user.getProvider());
        userDto.setSubscribedTo(user.getSubscribedTo());
        userDto.setUpdatedAt(user.getUpdatedAt());
        return userDto;
    }
}
