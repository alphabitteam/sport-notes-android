package sportnotes.alphabit.com.data.api.results;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fly12 on 19.11.2017.
 */

public class BaseResult <Type, ErrorType> {
    @SerializedName("result")
    @Expose
    Type result;
    @SerializedName("error")
    @Expose
    ErrorType error;

    public Type getResult() {
        return result;
    }

    public void setResult(Type result) {
        this.result = result;
    }

    public ErrorType getError() {
        return error;
    }

    public void setError(ErrorType error) {
        this.error = error;
    }
}