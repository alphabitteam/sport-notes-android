package sportnotes.alphabit.com.data.storages.mappers.db;

import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.business.model.SetSum;
import sportnotes.alphabit.com.business.model.SetTime;
import sportnotes.alphabit.com.business.utils.SetFactory;
import sportnotes.alphabit.com.data.storages.sets.db.SetEntity;

/**
 * Created by Yuri Zigunov on 15.10.2017.
 */

public class SetMapper implements PairMapper<SetEntity, Set> {
    @Override
    public Set from(final SetEntity setEntity) {
        Set set = SetFactory.createSet(setEntity.getRepeatType(), setEntity.getValue(), setEntity.getValueType());
        set.setUuid(setEntity.getUuid());
        set.setRepeat(setEntity.getRepeat());
        set.setCreatedAt(setEntity.getCreatedAt());
        set.setExerciseUuid(setEntity.getExerciseUuid());
        set.setComment(setEntity.getComment());
        set.setNumber(setEntity.getNumber());
        set.setStatus(setEntity.getStatus());
        set.setDeleted(setEntity.isDeleted());
        set.setCreatedAt(setEntity.getCreatedAt());
        return set;
    }

    @Override
    public SetEntity to(final Set set) {
        SetEntity setEntity = new SetEntity();
        setEntity.setUuid(set.getUuid());
        setEntity.setRepeat(set.getRepeat());
        setEntity.setCreatedAt(set.getCreatedAt());
        setEntity.setExerciseUuid(set.getExerciseUuid());
        setEntity.setComment(set.getComment());
        setEntity.setNumber(set.getNumber());
        setEntity.setRepeatType(set.getRepeatType());
        setEntity.setStatus(set.getStatus());
        setEntity.setDeleted(set.isDeleted());
        setEntity.setCreatedAt(set.getCreatedAt());

        if (set instanceof SetSum) {
            setEntity.setValue(Integer.valueOf(((SetSum) set).getValue()).longValue());
            setEntity.setValueType(((SetSum) set).getValueType());
        } else if (set instanceof SetTime) {
            setEntity.setValue(((SetTime) set).getValue());
        }
        return setEntity;
    }
}
