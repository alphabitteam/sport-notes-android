package sportnotes.alphabit.com.data.repository;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.business.repository.IExercisesNetworkRepository;
import sportnotes.alphabit.com.data.storages.exercise.ExerciseNetworkStorage;
import sportnotes.alphabit.com.data.storages.exercise.network.ExerciseDto;
import sportnotes.alphabit.com.data.storages.mappers.network.ExerciseMapper;

/**
 * Created by Yuri Zigunov on 21.10.2017.
 */

public class ExercisesNetworkRepository implements IExercisesNetworkRepository {

    private final ExerciseNetworkStorage storage;
    private final ExerciseMapper exerciseMapper = new ExerciseMapper();

    //TODO: заменить на реальные данные
    public ExercisesNetworkRepository(final ExerciseNetworkStorage storage) {
        this.storage = storage;
    }

    @Override
    public Completable setExercises(final List<Exercise> exercises) {
        List<ExerciseDto> list = new ArrayList<>();
        for (Exercise exercise : exercises) {
            list.add(exerciseMapper.to(exercise));
        }
        return storage.setExercises(list);
    }

    @Override
    public Completable deleteExercise(final List<Exercise> exercises) {
        List<ExerciseDto> list = new ArrayList<>();
        for (Exercise exercise : exercises) {
            list.add(exerciseMapper.to(exercise));
        }
        return storage.deleteExercise(list);
    }

    @Override
    public Single<List<Exercise>> getExercise(final int count, final long offset) {
        Log.d(getClass().getSimpleName(), "getExercise: start count = " + count + " ffset = " + offset);
        return storage.getExercise(count, offset)
                .flatMap(exerciseResult -> {
                    Log.d(getClass().getSimpleName(), "getExercise: " + new Gson().toJson(exerciseResult));
                    if (exerciseResult.getResult() != null)
                        return Single.just(exerciseResult.getResult());
                    else {
                        return Single.error(new RuntimeException(exerciseResult.getError().getMessage()));
                    }
                })
                .doOnError(throwable -> Log.d(getClass().getSimpleName(), "getExercise: error = " + throwable))
                .flatMap(exerciseDto ->
                        Observable.fromIterable(exerciseDto)
                                .map(exerciseMapper::from)
                                .toList());
    }
}
