package sportnotes.alphabit.com.data.storages.database.converters;

import android.arch.persistence.room.TypeConverter;

import sportnotes.alphabit.com.business.model.RepeatType;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public class RepeatTypeConverter {

    @TypeConverter
    public static RepeatType stringToType(String s) {
        if (s == null)
            return null;
        return RepeatType.valueOf(s);
    }

    @TypeConverter
    public static String typeToString(RepeatType repeatType) {
        if (repeatType == null)
            return null;
        return repeatType.toString();
    }
}
