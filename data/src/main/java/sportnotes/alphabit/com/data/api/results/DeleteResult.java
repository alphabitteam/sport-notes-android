package sportnotes.alphabit.com.data.api.results;

import sportnotes.alphabit.com.data.api.dto.DeleteDto;
import sportnotes.alphabit.com.data.api.dto.ErrorDto;

/**
 * Created by fly12 on 19.11.2017.
 */

public class DeleteResult extends BaseResult<DeleteDto, ErrorDto> {

}
