package sportnotes.alphabit.com.data.storages.alarm;

import com.annimon.stream.Optional;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.TimerTask;
import sportnotes.alphabit.com.data.storages.base.PreferenceStorage;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */

public interface AlarmStorage extends PreferenceStorage {

    Single<Optional<TimerTask>> getTimerTask();

    Completable setTimerTask(TimerTask timerTask);

}
