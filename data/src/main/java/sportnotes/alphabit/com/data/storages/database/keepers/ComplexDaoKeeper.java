package sportnotes.alphabit.com.data.storages.database.keepers;

import sportnotes.alphabit.com.data.storages.complex.db.ComplexDao;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public interface ComplexDaoKeeper extends DaoKeeper {

    ComplexDao getComplexDao();

}
