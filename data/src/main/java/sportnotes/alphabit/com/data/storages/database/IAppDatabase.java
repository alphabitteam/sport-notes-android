package sportnotes.alphabit.com.data.storages.database;

import sportnotes.alphabit.com.data.storages.complex.db.ComplexDao;
import sportnotes.alphabit.com.data.storages.complex.db.ComplexSyncDao;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseDao;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseSyncDao;
import sportnotes.alphabit.com.data.storages.sets.db.SetDao;
import sportnotes.alphabit.com.data.storages.sets.db.SetSyncDao;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public interface IAppDatabase {

    ComplexDao getComplexDao();

    ExerciseDao getExerciseDao();

    SetDao getSetDao();

    ComplexSyncDao getComplexSyncDao();

    ExerciseSyncDao getExerciseSyncDao();

    SetSyncDao getSetSyncDao();

    void beginTransaction();

    void endTransaction();
}
