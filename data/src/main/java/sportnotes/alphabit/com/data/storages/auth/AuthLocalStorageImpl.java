package sportnotes.alphabit.com.data.storages.auth;

import android.content.SharedPreferences;

import com.annimon.stream.Optional;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.User;
import sportnotes.alphabit.com.data.Serializer;
import sportnotes.alphabit.com.data.api.dto.AuthDto;
import sportnotes.alphabit.com.data.storages.base.cores.PreferenceCore;

/**
 * Created by fly12 on 19.11.2017.
 */

public class AuthLocalStorageImpl implements AuthLocalStorage {

    private final SharedPreferences sharedPreferences;
    private final Serializer serializer = new Serializer();

    private static final String AUTH = "auth";
    private static final String USER = "user";

    public AuthLocalStorageImpl(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }


    @Override
    public Completable setAuth(AuthDto authDto) {
        if (authDto == null) {
            sharedPreferences.edit().remove(AUTH).commit();
            return Completable.complete();
        }
        String json = serializer.serialize(authDto, AuthDto.class);
        sharedPreferences.edit().putString(AUTH, json).commit();
        return Completable.complete();
    }

    @Override
    public Single<Optional<AuthDto>> getAuth() {
        String json = sharedPreferences.getString(AUTH, null);
        if (json == null)
            return Single.just(Optional.empty());
        return Single.just(Optional.of(serializer.deserialize(json, AuthDto.class)));
    }

    @Override
    public Completable setUser(User user) {
        if (user == null) {
            sharedPreferences.edit().remove(USER).commit();
            return Completable.complete();
        }
        String json = serializer.serialize(user, User.class);
        sharedPreferences.edit().putString(USER, json).commit();
        return Completable.complete();
    }

    @Override
    public Single<Optional<User>> getUser() {
        String json = sharedPreferences.getString(USER, null);
        if (json == null)
            return Single.just(Optional.empty());
        return Single.just(Optional.of(serializer.deserialize(json, User.class)));
    }
}
