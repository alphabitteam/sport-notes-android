package sportnotes.alphabit.com.data.storages.auth;

import io.reactivex.Single;
import io.reactivex.SingleSource;
import sportnotes.alphabit.com.data.api.ServerApi;
import sportnotes.alphabit.com.data.api.results.AuthResult;
import sportnotes.alphabit.com.data.api.results.UserResult;

/**
 * Created by fly12 on 19.11.2017.
 */

public interface AuthNetworkStorage {

    void setServerApi(ServerApi serverApi);

    Single<AuthResult> login(String provider, String token);

    Single<AuthResult> retoken(String provider, String refresh_token);

    Single<UserResult> getUser();
}
