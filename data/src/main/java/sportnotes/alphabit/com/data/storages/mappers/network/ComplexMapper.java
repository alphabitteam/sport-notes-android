package sportnotes.alphabit.com.data.storages.mappers.network;

import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.data.storages.complex.network.ComplexDto;
import sportnotes.alphabit.com.data.storages.mappers.db.PairMapper;

/**
 * Created by Yuri Zigunov on 22.10.2017.
 */

public class ComplexMapper implements PairMapper<ComplexDto, Complex> {
    @Override
    public Complex from(final ComplexDto complexDto) {
        Complex complex = new Complex();
        complex.setUuid(complexDto.getUuid());
        complex.setPosition(complexDto.getPosition());
        complex.setName(complexDto.getName());
        complex.setCreatedAt(complexDto.getCreatedAt());
        return complex;
    }

    @Override
    public ComplexDto to(final Complex complex) {
        ComplexDto complexDto = new ComplexDto();
        complexDto.setUuid(complex.getUuid());
        complexDto.setPosition(complex.getPosition());
        complexDto.setName(complex.getName());
        complexDto.setCreatedAt(complex.getCreatedAt());
        return complexDto;
    }
}
