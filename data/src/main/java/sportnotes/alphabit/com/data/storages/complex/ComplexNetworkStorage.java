package sportnotes.alphabit.com.data.storages.complex;


import java.util.List;

import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.data.api.results.ComplexResult;
import sportnotes.alphabit.com.data.api.results.DeleteResult;
import sportnotes.alphabit.com.data.storages.base.NetworkStorage;
import sportnotes.alphabit.com.data.storages.complex.network.ComplexDto;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */

public interface ComplexNetworkStorage extends NetworkStorage {
    Single<ComplexResult> getComplexes(int count, long offset);

    Single<DeleteResult> deleteComplexes(List<ComplexDto> list);

    Single<ComplexResult> setComplexes(List<ComplexDto> list);
}
