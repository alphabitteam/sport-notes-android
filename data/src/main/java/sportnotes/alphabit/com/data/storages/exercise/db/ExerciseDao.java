package sportnotes.alphabit.com.data.storages.exercise.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;
import java.util.UUID;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import sportnotes.alphabit.com.business.Serializer;
import sportnotes.alphabit.com.business.model.Status;
import sportnotes.alphabit.com.business.model.WeekDay;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */
@Dao
public abstract class ExerciseDao {

    //int DOWN = 1;
    //int UP = -1;

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(ExerciseEntity... exerciseEntity);

    @Transaction
    public void insertExercise(ExerciseEntity exerciseEntity) {
        System.out.println("ExerciseDao.insertExercise = " + new Serializer().serialize(exerciseEntity));
        if (exerciseEntity != null && exerciseEntity.getComplexUuid() != null
                && exerciseEntity.getWeekDay() != null
                && exerciseEntity.getPosition() == -1) {
            long count = getExerciseCount(exerciseEntity.getComplexUuid(), exerciseEntity.getWeekDay());
            exerciseEntity.setPosition(count);
        }
        insert(exerciseEntity);
    }

    @Query("SELECT COUNT(*) FROM exercise WHERE complexUuid=:complexUuid AND weekDay=:weekDay")
    public abstract long getExerciseCount(UUID complexUuid, WeekDay weekDay);

    @Transaction
    public void saveExercise(ExerciseEntity exerciseEntity) {

    }

    @Query("UPDATE exercise SET deleted=1, position=0, createdAt=0, updatedAt=0, status=0 WHERE uuid=:exerciseUuid")
    public abstract void deleteExercise(UUID exerciseUuid);

    @Query("SELECT * FROM exercise WHERE deleted=0 AND uuid=:exerciseUuid")
    public abstract Flowable<ExerciseEntity> getExercise(UUID exerciseUuid);

    @Query("SELECT * FROM exercise WHERE deleted=0 AND uuid=:exerciseUuid")
    public abstract ExerciseEntity getExerciseSync(UUID exerciseUuid);

    @Delete
    public abstract void removeExercise(ExerciseEntity... exerciseEntity);

    @Query("UPDATE exercise SET candidateDelete=1")
    public abstract void moveAllToCandidateDelete();

    @Query("DELETE FROM exercise WHERE candidateDelete=1")
    public abstract void removeAllCandidateDelete();

    @Query("SELECT * FROM exercise LIMIT :count OFFSET :skip")
    public abstract Flowable<List<ExerciseEntity>> getAllExercise(int count, long skip);

    @Query("SELECT * FROM exercise WHERE deleted=0 AND weekDay=:weekDay AND complexUuid=:complexUuid LIMIT :count OFFSET :skip")
    public abstract Flowable<List<ExerciseEntity>> getWeekDayExercise(UUID complexUuid, WeekDay weekDay, int count, long skip);

    @Query("UPDATE exercise SET position= CASE position WHEN :oldPosition THEN :newPosition " +
            "WHEN :newPosition THEN :oldPosition ELSE position END ")
    public abstract void changePosition(int oldPosition, int newPosition);


    @Transaction
    public void updatePositionTransaction(List<ExerciseEntity> list, long skip) {
        for (int i = 0; i < list.size(); i++) {
            //выяснить почему не работает updatePosition
            //updatePosition(list.get(i).getUuid(), skip + i);
            ExerciseEntity item = list.get(i);
            item.setPosition(skip + i);
            item.setStatus(Status.SENDING);
            insert(item);
        }
    }

    @Query("UPDATE exercise SET position=:position, status=0 WHERE uuid=:uuid")
    public abstract void updatePosition(UUID uuid, long position);

    @Query("SELECT * FROM exercise WHERE status=0 AND deleted=0 LIMIT :count OFFSET :skip")
    public abstract Flowable<List<ExerciseEntity>> getNotSyncExercises(int count, long skip);

    @Query("SELECT * FROM exercise WHERE deleted=1 AND status=0 LIMIT :count OFFSET :skip")
    public abstract Flowable<List<ExerciseEntity>> getDeletedExercises(final int count, final long skip);


    @Transaction
    public void removeExercisesByComplexUuid(UUID uuid) {
        List<ExerciseEntity> list = getExercisesByComplexUuidSync(uuid);
        if (list.size() == 0)
            return;
        for (ExerciseEntity item : list) {
            removeExercise(getExerciseSync(item.getUuid()));
            removeSetsByExerciseUuid(item.getUuid());
        }
    }

    @Query("SELECT * FROM exercise WHERE complexUuid=:uuid")
    public abstract List<ExerciseEntity> getExercisesByComplexUuidSync(UUID uuid);

    @Query("DELETE FROM sets WHERE exerciseUuid=:uuid")
    public abstract void removeSetsByExerciseUuid(UUID uuid);

}
