package sportnotes.alphabit.com.data.repository;

import android.util.Log;

import com.annimon.stream.Optional;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.TimerTask;
import sportnotes.alphabit.com.business.repository.IPreferenceRepository;
import sportnotes.alphabit.com.data.storages.alarm.AlarmStorage;
import sportnotes.alphabit.com.data.storages.settings.SettingsStorage;

/**
 * Created by Yuri Zigunov on 30.09.2017.
 */

public class PreferenceRepository implements IPreferenceRepository {

    private final AlarmStorage mAlarmStorage;
    private final SettingsStorage mSettingsStorage;

    @Inject
    public PreferenceRepository(final AlarmStorage alarmStorage, final SettingsStorage settingsStorage) {
        mAlarmStorage = alarmStorage;
        mSettingsStorage = settingsStorage;
    }

    @Override
    public Flowable<String> getWeightUnit() {
        return mSettingsStorage.getWeightUnit();
    }

    @Override
    public Single<Optional<TimerTask>> getTimerTask() {
        return mAlarmStorage.getTimerTask();
    }

    @Override
    public Completable setTimerTask(final TimerTask timerTask) {
        mAlarmStorage.setTimerTask(timerTask);
        return Completable.complete();
    }

    @Override
    public Completable saveWeightUnit(String unit) {
        return mSettingsStorage.setWeightUnit(unit);
    }

    @Override
    public Completable saveRingtone(String uri) {
        return mSettingsStorage.setRingtone(uri);
    }

    @Override
    public Flowable<String> getRingtone() {
        return mSettingsStorage.getRingtone();
    }

}
