package sportnotes.alphabit.com.data.repository;


import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.repository.IComplexesNetworkRepository;
import sportnotes.alphabit.com.data.storages.complex.ComplexNetworkStorage;
import sportnotes.alphabit.com.data.storages.complex.network.ComplexDto;
import sportnotes.alphabit.com.data.storages.mappers.network.ComplexMapper;

/**
 * Created by Yuri Zigunov on 21.10.2017.
 */

public class ComplexesNetworkRepository implements IComplexesNetworkRepository {

    private final ComplexNetworkStorage storage;
    private final ComplexMapper complexMapper = new ComplexMapper();

    public ComplexesNetworkRepository(final ComplexNetworkStorage storage) {
        this.storage = storage;
    }

    @Override
    public Completable setComplexes(final List<Complex> complexes) {
        List<ComplexDto> list = new ArrayList<>();
        for (Complex complex : complexes) {
            list.add(complexMapper.to(complex));
        }
        return storage.setComplexes(list)
                .doOnError(throwable -> Log.d(getClass().getSimpleName(), "setComplexes: " + throwable)).toCompletable();
    }

    @Override
    public Completable deleteComplexes(final List<Complex> complexes) {
        List<ComplexDto> list = new ArrayList<>();
        for (Complex complex : complexes) {
            list.add(complexMapper.to(complex));
        }
        return storage.deleteComplexes(list)
                .doOnError(throwable -> Log.d(getClass().getSimpleName(), "deleteComplexes: " + throwable)).toCompletable();
    }

    @Override
    public Single<List<Complex>> getComplexes(final int count, final long offset) {
        Log.d(getClass().getSimpleName(), "getComplexes: start count = " + count + " offset = " + offset);
        return storage.getComplexes(count, offset)
                .flatMap(complexResult -> {
                    Log.d(getClass().getSimpleName(), "getComplexes: " + new Gson().toJson(complexResult));
                    if (complexResult.getResult() != null)
                        return Single.just(complexResult.getResult());
                    else {
                        return Single.error(new RuntimeException(complexResult.getError().getMessage()));
                    }
                })
                .doOnError(throwable -> Log.d(getClass().getSimpleName(), "getComplexes: error = " + throwable))
                .flatMap(complexDtos ->
                        Observable.fromIterable(complexDtos)
                                .map(complexMapper::from)
                                .toList());
    }
}
