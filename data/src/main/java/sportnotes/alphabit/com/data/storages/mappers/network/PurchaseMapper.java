package sportnotes.alphabit.com.data.storages.mappers.network;

import android.util.Log;

import com.google.gson.Gson;

import sportnotes.alphabit.com.business.exceptions.SerjException;
import sportnotes.alphabit.com.business.model.PurchaseResponseData;
import sportnotes.alphabit.com.business.model.Purchases;
import sportnotes.alphabit.com.data.api.dto.PurchasesDto;
import sportnotes.alphabit.com.data.api.dto.ResponseDataDto;
import sportnotes.alphabit.com.data.storages.mappers.db.PairMapper;

/**
 * Created by fly12 on 20.11.2017.
 */

public class PurchaseMapper implements PairMapper<PurchasesDto, Purchases> {
    @Override
    public Purchases from(PurchasesDto purchasesDto) {
        Log.d(getClass().getSimpleName(), "from: " + new Gson().toJson(purchasesDto));
        ResponseDataDto dataDto = purchasesDto.getPurchaseData();
        PurchaseResponseData data = new PurchaseResponseData();

        if (dataDto == null)
            throw new SerjException();

        data.setAutoRenewing(dataDto.getAutoRenewing());
        data.setDeveloperPayload(dataDto.getDeveloperPayload());
        data.setOrderId(dataDto.getOrderId());
        data.setPackageName(dataDto.getPackageName());
        data.setProductId(dataDto.getProductId());
        data.setPurchaseState(dataDto.getPurchaseState());
        data.setPurchaseTime(dataDto.getPurchaseTime());
        data.setPurchaseToken(dataDto.getPurchaseToken());

        return new Purchases(data, purchasesDto.getResponseData(), purchasesDto.getSignature());
    }

    @Override
    public PurchasesDto to(Purchases purchases) {
        ResponseDataDto dataDto = new ResponseDataDto();
        PurchaseResponseData data = purchases.getResponseData();

        dataDto.setAutoRenewing(data.getAutoRenewing());
        dataDto.setDeveloperPayload(data.getDeveloperPayload());
        dataDto.setOrderId(data.getOrderId());
        dataDto.setPackageName(data.getPackageName());
        dataDto.setProductId(data.getProductId());
        dataDto.setPurchaseState(data.getPurchaseState());
        dataDto.setPurchaseTime(data.getPurchaseTime());
        dataDto.setPurchaseToken(data.getPurchaseToken());

        return new PurchasesDto(dataDto, purchases.getOriginJson(), purchases.getSignature());
    }
}
