package sportnotes.alphabit.com.data.utils;

/**
 * Created by Fly on 03.05.2017.
 */

public enum Host {
    PRODUCTION("http://85.143.175.213/");

    String host;

    Host(String host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return host;
    }
}
