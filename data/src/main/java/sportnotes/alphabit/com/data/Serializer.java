package sportnotes.alphabit.com.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;

import javax.inject.Inject;
import javax.inject.Singleton;

import sportnotes.alphabit.com.data.utils.JodoDateTimeConverter;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */
@Singleton
public class Serializer {

    private final Gson gson;

    @Inject
    public Serializer() {
        gson = new GsonBuilder()
                .registerTypeAdapter(DateTime.class, new JodoDateTimeConverter())
                .create();
    }

    public String serialize(Object object, Class clazz) {
        return gson.toJson(object, clazz);
    }

    public <T> T deserialize(String string, Class<T> clazz) {
        return gson.fromJson(string, clazz);
    }
}
