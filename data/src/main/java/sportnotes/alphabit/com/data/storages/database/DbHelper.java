package sportnotes.alphabit.com.data.storages.database;

import android.util.Log;

import com.google.gson.Gson;

import org.joda.time.DateTime;

import java.util.UUID;

import sportnotes.alphabit.com.business.model.Status;
import sportnotes.alphabit.com.data.storages.base.db.DbEntity;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public class DbHelper {

    public DbHelper() {
    }

    public <Entity extends DbEntity> void prepare(Entity entity) {
        Log.d(getClass().getSimpleName(), "prepare: " + new Gson().toJson(entity));
        DateTime dateTime = DateTime.now();
        if (entity.getUuid() == null)
            entity.setUuid(UUID.randomUUID());
        if (entity.getCreatedAt() == null)
            entity.setCreatedAt(dateTime);
        if (entity.getStatus() == null)
            entity.setStatus(Status.SENDING);

        entity.setUpdatedAt(dateTime);
    }
}
