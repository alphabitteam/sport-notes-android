package sportnotes.alphabit.com.data.storages.database;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public interface IDatabaseCore {

    IAppDatabase getDatabase();

}
