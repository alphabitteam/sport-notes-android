package sportnotes.alphabit.com.data.storages.mappers.db;

import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.data.storages.complex.db.ComplexEntity;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public class ComplexMapper implements PairMapper<ComplexEntity, Complex> {

    @Override
    public Complex from(final ComplexEntity complexEntity) {
        Complex complex = new Complex();
        complex.setUuid(complexEntity.getUuid());
        complex.setName(complexEntity.getName());
        complex.setPosition(complexEntity.getPosition());
        complex.setStatus(complexEntity.getStatus());
        complex.setDeleted(complexEntity.isDeleted());
        complex.setCreatedAt(complexEntity.getCreatedAt());
        return complex;
    }

    @Override
    public ComplexEntity to(final Complex complex) {
        ComplexEntity complexEntity = new ComplexEntity();
        complexEntity.setUuid(complex.getUuid());
        complexEntity.setName(complex.getName());
        complexEntity.setPosition(complex.getPosition());
        complexEntity.setStatus(complex.getStatus());
        complexEntity.setDeleted(complex.isDeleted());
        complexEntity.setCreatedAt(complex.getCreatedAt());
        return complexEntity;
    }
}
