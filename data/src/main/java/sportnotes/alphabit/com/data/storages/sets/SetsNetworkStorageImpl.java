package sportnotes.alphabit.com.data.storages.sets;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Set;
import sportnotes.alphabit.com.data.api.ServerApi;
import sportnotes.alphabit.com.data.api.results.SetResult;
import sportnotes.alphabit.com.data.storages.mappers.network.SetMapper;
import sportnotes.alphabit.com.data.storages.sets.network.SetDto;

/**
 * Created by Yuri Zigunov on 07.10.2017.
 */

public class SetsNetworkStorageImpl implements SetsNetworkStorage {

    private final ServerApi serverApi;

    public SetsNetworkStorageImpl(final ServerApi serverApi) {
        this.serverApi = serverApi;
    }

    @Override
    public Completable setSets(final List<SetDto> list) {
        return serverApi.setSets(list).toCompletable();
    }

    @Override
    public Completable deleteSets(final List<SetDto> list) {
        return Observable.fromIterable(list)
                .map(SetDto::getUuid)
                .map(UUID::toString)
                .toList()
                .flatMap(serverApi::deleteSets).toCompletable();
    }

    @Override
    public Single<SetResult> getSets(final int count, final long offset) {
        return serverApi.getSets(count, offset);
    }
}
