package sportnotes.alphabit.com.data.storages.mappers.db;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public interface PairMapper<Src, Dst> {

    Dst from(Src src);

    Src to(Dst dst);

}
