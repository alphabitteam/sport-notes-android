package sportnotes.alphabit.com.data.storages.database.converters;

import android.arch.persistence.room.TypeConverter;

import org.joda.time.DateTime;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public class DateTimeConverter {

    @TypeConverter
    public static DateTime millisToDate(long millis) {
        return new DateTime(millis);
    }

    @TypeConverter
    public static long dateToMillis(DateTime dateTime) {
        if (dateTime == null)
            return 0;
        return dateTime.getMillis();
    }
}
