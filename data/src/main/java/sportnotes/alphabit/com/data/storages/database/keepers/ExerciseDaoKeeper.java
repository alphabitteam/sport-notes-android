package sportnotes.alphabit.com.data.storages.database.keepers;

import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseDao;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public interface ExerciseDaoKeeper extends DaoKeeper {

    ExerciseDao getExerciseDao();

}
