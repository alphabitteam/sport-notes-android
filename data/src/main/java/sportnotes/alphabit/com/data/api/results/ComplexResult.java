package sportnotes.alphabit.com.data.api.results;

import java.util.List;

import sportnotes.alphabit.com.data.api.dto.ErrorDto;
import sportnotes.alphabit.com.data.storages.complex.db.ComplexDao;
import sportnotes.alphabit.com.data.storages.complex.network.ComplexDto;

/**
 * Created by fly12 on 19.11.2017.
 */

public class ComplexResult extends BaseResult<List<ComplexDto>, ErrorDto> {

}
