package sportnotes.alphabit.com.data.api;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import sportnotes.alphabit.com.business.model.Complex;
import sportnotes.alphabit.com.business.model.Exercise;
import sportnotes.alphabit.com.data.Configuration;
import sportnotes.alphabit.com.data.api.dto.AuthDto;
import sportnotes.alphabit.com.data.api.dto.ErrorDto;
import sportnotes.alphabit.com.data.api.dto.PurchasesDto;
import sportnotes.alphabit.com.data.api.results.AuthResult;
import sportnotes.alphabit.com.data.api.results.BaseResult;
import sportnotes.alphabit.com.data.api.results.ComplexResult;
import sportnotes.alphabit.com.data.api.results.ExerciseResult;
import sportnotes.alphabit.com.data.api.results.PurchasesResult;
import sportnotes.alphabit.com.data.api.results.SetResult;
import sportnotes.alphabit.com.data.api.results.DeleteResult;
import sportnotes.alphabit.com.data.api.results.UserResult;
import sportnotes.alphabit.com.data.storages.complex.network.ComplexDto;
import sportnotes.alphabit.com.data.storages.exercise.network.ExerciseDto;
import sportnotes.alphabit.com.data.storages.sets.network.SetDto;

/**
 * Created by Yuri Zigunov on 22.10.2017.
 */

public interface ServerApi {


    @FormUrlEncoded
    @POST("oauth2/login")
    @Headers("Authorization: Basic dGVzdGNsaWVudDp0ZXN0cGFzcw==")
    Single<AuthResult> login(@Field("provider") String provider, @Field("token") String token);

    @FormUrlEncoded
    @POST("oauth2/retoken")
    @Headers("Authorization: Basic dGVzdGNsaWVudDp0ZXN0cGFzcw==")
    Single<AuthResult> retoken(@Field("provider") String provider, @Field("refresh_token") String refresh_token);

    @GET(Configuration.API_VERSION_NAME + "/users/me")
    Single<UserResult> getUser();

    @POST(Configuration.API_VERSION_NAME + "/sets")
    Single<SetResult> setSets(@Body List<SetDto> list);

    @POST(Configuration.API_VERSION_NAME + "/complexes")
    Single<ComplexResult> setComplexes(@Body List<ComplexDto> list);

    @POST(Configuration.API_VERSION_NAME + "/exercises")
    Single<ExerciseResult> setExercises(@Body List<ExerciseDto> list);

    @POST(Configuration.API_VERSION_NAME + "/sets/batch-delete")
    Single<DeleteResult> deleteSets(@Body List<String> list);

    @POST(Configuration.API_VERSION_NAME + "/complexes/batch-delete")
    Single<DeleteResult> deleteComplexes(@Body List<String> list);

    @POST(Configuration.API_VERSION_NAME + "/exercises/batch-delete")
    Single<DeleteResult> deleteExercise(@Body List<String> list);

    @GET(Configuration.API_VERSION_NAME + "/sets")
    Single<SetResult> getSets(@Query("pageSize")int count, @Query("page") long offset);

    @GET(Configuration.API_VERSION_NAME + "/complexes")
    Single<ComplexResult> getComplexes(@Query("pageSize")int count, @Query("page") long offset);

    @GET(Configuration.API_VERSION_NAME + "/exercises")
    Single<ExerciseResult> getExercise(@Query("pageSize")int count, @Query("page") long offset);


    @POST(Configuration.API_VERSION_NAME + "/purchases")
    Single<PurchasesResult> setPurchase(@Body PurchasesDto purchaseInfo);

}
