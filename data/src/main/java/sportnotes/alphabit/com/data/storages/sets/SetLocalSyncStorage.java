package sportnotes.alphabit.com.data.storages.sets;

import java.util.List;

import sportnotes.alphabit.com.data.storages.sets.db.SetEntity;

/**
 * Created by fly12 on 03.12.2017.
 */
public interface SetLocalSyncStorage {

    List<SetEntity> getNotSyncSets(final int count, final long skip);

    List<SetEntity> getDeletedSets(final int count, final long skip);

}
