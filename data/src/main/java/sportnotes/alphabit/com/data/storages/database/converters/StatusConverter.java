package sportnotes.alphabit.com.data.storages.database.converters;

import android.arch.persistence.room.TypeConverter;

import sportnotes.alphabit.com.business.model.Status;

/**
 * Created by Yuri Zigunov on 08.10.2017.
 */

public class StatusConverter {

    @TypeConverter
    public static Status intToStatus(int value) {
        return Status.getEnum(value);
    }

    @TypeConverter
    public static int statusToInt(Status status) {
        if (status == null)
            return 0;
        return status.getValue();
    }
}
