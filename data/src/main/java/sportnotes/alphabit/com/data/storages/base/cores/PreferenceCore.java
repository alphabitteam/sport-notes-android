package sportnotes.alphabit.com.data.storages.base.cores;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Single;
import sportnotes.alphabit.com.business.model.Purchase;
import sportnotes.alphabit.com.business.model.TimerTask;
import sportnotes.alphabit.com.data.R;
import sportnotes.alphabit.com.data.Serializer;

/**
 * Created by Yuri Zigunov on 03.10.2017.
 */
@Singleton
public class PreferenceCore {

    public static final String TIMER_TASK = "TIMER_TASK";
    public static final String WEIGHT_UNIT = "WEIGHT_UNIT";
    public static final String RINGTONE = "RINGTONE";
    public static final String CURRENT_COMPLEX = "CURRENT_COMPLEX";
    public static final String PURCHASES = "PURCHASES";

    private final Context mContext;
    private final SharedPreferences mSharedPreferences;
    private final Serializer mSerializer;

    @Inject
    public PreferenceCore(final Context context, final SharedPreferences sharedPreferences,
                          final Serializer serializer) {
        mContext = context;
        mSharedPreferences = sharedPreferences;
        mSerializer = serializer;
    }

    public TimerTask getTimerTask() {
        final String json = mSharedPreferences.getString(TIMER_TASK, "");
        return mSerializer.deserialize(json, TimerTask.class);
    }

    public void setTimerTask(TimerTask timerTask) {
        String json = mSerializer.serialize(timerTask, TimerTask.class);
        mSharedPreferences.edit().putString(TIMER_TASK, json).apply();
    }

    public String getWeightUnit() {
        return mSharedPreferences.getString(WEIGHT_UNIT, mContext.getString(R.string.kg));
    }

    public void setWeightUnit(String unit) {
        mSharedPreferences.edit().putString(WEIGHT_UNIT, unit).apply();
    }

    public UUID getCurrentComplex() {
        String uuidStr = mSharedPreferences.getString(CURRENT_COMPLEX, null);
        if (uuidStr == null)
            return null;
        return UUID.fromString(uuidStr);
    }

    public void setCurrentComplex(final UUID complexUuid) {
        mSharedPreferences.edit().putString(CURRENT_COMPLEX, complexUuid.toString()).apply();
    }

    @SuppressWarnings("unchecked")
    public List<Purchase> getPurchases() {
        String json = mSharedPreferences.getString(PURCHASES, null);
        ArrayList list = mSerializer.deserialize(json, ArrayList.class);
        if (list == null)
            return new ArrayList<>();
        return list;
    }

    public void setPurchases(List<Purchase> list) {
        String json = mSerializer.serialize(list, ArrayList.class);
        mSharedPreferences.edit().putString(PURCHASES, json).apply();
    }

    public void setRingtone(String uri) {
        mSharedPreferences.edit().putString(RINGTONE, uri).apply();
    }

    public String getRingtone() {
        return mSharedPreferences
                .getString(RINGTONE, String.valueOf(Settings.System.DEFAULT_ALARM_ALERT_URI));
    }
}
