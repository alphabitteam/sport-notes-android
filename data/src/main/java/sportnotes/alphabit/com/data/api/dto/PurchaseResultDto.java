package sportnotes.alphabit.com.data.api.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PurchaseResultDto {

    @SerializedName("response_data")
    @Expose
    private String responseData;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("signature")
    @Expose
    private String signature;
    @SerializedName("old_subscribed_to")
    @Expose
    private String oldSubscribedTo;
    @SerializedName("new_subscribed_to")
    @Expose
    private String newSubscribedTo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("user_uuid")
    @Expose
    private String userUuid;

    public String getResponseData() {
        return responseData;
    }

    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getOldSubscribedTo() {
        return oldSubscribedTo;
    }

    public void setOldSubscribedTo(String oldSubscribedTo) {
        this.oldSubscribedTo = oldSubscribedTo;
    }

    public String getNewSubscribedTo() {
        return newSubscribedTo;
    }

    public void setNewSubscribedTo(String newSubscribedTo) {
        this.newSubscribedTo = newSubscribedTo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

}