package sportnotes.alphabit.com.data.storages.database;

import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;


/**
 * Created by Yuri Zigunov on 08.10.2017.
 */
@Singleton
public class DatabaseCore implements IDatabaseCore {

    private final AppDatabase mAppDatabase;

    @Inject
    public DatabaseCore(Context context) {
        mAppDatabase = Room.databaseBuilder(context,
                AppDatabase.class, "sportnotes").build();
    }

    public IAppDatabase getDatabase() {
        return mAppDatabase;
    }

}
