package sportnotes.alphabit.com.data.api.results;

import java.util.List;

import sportnotes.alphabit.com.data.api.dto.ErrorDto;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseDao;
import sportnotes.alphabit.com.data.storages.exercise.network.ExerciseDto;

/**
 * Created by fly12 on 19.11.2017.
 */

public class ExerciseResult extends BaseResult<List<ExerciseDto>, ErrorDto> {

}
