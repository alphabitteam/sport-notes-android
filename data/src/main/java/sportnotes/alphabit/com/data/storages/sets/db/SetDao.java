package sportnotes.alphabit.com.data.storages.sets.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.util.Log;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.Flowable;
import sportnotes.alphabit.com.data.storages.exercise.db.ExerciseEntity;
import sportnotes.alphabit.com.data.storages.sets.SetsLocalStorage;


/**
 * Created by Yuri Zigunov on 08.10.2017.
 */
@Dao
public abstract class SetDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertSet(SetEntity... setEntity);

    @Transaction
    public void insertOrUpdateSetBySetNumber(SetEntity setEntity) {
        DateTime startDate = setEntity.getCreatedAt().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
        DateTime endDate = startDate.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
        int number = setEntity.getNumber();
        int repeat = setEntity.getRepeat();
        long value = setEntity.getValue();
        String comment = setEntity.getComment();
        int update = updateSetBySetNumber(setEntity.getExerciseUuid(), repeat, value, comment, number, startDate.getMillis(), endDate.getMillis());

        if (update == 0)
            insertSet(setEntity);
    }

    @Query("UPDATE sets SET repeat=:repeat, value=:value, comment=:comment, status=0 " +
            "WHERE exerciseUuid=:exerciseUuid AND createdAt>=:startDate AND createdAt<:endDate AND number=:setsNumber")
    protected abstract int updateSetBySetNumber(UUID exerciseUuid, int repeat, long value, String comment, int setsNumber,
                                             long startDate, long endDate);

    @Query("UPDATE sets SET deleted=1, position=0, createdAt=0, updatedAt=0, status=0 WHERE uuid=:uuid")
    public abstract void deleteSet(UUID uuid);

    @Delete
    public abstract void removeSet(SetEntity... setEntity);

    @Query("UPDATE sets SET candidateDelete=1")
    public abstract void moveAllToCandidateDelete();

    @Query("DELETE FROM sets WHERE candidateDelete=1")
    public abstract void removeAllCandidateDelete();

    @Query("SELECT * FROM sets LIMIT :count OFFSET :skip")
    public abstract Flowable<List<SetEntity>> getAllSet(int count, long skip);

    @Query("SELECT * FROM sets WHERE uuid=:uuid")
    public abstract Flowable<SetEntity> getSet(UUID uuid);

    @Query("SELECT * FROM sets WHERE exerciseUuid=:exerciseUuid AND deleted=0 ORDER BY createdAt DESC LIMIT :count OFFSET :skip")
    public abstract Flowable<List<SetEntity>> getSets(UUID exerciseUuid, int count, long skip);

    @Query("SELECT * FROM sets WHERE exerciseUuid=:exerciseUuid AND deleted=0 ORDER BY createdAt LIMIT :count OFFSET :skip")
    public abstract List<SetEntity> getSetsSync(UUID exerciseUuid, int count, long skip);

    @Query("SELECT * FROM sets WHERE exerciseUuid=:exerciseUuid AND deleted=0 ORDER BY createdAt DESC LIMIT :count OFFSET :skip")
    public abstract List<SetEntity> getLastSetsSync(UUID exerciseUuid, int count, long skip);

    @Query("SELECT * FROM sets WHERE deleted=0 LIMIT :count OFFSET :skip")
    public abstract Flowable<List<SetEntity>> getSets(int count, long skip);

    @Query("SELECT * FROM sets WHERE status=0 AND deleted=0 LIMIT :count OFFSET :skip")
    public abstract Flowable<List<SetEntity>> getNotSyncSets(final int count, final long skip);

    @Query("SELECT * FROM sets WHERE deleted=1 LIMIT :count OFFSET :skip")
    public abstract Flowable<List<SetEntity>> getDeletedSets(final int count, final long skip);

    @Query("DELETE FROM sets WHERE exerciseUuid=:uuid")
    public abstract void removeSetsByExerciseUuid(UUID uuid);

    @Transaction
    public List<List<SetEntity>> getSets(List<ExerciseEntity> exerciseEntities, int count, long skip, SetsLocalStorage.Vector vector) {
        List<List<SetEntity>> result = new ArrayList<>();

        for (ExerciseEntity entity : exerciseEntities) {
            List<SetEntity> list;
            if (vector == SetsLocalStorage.Vector.START)
                list = getSetsSync(entity.getUuid(), count, skip);
            else list = getLastSetsSync(entity.getUuid(), count, skip);
            List<SetEntity> setEntities = new ArrayList<>(list);
            result.add(setEntities);
        }
        return result;
    }

    @Query("SELECT * FROM sets WHERE exerciseUuid=:exerciseUuid AND deleted=0 ORDER BY createdAt DESC LIMIT :count OFFSET :skip")
    public abstract Flowable<List<SetEntity>> getLastSets(UUID exerciseUuid, int count, long skip);

    @Query("SELECT COUNT(*) FROM sets WHERE exerciseUuid=:uuid AND createdAt>:startDay AND createdAt<:endDay")
    public abstract Flowable<Integer> getCountSetsToday(UUID uuid, DateTime startDay, DateTime endDay);
}
