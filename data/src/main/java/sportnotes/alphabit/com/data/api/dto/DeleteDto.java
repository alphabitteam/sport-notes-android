package sportnotes.alphabit.com.data.api.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Yuri Zigunov (iurii.zigunov@domru.ru)
 * @since 25.11.2017.
 */

public class DeleteDto {

    @SerializedName("count")
    @Expose
    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
